<#include "../utils/header-index-empleado.ftl">

<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Registro Empleado</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>
<form method="" action="">
	<div class="container custom-main-container justify-content-center">
	  <div class="row row-register">
		  	<div class="col-md-12 col-sm-12">	  		
		      <div class="form-group">
		        <input class="form-control input-style" type="text" placeholder="Nombre">
		      </div>
		    </div>
	    	<div class="col-md-12 col-sm-12">
		      <div class="form-group">
		        <input class="form-control input-style" type="email" placeholder="Apellido">
		      </div>
		    </div>
		  	<div class="col-md-6 col-sm-6">
		      <div class="form-group">
		        <input class="form-control input-style" type="text" placeholder="C�dula">
		      </div>
		    </div>
	    	<div class="col-md-6 col-sm-6">
		      <div class="form-group">
		        <input class="form-control input-style" type="email" placeholder="Telefono">
		      </div>
		    </div>
		    <div class="col-md-12 col-sm-12">
		    	<button type="submit" class="btn btn-common btn-rm register-option-button">Guardar</button>
		    	<a class="btn btn-common btn-rm register-btn register-option-button">Volver</a>  
		    </div>
	  </div>
	</div>
</form>
<#include "../utils/footer.ftl">