<#include "../../utils/header.ftl">
<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Informaci�n de la Oferta de Empleo</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>

<div class="container-breadcrumb">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb breadcrumb-style">
	    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
	    <li class="breadcrumb-item"><a href="/ProyectoLaboratorio/ControladorIndex?rol=empleado&operacion=postulaciones">Lista de Ofertas de Empleo</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Oferta de Empleo: Soporte t�cnico</li>
	  </ol> 
	</nav>
</div>

<form method="" actison="">
	<div class="container custom-main-container justify-content-center">
	
	  <div class="row row-register">
	  
		  	<div class="col-md-6 col-sm-6">	  		
		      <div class="form-group">
		      	<label class="field-label">Nombre</label>
		        <input class="form-control input-style read-only-field" type="text" value="Soporte t�nico" readonly>
		      </div>
		    </div>    
		    
		    <div class="col-md-6 col-sm-6">	  		
		      <div class="form-group">
		      	<label class="field-label">Salario</label>
		        <input class="form-control input-style read-only-field" type="text" value="50000" readonly>
		      </div>
		    </div>
		    
		   	<div class="col-md-12 col-sm-12">	  		
		      <div class="form-group">
		      	<label class="field-label">Descripci�n</label>
		        <textarea class="form-control input-style read-only-field" rows="4" cols="30" readonly>Se solicita un empleado con experienca en call centers de soporte t�cnico, dispuesto a trabajar tiempo completo de lunes a viernes</textarea>
				</textarea>
		      </div>
		    </div>
			    
	        <div class="col-md-12 col-sm-12">
	            <div class="form-group">
	                <div class="controls">
	                    <label class="field-label">Fecha de culminacion</label>
	                    <input class="form-control input-style read-only-field" type="date" name="date_end" value="2018-12-08" readonly>
					</div>
	            </div>
	        </div>
	        
		    <div class="col-md-6 col-sm-6">
				<div class="search-category-container form-group">
					    <label class="field-label">Lista de empresas</label>
						<input class="form-control input-style read-only-field" type="text" value="Empresas polar" readonly>
				</div>
	        </div>
	        
	        <div class="col-md-6 col-sm-6">
				<div class="search-category-container form-group">
					    <label class="field-label">Pa�s</label>
						<input class="form-control input-style read-only-field" type="text" value="Venezuela" readonly>
				</div>
	        </div>
	        
	        <div class="col-md-6 col-sm-6">
				<div class="search-category-container form-group">
					    <label class="field-label">Dedicaci�n</label>
	                    <input class="form-control input-style read-only-field" type="text" value="Tiempo completo" readonly>
				</div>
	        </div>
	        
	        <div class="col-md-6 col-sm-6">
				<div class="search-category-container form-group">
					<label class="field-label">�rea de desempe�o</label>
					<input class="form-control input-style read-only-field" type="text" value="Tecnolog�a" readonly>
				</div>
	        </div>
	        
	        <div class="col-md-12 col-sm-12">
				<div class="search-category-container form-group">
				<label class="field-label">Estado de registro</label>
	                    <input class="form-control input-style read-only-field" type="text" value="Activo" readonly>
				</div>
	        </div>
	        
	        <div class="col-md-12 col-sm-12">
	        </div>
	        
	        <div class="col-md-12 col-sm-12">
		    	<a class="btn btn-common btn-rm boton-postulacion" href="/ProyectoLaboratorio/ControladorIndex?rol=empleado&operacion=estado_postulacion">Postularse</a>
		      	<a class="btn btn-common btn-rm delete-button" href="/ProyectoLaboratorio/ControladorIndex?rol=empleado&operacion=postulaciones">Volver</a>
		    </div>
		    
	  </div>
	</div>
</form>



<#include "../../utils/footer.ftl">