<#include "../../utils/header-empleado.ftl">

<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Postulaciones</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>

<form method="" action="">
	<div class="container-breadcrumb">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb breadcrumb-style">
		    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Postulaciones</li>
		  </ol>
		</nav>
	</div>
	
	<div class="container custom-main-container justify-content-center">
		
	  <table id="table-index-crud" class="table">
		  <thead class="thead-orange">
		    <tr>
		      <th scope="col">No.</th>
		      <th scope="col">Nombre de la empresa</th>
		      <th scope="col">Fecha de postulación</th>
		      <th scope="col">Estatus</th>
		      <th scope="col" class="justify-content-center">Opciones</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <th scope="row" class="p-top-15">1</th>
		      <td class="p-top-15">Empresas polar</td>
		      <td class="p-top-15">2018-10-02</td>
		      <td class="p-top-15">En espera</td>
		      <td class="td-options-buttons" style="width: 20%;">
		      	<a class="btn btn-common btn-rm index-option-button view-button" href="/ProyectoLaboratorio/ControladorIndex?rol=empleado&operacion=postulacion">Ver</a>
		      	<a class="btn btn-common btn-rm index-option-button delete-button" href="/ProyectoLaboratorio/ControladorIndex?rol=empleado&operacion=postulaciones">Cancelar</a>
		      </td>
		    </tr>
		    <tr>
		      <th scope="row" >2</th>
		      <td class="p-top-15">Intercable</td>
		      <td class="p-top-15">2019-01-09</td>
		      <td class="p-top-15">Rechazada</td>
		      <td class="td-options-buttons">
		      	<a class="btn btn-common btn-rm index-option-button view-button" href="/ProyectoLaboratorio/ControladorIndex?rol=empleado&operacion=postulacion">Ver</a>
		      	<a class="btn btn-common btn-rm index-option-button delete-button" href="/ProyectoLaboratorio/ControladorIndex?rol=empleado&operacion=postulaciones">Cancelar</a>
		      </td>
		    </tr>
		    <tr>
		      <th scope="row" >3</th>
		      <td class="p-top-15">CANTV</td>
		      <td class="p-top-15">2018-12-11</td>
		      <td class="p-top-15">Aprobada</td>
		      <td class="td-options-buttons">
		      	<a class="btn btn-common btn-rm index-option-button view-button" href="/ProyectoLaboratorio/ControladorIndex?rol=empleado&operacion=postulacion">Ver</a>
		      	<a class="btn btn-common btn-rm index-option-button delete-button" href="/ProyectoLaboratorio/ControladorIndex?rol=empleado&operacion=postulaciones">Cancelar</a>
		      </td>
		    </tr>
		  </tbody>
		</table>
	</div>
</form>
<#include "../../utils/footer.ftl">