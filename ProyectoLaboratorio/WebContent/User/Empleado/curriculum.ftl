<#include "../../utils/header-empleado.ftl">

<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Registro de curr�culo Vitae</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>
<form method="" action="">
	<div class="container custom-main-container justify-content-center">
	  <div class="row row-register" style="width: 80%;">
	  
	  	  <div class="col-md-12 col-sm-12">
		  	  <h4 class="h4-subtitle">Datos personales:</h4>
		  	<br/>
			   <div class="col-md-6 col-sm-6" style="margin-left: -13px">	  		
				      <div class="form-group">
				      	<label class="field-label">Nombre:</label>
				        <input class="form-control input-style read-only-field" type="text" readonly value="Jos� Perez">
				      </div>
				</div>
			    
			   <div class="col-md-6 col-sm-6" style="width: 414px;">	  		
			      <div class="form-group">
			      	<label class="field-label">C�dula:</label>
			        <input class="form-control input-style read-only-field" type="text" readonly value="8541215">
			      </div>
			    </div>
			    
		    	<div class="col-md-6 col-sm-6 file-custom" style="margin-top: 23px;">	
				    <label class="field-label" >Subir archivo:</label>
					<input type="file" name="file" class="margin" />
				</div>
			</div>  	  
	    
		<div class="col-md-12 col-sm-12" style="margin-top: 39px;">
			<h4 class="h4-subtitle">Estudios realizados:</h4>
			
			    
			<!--BOTON INCLUIR CARRERA-->
			
				<div class="col-md-6 col-sm-6" style="margin-bottom: 22px;">
					<a class="btn btn-common btn-rm default-white-button" data-toggle="modal" data-target="#modalEstudio" onclick="onIncluir();">Incluir</a>
				</div>
			
			<!-- BOTON -->
			
			<!--TABLA CARRERA-->
			
			<table id="table-index-crud" class="table white-background" style="margin-bottom: 40px;">
			
			  <thead class="thead-orange">
			    <tr>
			      <th scope="col">Nivel de estudio</th>
			      <th scope="col">Carrera</th>
			      <th scope="col">Instituci�n</th>
			      <th scope="col" class="justify-content-center">Opciones</th>
			    </tr>
			  </thead>
			  
			  <tbody>
			    <tr>
			      <td>Pregrado</td>
			      <td>Dise�ador gr�fico</td>
			      <td>Universidad Cat�lica Andr�s Bello</td>
			      <td class="td-options-buttons" style="width: 180px">
			      	<a class="btn btn-common btn-rm index-option-button view-button" href="#">Ver</a>
			      	<a class="btn btn-common btn-rm index-option-button delete-button" href="#">Eliminar</a>
			      </td>
			    </tr>
			    
			    <tr>
			      <td>Postgrado</td>
			      <td>Leyes internacionales</td>
			      <td>Universidad yacamb�</td>
			      <td class="td-options-buttons" style="width: 180px">
			      	<a class="btn btn-common btn-rm index-option-button view-button" href="#">Ver</a>
			      	<a class="btn btn-common btn-rm index-option-button delete-button" href="#">Eliminar</a>
			      </td>
			    </tr>
			  </tbody>
			</table>
			
			<!--TABLA FIN-->
		</div>
		
		<!--MODAL CARRERA-->
		
		<div class="modal" tabindex="-1" role="dialog" id="modalEstudio">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header"> 
		        <h3 class="modal-title h3-orange">Incluir carrera</h3>		    
		        
		      </div>
		      <div class="modal-body">
		      
					<div class="search-category-container form-group">
						<label class="contact-p1" id="id_nivel" name="id_nivel">Nivel de estudio:</label>
		                    <select class="dropdown-product selectpicker" style="background-color: #f2000033; border-radius: 10px;">
		                    		<option value="-1">Seleccione</option>
		                            <option>Primaria</option>
		                            <option>B�sico</option>
		                            <option>Universitario</option>                            
		                     </select>
					</div>
		        
					<div class="search-category-container form-group">
					<label class="contact-p1">Carrera:</label>
		                    <select id="id_profesion" name="id_profesion" class="dropdown-product selectpicker">
		                    		<option value="-1">Seleccionar</option>
		                            <option>Contador</option>
		                            <option>Dise�ador gr�fico</option>
		                            <option>Abogado</option>                            
		                     </select>
					</div>
		        		
			      <div class="form-group">
			      	<label class="contact-p1">Nombre de la instituci�n:</label>
			        <input class="form-control input-style" type="text" placeholder="Universidad Central de venezuela" id="institucion" name="institucion">
			      </div>  
		        
		      </div>
		      <div class="modal-footer">
		      	<td class="td-options-buttons" style="width: 180px">
			      	<a class="btn btn-common index-option-button view-button" href="#">Guardar</a>
			      	<a class="btn btn-common index-option-button delete-button" data-dismiss="modal">Cerrar</a>
		     	 </td>
		      </div>
		    </div>
		  </div>
		</div>
		
		<!--MODAL CARRERA FIN-->
		
		<div class="col-md-12 col-sm-12">
		  	<h4 class="h4-subtitle">Cursos realizados:</h4>
			
			<!--BOTON INCLUIR CURSO-->
			
				<div class="col-md-12 col-sm-12" style="margin-bottom: 20px;">
					<a class="btn btn-common btn-rm default-white-button" data-toggle="modal" data-target="#modalCurso">Incluir</a>
				</div>
			
			<!-- FIN BOTON CURSO -->
			
			<!--TABLA CURSO-->
			
				<table id="table-index-crud" class="table white-background" style="margin-bottom: 30px;">
				
				  <thead class="thead-orange">
				    <tr>
				      <th scope="col">Nombre</th>
				      <th scope="col">Instituci�n</th>
				      <th scope="col">Fecha inicio</th>
				      <th scope="col">Fecha culminaci�n</th>
				      <th scope="col" class="justify-content-center">Opciones</th>
				    </tr>
				  </thead>
				  
				  <tbody>
				  
				    <tr>
				      <td>Curso de Javascript</td>
				      <td>CADIF1</td>
				      <td>2017-08-12</td>
				      <td>2018-03-04</td>
				      <td class="td-options-buttons" style="width: 180px">
				      	<a class="btn btn-common btn-rm index-option-button view-button" href="#">Ver</a>
				      	<a class="btn btn-common btn-rm index-option-button delete-button" href="#">Eliminar</a>
				      </td>
				    </tr>
				    
				    <tr>
				      <td>Curso de html</td>
				      <td>CADIF1</td>
				      <td>2016-18-05</td>
				      <td>2016-03-11</td>
				      <td class="td-options-buttons" style="width: 180px">
				      	<a class="btn btn-common btn-rm index-option-button view-button" href="#">Ver</a>
				      	<a class="btn btn-common btn-rm index-option-button delete-button" href="#">Eliminar</a>
				      </td>
				    </tr>
				    
				  </tbody>
				</table>
			
			<!--TABLA CURSO FIN-->
		</div>
		
		<!--MODAL CURSO-->
		
			<div class="modal" tabindex="-1" role="dialog" id="modalCurso">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			    
			      <div class="modal-header"> 
			        <h3 class="modal-title h3-orange">Agregar curso</h3>		    	        
			      </div>
			      
			      <div class="modal-body">
			  	
				  	<div class="col-md-6 col-sm-6">	  		
				      <div class="form-group">
				      	<label class="contact-p1">Nombre:</label>
				        <input class="form-control input-style" type="text" placeholder="Reposter�a">
				      </div>
				    </div>    
				    
				    <div class="col-md-6 col-sm-6">	  		
				      <div class="form-group">
				      	<label class="contact-p1">Instituci�n:</label>
				        <input class="form-control input-style" type="text" placeholder="Universidad Central de Venezuela">
				      </div>
				    </div>	
				    
				    <div class="col-md-6 col-sm-6">
			            <div class="form-group">
			                <div class="controls">
			                    <label class="contact-p1">Fecha de inicio:</label>
			                    <input class="form-control form-field" type="date" name="date_end" >
							</div>
			            </div>
			        </div>	   
					    
			        <div class="col-md-6 col-sm-6">
			            <div class="form-group">
			                <div class="controls">
			                    <label class="contact-p1">Fecha de culminacion:</label>
			                    <input class="form-control form-field" type="date" name="date_end" >
							</div>
			            </div>
			        </div>
			        
				   	<div class="col-md-12 col-sm-12">	  		
				      <div class="form-group">
				      	<label class="contact-p1">Descripci�n:</label>
				        <textarea class="form-control input-style" rows="4" cols="30">
						</textarea>
				      </div>
				    </div>
				        
				      </div>
				      <div class="modal-footer">
				      	<td class="td-options-buttons" style="width: 180px">
					      	<a class="btn btn-common index-option-button view-button" href="#">Guardar</a>
					      	<a class="btn btn-common index-option-button delete-button" data-dismiss="modal">Cerrar</a>
				     	 </td>
				      </div>
				    </div>
			  </div>
			</div>
			
		<!--MODAL CURSO FIN-->
		    
		    <div class="col-md-12 col-sm-12">
		  	  <h4 class="h4-subtitle">Datos extra:</h4>
		  	</div>
		    
		    <div class="col-md-12 col-sm-12">	  		
		      <div class="form-group">
		      	<label class="field-label">Conocimientos adicionales:</label>
		        <textarea class="form-control input-style" rows="4" cols="30">
				</textarea>
		      </div>
		    </div>
		    
		   <div class="col-md-12 col-sm-12">	  		
		      <div class="form-group">
		      	<label class="field-label">Observaciones:</label>
		        <textarea class="form-control input-style" rows="4" cols="30">
				</textarea>
		      </div>
		    </div>
		    
	        <div class="col-md-6 col-sm-6">
		    	<a class="btn btn-common btn-rm default-white-button" href="/ProyectoLaboratorio/ControladorIndex?rol=empleado">Guardar</button>
		    	<a class="btn btn-common btn-rm default-white-button" href="/ProyectoLaboratorio/ControladorIndex?rol=empleado">Volver</a>    
		    </div>
		    
	  </div>
	</div>
</form>
<#include "../../utils/footer.ftl">