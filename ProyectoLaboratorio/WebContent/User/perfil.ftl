<#include "../utils/header.ftl">

<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Perfil de usuario</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>

<div class="container-breadcrumb">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb breadcrumb-style">
	    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Perfil de usuario</li>
	  </ol>
	</nav>
</div>
<div class="container custom-main-container justify-content-center">
	
  <div class="row row-register profile-container">
  	<div class="profile-picture">
  	<img src="img/ui-sherman.jpg" class="img-circle" width="150">
  	</div>
	  	<div class="col-md-12 col-sm-12" style="margin-top: 52px;">	  		
	      <div class="form-group">
	      	<label class="field-label">Nombre</label>
	        <input class="form-control input-style read-only-field" type="text" value="Carlos" readonly>
	      </div>
	    </div>
    	<div class="col-md-12 col-sm-12">
	      <div class="form-group">
	      	<label class="field-label">Apellido</label>
	        <input class="form-control input-style read-only-field" type="email" value="Colina" readonly>
	      </div>
	    </div>
	  	<div class="col-md-6 col-sm-6">
	      <div class="form-group">
	      	<label class="field-label">C�dula</label>
	        <input class="form-control input-style read-only-field" type="text" value="15874659" readonly>
	      </div>
	    </div>
    	<div class="col-md-6 col-sm-6">
	      <div class="form-group">
	      	<label class="field-label">Tel�fono</label>
	        <input class="form-control input-style read-only-field" type="number" value="04245304982" readonly>
	      </div>
	    </div>
	    <div class="col-md-12 col-sm-12">
	      <div class="form-group">
	      	<label class="field-label">Pa�s</label>
	        <input class="form-control input-style read-only-field" type="email" value="Venezuela" readonly>
	      </div>
	    </div>
	    <div class="col-md-12 col-sm-12">
	      <div class="form-group">
	      	<label class="field-label">Estado</label>
	        <input class="form-control input-style read-only-field" type="email" value="Lara" readonly>
	      </div>
	    </div>
	    <div class="col-md-12 col-sm-12">
	      <div class="form-group">
	      	<label class="field-label">Municipio</label>
	        <input class="form-control input-style read-only-field" type="email" value="Iribarren" readonly>
	      </div>
	    </div>    
	    
	    <div class="col-md-12 col-sm-12">
	    	<a class="btn btn-common btn-rm update-button" href="#">Modificar</a>
	    	<a class="btn btn-common btn-rm default-white-button">Ver curriculo</a>
	    	<a class="btn btn-common btn-rm default-white-button">Volver</a>  
	    </div>
  </div>
</div>

<#include "../utils/footer.ftl">