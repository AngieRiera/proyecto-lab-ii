<#include "../../../utils/header-empresa.ftl">

<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Curr&iacuteculo de Carlos Rodriguez</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>
<form method="" action="">
	<div class="container custom-main-container justify-content-center">
	  <div class="row row-register" style="width: 80%;">
	  
	  	  <div class="col-md-12 col-sm-12">
		  	  <h4 class="h4-subtitle">Datos personales:</h4>
		  	<br/>
			   <div class="col-md-6 col-sm-6" style="margin-left: -13px">	  		
				      <div class="form-group">
				      	<label class="field-label">Nombre:</label>
				        <input class="form-control input-style read-only-field" type="text" readonly value="Carlos Rodriguez">
				      </div>
				</div>
			    
			   <div class="col-md-6 col-sm-6" style="width: 414px;">	  		
			      <div class="form-group">
			      	<label class="field-label">C&eacutedula:</label>
			        <input class="form-control input-style read-only-field" type="text" readonly value="8542963">
			      </div>
			    </div>				
			</div>  	  
		    
		    
		<div class="col-md-12 col-sm-12" style="margin-top: 39px;" style="margin-bottom: 22px;">
			<h4 class="h4-subtitle">Estudios realizados:</h4>
			
		<div class="col-md-12 col-sm-12">
			<!--TABLA CARRERA-->
			
			<table id="table-index-crud" class="table white-background" style="margin-bottom: 40px;">
			
			  <thead class="thead-orange">
			    <tr>
			      <th scope="col">Nivel de estudio</th>
			      <th scope="col">Carrera</th>
			      <th scope="col">Instituci&oacuten</th>
			      <th scope="col" class="justify-content-center">Ver</th>
			    </tr>
			  </thead>
			  
			  <tbody>
			    <tr>
			      <td style="width: 25%;">Pregrado</td>
			      <td>Dise&ntildeador gr&aacutefico</td>
			      <td>Universidad Cat&aacutelica Andr&eacutes Bello</td>
			      <td class="td-options-buttons" style="width: 20%;">
			      	<a class="btn btn-common btn-rm index-option-button view-button" style="margin-left: 40px;" data-toggle="modal" data-target="#modalEstudio">Ver</a>
			      </td>
			    </tr>
			    
			    <tr>
			      <td style="width: 25%;">Postgrado</td>
			      <td>Leyes internacionales</td>
			      <td>Universidad yacamb&uacute </td>
			      <td class="td-options-buttons" style="width: 20%;">
			      	<a class="btn btn-common btn-rm index-option-button view-button" style="margin-left: 40px;" data-toggle="modal" data-target="#modalEstudio">Ver</a>
			      </td>
			    </tr>
			  </tbody>
			</table>
			
			<!--TABLA FIN-->
			</div>
		</div>
		
		<!--MODAL CARRERA-->
		
		<div class="modal" tabindex="-1" role="dialog" id="modalEstudio">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header"> 
		        <h3 class="modal-title h3-orange">Detalles de carrera</h3>		    
		        
		      </div>
		      <div class="modal-body">
		      
			      <div class="form-group">
			      	<label class="contact-p1">Nivel de estudio:</label>
			        <input class="form-control input-style read-only-field" type="text" value="Pregrado" id="institucion" name="institucion" readonly>
			      </div>  
		        
			      <div class="form-group">
			      	<label class="contact-p1">Carrera:</label>
			        <input class="form-control input-style read-only-field" type="text" value="Dise�ador gr�fico" id="institucion" name="institucion" readonly>
			      </div>  
		        		
			      <div class="form-group">
			      	<label class="contact-p1">Nombre de la institución </label>
			        <input class="form-control input-style read-only-field" type="text"  id="institucion" name="institucion" value="Universidad Cat�lica Andr�s Bello" readonly>
			      </div>  
		        
		      </div>
		      <div class="modal-footer">
		      	<td class="td-options-buttons" style="width: 180px">
			      	<a class="btn btn-common index-option-button delete-button" data-dismiss="modal">Cerrar</a>
		     	 </td>
		      </div>
		    </div>
		  </div>
		</div>
		
		<!--MODAL CARRERA FIN-->		
		
		
		<div class="col-md-12 col-sm-12">
		  	<h4 class="h4-subtitle">Cursos realizados:</h4>
		
		<div class="col-md-12 col-sm-12">
			
			<!--TABLA CURSO-->
			
				<table id="table-index-crud" class="table white-background" style="margin-bottom: 30px;">
				
				  <thead class="thead-orange">
				    <tr>
				      <th scope="col">Nombre</th>
				      <th scope="col">Instituci&oacuten</label>
				      <th scope="col">Fecha inicio</th>
				      <th scope="col">Fecha culminaci&oacuten</th>
				      <th scope="col" class="justify-content-center">Ver</th>
				    </tr>
				  </thead>
				  
				  <tbody>
				  
				    <tr>
				      <td>Curso de Javascript</td>
				      <td>CADIF1</td>
				      <td>2017-08-12</td>
				      <td style="width:25%;">2018-03-04</td>
				      <td class="td-options-buttons" style="width: 20%;">
				      	<a class="btn btn-common btn-rm index-option-button view-button" style="margin-left: 40px;" data-toggle="modal" data-target="#modalCurso">Ver</a>
				      </td>
				    </tr>
				    
				    <tr>
				      <td>Curso de html</td>
				      <td>CADIF1</td>
				      <td>2016-18-05</td>
				      <td style="width:25%;">2016-03-11</td>
				      <td class="td-options-buttons" style="width: 20%;">
				      	<a class="btn btn-common btn-rm index-option-button view-button" style="margin-left: 40px;" data-toggle="modal" data-target="#modalCurso">Ver</a>
				      </td>
				    </tr>
				    
				  </tbody>
				</table>
			
			<!--TABLA CURSO FIN-->
			</div>
		</div>
		
		<!--MODAL CURSO-->
		
			<div class="modal" tabindex="-1" role="dialog" id="modalCurso">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			    
			      <div class="modal-header"> 
			        <h3 class="modal-title h3-orange">Detalles del curso</h3>		    	        
			      </div>
			      
			      <div class="modal-body">
			  	
				  	<div class="col-md-6 col-sm-6">	  		
				      <div class="form-group">
				      	<label class="contact-p1">Nombre:</label>
				        <input class="form-control input-style read-only-field" type="text" value="Curso de Javascript" readonly>
				      </div>
				    </div>    
				    
				    <div class="col-md-6 col-sm-6">	  		
				      <div class="form-group">
				      	<label class="contact-p1">Institución</label>
				        <input class="form-control input-style read-only-field" type="text" value="CADIF1" readonly>
				      </div>
				    </div>
				    
				    <div class="col-md-6 col-sm-6">	  		
				      <div class="form-group">
				      	<label class="contact-p1">Fecha de inicio:</label>
				        <input class="form-control input-style read-only-field" type="text" value="2017-08-12" readonly>
				      </div>
				    </div>
				    				  
				    <div class="col-md-6 col-sm-6">	  		
				      <div class="form-group">
				      	<label class="contact-p1">Fecha de culminación </label>
				        <input class="form-control input-style read-only-field" type="text" value="2016-18-05" readonly>
				      </div>
				    </div>							   
			        
				   	<div class="col-md-12 col-sm-12">	  		
				      <div class="form-group">
				      	<label class="contact-p1">Descripción </label>
				        <textarea class="form-control input-style read-only-field" rows="4" cols="30">En este curso se dictaron los conocimientos b�sicos e intermedios sobre javascript</textarea>
				      </div>
				    </div>
				        
				      </div>
				      <div class="modal-footer">
				      	<td class="td-options-buttons">
					      	<a class="btn btn-common index-option-button delete-button" data-dismiss="modal">Cerrar</a>
				     	 </td>
				      </div>
				    </div>
			  </div>
			</div>
			
		<!--MODAL CURSO FIN-->
		
		
		    
		    <div class="col-md-12 col-sm-12">
		  	  <h4 class="h4-subtitle">Datos extra:</h4>
		    
			    <div class="col-md-12 col-sm-12">	  		
			      <div class="form-group">
			      	<label class="field-label">Conocimientos adicionales:</label>
			        <textarea class="form-control input-style" rows="4" cols="30">Conocimientos en php y Ruby adquiridos por cuenta propia</textarea>
			      </div>
			    </div>
			    
			    
			   <div class="col-md-12 col-sm-12">	  		
			      <div class="form-group">
			      	<label class="field-label">Observaciones:</label>
			        <textarea class="form-control input-style" rows="4" cols="30">Ninguna</textarea>
			      </div>
			    </div>
		    </div>
		    
	        <div class="col-md-12 col-sm-12 ">
		    	<a class="btn btn-common btn-rm default-white-button" href="/ProyectoLaboratorio/ControladorIndex?rol=empresa&gestion=postulacion&operacion=listado" >Aprobar</a>
		    	<a class="btn btn-common btn-rm default-white-button" href="/ProyectoLaboratorio/ControladorIndex?rol=empresa&gestion=postulacion&operacion=listado" >Rechazar</a>
		    	<a class="btn btn-common btn-rm default-white-button" href="/ProyectoLaboratorio/ControladorIndex?rol=empresa&gestion=postulacion&operacion=listado" >Volver</a>    
		    </div>
		    
	  </div>
	</div>
</form>
<#include "../../../utils/footer.ftl">