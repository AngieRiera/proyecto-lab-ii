<#include "../../../utils/header-empresa.ftl">

<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Postulaciones Realizadas</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>

<form method="" action="">
	<div class="container-breadcrumb">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb breadcrumb-style">
		    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Lista de postulados para el empleo</li>
		  </ol>
		</nav>
	</div>
	
	<div class="container custom-main-container justify-content-center">
		
	  <table id="table-index-crud" class="table">
		  <thead class="thead-orange">
		    <tr>
		      <th scope="col">Nombre</th>
		      <th scope="col">Cedula</th>
		      <th scope="col">Telefono</th>
		      <th scope="col">Pais</th>
		      <th scope="col">Fecha de aplicacion</th>
		      <th scope="col" class="justify-content-center">Opciones</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <td class="p-top-15">Carlos Rodriguez</td>
		      <td class="p-top-15">8542963</td>
		      <td class="p-top-15">0412-8459785</td>
		      <td class="p-top-15">Venezuela</td>
		      <td class="p-top-15">2018-08-12</td>
		      <td class="td-options-buttons">
		      	<a class="btn btn-common btn-rm index-option-button view-button" href="/ProyectoLaboratorio/ControladorIndex?rol=empresa&gestion=postulacion&operacion=ver">Ver</a>
		      	<a class="btn btn-common btn-rm index-option-button green-button" style="background-color: #1dbc1f;" href="#">Aprobar</a>
		      	<a class="btn btn-common btn-rm index-option-button delete-button" href="#">Rechazar</a>
		      </td>
		    </tr>
		    <tr>
		      <td class="p-top-15">Maria Gomez</td>
		      <td class="p-top-15">10547960</td>
		      <td class="p-top-15">0424-52178452</td>
		      <td class="p-top-15">Venezuela</td>
		      <td class="p-top-15">2018-08-15</td>
		      <td class="td-options-buttons">
		      	<a class="btn btn-common btn-rm index-option-button view-button" href="/ProyectoLaboratorio/ControladorIndex?rol=empresa&gestion=postulacion&operacion=ver">Ver</a>
		      	<a class="btn btn-common btn-rm index-option-button green-button" style="background-color: #1dbc1f;" href="#">Aprobar</a>
		      	<a class="btn btn-common btn-rm index-option-button delete-button" href="#">Rechazar</a>
		      </td>
		    </tr>
		  </tbody>
		</table>
	</div>
</form>
<#include "../../../utils/footer.ftl">