<#include "../utils/header.ftl">

<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Modificar perfil</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>

<div class="container-breadcrumb">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb breadcrumb-style">
	    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Perfil de usuario</li>
	  </ol>
	</nav>
</div>
<form method="" action="">
<div class="container custom-main-container justify-content-center">
	
  <div class="row row-register profile-container">
  	<div class="profile-picture">
  	<img src="img/ui-sherman.jpg" class="img-circle" width="150">
  	</div>
	  	<div class="col-md-12 col-sm-12" style="margin-top: 52px;">	  		
	      <div class="form-group">
	      	<label class="field-label">Nombre</label>
	        <input class="form-control input-style read-only-field" type="text" value="Carlos">
	      </div>
	    </div>
    	<div class="col-md-12 col-sm-12">
	      <div class="form-group">
	      	<label class="field-label">Apellido</label>
	        <input class="form-control input-style read-only-field" type="email" value="Colina">
	      </div>
	    </div>
	  	<div class="col-md-6 col-sm-6">
	      <div class="form-group">
	      	<label class="field-label">C�dula</label>
	        <input class="form-control input-style read-only-field" type="text" value="15874659">
	      </div>
	    </div>
    	<div class="col-md-6 col-sm-6">
	      <div class="form-group">
	      	<label class="field-label">Tel�fono</label>
	        <input class="form-control input-style read-only-field" type="text" value="04245304982">
	      </div>
	    </div>
	    <div class="col-md-12 col-sm-12">
	      <div class="form-group">
	      	<label class="field-label">Pa�s</label>
	        	<select class="dropdown-product selectpicker">
		            <option>Venezuela</option>
		            <option>Colombia</option>
		            <option>Brasil</option>
		            <option>Argentina</option>
		            <option>Holanda</option>
		         </select>
	      </div>
	    </div>
	    <div class="col-md-12 col-sm-12">
	      <div class="form-group">
	      	<label class="field-label">Estado</label>
	        	<select class="dropdown-product selectpicker">
		            <option>Lara</option>
		            <option>Falcon</option>
		            <option>Zulia</option>
		            <option>Tachira</option>
		         </select>
	      </div>
	    </div>
	    <div class="col-md-12 col-sm-12">
	      <div class="form-group">
	      	<label class="field-label">Municipio</label>
	      		<select class="dropdown-product selectpicker">
		            <option>Iribarren</option>
		            <option>Palavecino</option>
		            <option>Urdaneta</option>
		            <option>Torres</option>
		         </select>
	      </div>
	    </div>    
	    
	    <div class="col-md-12 col-sm-12">
	    	<button type="submit" class="btn btn-common btn-rm register-option-button">Guardar</button>
	    	<a class="btn btn-common btn-rm default-white-button">Cancelar</a>  
	    </div>
  </div>
</div>
</form>
<#include "../utils/footer.ftl">