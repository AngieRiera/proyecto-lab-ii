<#include "../../utils/header.ftl">
<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Lista de �reas de Desempe�o</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>
<form method="" action="">
		<div class="container-breadcrumb">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb breadcrumb-style">
			    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
			    <li class="breadcrumb-item"><a href="#">Panel de administrador</a></li>
			    <li class="breadcrumb-item active" aria-current="page">Lista de �reas de Desempe�o</li>
			  </ol>
			</nav>
		</div>
	</div>
	<div> 
		<a class="btn btn-common btn-rm update-button" href="/ProyectoLaboratorio/ControladorArea?operacion=registrar">Registrar</a>
	</div>
		<div class="container custom-main-container justify-content-center">
			
		  <table id="table-index-crud" class="table">
			  <thead class="thead-orange">
			    <tr>
			      <th scope="col">id</th>
			      <th scope="col">Nombre</th>
			      <th scope="col">Descripci�n</th>
			      <th scope="col">Estado de registro</th>
			      <th scope="col" class="justify-content-center">Opciones</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <th scope="row">1</th>
			      <td>Tecnolog�a</td>
			      <td>Ciencia y tecnolog�a</td>
			      <td>Activo</td>
			      <td class="td-options-buttons">
			      	<a class="btn btn-common btn-rm index-option-button view-button" href="/ProyectoLaboratorio/ControladorArea?operacion=ver">Ver</a>
			      	<a class="btn btn-common btn-rm index-option-button update-button" href="/ProyectoLaboratorio/ControladorArea?operacion=modificar">Modificar</a>
			      	<a class="btn btn-common btn-rm index-option-button delete-button" href="/ProyectoLaboratorio/ControladorArea?operacion=listado">Eliminar</a>
			      </td>
			    </tr>
			    <tr>
			      <th scope="row" >2</th>
			      <td>Medicina</td>
			      <td>Salud</td>
			      <td>Activo</td>
			      <td class="td-options-buttons">
			      	<a class="btn btn-common btn-rm index-option-button view-button" href="/ProyectoLaboratorio/ControladorArea?operacion=ver">Ver</a>
			      	<a class="btn btn-common btn-rm index-option-button update-button" href="/ProyectoLaboratorio/ControladorArea?operacion=modificar">Modificar</a>
			      	<a class="btn btn-common btn-rm index-option-button delete-button" href="/ProyectoLaboratorio/ControladorArea?operacion=listado">Eliminar</a>
			      </td>
			    </tr>
			    <tr>
			      <th scope="row">3</th>
			      <td>Matemática</td>
			      <td>Ciencia</td>
			      <td>Inactivo</td>
			      <td class="td-options-buttons">
			      	<a class="btn btn-common btn-rm index-option-button view-button" href="/ProyectoLaboratorio/ControladorArea?operacion=ver">Ver</a>
			      	<a class="btn btn-common btn-rm index-option-button update-button" href="/ProyectoLaboratorio/ControladorArea?operacion=modificar">Modificar</a>
			      	<a class="btn btn-common btn-rm index-option-button delete-button" href="/ProyectoLaboratorio/ControladorArea?operacion=listado">Eliminar</a>
			      </td>
			    </tr>
			  </tbody>
			</table>
		</div>
	</form>
<#include "../../utils/footer.ftl">