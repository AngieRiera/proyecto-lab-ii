<#include "../../utils/header.ftl">
<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Información de la Jornada</h1>
        </div>
        <div class="col-md-12 justify-content-center">
          <h2 class="h2-orange"> <strong></strong> </h2>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>

<div class="container-breadcrumb">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb breadcrumb-style">
	    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
	    <li class="breadcrumb-item"><a href="#">Panel de administrador</a></li>
	    <li class="breadcrumb-item"><a href="/ProyectoLaboratorio/ControladorJornada?operacion=listado">Listado de Jornadas</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Jornada: Tiempo Completo</li>
	  </ol>
	</nav>
</div>
	
<form method="" action="">
	<div class="container custom-main-container justify-content-center">
	  <div class="row row-register">
		  	<div class="col-md-12 col-sm-12">	  		
		      <div class="form-group">
		      	<label class="field-label">Nombre</label>
		        <input class="form-control input-style read-only-field" type="text" value="Tiempo Completo" readonly>
		      </div>
		    </div>
	    	<div class="col-md-12 col-sm-12">
		      <div class="form-group">
		      	<label class="field-label">Descripción</label>
		        <input class="form-control input-style read-only-field" type="text" value="Trabaja 14 horas diarias" readonly>
		      </div>
		    </div>
		    <div class="col-md-6 col-sm-6">
		      <div class="form-group">
		      	<label class="field-label">Estado de registro</label>
		      	<input class="form-control input-style read-only-field" type="text" value="Activo" readonly>
		      </div>
		    </div>
		    <div class="col-md-12 col-sm-12">
		    	<a class="btn btn-common btn-rm update-button" href="/ProyectoLaboratorio/ControladorJornada?operacion=modificar">Modificar</a>
		      	<a class="btn btn-common btn-rm delete-button" href="/ProyectoLaboratorio/ControladorJornada?operacion=listado">Eliminar</a>
		    	<a class="btn btn-common btn-rm default-white-button" href="/ProyectoLaboratorio/ControladorJornada?operacion=listado">Cancelar</a>  
		    </div>
	  </div>
	</div>
</form>
<#include "../../utils/footer.ftl">