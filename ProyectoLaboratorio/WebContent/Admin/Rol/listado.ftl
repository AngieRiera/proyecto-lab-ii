<#include "../../utils/header.ftl">
<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Listado de Roles</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>
<form method="" action="">
	<div class="container-breadcrumb">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb breadcrumb-style">
		    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
		    <li class="breadcrumb-item"><a href="#">Panel de administrador</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Listado de Roles</li>
		  </ol>
		</nav>
	</div>
	<div> 
		<a class="btn btn-common btn-rm update-button" href="/ProyectoLaboratorio/ControladorRol?operacion=registrar">Registrar</a>
	</div>
	<div class="container custom-main-container justify-content-center">
		
	  <table id="table-index-crud" class="table">
		  <thead class="thead-orange">
		    <tr>
		      <th scope="col">id</th>
		      <th scope="col">Nombre</th>
		      <th scope="col">Descripci�n</th>
		      <th scope="col">Estado de registro</th>
		      <th scope="col" class="justify-content-center">Opciones</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <th scope="row">1</th>
		      <td>Administrador</td>
		      <td class="td-descipcion-rol-listado">Se encarga de llevar el control de la p�gina web</td>
		      <td>Activo</td>
		      <td class="td-options-buttons">
		      	<a class="btn btn-common btn-rm index-option-button view-button" href="/ProyectoLaboratorio/ControladorRol?operacion=ver">Ver</a>
		      	<a class="btn btn-common btn-rm index-option-button update-button" href="/ProyectoLaboratorio/ControladorRol?operacion=modificar">Modificar</a>
		      	<a class="btn btn-common btn-rm index-option-button delete-button" href="/ProyectoLaboratorio/ControladorRol?operacion=listado">Eliminar</a>
		      </td>
		    </tr>
		    <tr>
		      <th scope="row">2</th>
		      <td>Usuario</td>
		      <td class="td-descipcion-rol-listado">Miembro de la comunidad de la p�gina web</td>
		      <td>Activo</td>
		      <td class="td-options-buttons">
		      	<a class="btn btn-common btn-rm index-option-button view-button" href="/ProyectoLaboratorio/ControladorRol?operacion=ver">Ver</a>
		      	<a class="btn btn-common btn-rm index-option-button update-button" href="/ProyectoLaboratorio/ControladorRol?operacion=modificar">Modificar</a>
		      	<a class="btn btn-common btn-rm index-option-button delete-button" href="/ProyectoLaboratorio/ControladorRol?operacion=listado">Eliminar</a>
		      </td>
		    </tr>
		  </tbody>
		</table>
	</div>
</form>
<#include "../../utils/footer.ftl">