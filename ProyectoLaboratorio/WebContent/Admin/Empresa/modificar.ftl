<#include "../../utils/header.ftl">

<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Modificar Empresa</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>
<div class="container-breadcrumb">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb breadcrumb-style">
	    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
	    <li class="breadcrumb-item"><a href="#">Panel de administrador</a></li>
	    <li class="breadcrumb-item"><a href="/ProyectoLaboratorio/ControladorEmpresa?operacion=listado">Listado de Empresas</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Empresa: Polar</li>
	  </ol>
	</nav>
</div>
	
<form method="" action="">
	<div class="container custom-main-container justify-content-center">
	  <div class="row row-register">
		  	<div class="col-md-12 col-sm-12">	  		
		      <div class="form-group">
		      	<label class="field-label">Nombre</label>
		        <input class="form-control input-style" type="text" placeholder="Nombre" value="Polar">
		      </div>
		    </div>
	    	<div class="col-md-12 col-sm-12">
		      <div class="form-group">
		      	<label class="field-label">Rif</label>
		        <input class="form-control input-style" type="text" placeholder="rif" value="156498756982">
		      </div>
		    </div>		  	
	    	<div class="col-md-12 col-sm-12">
		      <div class="form-group">
		      	<label class="field-label">Sitio web</label>
		        <input class="form-control input-style" type="text" placeholder="Sitio web" value="www.empresa.com">
		      </div>
		    </div>
		    <div class="col-md-12 col-sm-12">
		      <div class="form-group">
		      	<label class="field-label">Area de desempe�o</label>
                  <div class="search-category-container">
                      <select class="dropdown-product selectpicker">
                        <option>Tecnolog�a</option>
                        <option>Medicina</option>
                        <option>Matem�tica</option>
                      </select>
                    </label>
                  </div>
		      </div>
		    </div>
		    <div class="col-md-12 col-sm-12">
		      <div class="form-group">
		      	<label class="field-label">Tel�fono</label>
		        <input class="form-control input-style" type="text" placeholder="Telefono" value="04245304982">
		      </div>
		    </div>
		    <div class="col-md-12 col-sm-12">
		      <div class="form-group">
		      	<label class="field-label">Direcci�n</label>
		        <textarea rows="4" cols="54" class="form-control form-field input-style" placeholder="direcci�n">dwadawd</textarea>
		      </div>
		    </div>
		    <div class="col-md-12 col-sm-12">
		      <div class="form-group">
		      	<label class="field-label">Estado de registro</label>
                  <div class="search-category-container">
                      <select class="dropdown-product selectpicker">
                        <option>Activo</option>
                        <option>Inactivo</option>
                      </select>
                    </label>
                  </div>
		      </div>
		    </div>
		    <div class="col-md-12 col-sm-12">
		    	<a class="btn btn-common btn-rm default-white-button" href="/ProyectoLaboratorio/ControladorEmpresa?operacion=ver">Guardar</a>
		    	<a class="btn btn-common btn-rm default-white-button" href="/ProyectoLaboratorio/ControladorEmpresa?operacion=listado">Cancelar</a>  
		    </div>
	  </div>
	</div>
</form>
<#include "../../utils/footer.ftl">