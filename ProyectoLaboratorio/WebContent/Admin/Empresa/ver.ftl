<#include "../../utils/header.ftl">

<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Informaci�n de la Empresa</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>
<div class="container-breadcrumb">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb breadcrumb-style">
	    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
	    <li class="breadcrumb-item"><a href="#">Panel de administrador</a></li>
	    <li class="breadcrumb-item"><a href="/ProyectoLaboratorio/ControladorEmpresa?operacion=listado">Listado de Empresas</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Empresa: Polar</li>
	  </ol>
	</nav>
</div>
	
<form method="" action="">
	<div class="container custom-main-container justify-content-center">
	  <div class="row row-register">
		  	<div class="col-md-12 col-sm-12">	  		
		      <div class="form-group">
		      	<label class="field-label">Nombre</label>
		        <input class="form-control input-style read-only-field" type="text" placeholder="Nombre" value="Polar" readonly>
		      </div>
		    </div>
	    	<div class="col-md-12 col-sm-12">
		      <div class="form-group">
		      	<label class="field-label">Rif</label>
		        <input class="form-control input-style read-only-field" type="text" placeholder="rif" value="156498756982" readonly>
		      </div>
		    </div>		  	
	    	<div class="col-md-12 col-sm-12">
		      <div class="form-group">
		      	<label class="field-label">Sitio web</label>
		        <input class="form-control input-style read-only-field" type="text" placeholder="Sitio web" value="04245304982" readonly>
		      </div>
		    </div>
		    <div class="col-md-12 col-sm-12">
		      <div class="form-group">
		      	<label class="field-label">Area</label>
		        <input class="form-control input-style read-only-field" type="text" placeholder="Area" value="04245304982" readonly>
		      </div>
		    </div>
		    <div class="col-md-12 col-sm-12">
		      <div class="form-group">
		      	<label class="field-label">Tel�fono</label>
		        <input class="form-control input-style read-only-field" type="text" placeholder="Telefono" value="04245304982" readonly>
		      </div>
		    </div>
		    <div class="col-md-12 col-sm-12">
		      <div class="form-group">
		      	<label class="field-label">Direcci�n</label>
		        <textarea rows="4" cols="54" class="form-control form-field input-style read-only-field" placeholder="direcci�n" readonly>dwadawd</textarea>
		      </div>
		    </div>
		    <div class="col-md-6 col-sm-6">
		      <div class="form-group">
		      	<label class="field-label">Estado de registro</label>
		      	<input class="form-control input-style read-only-field" type="text" value="Activo" readonly>
		      </div>
		    </div>
		    <div class="col-md-12 col-sm-12">
		    	<a class="btn btn-common btn-rm update-button" href="/ProyectoLaboratorio/ControladorEmpresa?operacion=modificar">Modificar</a>
		      	<a class="btn btn-common btn-rm delete-button" href="/ProyectoLaboratorio/ControladorEmpresa?operacion=listado">Eliminar</a>
		    	<a class="btn btn-common btn-rm default-white-button" href="/ProyectoLaboratorio/ControladorEmpresa?operacion=listado">Cancelar</a>  
		    </div>
	  </div>
	</div>
</form>
<#include "../../utils/footer.ftl">