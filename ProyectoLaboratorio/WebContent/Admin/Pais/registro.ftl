<#include "../../utils/header.ftl">
<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Registro Pais</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>
<div class="container-breadcrumb">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb breadcrumb-style">
	    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
	    <li class="breadcrumb-item"><a href="#">Panel de administrador</a></li>
	    <li class="breadcrumb-item"><a href="/ProyectoLaboratorio/ControladorPais?operacion=listado">Lista de Paises</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Nuevo Pa�s</li>
	  </ol>
	</nav>
</div>
<form method="" action="">
	<div class="container custom-main-container justify-content-center">
	  <div class="row row-register">
		  	<div class="col-md-12 col-sm-12">	  		
		      <div class="form-group">
		      <label class="field-label">Nombre</label>
		        <input class="form-control input-style" type="text" placeholder="Nombre">
		      </div>
		    </div>
	    	
            <div class="col-md-12 col-sm-12">
	    		<div class="form-group">
	    		<label class="field-label">Estado de registro</label>
                  <div class="search-category-container">
                    <label class="styled-select">
                      <select class="dropdown-product selectpicker">
                        <option>Estado de registro</option>
                        <option>Activo</option>
                        <option>Inactivo</option>
                      </select>
                    </label>
                  </div>
              </div>
            </div>
            
		    <div class="col-md-12 col-sm-12">
		    	<a class="btn btn-common btn-rm default-white-button" href="/ProyectoLaboratorio/ControladorPais?operacion=ver">Guardar</a>
		    	<a class="btn btn-common btn-rm register-btn default-white-button" href="/ProyectoLaboratorio/ControladorPais?operacion=listado">Cancelar</a>  
		    </div>
	  </div>
	</div>
</form>
<#include "../../utils/footer.ftl">