<#include "../../utils/header.ftl">

<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Informaci�n del Empleado</h1>
        </div>
        <div class="col-md-12 justify-content-center">
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>

<div class="container-breadcrumb">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb breadcrumb-style">
	    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
	    <li class="breadcrumb-item"><a href="#">Panel de administrador</a></li>
	    <li class="breadcrumb-item"><a href="/ProyectoLaboratorio/ControladorEmpleado?operacion=listado">Lista de Empleados</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Empleado: Carlos Colina</li>
	  </ol>
	</nav>
</div>
	
<form method="" action="">
	<div class="container custom-main-container justify-content-center">
	  <div class="row row-register">
		  	<div class="col-md-12 col-sm-12">	  		
		      <div class="form-group">
		      	<label class="field-label">Nombre</label>
		        <input class="form-control input-style read-only-field" type="text" value="Carlos" readonly>
		      </div>
		    </div>
	    	<div class="col-md-12 col-sm-12">
		      <div class="form-group">
		      	<label class="field-label">Apellido</label>
		        <input class="form-control input-style read-only-field" type="email" value="Colina" readonly>
		      </div>
		    </div>
		  	<div class="col-md-6 col-sm-6">
		      <div class="form-group">
		      	<label class="field-label">C�dula</label>
		        <input class="form-control input-style read-only-field" type="text" value="15874659" readonly>
		      </div>
		    </div>
	    	<div class="col-md-6 col-sm-6">
		      <div class="form-group">
		      	<label class="field-label">Tel�fono</label>
		        <input class="form-control input-style read-only-field" type="number" value="04245304982" readonly>
		      </div>
		    </div>
		    <div class="col-md-12 col-sm-12">
		      <div class="form-group">
		      	<label class="field-label">Estado de registro</label>
		      	<input class="form-control input-style read-only-field" type="text" value="Activo" readonly>
		      </div>
		    </div>
		    <div class="col-md-12 col-sm-12">
		    	<a class="btn btn-common btn-rm update-button" href="/ProyectoLaboratorio/ControladorEmpleado?operacion=modificar">Modificar</a>
		      	<a class="btn btn-common btn-rm delete-button" href="/ProyectoLaboratorio/ControladorEmpleado?operacion=listado">Eliminar</a>
		    	<a class="btn btn-common btn-rm default-white-button" href="/ProyectoLaboratorio/ControladorEmpleado?operacion=listado">Cancelar</a>  
		    </div>
	  </div>
	</div>
</form>
<#include "../../utils/footer.ftl">