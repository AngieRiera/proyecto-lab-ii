<#include "../../utils/header.ftl">
<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Registro de Usuario</h1>
        </div>
        <div class="col-md-12 justify-content-center">
          <h2 class="h2-orange"><strong></strong></h2>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>
<div class="container-breadcrumb">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb breadcrumb-style">
	    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
	    <li class="breadcrumb-item"><a href="#">Panel de administrador</a></li>
	    <li class="breadcrumb-item"><a href="/ProyectoLaboratorio/ControladorUsuario?operacion=listado">Lista de Usuarios</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Nuevo usuario</li>
	  </ol>
	</nav>
</div>
<form method="" action="">
	<div class="container custom-main-container justify-content-center">
	  <div class="row row-register">
		  	<div class="col-md-12 col-sm-12">	  		
		      <div class="form-group">
		      	<label class="field-label">Usuario</label>
		        <input class="form-control input-style" type="email" placeholder="Usuario">
		      </div>
		    </div>
	    	<div class="col-md-12 col-sm-12">
		      <div class="form-group">
		      	<label class="field-label">Contraseņa</label>
		        <input class="form-control input-style" type="password" placeholder="Contraseņa" >
		      </div>
		    </div>
		    <div class="col-md-12 col-sm-12">
		      <div class="form-group">
		      	<label class="field-label">Confirmar contraseņa</label>
		        <input class="form-control input-style" type="password" placeholder="Confirmar contraseņa" >
		      </div>
		    </div>
		    <div class="col-md-12 col-sm-12">
		      <div class="form-group">
		      	<label class="field-label">Rol</label>
                  <div class="search-category-container">
                      <select class="dropdown-product selectpicker">
                      	<option>Rol</option>
                        <option>Empleado</option>
                        <option>Empresa</option>
                      </select>
                    </label>
                  </div>
		      </div>
		    </div>
		    <div class="col-md-12 col-sm-12">
		      <div class="form-group">
		      	<label class="field-label">Estado de registro</label>
                  <div class="search-category-container">
                      <select class="dropdown-product selectpicker">
                      	<option>Estado de registro</option>
                        <option>Activo</option>
                        <option>Inactivo</option>
                      </select>
                    </label>
                  </div>
		      </div>
		    </div>
		    <div class="col-md-12 col-sm-12">
		    	<a class="btn btn-common btn-rm default-white-button" href="/ProyectoLaboratorio/ControladorUsuario?operacion=ver">Guardar</a>
		    	<a class="btn btn-common btn-rm register-btn default-white-button" href="/ProyectoLaboratorio/ControladorUsuario?operacion=listado">Cancelar</a>  
		    </div>
	  </div>
	</div>
</form>
<#include "../../utils/footer.ftl">