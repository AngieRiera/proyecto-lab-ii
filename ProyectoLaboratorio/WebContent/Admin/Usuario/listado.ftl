<#include "../../utils/header.ftl">
<div class="main-banner">
	    <div class="container">
	      <div class="row">
	        <div class="col-md-12 justify-content-center">
	          <h1 class="h1-banner">Lista de Usuarios</h1>
	        </div>
	      </div>
	    </div>
	  </div>
	</section>
	<!-- end intro section -->
	</div>
	<form method="" action="">
		<div class="container-breadcrumb">
			<nav aria-label="breadcrumb">
			  <ol class="breadcrumb breadcrumb-style">
			    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
			    <li class="breadcrumb-item"><a href="#">Panel de administrador</a></li>
			    <li class="breadcrumb-item active" aria-current="page">Lista de Usuarios</li>
			  </ol>
			</nav>
		</div>
	<div> 
		<a class="btn btn-common btn-rm update-button" href="/ProyectoLaboratorio/ControladorUsuario?operacion=registrar">Registrar</a>
	</div>
		<div class="container custom-main-container justify-content-center">
			
		  <table id="table-index-crud" class="table">
			  <thead class="thead-orange">
			    <tr>
			      <th scope="col">id</th>
			      <th scope="col">Usuario</th>
			      <th scope="col">Rol</th>
			      <th scope="col">Estado de registro</th>			      
			      <th scope="col" class="justify-content-center">Opciones</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <th scope="row">1</th>
			      <td>empleado1@correo.com</td>
			      <td>Empleado</td>
			      <td>Activo</td>
			      <td class="td-options-buttons">
			      	<a class="btn btn-common btn-rm index-option-button view-button" href="/ProyectoLaboratorio/ControladorUsuario?operacion=ver">Ver</a>
			      	<a class="btn btn-common btn-rm index-option-button update-button" href="/ProyectoLaboratorio/ControladorUsuario?operacion=modificar">Modificar</a>
			      	<a class="btn btn-common btn-rm index-option-button delete-button" href="/ProyectoLaboratorio/ControladorUsuario?operacion=listado">Eliminar</a>
			      </td>
			    </tr>
			    <tr>
			      <th scope="row" >2</th>
			      <td>empleado2@correo.com</td>
			      <td>Empleado</td>
			      <td>Inactivo</td>
			      <td class="td-options-buttons">
			      	<a class="btn btn-common btn-rm index-option-button view-button" href="/ProyectoLaboratorio/ControladorUsuario?operacion=ver">Ver</a>
			      	<a class="btn btn-common btn-rm index-option-button update-button" href="/ProyectoLaboratorio/ControladorUsuario?operacion=modificar">Modificar</a>
			      	<a class="btn btn-common btn-rm index-option-button delete-button" href="/ProyectoLaboratorio/ControladorUsuario?operacion=listado">Eliminar</a>
			      </td>
			    </tr>
			    <tr>
			      <th scope="row">3</th>
			      <td>empleado3@correo.com</td>
			      <td>Empleado</td>
			      <td>Activo</td>
			      <td class="td-options-buttons">
			      	<a class="btn btn-common btn-rm index-option-button view-button" href="/ProyectoLaboratorio/ControladorUsuario?operacion=ver">Ver</a>
			      	<a class="btn btn-common btn-rm index-option-button update-button" href="/ProyectoLaboratorio/ControladorUsuario?operacion=modificar">Modificar</a>
			      	<a class="btn btn-common btn-rm index-option-button delete-button" href="/ProyectoLaboratorio/ControladorUsuario?operacion=listado">Eliminar</a>
			      </td>
			    </tr>
			  </tbody>
			</table>
		</div>
	</form>
	<#include "../../utils/footer.ftl">
