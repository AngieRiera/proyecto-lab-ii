<#include "../../utils/header.ftl">

<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Listado de Profesiones</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>
<form method="" action="">
	<div class="container-breadcrumb">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb breadcrumb-style">
		    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
		    <li class="breadcrumb-item"><a href="#">Panel de administrador</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Listado de Profesiones</li>
		  </ol>
		</nav>
	</div>
	<div> 
		<a class="btn btn-common btn-rm update-button" href="/ProyectoLaboratorio/ControladorProfesion?operacion=registrar">Registrar</a>
	</div>
	<div class="container custom-main-container justify-content-center">
		
	  <table id="table-index-crud" class="table">
		  <thead class="thead-orange">
		    <tr>
		      <th scope="col">id</th>
		      <th scope="col">Nombre</th>
		      <th scope="col">Nivel de estudio</th>
		      <th scope="col">�rea de desempe�o</th>
		      <th scope="col">Estado de registro</th>
		      <th scope="col" class="justify-content-center">Opciones</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <th scope="row" class="p-top-15">1</th>
		      <td class="p-top-15">Ing. Inform�tica</td>
		      <td class="p-top-15">Postgrado</td>
		      <td class="p-top-15">Tecnolog�a</td>
		      <td class="p-top-15">Activo</td>
		      <td class="td-options-buttons">
		      	<a class="btn btn-common btn-rm index-option-button view-button" href="/ProyectoLaboratorio/ControladorProfesion?operacion=ver">Ver</a>
		      	<a class="btn btn-common btn-rm index-option-button update-button" href="/ProyectoLaboratorio/ControladorProfesion?operacion=modificar">Modificar</a>
		      	<a class="btn btn-common btn-rm index-option-button delete-button" href="/ProyectoLaboratorio/ControladorProfesion?operacion=listado">Eliminar</a>
		      </td>
		    </tr>
		    <tr>
		      <th scope="row" >2</th>
		      <td>Dise�ador gr�fico</td>
		      <td>Postgrado</td>
		      <td>Artes gr�ficas</td>
		      <td>Activo</td>
		      <td class="td-options-buttons">
		      	<a class="btn btn-common btn-rm index-option-button view-button" href="/ProyectoLaboratorio/ControladorProfesion?operacion=ver">Ver</a>
		      	<a class="btn btn-common btn-rm index-option-button update-button" href="/ProyectoLaboratorio/ControladorProfesion?operacion=modificar">Modificar</a>
		      	<a class="btn btn-common btn-rm index-option-button delete-button" href="/ProyectoLaboratorio/ControladorProfesion?operacion=listado">Eliminar</a>
		      </td>
		    </tr>
		    <tr>
		      <th scope="row">3</th>
		      <td>Contador</td>
		      <td>Postgrado</td>
		      <td>Finanzas</td>
		      <td>Inactivo</td>
		      <td class="td-options-buttons">
		      	<a class="btn btn-common btn-rm index-option-button view-button" href="/ProyectoLaboratorio/ControladorProfesion?operacion=ver">Ver</a>
		      	<a class="btn btn-common btn-rm index-option-button update-button" href="/ProyectoLaboratorio/ControladorProfesion?operacion=modificar">Modificar</a>
		      	<a class="btn btn-common btn-rm index-option-button delete-button" href="/ProyectoLaboratorio/ControladorProfesion?operacion=listado">Eliminar</a>
		      </td>
		    </tr>
		  </tbody>
		</table>
	</div>
</form>
<#include "../../utils/footer.ftl">