<#include "../../utils/header.ftl">
<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Ver Profesi�n</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>

<div class="container-breadcrumb">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb breadcrumb-style">
	    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
	    <li class="breadcrumb-item"><a href="#">Panel de administrador</a></li>
	    <li class="breadcrumb-item"><a href="/ProyectoLaboratorio/ControladorProfesion?operacion=listado">Listado de Profesiones</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Profesi�n: Dise�ador gr�fico</li>
	  </ol>
	</nav>
</div>


<form method="" action="">
	<div class="container custom-main-container justify-content-center">
	
	  <div class="row row-register">
	  
		  	<div class="col-md-12 col-sm-12">	  		
		      <div class="form-group">
		      	<label class="field-label">Nombre de profesi�n</label>
		        <input class="form-control input-style read-only-field" type="text" value="Dise�ador gr�fico" readonly>
		      </div>
		    </div>
		    
		    
		    <div class="col-md-6 col-sm-6">
				<div class="search-category-container form-group">
					<label class="field-label">Nivel de estudio</label>
		            <input class="form-control input-style read-only-field" type="text" value="Postgrado" readonly>
				</div>
	        </div>
	        
		    <div class="col-md-6 col-sm-6">
				<div class="search-category-container form-group">
					<label class="field-label">�rea de desempe�o</label>
		            <input class="form-control input-style read-only-field" type="text" value="Dise�o gr�fico" readonly>
				</div>
	        </div>
	        
	        <div class="col-md-12 col-sm-12">
				<div class="search-category-container form-group">
				<label class="field-label">Estado de registro</label>
	               <input class="form-control input-style read-only-field" type="text" value="Activo" readonly>
				</div>
	        </div>
	        
	        <div class="col-md-12 col-sm-12">
		    	<a class="btn btn-common btn-rm update-button" href="/ProyectoLaboratorio/ControladorProfesion?operacion=modificar">Modificar</a>
		      	<a class="btn btn-common btn-rm delete-button" href="/ProyectoLaboratorio/ControladorProfesion?operacion=listado">Eliminar</a>
		    	<a class="btn btn-common btn-rm default-white-button" href="/ProyectoLaboratorio/ControladorProfesion?operacion=listado">Cancelar</a>  
		    </div>
		    
	  </div>
	</div>
</form>
<#include "../../utils/footer.ftl">