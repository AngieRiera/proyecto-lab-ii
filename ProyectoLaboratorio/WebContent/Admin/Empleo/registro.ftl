<#include "../../utils/header.ftl">

<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Registro de oferta de trabajo</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>

<div class="container-breadcrumb">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb breadcrumb-style">
	    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
	    <li class="breadcrumb-item"><a href="#">Panel de administrador</a></li>
	    <li class="breadcrumb-item"><a href="/ProyectoLaboratorio/ControladorEmpleo?operacion=listado">Listado de Ofertas de empleo</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Nueva oferta de empleo</li>
	  </ol>
	</nav>
</div>

<form method="" actison="">
	<div class="container custom-main-container justify-content-center">
	
	  <div class="row row-register">
	  
		  	<div class="col-md-6 col-sm-6">	  		
		      <div class="form-group">
		      	<label class="field-label">Nombre</label>
		        <input class="form-control input-style" type="text" placeholder="Nombre">
		      </div>
		    </div>    
		    
		    <div class="col-md-6 col-sm-6">	  		
		      <div class="form-group">
		      	<label class="field-label">Salario</label>
		        <input class="form-control input-style" type="text" placeholder="50000">
		      </div>
		    </div>
		    
		   	<div class="col-md-12 col-sm-12">	  		
		      <div class="form-group">
		      	<label class="field-label">Descripci�n</label>
		        <textarea class="form-control input-style" rows="4" cols="30"></textarea>
		      </div>
		    </div>
			    
	        <div class="col-md-12 col-sm-12">
	            <div class="form-group">
	                <div class="controls">
	                    <label class="field-label">Fecha de culminacion</label>
	                    <input class="form-control form-field" type="date" name="date_end" >
					</div>
	            </div>
	        </div>
	        
		    <div class="col-md-6 col-sm-6">
				<div class="search-category-container form-group">
					    <label class="field-label">Lista de empresas</label>
	                    <select class="dropdown-product selectpicker">
	                            <option>Seleccione</option>
	                            <option>Empresas polar</option>
	                            <option>ANCA</option>
	                            <option>CAPCA</option>
	                            <option>Intercable</option>
	                            <option>CANTV</option>	                            
	                     </select>
				</div>
	        </div>
	        
	        <div class="col-md-6 col-sm-6">
				<div class="search-category-container form-group">
					<label class="field-label">Paises</label>
	                    <select class="dropdown-product selectpicker">
	                            <option>Seleccione</option>
	                            <option>Venezuela</option>
	                            <option>Chile</option>
	                            <option>Colombia</option>
	                            <option>EEUU</option>                            
	                     </select>
				</div>
	        </div>
	        
	        <div class="col-md-6 col-sm-6">
				<div class="search-category-container form-group">
					    <label class="field-label">Dedicaci�n</label>
	                    <select class="dropdown-product selectpicker">
	                    		<option>Seleccione</option>
	                            <option>Medio tiempo</option>
	                            <option>Tiempo completo</option>	                            
	                     </select>
				</div>
	        </div>
	        
	        <div class="col-md-6 col-sm-6">
				<div class="search-category-container form-group">
					<label class="field-label">�rea de desempe�o</label>
	                    <select class="dropdown-product selectpicker">
	                    		<option>Seleccione</option>
	                            <option>Tecnolog�a IT</option>
	                            <option>Salud</option>
	                            <option>Deporte</option>
	                            <option>Literatura</option>
	                            <option>Ventas</option>
	                            <option>Educaci�n</option>	                   
	                     </select>
				</div>
	        </div>
	        
	        <div class="col-md-12 col-sm-12">
				<div class="search-category-container form-group">
				<label class="field-label">Estado de registro</label>
	                    <select class="dropdown-product selectpicker">
	                    		<option>Estado de registro</option>
	                            <option>Activo</option>
	                            <option>Inactivo</option>                            
	                     </select>
				</div>
	        </div>
	        
	        <div class="col-md-6 col-sm-6">
		    	<a class="btn btn-common btn-rm default-white-button" href="/ProyectoLaboratorio/ControladorEmpleo?operacion=ver">Guardar</button>
		    	<a class="btn btn-common btn-rm default-white-button" href="/ProyectoLaboratorio/ControladorEmpleo?operacion=listado">Cancelar</a>    
		    </div>
		    
	  </div>
	</div>
</form>
<#include "../../utils/footer.ftl">