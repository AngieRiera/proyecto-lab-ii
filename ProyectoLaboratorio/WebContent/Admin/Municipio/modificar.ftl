<#include "../../utils/header.ftl">
<div class="main-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-12 justify-content-center">
          <h1 class="h1-banner">Modificar Informaci�n del Municipio</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end intro section -->
</div>
<div class="container-breadcrumb">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb breadcrumb-style">
	    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
	    <li class="breadcrumb-item"><a href="#">Panel de administrador</a></li>
	    <li class="breadcrumb-item"><a href="/ProyectoLaboratorio/ControladorMunicipio?operacion=listado">Municipios</a></li>
	    <li class="breadcrumb-item active" aria-current="page">Municipio: Urdaneta</li>
	  </ol>
	</nav>
</div>
<form method="" action="">
	<div class="container custom-main-container justify-content-center">
	  <div class="row row-register">
		  	<div class="col-md-12 col-sm-12">	  		
		      <div class="form-group">
		      	<label class="field-label">Nombre</label>
		        <input class="form-control input-style" type="text" placeholder="Nombre" value="Urdaneta">
		      </div>
		    </div>		    
		    <div class="col-md-12 col-sm-12">
	    		<div class="form-group">
	    		  <label class="field-label">Pa�s</label>
                  <div class="search-category-container">
                    <label class="styled-select">
                     <select class="dropdown-product selectpicker">                        
                        <option>Venezuela</option>
                        <option>Francia</option>
                        <option>Italia</option>
                        <option>Holanda</option>
                        <option>M�xico</option>
                        <option>Colombia</option>
                        <option>Chile</option>
                      </select>
                    </label>
                  </div>
              </div>
            </div>
	    	<div class="col-md-12 col-sm-12">
	    		<div class="form-group">
	    		  <label class="field-label">Estado</label>
                  <div class="search-category-container">
                    <label class="styled-select">
                      <select class="dropdown-product selectpicker">                        
                        <option>Lara</option>
                        <option>Carabobo</option>
                        <option>Tachira</option>
                        <option>Nueva Esparta</option>
                        <option>Bol�var</option>
                        <option>Falcon</option>
                        <option>Cojedes</option>
                      </select>
                    </label>
                  </div>
              </div>
            </div>
            <div class="col-md-12 col-sm-12">
	    		<div class="form-group">
	    		  <label class="field-label">Estado de registro</label>
                  <div class="search-category-container">
                    <label class="styled-select">
                      <select class="dropdown-product selectpicker">                        
                        <option>Activo</option>
                        <option>Inactivo</option>
                      </select>
                    </label>
                  </div>
              </div>
            </div>
            
		    <div class="col-md-12 col-sm-12">
		    	<a href="/ProyectoLaboratorio/ControladorMunicipio?operacion=ver" class="btn btn-common btn-rm register-btn default-white-button">Guardar</a>
		    	<a href="/ProyectoLaboratorio/ControladorMunicipio?operacion=listado" class="btn btn-common btn-rm register-btn default-white-button">Cancelar</a>  
		    </div>
	  </div>
	</div>
</form>
<#include "../../utils/footer.ftl">