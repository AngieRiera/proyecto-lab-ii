<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Dashboard - Administrador</title>

  <!-- Favicons -->
  <link href=" img/favicon.png" rel="icon">
  <link href=" img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href=" lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href=" lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link rel=" stylesheet" type="text/css" href=" css/zabuto_calendar.css">
  <link rel=" stylesheet" type="text/css" href=" lib/gritter/css/jquery.gritter.css" />
  <!-- Custom styles for this template -->
  <link href=" css/style.css" rel="stylesheet">
  <link href=" css/style-responsive.css" rel="stylesheet">
  <script src=" lib/chart-master/Chart.js"></script>
  
  <!-- Estilos custom -->
  <link href=" css/custom.css" rel="stylesheet">

  <!-- =======================================================
    Template Name: Dashio
    Template URL: https://templatemag.com/dashio-bootstrap-admin-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>
  <section id="container">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->

      <!--logo end-->
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout custom-logout" href="#">Salir</a></li>
        </ul>
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="profile.html"><img src="img/ui-sam.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered">Sam Soffes</h5>
          <li class="mt">
            <a class="active" href="index.ftl">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-th"></i>
              <span>Archivo</span>
              </a>
            <ul class="sub">
              <li><a href="/ProyectoLaboratorio/ControladorDashboard?gestion=usuario">Gesti&oacute;n de usuarios</a></li>
              <li><a href="/ProyectoLaboratorio/ControladorDashboard?gestion=empleado">Gesti&oacute;n de empleados</a></li>
              <li><a href="/ProyectoLaboratorio/ControladorDashboard?gestion=empresa">Gesti&oacute;n de empresas</a></li>
              <li><a href="/ProyectoLaboratorio/ControladorDashboard?gestion=empleo">Gesti&oacute;n de ofertas de empleos</a></li>
              <li><a href="/ProyectoLaboratorio/ControladorDashboard?gestion=jornada">Gesti&oacute;n de jornadas</a></li>
              <li><a href="/ProyectoLaboratorio/ControladorDashboard?gestion=area">Gesti&oacute;n de areas de desempeño</a></li>
              <li><a href="/ProyectoLaboratorio/ControladorDashboard?gestion=pais">Gesti&oacute;n de pa&iacute;ses</a></li>
              <li><a href="/ProyectoLaboratorio/ControladorDashboard?gestion=estado">Gesti&oacute;n de estados</a></li>
              <li><a href="/ProyectoLaboratorio/ControladorDashboard?gestion=municipio">Gesti&oacute;n de municipios</a></li>
              <li><a href="/ProyectoLaboratorio/ControladorDashboard?gestion=profesion">Gesti&oacute;n de profesiones</a></li>
              <li><a href="/ProyectoLaboratorio/ControladorDashboard?gestion=rol">Gesti&oacute;n de roles</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class=" fa fa-bar-chart-o"></i>
              <span>Estadisticas</span>
              </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="col-lg-9 main-chart">
            <!--CUSTOM CHART START -->
            <h3><i class="fa fa-angle-right"></i> Empresas destacadas</h3>
            <div class="col-lg-4 col-md-4 col-sm-4 mb">
               <div class="product-panel-2 pn">
               	 <div class="image-container-dashboard justify-content-center">
                 	<img src="img/logo-coca-cola-lead.png" width="200" alt="">
                 </div>
                 <h5 class="mt">Coca-Cola</h5>
                 <button class="btn btn-small btn-theme04 view-more">VER M&Aacute;S</button>
               </div>
             </div>
             <div class="col-lg-4 col-md-4 col-sm-4 mb">
               <div class="product-panel-2 pn">
               	 <div class="image-container-dashboard justify-content-center">
                 	<img src="img/nintendo.jpg" width="200" alt="">
                 </div>
                 <h5 class="mt">Nintendo</h5>
                 <button class="btn btn-small btn-theme04 view-more">VER M&Aacute;S</button>
               </div>
             </div>
             <div class="col-lg-4 col-md-4 col-sm-4 mb">
               <div class="product-panel-2 pn">
               	 <div class="image-container-dashboard justify-content-center">
                 	<img src="img/product.jpg" width="200" alt="">
                 </div>
                 <h5 class="mt">CH</h5>
                 <button class="btn btn-small btn-theme04 view-more">VER M&Aacute;S</button>
               </div>
             </div>
             <div class="col-lg-4 col-md-4 col-sm-4 mb">
               <div class="product-panel-2 pn">
               	 <div class="image-container-dashboard justify-content-center">
               	 	<img src="img/abstergo.png" width="200" alt="">
               	 </div>
                 <h5 class="mt">Abstergo</h5>
                 <button class="btn btn-small btn-theme04 view-more">VER M&Aacute;S</button>
               </div>
             </div>
             <div class="col-lg-4 col-md-4 col-sm-4 mb">
               <div class="product-panel-2 pn">
               	 <div class="image-container-dashboard justify-content-center">
                 	<img src="img/adidas.png" width="200" max-height="125" alt="">
                 </div>
                 <h5 class="mt">Adidas</h5>
                 <button class="btn btn-small btn-theme04 view-more">VER M&Aacute;S</button>
               </div>
             </div>
             <div class="col-lg-4 col-md-4 col-sm-4 mb">
               <div class="product-panel-2 pn">
               	 <div class="image-container-dashboard justify-content-center">
                 	<img src="img/alegra.png" width="200" alt="">
                 </div>
                 <h5 class="mt">Alegra</h5>
                 <button class="btn btn-small btn-theme04 view-more">VER M&Aacute;S</button>
               </div>
             </div>

          </div>
          <!-- /col-lg-9 END SECTION MIDDLE -->
          <!-- **********************************************************************************************************************************************************
              RIGHT SIDEBAR CONTENT
              *********************************************************************************************************************************************************** -->
          <div class="col-lg-3 ds">
            <!--COMPLETED ACTIONS DONUTS CHART-->
            <div class="donut-main">
              <h4>Usuarios femeninos vs usuarios masculinos</h4>
              <canvas id="newchart" height="130" width="130"></canvas>
              <script>
                var doughnutData = [{
                    value: 55,
                    color: "#71a3ff"
                  },
                  {
                    value: 45,
                    color: "#ff71ac"
                  }
                ];
                var myDoughnut = new Chart(document.getElementById("newchart").getContext("2d")).Doughnut(doughnutData);
              </script>
              <div class="MvW">	
	              <div class="MvW-W"></div><p>Mujeres <b>45%</b></p>
	              </br>
	              <div class="MvW-M"></div><p>Hombres <b>55%</b></p>
              </div>  
            </div>
            <!-- USERS ONLINE SECTION -->
            <h4 class="centered mt">Administradores</h4>
            <!-- First Member -->
            <div class="desc">
              <div class="thumb">
                <img class="img-circle" src=" img/ui-divya.jpg" width="35px" height="35px" align="">
              </div>
              <div class="details">
                <p>
                  <a href="#">DIVYA MANIAN</a><br/>
                </p>
              </div>
            </div>
            <!-- Second Member -->
            <div class="desc">
              <div class="thumb">
                <img class="img-circle" src=" img/ui-sherman.jpg" width="35px" height="35px" align="">
              </div>
              <div class="details">
                <p>
                  <a href="#">DJ SHERMAN</a><br/>
                </p>
              </div>
            </div>
            <!-- Third Member -->
            <div class="desc">
              <div class="thumb">
                <img class="img-circle" src=" img/ui-danro.jpg" width="35px" height="35px" align="">
              </div>
              <div class="details">
                <p>
                  <a href="#">DAN ROGERS</a><br/>
                </p>
              </div>
            </div>
            <!-- Fourth Member -->
            <div class="desc">
              <div class="thumb">
                <img class="img-circle" src=" img/ui-zac.jpg" width="35px" height="35px" align="">
              </div>
              <div class="details">
                <p>
                  <a href="#">Zac Sniders</a><br/>
                </p>
              </div>
            </div>
          </div>
          <!-- /col-lg-3 -->
        </div>
        <!-- /row -->
      </section>
    </section>
    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer custom-site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Dashio</strong>. All Rights Reserved
        </p>
        <div class="credits">
          <!--
            You are NOT allowed to delete the credit link to TemplateMag with free version.
            You can delete the credit link only if you bought the pro version.
            Buy the pro version with working PHP/AJAX contact form: https://templatemag.com/dashio-bootstrap-admin-template/
            Licensing information: https://templatemag.com/license/
          -->
          Created with Dashio template by <a href="https://templatemag.com/">TemplateMag</a>
        </div>
        <a href="index.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src=" lib/jquery/jquery.min.js"></script>

  <script src=" lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src=" lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src=" lib/jquery.scrollTo.min.js"></script>
  <script src=" lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src=" lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src=" lib/common-scripts.js"></script>

  
  <!--script for this page-->
  <script src=" lib/sparkline-chart.js"></script>
  <script src=" lib/zabuto_calendar.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      var unique_id = $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: 'Welcome to Dashio!',
        // (string | mandatory) the text inside the notification
        text: 'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo.',
        // (string | optional) the image to display on the left
        image: ' img/ui-sam.jpg',
        // (bool | optional) if you want it to fade out on its own or just sit there
        sticky: false,
        // (int | optional) the time you want it to be alive for before fading out
        time: 8000,
        // (string | optional) the class name you want to apply to that specific message
        class_name: 'my-sticky-class'
      });

      return false;
    });
  </script>
  <script type="application/javascript">
    $(document).ready(function() {
      $("#date-popover").popover({
        html: true,
        trigger: "manual"
      });
      $("#date-popover").hide();
      $("#date-popover").click(function(e) {
        $(this).hide();
      });

      $("#my-calendar").zabuto_calendar({
        action: function() {
          return myDateFunction(this.id, false);
        },
        action_nav: function() {
          return myNavFunction(this.id);
        },
        ajax: {
          url: "show_data.php?action=1",
          modal: true
        },
        legend: [{
            type: "text",
            label: "Special event",
            badge: "00"
          },
          {
            type: "block",
            label: "Regular event",
          }
        ]
      });
    });

    function myNavFunction(id) {
      $("#date-popover").hide();
      var nav = $("#" + id).data("navigation");
      var to = $("#" + id).data("to");
      console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
  </script>
</body>

</html>
