<html>
<head>
<title>FreeMarker Hello World</title>

<!-- Bootstrap core CSS -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">
<!-- Bootstrap theme -->
<link href="resources/css/bootstrap-theme.min.css" rel="stylesheet">
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link href="resources/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
 
</head>

<body>
<div class="container theme-showcase" role="main">
  <form name="user" action="hello" method="post">
    <div class="form-group">
      <label for="firstname">Firstname:</label>
      <input type="text" class="form-control" id="firstname"  name="firstname"placeholder="Firstname">
    </div>
    <div class="form-group">
      <label for="lastname">Lastname:</label>
      <input type="text" class="form-control" id="lastname"  name="lastname"placeholder="Lastname">
    </div>
  	<button class="btn btn-primary" type="submit">Save</button>
  </form>

  <div class="page-header">
     <h1>Users</h1>
  </div>
  <div class="row">
    <div class="col-md-6">
	  <table class="table table-bordered">
	  	<tr>
	  		<th>Firstname</th>  <th>Lastname</th>
	  	</tr>
	    <#list users as user>
	  	<tr>
	  		<td>${user.firstname}</td> <td>${user.lastname}</td>
	  	</tr>
	    </#list>
	  </table>
	</div>
  </div>
</div>
</body>
</html>