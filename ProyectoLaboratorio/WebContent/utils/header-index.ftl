<!DOCTYPE html>
	<html lang="en">
	  <head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">    
	    <meta http-equiv="content-type" content="text/html; charset=utf-8">
	    <meta name="author" content="Jobboard">
	    
	    <title>FreeJobs - Encuentra trabajos aqu�</title>    
	
	    <!-- Favicon -->
	    <link rel="shortcut icon" href="assets/img/favicon.png">
	    <!-- Bootstrap CSS -->
	    <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">    
	    <link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">  
	    <link rel="stylesheet" href="assets/css/bootstrap-select.min.css" type="text/css">  
	    <!-- Material CSS -->
	    <link rel="stylesheet" href="assets/css/material-kit.css" type="text/css">
	    <!-- Font Awesome CSS -->
	    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css" type="text/css"> 
	    <link rel="stylesheet" href="assets/fonts/themify-icons.css"> 
	
	    <!-- Animate CSS -->
	    <link rel="stylesheet" href="assets/extras/animate.css" type="text/css">
	    <!-- Owl Carousel -->
	    <link rel="stylesheet" href="assets/extras/owl.carousel.css" type="text/css">
	    <link rel="stylesheet" href="assets/extras/owl.theme.css" type="text/css">
	    <!-- Rev Slider CSS -->
	    <link rel="stylesheet" href="assets/extras/settings.css" type="text/css"> 
	    <!-- Slicknav js -->
	    <link rel="stylesheet" href="assets/css/slicknav.css" type="text/css">
	    <!-- Main Styles -->
	    <link rel="stylesheet" href="assets/css/main.css" type="text/css">
	    <!-- Responsive CSS Styles -->
	    <link rel="stylesheet" href="assets/css/responsive.css" type="text/css">
	
	    <!-- Color CSS Styles  -->
	    <link rel="stylesheet" type="text/css" href="assets/css/colors/red.css" media="screen" />

		<!-- Custom CSS Styles  -->
	    <link rel="stylesheet" href="assets/css/custom.css" type="text/css">

	    
	  </head>
	
	  <body>  
	      <!-- Header Section Start -->
	      <div class="header">    
	        <!-- Start intro section -->
	        <section id="intro" class="section-intro">
	          <div class="logo-menu">
	            <nav class="navbar navbar-default" role="navigation" data-spy="affix" data-offset-top="50">
	              <div class="container">
	                <!-- Brand and toggle get grouped for better mobile display -->
	                <div class="navbar-header">
	                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" class="btn">
	                    <span class="sr-only">Toggle navigation</span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                  </button>
	                  <a class="navbar-brand logo" href="index.html"><img src="assets/img/logo.png" alt=""></a>
	                </div>
	
	                <div class="collapse navbar-collapse" id="navbar">              
	                <!-- Start Navigation List -->
	                <ul class="nav navbar-nav">
	                  <li>
	                    <a href="#">
	                    Sobre nosotros
	                    </a>
	                  </li>

	                  <li>
	                    <a href="#">
	                    Contacto 
	                    </a>
	                    
	                  </li>
	                  
	                </ul>
	                <ul class="nav navbar-nav navbar-right float-right">
	                  <li class="left"><a href="#"><i class="ti-pencil-alt"></i> Registrarme</a></li>
	                  <li class="right"><a href="#"><i class="ti-lock"></i>  Iniciar sesi�n</a></li>
	                </ul>
	              </div>                           
	            </div>
	            <!-- Mobile Menu Start -->
	            <ul class="wpb-mobile-menu">
	              <li>
	                <a class="active" href="index.html">Home</a>
	                <ul>
	                  <li><a class="active" href="index.html">Home 1</a></li>
	                  <li><a href="index-02.html">Home 2</a></li>
	                  <li><a href="index-03.html">Home 3</a></li>
	                  <li><a href="index-04.html">Home 4</a></li>
	                </ul>                       
	              </li>
	              <li>
	                <a href="about.html">Pages</a>
	                <ul>
	                  <li><a href="about.html">About</a></li>
	                  <li><a href="job-page.html">Job Page</a></li>
	                  <li><a href="job-details.html">Job Details</a></li>
	                  <li><a href="resume.html">Resume Page</a></li>
	                  <li><a href="privacy-policy.html">Privacy Policy</a></li>
	                  <li><a href="faq.html">FAQ</a></li>
	                  <li><a href="pricing.html">Pricing Tables</a></li>
	                  <li><a href="contact.html">Contact</a></li>
	                </ul>
	              </li>
	              <li>
	                <a href="#">For Candidates</a>
	                <ul>
	                  <li><a href="browse-jobs.html">Browse Jobs</a></li>
	                  <li><a href="browse-categories.html">Browse Categories</a></li>
	                  <li><a href="add-resume.html">Add Resume</a></li>
	                  <li><a href="manage-resumes.html">Manage Resumes</a></li>
	                  <li><a href="job-alerts.html">Job Alerts</a></li>
	                </ul>
	              </li>
	              <li>
	                <a href="#">For Employers</a>
	                <ul>
	                  <li><a href="post-job.html">Add Job</a></li>
	                  <li><a href="manage-jobs.html">Manage Jobs</a></li>
	                  <li><a href="manage-applications.html">Manage Applications</a></li>
	                  <li><a href="browse-resumes.html">Browse Resumes</a></li>
	                </ul>
	              </li> 
	                
	              <li class="btn-m"><a href="post-job.html"><i class="ti-pencil-alt"></i> Registrarme</a></li>
	              <li class="btn-m"><a href="my-account.html"><i class="ti-lock"></i>  Iniciar sesi�nn</a></li>          
	            </ul>
	            <!-- Mobile Menu End --> 
	          </nav>	 
	      
	      <div class="search-container">
	        <div class="container">
	          <div class="row">
	            <div class="col-md-12">
	              <h1>Busca un trabajo acorde a ti</h1><br><h2>Mas de <strong>1,000</strong> trabajos se encuentran diponibles</h2>
	              <div class="content">
	                <form method="" action="">
	                  <div class="row" style="position: relative;">

	            		<div class="col-md-11 col-sm-11">

			            	<div class="row">
			                    <div class="col-md-4 col-sm-6">
			                      <div class="form-group">
			                        <input class="form-control" type="text" placeholder="B�squeda por trabajo">
			                        
			                      </div>
			                    </div>
			                    <div class="col-md-2 col-sm-6">
			                      <div class="search-category-container">
			                        <label >
			                          <select class="dropdown-product selectpicker">
			                            <option>Salario</option>
			                            <option>0-100</option>
			                            <option>101-300</option>
			                            <option>301-500</option>
			                            <option>501-1000</option>
			                          </select>
			                        </label>
			                      </div>
			                    </div>
			                    <div class="col-md-2 col-sm-6">
			                      <div class="search-category-container">
			                        <label >
			                          <select class="dropdown-product selectpicker">
			                            <option>Dedicaci�n</option>
			                            <option>Freelancer</option>
			                            <option>Part Time</option>
			                            <option>Pasant�as</option>
			                            <option>Full time</option>
			                          </select>
			                        </label>
			                      </div>
			                    </div>
			                    <div class="col-md-4 col-sm-6">
			                      <div class="search-category-container">
			                        <label >
			                          <select class="dropdown-product selectpicker">
			                            <option>Areas de desempe�o</option>
			                            <option>Tecnolog�a</option>
			                            <option>Medicina</option>
			                            <option>F�sica</option>
			                            <option>Psicolog�a</option>
			                            <option>Dise�o</option>
			                          </select>
			                        </label>
			                      </div>
			                    </div>
		                	</div>
		            		<!--fin row-->

							<div class="row location-row">
								<div class="col-md-4 col-sm-6">
			                      <div class="search-category-container">			                        
			                          <select class="dropdown-product selectpicker">
			                            <option>Pa�s</option>
			                            <option>Tecnolog�a</option>
			                            <option>Medicina</option>
			                            <option>F�sica</option>
			                            <option>Psicolog�a</option>
			                            <option>Dise�o</option>
			                          </select>
			                        </label>
			                      </div>
			                    </div>

			                    <div class="col-md-4 col-sm-6">
			                      <div class="search-category-container">			                        
			                          <select class="dropdown-product selectpicker">
			                            <option>Estado</option>
			                            <option>Lara</option>
			                            <option>Carabobo</option>
			                            <option>Anzo�tegui</option>
			                            <option>Zulia</option>
			                          </select>
			                        </label>
			                      </div>
			                    </div>

			                    <div class="col-md-4 col-sm-6">
			                      <div class="search-category-container">			                        
			                          <select class="dropdown-product selectpicker">
			                            <option>Municipio</option>
			                            <option>Urdaneta</option>
			                            <option>Palavecino</option>
			                            <option>Iribarren</option>
			                            <option>Torres</option>
			                          </select>
			                        </label>
			                      </div>
			                    </div>
			            	</div>
			                <!--fin row-->
			            </div>
			            <!--fin col11-->
				<div class="col-md-1 col-sm-6 index-search">
                  <button type="button" class="btn btn-search-icon"><i class="ti-search"></i></button>
                </div>
	        </div>

			</div>
	      <!-- Header Section End -->	
	                </form>
	              </div>
	              <div class="popular-jobs">
	                <b>Palabras claves populares: </b>
	                <a href="#">Desarrollador</a>
	                <a href="#">Asistente</a>
	                <a href="#">Dise�o gr�fico</a>
	              </div>
	            </div>
	          </div>
	        </div>
	      </div>
	    </section>
	    <!-- end intro section -->
	    </div>