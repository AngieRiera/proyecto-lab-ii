package controlador;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.User;

@WebServlet("/ControladorIndex")
public class ControladorIndex extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private static List<User> userList = new ArrayList<User>();
	
	//Just prepare static data to display on screen
	static {
		userList.add(new User("Bill", "Gates"));
		userList.add(new User("Steve", "Jobs"));
		userList.add(new User("Larry", "Page"));
		userList.add(new User("Sergey", "Brin"));
		userList.add(new User("Larry", "Ellison"));
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//Put the user list in request and 
		//let freemarker paint it.
		request.setAttribute("users", userList);
		
		//request.getRequestDispatcher("/index1.ftl").forward(request, response);
		try {
			procesarPeticion(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		
		if(null != firstname && null != lastname
				&& !firstname.isEmpty() && !lastname.isEmpty()) {
			
			synchronized (userList) {
				userList.add(new User(firstname, lastname));
			}
			
		}
		
		doGet(request, response);
	}
protected void procesarPeticion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		
		String rol=request.getParameter("rol");
		String gestion=request.getParameter("gestion");
		String operacion=request.getParameter("operacion");
		
		System.out.println(request.getParameter("rol"));
		//enviar a la página de registro de estudiantes
		if(rol.equals("empleado")) {
			if(operacion.equals("postulaciones")) {
				request.getRequestDispatcher("/User/Empleado/listado-postulaciones.ftl").forward(request, response);
			}else
			{
				if(operacion.equals("postulacion")) {
					request.getRequestDispatcher("/User/Empleado/postulacion-empleo.ftl").forward(request, response);
				}
				else {
					if(operacion.equals("estado_postulacion")) {
						request.getRequestDispatcher("/User/Empleado/estado-postulacion-empleado.ftl").forward(request, response);
					}
					else {
						if(operacion.equals("curriculum")) {
							request.getRequestDispatcher("/User/Empleado/curriculum.ftl").forward(request, response);
						}
						else {
							request.getRequestDispatcher("empleado_index.ftl").forward(request, response);
						}
					}
				}
			}
		}else {
			if(rol.equals("empresa"))
				if(gestion !=null & gestion.equals("postulacion")) {
					if(operacion != null & operacion.equals("ver")) {
						request.getRequestDispatcher("/User/Empresa/Postulados/ver.ftl").forward(request, response);
					}
					else {
						if(operacion !=null & operacion.equals("listado")) {
							request.getRequestDispatcher("/User/Empresa/Postulados/listado.ftl").forward(request, response);
						}else {
							request.getRequestDispatcher("empresa_index.ftl").forward(request, response);
						}
					}
				}else {
					if(operacion != null & operacion.equals("ver")) {
						request.getRequestDispatcher("/User/Empresa/Empleo/ver.ftl").forward(request, response);
					}
					else {
						if(operacion != null & operacion.equals("modificar")) {
							request.getRequestDispatcher("/User/Empresa/Empleo/modificar.ftl").forward(request, response);
						}
						else {
							if(operacion != null & operacion.equals("registro")) {
								request.getRequestDispatcher("/User/Empresa/Empleo/registro.ftl").forward(request, response);
							}
							else {
								if(operacion !=null & operacion.equals("listado")) {
									request.getRequestDispatcher("/User/Empresa/Empleo/listado.ftl").forward(request, response);
								}else {
									request.getRequestDispatcher("empresa_index.ftl").forward(request, response);
								}
							}
						}
					}
				}
			else {
				if(rol.equals("administrador")) {
					
					if(operacion != null & operacion.equals("dashboard")) {
						request.getRequestDispatcher("index-dashboard.ftl").forward(request, response);
					}else {
						request.getRequestDispatcher("index-admin.ftl").forward(request, response);
					}
				}
				else {
					request.getRequestDispatcher("index.ftl").forward(request, response);
				}
			}
		}
			
		

		
	}
}