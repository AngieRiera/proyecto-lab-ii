package controlador;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ControladorDashboard")
public class ControladorDashboard extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		try {
			procesarPeticion(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		doGet(request, response);
		
	}
protected void procesarPeticion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		
		String gestion=request.getParameter("gestion");
	
		if(gestion.equals("area")) {
			request.getRequestDispatcher("/Admin/Area/listado.ftl").forward(request, response);
		}else {
			if(gestion.equals("empleado")) {
				request.getRequestDispatcher("/Admin/Empleado/listado.ftl").forward(request, response);
			}else {
				if(gestion.equals("empleo")) {
					request.getRequestDispatcher("/Admin/Empleo/listado.ftl").forward(request, response);
				}
				else {
					if(gestion.equals("empresa")) {
						request.getRequestDispatcher("/Admin/Empresa/listado.ftl").forward(request, response);
					}
					else {
						if(gestion.equals("estado")) {
							request.getRequestDispatcher("/Admin/Estado/listado.ftl").forward(request, response);
						}
						else {
							if(gestion.equals("jornada")) {
								request.getRequestDispatcher("/Admin/Jornada/listado.ftl").forward(request, response);
							}
							else {
								if(gestion.equals("municipio")) {
									request.getRequestDispatcher("/Admin/Municipio/listado.ftl").forward(request, response);
								}
								else {
									if(gestion.equals("pais")) {
										request.getRequestDispatcher("/Admin/Pais/listado.ftl").forward(request, response);
									}
									else {
										if(gestion.equals("profesion")) {
											request.getRequestDispatcher("/Admin/Profesion/listado.ftl").forward(request, response);
										}
										else {
											if(gestion.equals("rol")) {
												request.getRequestDispatcher("/Admin/Rol/listado.ftl").forward(request, response);
											}
											else {
												request.getRequestDispatcher("/Admin/Usuario/listado.ftl").forward(request, response);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}