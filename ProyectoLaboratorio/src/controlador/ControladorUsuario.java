package controlador;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ControladorUsuario")
public class ControladorUsuario extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		try {
			procesarPeticion(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		doGet(request, response);
		
	}
protected void procesarPeticion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		
		String operacion=request.getParameter("operacion");
	
		if(operacion.equals("ver")) {
			request.getRequestDispatcher("/Admin/Usuario/ver.ftl").forward(request, response);
		}else {
			if(operacion.equals("registrar")) {
				request.getRequestDispatcher("/Admin/Usuario/registro.ftl").forward(request, response);
			}else {
				if(operacion.equals("modificar")) {
					request.getRequestDispatcher("/Admin/Usuario/modificar.ftl").forward(request, response);
				}
				else {
					request.getRequestDispatcher("/Admin/Usuario/listado.ftl").forward(request, response);
				}
			}
		}
	}
}