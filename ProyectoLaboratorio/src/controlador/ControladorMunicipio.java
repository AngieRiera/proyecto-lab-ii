package controlador;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.User;

@WebServlet("/ControladorMunicipio")
public class ControladorMunicipio extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private static List<User> userList = new ArrayList<User>();
	
	//Just prepare static data to display on screen
	static {
		userList.add(new User("Bill", "Gates"));
		userList.add(new User("Steve", "Jobs"));
		userList.add(new User("Larry", "Page"));
		userList.add(new User("Sergey", "Brin"));
		userList.add(new User("Larry", "Ellison"));
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//Put the user list in request and 
		//let freemarker paint it.
		request.setAttribute("users", userList);
		
		try {
			procesarPeticion(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		
		if(null != firstname && null != lastname
				&& !firstname.isEmpty() && !lastname.isEmpty()) {
			
			synchronized (userList) {
				userList.add(new User(firstname, lastname));
			}
			
		}
		
		doGet(request, response);
		
	}
protected void procesarPeticion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		
		String operacion=request.getParameter("operacion");
		
		if(operacion.equals("ver")) {
			request.getRequestDispatcher("/Admin/Municipio/ver.ftl").forward(request, response);
		}else {
			if(operacion.equals("registrar")) {
				request.getRequestDispatcher("/Admin/Municipio/registro.ftl").forward(request, response);
			}else {
				if(operacion.equals("modificar")) {
					request.getRequestDispatcher("/Admin/Municipio/modificar.ftl").forward(request, response);
				}
				else {
					request.getRequestDispatcher("/Admin/Municipio/listado.ftl").forward(request, response);
				}
			}
		}
	}
}