<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Dashboard - Empleado</title>

    <!-- Favicons -->
  <link href="/ProyectoLaboratorioII/dashboard/img/favicon.png" rel="icon">
  <link href="/ProyectoLaboratorioII/dashboard/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="/ProyectoLaboratorioII/dashboard/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="/ProyectoLaboratorioII/dashboard/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="/ProyectoLaboratorioII/dashboard/lib/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="/ProyectoLaboratorioII/dashboard/lib/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="/ProyectoLaboratorioII/dashboard/lib/advanced-datatable/css/DT_bootstrap.css" />
  <!-- Custom styles for this template -->
  <link href="/ProyectoLaboratorioII/dashboard/css/style.css" rel="stylesheet">
  <link href="/ProyectoLaboratorioII/dashboard/css/style-responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="/ProyectoLaboratorioII/dashboard/css/to-do.css">
  

    <!-- Estilos custom -->
  <link href="/ProyectoLaboratorioII/dashboard/css/custom.css" rel="stylesheet">
  <link href="/ProyectoLaboratorioII/assets/css/custom.css" rel="stylesheet">

</head>
<body>
  <section id="container" class="section-full-size">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->

      <!--logo end-->
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout custom-logout" href="#">Salir</a></li>
        </ul>
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse position-sidebar">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="profile.html"><img src="/ProyectoLaboratorioII/dashboard/img/ui-sam.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered">Sam Soffes</h5>
          <li class="mt">
            <a class="active" href="admin">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-th"></i>
              <span>Archivo</span>
              </a>
            <ul class="sub">
              <li><a href="/ProyectoLaboratorio/ControladorDashboard?gestion=usuario">Curriculo</a></li>

            </ul>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->

 
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">

		<div class="row mt">
          <div class="col-lg-12">
            <h4><i class="fa fa-angle-right"></i> Perfil de usuario</h4>
            <div class="form-panel">
              <div class=" form">
                <form class="cmxform form-horizontal style-form" id="commentForm" method="post" action="/ProyectoLaboratorioII/perfil?operacion=guardar"  enctype="multipart/form-data">
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Nombre</label>
                    <div class="col-lg-10">
                      <input class=" form-control" id="cname" name="nombre" minlength="2" type="text" required value="${usuario.getUserProfile().getNombre()}"/>
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Apellido</label>
                    <div class="col-lg-10">
                      <input class=" form-control" id="cname" name="apellido" minlength="2" type="text" required value="${usuario.getUserProfile().getApellido()}"/>
                    </div>
                  </div>              
                         
                  <div class="form-group ">
                    <label for="ccomment" class="control-label col-lg-2">Genero</label>
                    <div class="col-lg-10">
                      <select class="form-control" name="genero">
	                      <#if empleado.genero == 1>
			                  <option value=1 selected>Mujer</option>
			                  <option value=2>Hombre</option>
			              <#else>
			              	  <option value=1>Mujer</option>
			                  <option value=2 selected>Hombre</option>	
			              </#if>
		               </select>
                    </div>
                  </div> 
                  
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Direcci&oacute;n</label>
                    <div class="col-lg-10">
                      <textarea class="form-control" placeholder="*Direccion" name="direccion" required >${usuario.getUserProfile().getDireccion()}</textarea>
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Tel&eacute;fono</label>
                    <div class="col-lg-10">
                      <input class=" form-control" id="cname" name="telefono" minlength="2" type="text" required value="${usuario.getUserProfile().getTelefono()}"/>
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Twitter</label>
                    <div class="col-lg-10">
                      <input class=" form-control" id="cname" name="url_twitter" minlength="2" type="text"  value="${usuario.getUserProfile().getUrl_twitter()}"/>
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Instagram</label>
                    <div class="col-lg-10">
                      <input class=" form-control" id="cname" name="url_instagram" minlength="2" type="text" value="${usuario.getUserProfile().getUrl_instagram()}"/>
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Facebook</label>
                    <div class="col-lg-10">
                      <input class=" form-control" id="cname" name="url_facebook" minlength="2" type="text"  value="${usuario.getUserProfile().getUrl_facebook()}"/>
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="ccomment" class="control-label col-lg-2">Pa&iacute;s</label>
                    <div class="col-lg-10">
                    <select class="form-control" name="pais" required>
						<option value="">Seleccione</option> 
							<#list paises as pais>
								<#if pais.id == usuario.idPais()>
			                  		<option value="${pais.id}" selected>${pais.nombre}</option>
			                  	<#else>
			                  		<option value="${pais.id}">${pais.nombre}</option> 
			                  	</#if>  
							</#list>
                    </select>
                    </div>
                  </div>
                  
                  <div class="form-group form-group-file">
	                  <span class="btn btn-success fileinput-button" style="background-color:#78cd51">
	                    <i class="glyphicon glyphicon-plus"></i>
	                    <span>A&ntilde;adir imagen de perfil</span>
	                  <input type="file" name="file">
	                  </span>
                  </div>
                  
                  <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                      <button class="btn btn-theme" type="submit">Guardar cambios</button>
                      <button class="btn btn-theme04" type="button">Cancelar</button>
                    </div>
                  </div>
                  
                </form>
              </div>
            </div>
            <!-- /form-panel -->
          </div>
          <!-- /col-lg-12 -->
        </div>
        <!-- /row -->
      </section>
    </section>
    <!--main content end-->
    <!--footer start-->
    <!-- footer class="site-footer custom-site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Dashio</strong>. All Rights Reserved
        </p>
        <div class="credits">
          Created with Dashio template by <a href="https://templatemag.com/">TemplateMag</a>
        </div>
        <a href="index.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer-->
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="/ProyectoLaboratorioII/dashboard/lib/jquery/jquery.min.js"></script>

  <script src="/ProyectoLaboratorioII/dashboard/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="/ProyectoLaboratorioII/dashboard/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="/ProyectoLaboratorioII/dashboard/lib/jquery.scrollTo.min.js"></script>
  <script src="/ProyectoLaboratorioII/dashboard/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="/ProyectoLaboratorioII/dashboard/lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="/ProyectoLaboratorioII/dashboard/lib/common-scripts.js"></script>

 <script class="include" type="text/javascript" src="/ProyectoLaboratorioII/dashboard/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="/ProyectoLaboratorioII/dashboard/lib/jquery.scrollTo.min.js"></script>
  <script src="/ProyectoLaboratorioII/dashboard/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <!--common script for all pages-->
  <script src="/ProyectoLaboratorioII/dashboard/lib/common-scripts.js"></script>
  <!--script for this page-->
  <script src="/ProyectoLaboratorioII/dashboard/lib/jquery-ui-1.9.2.custom.min.js"></script>
  <script type="text/javascript" src="/ProyectoLaboratorioII/dashboard/lib/bootstrap-fileupload/bootstrap-fileupload.js"></script>
  <script type="text/javascript" src="/ProyectoLaboratorioII/dashboard/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="/ProyectoLaboratorioII/dashboard/lib/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="/ProyectoLaboratorioII/dashboard/lib/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="/ProyectoLaboratorioII/dashboard/lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="/ProyectoLaboratorioII/dashboard/lib/bootstrap-daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="/ProyectoLaboratorioII/dashboard/lib/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  <script src="/ProyectoLaboratorioII/dashboard/lib/advanced-form-components.js"></script>
  
  <!--script for this page-->
  <script src="/ProyectoLaboratorioII/dashboard/lib/sparkline-chart.js"></script>
  <script src="/ProyectoLaboratorioII/dashboard/lib/zabuto_calendar.js"></script>


  <!-- The basic File Upload plugin -->
  <script src="/ProyectoLaboratorioII/dashboard/lib/file-uploader/js/jquery.fileupload.js"></script>
  <!-- The File Upload processing plugin -->
  <script src="/ProyectoLaboratorioII/dashboard/lib/file-uploader/js/jquery.fileupload-process.js"></script>
  <!-- The File Upload image preview & resize plugin -->
  <script src="/ProyectoLaboratorioII/dashboard/lib/file-uploader/js/jquery.fileupload-image.js"></script>
  <!-- The File Upload audio preview plugin -->
  <script src="/ProyectoLaboratorioII/dashboard/lib/file-uploader/js/jquery.fileupload-audio.js"></script>
  <!-- The File Upload video preview plugin -->
  <script src="/ProyectoLaboratorioII/dashboard/lib/file-uploader/js/jquery.fileupload-video.js"></script>
  <!-- The File Upload validation plugin -->
  <script src="/ProyectoLaboratorioII/dashboard/lib/file-uploader/js/jquery.fileupload-validate.js"></script>
  <!-- The File Upload user interface plugin -->
  <script src="/ProyectoLaboratorioII/dashboard/lib/file-uploader/js/jquery.fileupload-ui.js"></script>
  <!-- The main application script -->
  <script src="/ProyectoLaboratorioII/dashboard/lib/file-uploader/js/main.js"></script>
  <!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
  <!--[if (gte IE 8)&(lt IE 10)]>
    <script src="assets/file-uploader/js/cors/jquery.xdr-transport.js"></script>
    <![endif]-->
  <!-- The template to display files available for upload -->


</body>

</html>
