<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Inicio de sesi&oacute;n</title>

  <!-- Favicons -->
  <link href="dashboard/img/favicon.png" rel="icon">
  <link href="dashboard/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="dashboard/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="dashboard/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="dashboard/css/style.css" rel="stylesheet">
  <link href="dashboard/css/style-responsive.css" rel="stylesheet">
  
  <!-- Estilos custom -->
  <link href="dashboard/css/custom.css" rel="stylesheet">
  
  <link href="assets/css/custom.css" rel="stylesheet">

</head>

<body>
  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
  <div id="login-page">
    <div class="container">
      <#if tipo_registro == 2>
      	<form class="form-register form-login form-register-style" method="post" action="/ProyectoLaboratorioII/registro?operacion=registrar&tipo=empleado" >
      <#else>
        <form class="form-register form-login form-register-style" method="post" action="/ProyectoLaboratorioII/registro?operacion=registrar&tipo=empresa" >
       </#if>
        <h2 class="form-login-heading orange-background">Registro</h2>
        <div class="login-wrap">
        	<label style="color: #FF4F57 !important;"><b>(*) Campos obligatorios</b></label>
        	<div class="col-md-12">
	        	<div class="col-md-6">
	        	  <label>*Correo electr&oacute;nico</label>
		          <input type="text" class="form-control" placeholder="Correo electr&oacute;nico" name="correo" required>
		          <br>
		        </div>
		        <div class="col-md-6">
		          <label>*Pa&iacute;s</label>
		          <select class="form-control" name="pais" required>		          	
		          	 <option value="">Seleccione un pa&iacute;s</option>
		          	 <#list paises as pais>
		          	 	<option value="${pais.id}">${pais.nombre}</option>                 
	               	 </#list>
	              </select>
	              <br>
	            </div>
			  	<div class="col-md-6">
	        	  <label>*Contrase&ntilde;a</label>
 		          <input type="password" class="form-control" placeholder="Contrase�a" name="contrasenna" required>
		          <br>
		        </div>
		        <div class="col-md-6">
				  <label>*Confirmaci&oacute; de contrase&ntilde;a</label>
		          <input type="password" class="form-control" placeholder="Confirmar contrase�a" name="confirmar_contrasenna" required>
		          <br>
			  	</div>
			  	
			</div>
			<div class="col-md-12"><hr class="hr-registro"></div>
			<div class="col-md-12">
			  	<div class="col-md-6">
			  		<label>*Nombre</label>
		         	<input type="text" class="form-control" placeholder="Nombre" name="nombre" required>
		        	<br>
		        </div>
		        <!-- 2.Empleado / 3.Empresa-->
		        <#if tipo_registro == 2> 
	          		<div class="col-md-6">
			        	<label>*Apellido</label>
			         	<input type="text" class="form-control" placeholder="Apellido" name="apellido" required>
			        	<br>
			        </div>
			        <div class="col-md-6">
			        	<label>*C&eacute;dula</label>
			         	<input type="text" class="form-control" placeholder="C&eacute;dula" name="cedula" required>
			        	<br>
			        </div>
			        <div class="col-md-6">
			        	<label>*Fecha de nacimiento</label>
                      <input class=" form-control" id="cname" name="fecha_nacimiento" minlength="2" type="date" required />
                    <br>
                    </div>
                    <div class="col-md-6">
			        <label>*G&eacute;nero</label>
			        <select class="form-control" name="genero" required>		                  
		                 <option value="">Seleccione un g&eacute;nero</option>
		                 <option value=1>Mujer</option>
		                 <option value=2>Hombre</option>
		            </select>
		            <br>
		           	</div>
		           	
		        
		           	<#else>
				        <div class="col-md-6">
				        	<label>*Rif</label>
				         	<input type="text" class="form-control" placeholder="Rif" name="rif" required>
				        	<br>
				        </div>
				        
	                    <div class="col-md-6">
					        <label>*Sector de actividad</label>
					        <select class="form-control" name="sector_actividad" required>		                  
				                 <option value="">Seleccione sector de actividad</option>
				                 <#list sectores as sector>
				                 	<option value="${sector.id}">${sector.nombre}</option>
				                 </#list>
				            </select>
				            <br>
			           	</div>
			           	
						<div class="col-md-6">
				        	<label>Sitio web</label>
				         	<input type="text" class="form-control" placeholder="Sitio web" name="sitio_web" required>
				        	<br>
			        	</div>
              	</#if>		        
		        <div class="col-md-6">
		        	<label>*Tel&eacute;fono</label>
		         	<input type="text" class="form-control" placeholder="Nro de tel&eacute;fono" name="telefono" required>
		        	<br>
		        </div>
			  	<div class="col-md-6">
			  		<label>Instagram</label>
		        	<input type="text" class="form-control" placeholder="Instagram" name="url_instagram">
		        	<br>
		        </div>
		        <div class="col-md-6">
		        	<label>Twitter</label>
		        	<input type="text" class="form-control" placeholder="Twitter" name="url_twitter">
		        	<br>
		        </div>
		        <div class="col-md-6">
		        	<label>Facebook</label>
					<input type="text" class="form-control" placeholder="Facebook" name="url_facebook">
			  		<br>
			  	</div>
			  	<div class="col-md-6">
		        	<label>Direcci�n</label>
					<textarea class="form-control" placeholder="*Direccion" name="direccion" required></textarea>
			  		<br>
			  	</div>
			</div>
          <button class="btn btn-theme btn-block orange-background" type="submit"> Registrarme</button>
          <div class="col-md-12 justify-content-center return-label">
          	<a href="/ProyectoLaboratorioII/registro" class="">Regresar</a>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="dashboard/lib/jquery/jquery.min.js"></script>
  <script src="dashboard/lib/bootstrap/js/bootstrap.min.js"></script>
  <!--BACKSTRETCH-->
  <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
  <script type="text/javascript" src="dashboard/lib/jquery.backstretch.min.js"></script>
  <script>
    $.backstretch("assets/img/slider/img3.jpg", {
      speed: 500
      
    });
  </script>
</body>

</html>
