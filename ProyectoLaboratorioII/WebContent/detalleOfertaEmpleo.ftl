<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="author" content="Jobboard">
    
    <title>Oferta Empleo</title>    

    <!-- Favicon -->
    <link rel="shortcut icon" href="/ProyectoLaboratorioII/assets/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/ProyectoLaboratorioII/assets/css/bootstrap.min.css" type="text/css">    
    <link rel="stylesheet" href="/ProyectoLaboratorioII/assets/css/jasny-bootstrap.min.css" type="text/css">  
    <link rel="stylesheet" href="/ProyectoLaboratorioII/assets/css/bootstrap-select.min.css" type="text/css">  
    <!-- Material CSS -->
    <link rel="stylesheet" href="/ProyectoLaboratorioII/assets/css/material-kit.css" type="text/css">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="/ProyectoLaboratorioII/assets/fonts/font-awesome.min.css" type="text/css"> 
    <link rel="stylesheet" href="/ProyectoLaboratorioII/assets/fonts/themify-icons.css"> 

    <!-- Animate CSS -->
    <link rel="stylesheet" href="/ProyectoLaboratorioII/assets/extras/animate.css" type="text/css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="/ProyectoLaboratorioII/assets/extras/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="/ProyectoLaboratorioII/assets/extras/owl.theme.css" type="text/css">
    <!-- Rev Slider CSS -->
    <link rel="stylesheet" href="/ProyectoLaboratorioII/assets/extras/settings.css" type="text/css">
    <!-- Main Styles -->
    <link rel="stylesheet" href="/ProyectoLaboratorioII/assets/css/main.css" type="text/css">
    <!-- Slicknav js -->
    <link rel="stylesheet" href="/ProyectoLaboratorioII/assets/css/slicknav.css" type="text/css">
    <!-- Responsive CSS Styles -->
    <link rel="stylesheet" href="/ProyectoLaboratorioII/assets/css/responsive.css" type="text/css">

    <!-- Color CSS Styles  -->
    <link rel="stylesheet" type="text/css" href="/ProyectoLaboratorioII/assets/css/colors/red.css" media="screen" />
    
  </head>

  <body>  
      <!-- Header Section Start -->
      <div class="header">    
        <div class="logo-menu">
          <nav class="navbar navbar-default main-navigation" role="navigation" data-spy="affix" data-offset-top="50">
            <div class="container">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand logo" href="/ProyectoLaboratorioII"><img src="/ProyectoLaboratorioII/assets/img/logo.png" alt=""></a>
              </div>

              <div class="collapse navbar-collapse" id="navbar">              
                 <!-- Start Navigation List -->
                <ul class="nav navbar-nav">
                  <li>
                    <a href="/ProyectoLaboratorioII">
                    Inicio <i class="fa "></i>
                    </a>

                  </li>
                </ul>

              </div>
            </div>
            <!-- Mobile Menu Start -->
            <ul class="wpb-mobile-menu">
              <li>
                <a href="#">Home</a>                      
              </li> 
              <li class="btn-m"><a href="post-job.html"><i class="ti-pencil-alt"></i> Post A Job</a></li>
              <li class="btn-m"><a href="my-account.html"><i class="ti-lock"></i>  Log In</a></li>          
            </ul>
            <!-- Mobile Menu End -->  
          </nav>
        </div>
      </div>
      <!-- Header Section End -->  

      <!-- Page Header Start -->
      <div class="page-header" style="background: url(/ProyectoLaboratorioII/assets/img/banner1.jpg);">
        <div class="container">
          <div class="row">         
            <div class="col-md-12">
              <div class="breadcrumb-wrapper">
                <h2 class="product-title">Oferta de Empleo</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Page Header End -->        

      <!-- Main container Start -->  
      <div class="about section">
        <div class="container">
          <div class="row">          
            <div class="col-md-5 col-sm-12">
              <img src="/ProyectoLaboratorioII/assets/img/about/img1.jpg" alt="">              
            </div>
            <div class="col-md-7 col-sm-12">
              <div class="about-content">
                <h2 class="medium-title" style="margin-bottom:10px;"><b>Oferta de empleo: </b>${ofertaEmpleo.getNombre()}</h2>
                <p><span class="desc">Fecha emisi&oacute;n: </span>${ofertaEmpleo.fecha_emision}</p>
                <hr>
                <p><span class="desc">Salario: </span>${ofertaEmpleo.salario}</p>
                <hr>
                <p><span class="desc">Empresa: </span>${ofertaEmpleo.nombreEmpresa()}</p>
                <hr>
                <p><span class="desc">Especialidad: </span> ${ofertaEmpleo.nombreEspecialidad()}</p>
                <hr>
                <p><span class="desc">Jornada: </span> ${ofertaEmpleo.nombreJornada()}</p>
                <hr>
                <p><span class="desc">Pa&iacute;s: </span> ${ofertaEmpleo.nombrePais()}</p>
                <hr>
                <!-- Mensaje = 1 es que esta postulado, mensaje =2 es que no esta postulado y el 3 es cuando el usuario no esta logea  -->
                <#if mensaje=2> 
                	<a href="/ProyectoLaboratorioII/oferta-empleo?operacion=postular&id=${ofertaEmpleo.id}" class="btn btn-common">Postularme</a>
              	</#if>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Main container End -->  

      <!-- Footer Section Start -->
      <footer>
        <!-- Footer Area Start -->
        <section class="footer-Content">
          <div class="container">
            <div class="row">
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="widget">
                  <h3 class="block-title"><img src="/ProyectoLaboratorioII/assets/img/logo.png" class="img-responsive" alt="Footer Logo"></h3>
                  <div class="textwidget">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lobortis tincidunt est, et euismod purus suscipit quis. Etiam euismod ornare elementum. Sed ex est, consectetur eget facilisis sed, auctor ut purus.</p>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="widget">
                  <h3 class="block-title">Quick Links</h3>
                  <ul class="menu">
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Support</a></li>
                    <li><a href="#">License</a></li>
                    <li><a href="#">Terms & Conditions</a></li>
                    <li><a href="#">Contact</a></li>
                  </ul>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="widget">
                  <h3 class="block-title">Trending Jobs</h3>
                  <ul class="menu">
                    <li><a href="#">Android Developer</a></li>
                    <li><a href="#">Senior Teamleader</a></li>
                    <li><a href="#">iOS Developer</a></li>
                    <li><a href="#">Junior Tester</a></li>
                    <li><a href="#">Full Stack Developer</a></li>
                  </ul>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="widget">
                  <h3 class="block-title">Follow Us</h3>
                  <div class="bottom-social-icons social-icon">  
                    <a class="twitter" href="https://twitter.com/GrayGrids"><i class="ti-twitter-alt"></i></a>
                    <a class="facebook" href="https://web.facebook.com/GrayGrids"><i class="ti-facebook"></i></a>                   
                    <a class="youtube" href="https://youtube.com"><i class="ti-youtube"></i></a>
                    <a class="dribble" href="https://dribbble.com/"><i class="ti-dribbble"></i></a>
                    <a class="linkedin" href="https://www.linkedin.com/"><i class="ti-linkedin"></i></a>
                  </div>  
                  <p>Join our mailing list to stay up to date and get notices about our new releases!</p>
                  <form class="subscribe-box">
                    <input type="text" placeholder="Your email">
                    <input type="submit" class="btn-system" value="Send">
                  </form> 
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- Footer area End -->
        
        <!-- Copyright Start  -->
        <div id="copyright">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <p>All Rights reserved &copy; 2017 - Designed & Developed by <a rel="nofollow" href="http://graygrids.com">GrayGrids</a></p>   
              </div>
            </div>
          </div>
        </div>
        <!-- Copyright End -->

      </footer>
      <!-- Footer Section End -->  
      
      <!-- Go To Top Link -->
      <a href="#" class="back-to-top">
        <i class="ti-arrow-up"></i>
      </a>
        
      <div id="loading">
        <div id="loading-center">
          <div id="loading-center-absolute">
            <div class="object" id="object_one"></div>
            <div class="object" id="object_two"></div>
            <div class="object" id="object_three"></div>
            <div class="object" id="object_four"></div>
            <div class="object" id="object_five"></div>
            <div class="object" id="object_six"></div>
            <div class="object" id="object_seven"></div>
            <div class="object" id="object_eight"></div>
          </div>
        </div>
      </div>
        
    <!-- Main JS  -->
    <script type="text/javascript" src="/ProyectoLaboratorioII/assets/js/jquery-min.js"></script>      
    <script type="text/javascript" src="/ProyectoLaboratorioII/assets/js/bootstrap.min.js"></script>    
    <script type="text/javascript" src="/ProyectoLaboratorioII/assets/js/material.min.js"></script>
    <script type="text/javascript" src="/ProyectoLaboratorioII/assets/js/material-kit.js"></script>
    <script type="text/javascript" src="/ProyectoLaboratorioII/assets/js/jquery.parallax.js"></script>
    <script type="text/javascript" src="/ProyectoLaboratorioII/assets/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="/ProyectoLaboratorioII/assets/js/jquery.slicknav.js"></script>
    <script type="text/javascript" src="/ProyectoLaboratorioII/assets/js/main.js"></script>
    <script type="text/javascript" src="/ProyectoLaboratorioII/assets/js/jquery.counterup.min.js"></script>
    <script type="text/javascript" src="/ProyectoLaboratorioII/assets/js/waypoints.min.js"></script>
    <script type="text/javascript" src="/ProyectoLaboratorioII/assets/js/jasny-bootstrap.min.js"></script>
    <script type="text/javascript" src="/ProyectoLaboratorioII/assets/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="/ProyectoLaboratorioII/assets/js/form-validator.min.js"></script>
    <script type="text/javascript" src="/ProyectoLaboratorioII/assets/js/contact-form-script.js"></script>    
    <script type="text/javascript" src="/ProyectoLaboratorioII/assets/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="/ProyectoLaboratorioII/assets/js/jquery.themepunch.tools.min.js"></script>
      
  </body>
</html>