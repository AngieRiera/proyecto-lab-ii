<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Inicio de sesi&oacute;n</title>

  <!-- Favicons -->
  <link href="dashboard/img/favicon.png" rel="icon">
  <link href="dashboard/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="dashboard/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="dashboard/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link href="dashboard/css/style.css" rel="stylesheet">
  <link href="dashboard/css/style-responsive.css" rel="stylesheet">
  
  <!-- Estilos custom -->
  <link href="dashboard/css/custom.css" rel="stylesheet">
  <link href="assets/css/custom.css" rel="stylesheet">

</head>

<body>
  <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
  <div id="login-page">
    <div class="container container-tipo-registro">
    
      <form class="form-login form-tipo-registro" action="login?operacion=login" method="post">
        <h2 class="form-login-heading orange-background">Registrarse como...</h2>
        <div class="login-wrap">
          <div class="showback">
              <a href="/ProyectoLaboratorioII/registro?tipo=empresa" type="button" class="btn btn-primary btn-lg btn-block">Empresa</a>
              <a  href="/ProyectoLaboratorioII/registro?tipo=empleado" type="button" class="btn btn-default btn-lg btn-block">Empleado</a>
            </div>
            <div class="col-md-12 justify-content-center" style="padding: 13px 0px 20px 0px;">
          		<a href="/ProyectoLaboratorioII/login" class="">Regresar</a>
          	</div>
        </div>
      </form>
    </div>
  </div>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="dashboard/lib/jquery/jquery.min.js"></script>
  <script src="dashboard/lib/bootstrap/js/bootstrap.min.js"></script>
  <!--BACKSTRETCH-->
  <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
  <script type="text/javascript" src="dashboard/lib/jquery.backstretch.min.js"></script>
  <script>
    $.backstretch("assets/img/slider/slide4.jpg", {
      speed: 500
      
    });
  </script>
</body>

</html>
