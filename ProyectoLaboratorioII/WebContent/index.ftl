<!DOCTYPE html>
	<html lang="en">
	  <head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">    
	    <meta http-equiv="content-type" content="text/html; charset=utf-8">
	    <meta name="author" content="Jobboard">
	    
	    <title>JobBoard - Encuentra trabajos aquí­</title>    
	
	    <!-- Favicon -->
	    <link rel="shortcut icon" href="assets/img/favicon.png">
	    <!-- Bootstrap CSS -->
	    <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">    
	    <link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">  
	    <link rel="stylesheet" href="assets/css/bootstrap-select.min.css" type="text/css">  
	    <!-- Material CSS -->
	    <link rel="stylesheet" href="assets/css/material-kit.css" type="text/css">
	    <!-- Font Awesome CSS -->
	    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css" type="text/css"> 
	    <link rel="stylesheet" href="assets/fonts/themify-icons.css"> 
	
	    <!-- Animate CSS -->
	    <link rel="stylesheet" href="assets/extras/animate.css" type="text/css">
	    <!-- Owl Carousel -->
	    <link rel="stylesheet" href="assets/extras/owl.carousel.css" type="text/css">
	    <link rel="stylesheet" href="assets/extras/owl.theme.css" type="text/css">
	    <!-- Rev Slider CSS -->
	    <link rel="stylesheet" href="assets/extras/settings.css" type="text/css"> 
	    <!-- Slicknav js -->
	    <link rel="stylesheet" href="assets/css/slicknav.css" type="text/css">
	    <!-- Main Styles -->
	    <link rel="stylesheet" href="assets/css/main.css" type="text/css">
	    <!-- Responsive CSS Styles -->
	    <link rel="stylesheet" href="assets/css/responsive.css" type="text/css">
	
	    <!-- Color CSS Styles  -->
	    <link rel="stylesheet" type="text/css" href="assets/css/colors/red.css" media="screen" />

		<!-- Custom CSS Styles  -->
	    <link rel="stylesheet" href="assets/css/custom.css" type="text/css">

	    
	  </head>
	
	  <body>  
	      <!-- Header Section Start -->
	      <div class="header">    
	        <!-- Start intro section -->
	        <section id="intro" class="section-intro">
	          <div class="logo-menu">
	            <nav class="navbar navbar-default" role="navigation" data-spy="affix" data-offset-top="50">
	              <div class="container">
	                <!-- Brand and toggle get grouped for better mobile display -->
	                <div class="navbar-header">
	                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" class="btn">
	                    <span class="sr-only">Toggle navigation</span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                  </button>
	                  <a class="navbar-brand logo" href="index.html"><img src="assets/img/logo.png" alt=""></a>
	                </div>
	
	                <div class="collapse navbar-collapse" id="navbar">              
	                <!-- Start Navigation List -->
	                <ul class="nav navbar-nav">
	                  <li>
	                    <a href="#">
	                    Sobre nosotros
	                    </a>
	                  </li>

	                  <li>
	                    <a href="#">
	                    Contacto 
	                    </a>
	                    
	                  </li>
	                  
	                </ul>
	                <ul class="nav navbar-nav navbar-right float-right">
	                <#if !empresa?? && !empleado??>
	                  <li class="left"><a href="#"><i class="ti-pencil-alt"></i> Registrarme</a></li>
	                  <li class="right"><a href="/ProyectoLaboratorioII/login"><i class="ti-lock"></i>  Iniciar sesi&oacute;n</a></li>
	                <#else>
	                	<#if empresa?? && !empleado??>
	                	  <li class="left"><a href="/ProyectoLaboratorioII/empresa"><i class="ti-pencil-alt"></i> DashBoard Empresa</a></li>
	                  	  <li class="right"><a href="/ProyectoLaboratorioII/login?operacion=cerrarSesion"><i class="ti-lock"></i>  Cerrar sesi&oacute;n</a></li>
	                    <#else>
	                      <li class="left"><a href="/ProyectoLaboratorioII/empleado/curriculum?operacion=consultar"><i class="ti-pencil-alt"></i> DashBoard Empleado</a></li>
	                  	  <li class="right"><a href="/ProyectoLaboratorioII/login?operacion=cerrarSesion"><i class="ti-lock"></i>  Cerrar sesi&oacute;n</a></li>
	                    </#if>
	                </#if>
	                </ul>
	              </div>                           
	            </div>
	            <!-- Mobile Menu Start -->
	            <ul class="wpb-mobile-menu">
	              <li>
	                <a class="active" href="index.html">Home</a>
	                <ul>
	                  <li><a class="active" href="index.html">Home 1</a></li>
	                  <li><a href="index-02.html">Home 2</a></li>
	                  <li><a href="index-03.html">Home 3</a></li>
	                  <li><a href="index-04.html">Home 4</a></li>
	                </ul>                       
	              </li>
	              <li>
	                <a href="about.html">Pages</a>
	                <ul>
	                  <li><a href="about.html">About</a></li>
	                  <li><a href="job-page.html">Job Page</a></li>
	                  <li><a href="job-details.html">Job Details</a></li>
	                  <li><a href="resume.html">Resume Page</a></li>
	                  <li><a href="privacy-policy.html">Privacy Policy</a></li>
	                  <li><a href="faq.html">FAQ</a></li>
	                  <li><a href="pricing.html">Pricing Tables</a></li>
	                  <li><a href="contact.html">Contact</a></li>
	                </ul>
	              </li>
	              <li>
	                <a href="#">For Candidates</a>
	                <ul>
	                  <li><a href="browse-jobs.html">Browse Jobs</a></li>
	                  <li><a href="browse-categories.html">Browse Categories</a></li>
	                  <li><a href="add-resume.html">Add Resume</a></li>
	                  <li><a href="manage-resumes.html">Manage Resumes</a></li>
	                  <li><a href="job-alerts.html">Job Alerts</a></li>
	                </ul>
	              </li>
	              <li>
	                <a href="#">For Employers</a>
	                <ul>
	                  <li><a href="post-job.html">Add Job</a></li>
	                  <li><a href="manage-jobs.html">Manage Jobs</a></li>
	                  <li><a href="manage-applications.html">Manage Applications</a></li>
	                  <li><a href="browse-resumes.html">Browse Resumes</a></li>
	                </ul>
	              </li> 
	                
	              <li class="btn-m"><a href="post-job.html"><i class="ti-pencil-alt"></i> Registrarme</a></li>
	              <li class="btn-m"><a href="my-account.html"><i class="ti-lock"></i>  Iniciar sesión</a></li>          
	            </ul>
	            <!-- Mobile Menu End --> 
	          </nav>	 
	      
	      <div class="search-container">
	        <div class="container">
	          <div class="row">
	            <div class="col-md-12">
	              <h1>Busca un trabajo acorde a ti</h1><br><h2>Mas de <strong>1.000</strong> trabajos se encuentran disponibles</h2>
	              <div class="content">
	                <form method="post" action="/ProyectoLaboratorioII/index?operacion=filtro">
	                  <div class="row" style="position: relative;">

	            		<div class="col-md-11 col-sm-11">

			            	<div class="row">
			                    <div class="col-md-4 col-sm-6">
			                      <div class="form-group">
			                        <input class="form-control" name="by_name" type="text" placeholder="Busqueda por trabajo">
			                        
			                      </div>
			                    </div>
			                    <div class="col-md-2 col-sm-6">
			                      <div class="search-category-container">
			                        <label >
			                          <select name="by_salario" class="dropdown-product selectpicker">
			                            <option value="0">Salario</option>
			                            <option value="0-100">0-100</option>
			                            <option value="101-300">101-300</option>
			                            <option value="301-500">301-500</option>
			                            <option value="501-1000">501-1000</option>
			                            <option value="gt1000">1000 o mas</option>
			                          </select>
			                        </label>
			                      </div>
			                    </div>
			                    <div class="col-md-2 col-sm-6">
			                      <div class="search-category-container">
			                        <label >
			                          <select name="jornada" class="dropdown-product selectpicker">
			                            <option id="" value="0">Jornada</option>
			                            	<#list Jornadas as jornada>
				                            	<option id=${jornada.id} value=${jornada.id}>${jornada.nombre}</option>
				                            </#list>
			                          </select> 
			                        </label>
			                      </div>
			                    </div>
			                    <div class="col-md-4 col-sm-6">
			                      <div class="search-category-container">
			                        <label >
			                          <select name="especialidad" class="dropdown-product selectpicker">
			                            <option id="" value="0" >Areas de Desempe&ntildeo</option>
			                            	<#list Sectores as sector>
				                            	<option id=${sector.id} value=${sector.id}>${sector.nombre}</option>
				                            </#list>
			                          </select>
			                        </label>
			                      </div>
			                    </div>
		                	</div>
		            		<!--fin row-->

							<div class="row location-row">
								<div class="col-md-4 col-sm-6">
			                      <div class="search-category-container">			                        
			                          <select name="pais" class="dropdown-product selectpicker">
			                            <option id="" value="0">Pais</option>
				                            <#list Paises as pais>
				                            	<option id=${pais.id} value=${pais.id}>${pais.nombre}</option>
				                            </#list>
			                          </select>
			                        </label>
			                      </div>
			                    </div>

			                    <div class="col-md-4 col-sm-6">
			                      <div class="search-category-container">			                        
			                          <select name="estado" class="dropdown-product selectpicker">
			                            <option id="" value="0">Estado</option>
			                            	<#list Estados as estado>
				                            	<option id=${estado.id} value=${estado.id}>${estado.nombre}</option>
				                            </#list>
			                          </select>
			                        </label>
			                      </div>
			                    </div>

			                    <div class="col-md-4 col-sm-6">
			                      <div class="search-category-container">			                        
			                          <select name="municipio" class="dropdown-product selectpicker">
			                            <option id="" value="0" >Municipio</option>
			                            	<#list Municipios as municipio>
				                            	<option id=${municipio.id} value=${municipio.id}>${municipio.nombre}</option>
				                            </#list>
			                          </select>
			                        </label>
			                      </div>
			                    </div>
			            	</div>
			                <!--fin row-->
			            </div>
			            <!--fin col11-->
				<div class="col-md-1 col-sm-6 index-search">
                  <button type="submit" class="btn btn-search-icon"><i class="ti-search"></i></button>
                </div>
	        </div>

			</div>
	      <!-- Header Section End -->	
        </form>
      <!--    	</div>
	              <div class="popular-jobs">
	                <b>Palabras claves populares: </b>
	                <a href="#">Desarrollador</a>
	                <a href="#">Asistente</a>
	                <a href="#">DiseÃ±o grÃ¡fico</a>
	              </div>
	            </div>
	            -->
	          </div>
	        </div>
	      </div>
	    </section>
	    <!-- end intro section -->
	    </div>
    
    
    
    
    <!-- Find Job Section Start -->
    <section class="find-job section">
      <div class="container">
        <h2 class="section-title">Ofertas de empleos disponibles</h2>
        <div class="row">
          <div class="col-md-12">
          <#if Ofertas?size gt 0>
          	<#list Ofertas as oferta>
          		<div class="job-list">
	              <div class="thumb">
	                <a href="job-details.html"><img src="assets/img/jobs/img-1.jpg" alt=""></a>
	              </div>
	              <div class="job-list-content">
	                <h4><a href="job-details.html">Se necesita personal en ${oferta.nombre}</a><span class="full-time">${oferta.jornada.getNombre()}</span></h4>
	                <p margin-right: 471px;>La empresa ${oferta.getEmpresa().getUsuario().getUserProfile().getNombre()} est&aacute solicitando personal para el &aacuterea de ${oferta.especialidad.getNombre()}                                                                                                               </p>
	                <p style="margin-right: 471px;">Puedes postularte enviando tu curriculum o si ya lo tienes, presiona postularse. </p>
	                <div class="job-tag">
	                  <div class="pull-left">
	                    <div class="meta-tag">
	                      <span><a href="browse-categories.html"><i class="ti-brush"></i>${oferta.especialidad.nombre}</a></span>
	                      <span><i class="ti-location-pin"></i>${oferta.pais.nombre}</span>
	                      <span><i class="ti-time"></i>${oferta.jornada.getNombre()}</span>
	                  	  <span><i class="ti-money"></i> Salario: <strong> ${oferta.getSalario()} $ </strong> </span>
	                    </div>
	                  </div>
	                  <div class="pull-right">
	                    <!--
	                    <div class="icon">
	                      <i class="ti-heart"></i>
	                    </div>
	                    -->
	                    <a class="btn btn-common btn-rm" href="/ProyectoLaboratorioII/oferta-empleo?id=${oferta.id}">Ver Detalle</a>
	                  </div>
	                </div>
	              </div>
	            </div>
	          </#list>
          <#else>
          	<p>No hay ofertas de empleos disponibles </p>
          </#if>
          
            
           <!-- 
            <div class="job-list">
              <div class="thumb">
                <a href="job-details.html"><img src="assets/img/jobs/img-2.jpg" alt=""></a>
              </div>
              <div class="job-list-content">
                <h4><a href="job-details.html">Front-end developer needed</a><span class="full-time">Full-Time</span></h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum quaerat aut veniam molestiae atque dolorum omnis temporibus consequuntur saepe. Nemo atque consectetur saepe corporis odit in dicta reprehenderit, officiis, praesentium?</p>
                <div class="job-tag">
                  <div class="pull-left">
                    <div class="meta-tag">
                      <span><a href="browse-categories.html"><i class="ti-desktop"></i>Technologies</a></span>
                      <span><i class="ti-location-pin"></i>Cupertino, CA, USA</span>
                      <span><i class="ti-time"></i>60/Hour</span>
                    </div>
                  </div>
                  <div class="pull-right">
                    <div class="icon">
                      <i class="ti-heart"></i>
                    </div>
                    <a href="job-details.html" class="btn btn-common btn-rm">More Detail</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="job-list">
              <div class="thumb">
                <a href="job-details.html"><img src="assets/img/jobs/img-3.jpg" alt=""></a>
              </div>
              <div class="job-list-content">
                <h4><a href="job-details.html">Senior Accountant</a><span class="part-time">Part-Time</span></h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum quaerat aut veniam molestiae atque dolorum omnis temporibus consequuntur saepe. Nemo atque consectetur saepe corporis odit in dicta reprehenderit, officiis, praesentium?</p>
                <div class="job-tag">
                  <div class="pull-left">
                    <div class="meta-tag">
                      <span><a href="browse-categories.html"><i class="ti-home"></i>Finance</a></span>
                      <span><i class="ti-location-pin"></i>Delaware, USA</span>
                      <span><i class="ti-time"></i>60/Hour</span>
                    </div>
                  </div>
                  <div class="pull-right">
                    <div class="icon">
                      <i class="ti-heart"></i>
                    </div>
                    <a href="job-details.html" class="btn btn-common btn-rm">More Detail</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="job-list">
              <div class="thumb">
                <a href="job-details.html"><img src="assets/img/jobs/img-4.jpg" alt=""></a>
              </div>
              <div class="job-list-content">
                <h4><a href="job-details.html">Fullstack web developer needed</a><span class="full-time">Full-Time</span></h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum quaerat aut veniam molestiae atque dolorum omnis temporibus consequuntur saepe. Nemo atque consectetur saepe corporis odit in dicta reprehenderit, officiis, praesentium?</p>
                <div class="job-tag">
                  <div class="pull-left">
                    <div class="meta-tag">
                      <span><a href="browse-categories.html"><i class="ti-desktop"></i>Technologies</a></span>
                      <span><i class="ti-location-pin"></i>New York, USA</span>
                      <span><i class="ti-time"></i>60/Hour</span>
                    </div>
                  </div>
                  <div class="pull-right">
                    <div class="icon">
                      <i class="ti-heart"></i>
                    </div>
                    <a href="job-details.html" class="btn btn-common btn-rm">More Detail</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          -->
          <!-- div class="col-md-12">
            <div class="showing pull-left">
              <a href="#">Showing <span>6-10</span> Of 24 Jobs</a>
            </div>                    
            <ul class="pagination pull-right">              
              <li class="active"><a href="#" class="btn btn-common" ><i class="ti-angle-left"></i> prev</a></li>
              <li><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li class="active"><a href="#" class="btn btn-common">Next <i class="ti-angle-right"></i></a></li>
            </ul>
          </div-->
        </div>
      </div>
    </section>
    <!-- Find Job Section End -->  

    <!-- Start Purchase Section -->
    <section class="section purchase" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row">
          <div class="section-content text-center">
            <#if empleado?? && !empresa??>	
            <h1 class="title-text">
             Te ofrecemos m&aacutes que un empleo, un estilo de vida.
            </h1>
            	<p>Postulate Ya!</p>
            	<p style="font-size: large;">Comienza por buscar tu empleo ideal.</p>
            	    <a href="#" >
      					<i class="ti-arrow-up" style="background-color: cornsilk;border-radius: 19px;font-size: xx-large;"></i>
    				</a>
            <#else>
            	<#if !empleado?? && empresa??>
            	  <h1 class="title-text">
             		Te ofrecemos m&aacutes que un portal de publicacion de empleo, un estilo de vida.
            	  </h1>
            		<p>Publica tus ofertas y obten empleados!</p>
            		 <a href="#" >
      					<i class="ti-cloud-up" style="color: white; font-size: xx-large;" title="Registra la oferta de empleo!"></i>
    				</a>
            	<#else>
            	  <h1 class="title-text">
             		Te ofrecemos m&aacutes que un empleo, un estilo de vida.
            	  </h1>
            		<p>&iquestAun no has iniciado sesi&oacuten?</p>
            		<a href="my-account.html" class="btn btn-common">Hazlo Ahora</a>
            	</#if>
            </#if>
          </div>
        </div>
      </div>
    </section>
    <!-- End Purchase Section -->

<#if empresa?? && !empleado??>
<section class="featured-jobs section">
	<div class="">
		<div class="container">
      		<div class="row">
            <form id="frmSearchEmpresa" method="post" action="/ProyectoLaboratorioII/index?operacion=filtro_empleado">
	            <div class="col-md-12">
	              <h3>&iquestQue empleado(s) buscas? </h3>
	              <div class="content">
	                  <div class="row" style="position: relative;">
	            		<div class="col-md-11 col-sm-11">
			            	<div class="row">
			                    <div class="col-md-4 col-sm-6">
				                      <div class="form-group">
				                        <input class="form-control" name="by_name_empleado" type="text" placeholder="Busqueda por Nombre de Empleado">
					                  </div>
			                    </div>
								<div class="row location-row" style="background-color: antiquewhite;">
								<div class="col-md-4 col-sm-6" >
			                      <div class="search-category-container" style="margin-top: -10px;" >			                        
			                          <select name="by_ubicacion_empleado" class="dropdown-product selectpicker">
			                            <option value="0" selected >Pais</option>
				                            <#list Paises as pais>
				                            	<option id=${pais.id} value=${pais.id}>${pais.nombre}</option>
				                            </#list>
			                          </select>
			                        </label>
			                      </div>
			                    </div>	                    
			                    <div class="col-md-1 col-sm-6 index-search">
			                  		<button type="submit" class="btn btn-search-icon" style="margin-top: -54px;"><i class="ti-search"></i></button>
			                	  </div>
			            	</div>
			           	</div>	
			          </div>
	         	 </div>
         	 	</div> 	
	        </form>
      		</div>
      	</div>
      </div>		
</section>

    <!-- Featured Jobs Section Start -->
    <section class="featured-jobs section" style="padding-top: 0px;">
      <div class="container">
        <h2 class="section-title">
          Empleados Asociados
        </h2>
        <div class="row">
        <#if Empleados?size gt 0>
	        <#list Empleados as empleado>
	          <div class="col-md-4 col-sm-6 col-xs-12">
	            <div class="featured-item">
	              <div class="featured-wrap">
	                <div class="featured-inner">
	                  <figure class="item-thumb">
	                    <a class="hover-effect" href="../ProyectoLaboratorioII/detalle-empleado?id=${empleado.id}">
	                      <img src="assets/img/features/img-1.jpg" alt="">
	                    </a>
	                  </figure>
	                  <div class="item-body">
	                    <h3 class="job-title"><a href="../ProyectoLaboratorioII/detalle-empleado?id=${empleado.id}" style="text-transform: none;">${empleado.usuario.getUserProfile().getNombre()}</a></h3>
	                    <div class="adderess"><i class="ti-location-pin"></i> ${empleado.usuario.nombrePais()}</div>
	                    <div class="adderess"><i class="ti-email"></i> Correo de contacto: ${empleado.usuario.correo}</div>
	                    <div class="adderess"><i class="ti-facebook"></i> Facebook: ${empleado.usuario.getUserProfile().obtenerFacebook()}</div>
	                  </div>
	                </div>
	              </div>
	              <div class="item-foot">
	                <span><i class="ti-hand-open"></i> Telefono: ${empleado.usuario.getUserProfile().getTelefono()}</span>
	                <div class="view-iocn">
	                  <a href="/ProyectoLaboratorioII/detalle-empleado?id=${empleado.id}"><i class="ti-arrow-right" ></i></a>
	                </div>
	              </div>
	            </div>
	          </div>
	        </#list>
          <#else>
          	<p>No hay Empleados disponibles </p>
          </#if>
        </div>
      </div>
    </section>
    <!-- Featured Jobs Section End -->  
<#else>
<section class="featured-jobs section">
	<div class="">
		<div class="container">
      		<div class="row">
            <form id="frmSearchEmpresa" method="post" action="/ProyectoLaboratorioII/index?operacion=filtro_empresa">
	            <div class="col-md-12">
	              <h3>&iquestQue empresa(s) buscas? </h3>
	              <div class="content">
	                  <div class="row" style="position: relative;">
	            		<div class="col-md-11 col-sm-11">
			            	<div class="row">
			                    <div class="col-md-4 col-sm-6">
				                      <div class="form-group">
				                        <input class="form-control" name="by_name_empresa" type="text" placeholder="Busqueda por Nombre de Empresa">
					                  </div>
			                    </div>
			                    <div class="col-md-1 col-sm-6 index-search">
			                  		<button type="submit" class="btn btn-search-icon" style="margin-top: -30px;"><i class="ti-search"></i></button>
			                	  </div>
			            	</div>
			           	</div>	
			          </div>
	         	 </div>
         	 	</div> 	
	        </form>
      		</div>
      	</div>
      </div>		
</section>

    <!-- Featured Jobs Section Start -->
    <section class="featured-jobs section" style="padding-top: 0px;">
      <div class="container">
        <h2 class="section-title">
          Empresas Asociadas
        </h2>
        <div class="row">
        <#if Empresas?size gt 0>
	        <#list Empresas as empresa>
	          <div class="col-md-4 col-sm-6 col-xs-12">
	            <div class="featured-item">
	              <div class="featured-wrap">
	                <div class="featured-inner">
	                  <figure class="item-thumb">
	                    <a class="hover-effect" href="../ProyectoLaboratorioII/detalle-empresa?id=${empresa.id}">
	                      <img src="assets/img/features/img-1.jpg" alt="">
	                    </a>
	                  </figure>
	                  <div class="item-body">
	                    <h3 class="job-title"><a href="../ProyectoLaboratorioII/detalle-empresa?id=${empresa.id}" style="text-transform: none;">${empresa.usuario.getUserProfile().getNombre()}</a></h3>
	                    <div class="adderess"><i class="ti-location-pin"></i> ${empresa.direccion}</div>
	                    <div class="adderess"><i class="ti-email"></i> Correo de contacto: ${empresa.usuario.correo}</div>
	                    <div class="adderess"><i class="ti-world"></i> Sitio Web: ${empresa.sitio_web}</div>
	                  </div>
	                </div>
	              </div>
	              <div class="item-foot">
	                <span><i class="ti-hand-open"></i> Actividad: ${empresa.sector_actividad.nombre}</span>
	                <div class="view-iocn">
	                  <a href="../ProyectoLaboratorioII/detalle-empresa?id=${empresa.id}"><i class="ti-arrow-right" ></i></a>
	                </div>
	              </div>
	            </div>
	          </div>
	        </#list>
          <#else>
          	<p>No hay Empleados disponibles </p>
          </#if>
        </div>
      </div>
    </section>
    <!-- Featured Jobs Section End -->     
</#if>
    <!-- Testimonial Section Start >
  <!-- 
    <section id="testimonial" class="section">
      <div class="container">
        <div class="row">
          <div class="touch-slider" class="owl-carousel owl-theme">
            <div class="item active text-center">  
              <img class="img-member" src="assets/img/testimonial/img1.jpg" alt=""> 
              <div class="client-info">
               <h2 class="client-name">Jessica <span>(Senior Accountant)</span></h2>
              </div>
              <p><i class="fa fa-quote-left quote-left"></i> The team that was assigned to our project... were extremely professional <i class="fa fa-quote-right quote-right"></i><br> throughout the project and assured that the owner expectations were met and <br> often exceeded. </p>
            </div>
            <div class="item text-center">
              <img class="img-member" src="assets/img/testimonial/img2.jpg" alt=""> 
              <div class="client-info">
               <h2 class="client-name">John Doe <span>(Project Menager)</span></h2>
              </div>
              <p><i class="fa fa-quote-left quote-left"></i> The team that was assigned to our project... were extremely professional <i class="fa fa-quote-right quote-right"></i><br> throughout the project and assured that the owner expectations were met and <br> often exceeded. </p>
            </div>
            <div class="item text-center">
              <img class="img-member" src="assets/img/testimonial/img3.jpg" alt=""> 
              <div class="client-info">
                <h2 class="client-name">Helen <span>(Engineer)</span></h2>
              </div>
              <p><i class="fa fa-quote-left quote-left"></i> The team that was assigned to our project... were extremely professional <i class="fa fa-quote-right quote-right"></i><br> throughout the project and assured that the owner expectations were met and <br> often exceeded. </p>
            </div>
          </div>
        </div>
      </div>
    </section>
-->
    <!-- Testimonial Section End -->


     <!-- Counter Section Start -->
<!--
    <section id="counter">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="counting">
              <div class="icon">
                <i class="ti-briefcase"></i>
              </div>
              <div class="desc">                
                <h2>Jobs</h2>
                <h1 class="counter">12050</h1>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="counting">
              <div class="icon">
                <i class="ti-user"></i>
              </div>
              <div class="desc">
                <h2>Members</h2>
                <h1 class="counter">10890</h1>                
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="counting">
              <div class="icon">
                <i class="ti-write"></i>
              </div>
              <div class="desc">
                <h2>Resume</h2>
                <h1 class="counter">700</h1>                
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="counting">
              <div class="icon">
                <i class="ti-heart"></i>
              </div>
              <div class="desc">
                <h2>Company</h2>
                <h1 class="counter">9050</h1>                
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  
  -->
    <!-- Counter Section End -->
  
  
  
  <!-- Footer Section Start -->
    <footer>
      <!-- Footer Area Start -->
      <section class="footer-Content">
        <div class="container">
          <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="widget">
                <h3 class="block-title"><img src="assets/img/logo.png" class="img-responsive" alt="Footer Logo"></h3>
                <div class="textwidget">
                  <p>En este sitio web podr&aacutes encontrar las mejores ofertas de trabajo adaptadas a tus necesidades.</p>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              
              <!-- InstaWidget -->
				<a href="https://instawidget.net/v/user/pruebalab" id="link-e631eb411a09dd45d24250326470283c45be1a4970b861553fd4ed2b14d81b8e">@pruebalab</a>
				<script src="https://instawidget.net/js/instawidget.js?u=e631eb411a09dd45d24250326470283c45be1a4970b861553fd4ed2b14d81b8e&width=260px"></script>
              
              
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="widget">
                <h3 class="block-title">Trabajos populares</h3>
                <#if OfertasFooter?size gt 0>
                	<#list OfertasFooter as oferta>
                  	 <ul class="menu">
                 	 	<li><a href="/ProyectoLaboratorioII/oferta-empleo?id=${oferta.id}">${oferta.nombre}</a></li>
                	 </ul>
                	</#list>
                <#else> 
                	<p> No hay trabajos populares hasta el momento </p>
                </#if>
              </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="widget">
                <h3 class="block-title">S&iacuteguenos</h3>
                <div class="bottom-social-icons social-icon">  
                  <a class="twitter" href="https://twitter.com"><i class="ti-twitter-alt"></i></a>
                  <a class="facebook" href="https://web.facebook.com"><i class="ti-facebook"></i></a>                   
                  <a class="youtube" href="https://youtube.com"><i class="ti-youtube"></i></a>
                </div>  
                <p>Reg&iacutestrate en nuestra lista de correo y recibe ofertas de trabajo</p>
                <form class="subscribe-box">
                  <input type="text" placeholder="Tu correo">
                  <input type="submit" class="btn-system" value="Enviar">
                </form> 
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Footer area End -->
      
      <!-- Copyright Start  -->
      <div id="copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="site-info text-center">
                <p>Todos los derechos reservados &copy; 2018 - Dise&ntildeado & Desarrollado por <a rel="nofollow" >Team Lab</a></p>
              </div>   
            </div>
          </div>
        </div>
      </div>
      <!-- Copyright End -->

    </footer>
    <!-- Footer Section End -->  
    
    <!-- Go To Top Link -->
    <a href="#" class="back-to-top">
      <i class="ti-arrow-up"></i>
    </a>

    <div id="loading">
      <div id="loading-center">
        <div id="loading-center-absolute">
          <div class="object" id="object_one"></div>
          <div class="object" id="object_two"></div>
          <div class="object" id="object_three"></div>
          <div class="object" id="object_four"></div>
          <div class="object" id="object_five"></div>
          <div class="object" id="object_six"></div>
          <div class="object" id="object_seven"></div>
          <div class="object" id="object_eight"></div>
        </div>
      </div>
    </div>
        
    <!-- Main JS  -->
    <script type="text/javascript" src="assets/js/jquery-min.js"></script>      
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>    
    <script type="text/javascript" src="assets/js/material.min.js"></script>
    <script type="text/javascript" src="assets/js/material-kit.js"></script>
    <script type="text/javascript" src="assets/js/jquery.parallax.js"></script>
    <script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.slicknav.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
    <script type="text/javascript" src="assets/js/jquery.counterup.min.js"></script>
    <script type="text/javascript" src="assets/js/waypoints.min.js"></script>
    <script type="text/javascript" src="assets/js/jasny-bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="assets/js/form-validator.min.js"></script>
    <script type="text/javascript" src="assets/js/contact-form-script.js"></script>    
    <script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
    
  </body>
</html>

