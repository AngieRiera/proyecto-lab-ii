<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Dashio - Bootstrap Admin Template</title>

  <#include "../utils/menu.ftl">
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Listado de contrataciones por la empresa</h3>
        <a type="button" class="btn btn-success btn-aregar-elemento-curriculum" href="/ProyectoLaboratorioII/empresa/oferta-empleo?operacion=registrar" style="margin-top:10px; margin-left:0px; margin-bottom:10px">Nuevo registro</a>
        <div class="row mb">
          <!-- page start-->
          <div class="content-panel">
            <div class="adv-table">
              <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="hidden-table-info">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Salario</th>
                    <th>Estado de registro</th>
                    <th>Nombre del empleado</th>
                    <th class="hidden-phone table-field-options">Opciones</th>
                  </tr>
                </thead>
                <tbody>
                <#list postulaciones as postulacion>
                  <tr class="gradeA">
                    <td>${postulacion.id}</td>
                    <td>${postulacion.oferta_empleo.nombre}</td>
                    <td>${postulacion.oferta_empleo.salario}</td>
                    <#if postulacion.status=2>                   
                    <td class="hidden-phone">Aceptada</td>
                    <#else>
                    <td class="hidden-phone">Vacio</td>
					</#if>
					<td>${postulacion.empleado.getUsuario().getUserProfile().getNombre()}</td>
					<td>
						<a type="button" class="btn btn-round btn-info" href="/ProyectoLaboratorioII/detalle-empleado?id=${postulacion.empleado.getId()}">Ver Curriculum</a>
					</td>
                  </tr>
                </#list>
               </tbody>
              </table>
            </div>
          </div>
          <!-- page end-->
        </div>
        <!-- /row -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <!--footer class="site-footer custom-site-footer position-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Dashio</strong>. All Rights Reserved
        </p>
        <div class="credits">

          Created with Dashio template by <a href="https://templatemag.com/">TemplateMag</a>
        </div>
        <a href="index.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer  -->
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="/ProyectoLaboratorioII/dashboard/lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="/ProyectoLaboratorioII/dashboard/lib/advanced-datatable/js/jquery.js"></script>
  <script src="/ProyectoLaboratorioII/dashboard/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="/ProyectoLaboratorioII/dashboard/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="/ProyectoLaboratorioII/dashboard/lib/jquery.scrollTo.min.js"></script>
  <script src="/ProyectoLaboratorioII/dashboard/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="/ProyectoLaboratorioII/dashboard/lib/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="/ProyectoLaboratorioII/dashboard/lib/advanced-datatable/js/DT_bootstrap.js"></script>
  <!--common script for all pages-->
  <script src="/ProyectoLaboratorioII/dashboard/lib/common-scripts.js"></script>
  <!--script for this page-->
  <script type="text/javascript">
    /* Formating function for row details */
    function fnFormatDetails(oTable, nTr) {
      var aData = oTable.fnGetData(nTr);
      var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
      sOut += '<tr><td>Rendering engine:</td><td>' + aData[1] + ' ' + aData[4] + '</td></tr>';
      sOut += '<tr><td>Link to source:</td><td>Could provide a link here</td></tr>';
      sOut += '<tr><td>Extra info:</td><td>And any further details here (images etc)</td></tr>';
      sOut += '</table>';

      return sOut;
    }

    $(document).ready(function() {
      /*
       * Insert a 'details' column to the table
       */
      var nCloneTh = document.createElement('th');
      var nCloneTd = document.createElement('td');
      nCloneTd.innerHTML = '<img src="lib/advanced-datatable/images/details_open.png">';
      nCloneTd.className = "center";

      $('#hidden-table-info thead tr').each(function() {
        this.insertBefore(nCloneTh, this.childNodes[0]);
      });

      $('#hidden-table-info tbody tr').each(function() {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
      });

      /*
       * Initialse DataTables, with no sorting on the 'details' column
       */
      var oTable = $('#hidden-table-info').dataTable({
        "aoColumnDefs": [{
          "bSortable": false,
          "aTargets": [0]
        }],
        "aaSorting": [
          [1, 'asc']
        ]
      });

      /* Add event listener for opening and closing details
       * Note that the indicator for showing which row is open is not controlled by DataTables,
       * rather it is done here
       */
      $('#hidden-table-info tbody td img').live('click', function() {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
          /* This row is already open - close it */
          this.src = "lib/advanced-datatable/media/images/details_open.png";
          oTable.fnClose(nTr);
        } else {
          /* Open this row */
          this.src = "lib/advanced-datatable/images/details_close.png";
          oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
      });
    });
  </script>
</body>

</html>
