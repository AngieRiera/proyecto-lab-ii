  <!-- Favicons -->
  <link href="../dashboard/img/favicon.png" rel="icon">
  <link href="../dashboard/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="../dashboard/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="../dashboard/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="../dashboard/lib/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="../dashboard/lib/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="../dashboard/lib/advanced-datatable/css/DT_bootstrap.css" />
  <!-- Custom styles for this template -->
  <link href="../dashboard/css/style.css" rel="stylesheet">
  <link href="../dashboard/css/style-responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="../dashboard/css/to-do.css">
  
    <!-- Estilos custom -->
  <link href="../dashboard/css/custom.css" rel="stylesheet">

</head>
<body>
  <section id="container" class="section-full-size">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->

      <!--logo end-->
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout custom-logout" href="#">Salir</a></li>
        </ul>
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse position-sidebar">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="profile.html"><img src="../dashboard/img/ui-sam.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered">Sam Soffes</h5>
          <li class="mt">
            <a class="active" href="#">
              <i class="fa fa-dashboard"></i>
              <span>Ver Perfil</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-th"></i>
              <span>Archivo</span>
              </a>
            <ul class="sub">
              <li><a href="/ProyectoLaboratorioII/empresa/oferta-empleo">Ofertas empleo</a></li>
              <li><a href="/ProyectoLaboratorioII/empresa/contrataciones">Listado Contratados</a></li>
            </ul>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->