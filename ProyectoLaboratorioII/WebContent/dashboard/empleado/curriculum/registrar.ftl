<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Dashboard - Administrador</title>
	<#include "../utils/menu.ftl">

    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">

		<div class="row mt">
          <div class="col-lg-12">
            <h4><i class="fa fa-angle-right"></i> Registro de datos de usuario</h4>
            <div class="form-panel">
              <div class=" form">
                <form class="cmxform form-horizontal style-form" id="commentForm" method="post" action="/ProyectoLaboratorioII/empleado/curriculum?operacion=guardar">
 					<div class="col-lg-12">
	 					<div class="col-lg-6">
		                  <div class="form-group readonly-field">
		                    <label for="cname" class="control-label col-lg-2">Nombre</label>
		                    <div class="col-lg-10">
		                      <input class=" form-control" id="cname" value='${usuario.getUserProfile().getNombre()}' name="name" minlength="2" type="text" readonly />
		                    </div>
		                  </div>
		                  <div class="form-group readonly-field ">
		                    <label for="cname" class="control-label col-lg-2">Apellido</label>
		                    <div class="col-lg-10">
		                      <input class=" form-control" id="cname" value='${usuario.getUserProfile().getApellido()}' name="name" minlength="2" type="text" readonly />
		                    </div>
		                  </div>
		                  <div class="form-group readonly-field">
		                    <label for="cname" class="control-label col-lg-2">C&eacute;dula</label>
		                    <div class="col-lg-10">
		                      <input class=" form-control" id="cname" value='${empleado.getCedula()}' name="name" minlength="2" type="text" readonly />
		                    </div>
		                  </div>
		                  <div class="form-group ">
		                    <label for="cname" class="control-label col-lg-2">G&eacute;nero</label>
		                    <div class="col-lg-10">
		                    <#if empleado.genero == 2>
		                      <input class=" form-control" id="cname" value="Masculino" name="name" minlength="2" type="text" readonly />
		                    <#else>
		                      <input class=" form-control" id="cname" value="Femenino" name="name" minlength="2" type="text" readonly />
		                    </#if>
		                    </div>
		                  </div>
		                  <div class="form-group readonly-field">
		                    <label for="cname" class="control-label col-lg-2" style="width: 156px;">Fecha de nacimiento</label>
		                    <div class="col-lg-8" style="padding-right: 0px;">
		                      <input class=" form-control" id="cname" value='${empleado.getFecha_nacimiento()}' name="name" minlength="2" type="text" readonly />
		                    </div>
		                  </div>
		                  <div class="form-group readonly-field">
		                    <label for="cemail" class="control-label col-lg-2">Tel&eacute;fono</label>
		                    <div class="col-lg-10">
		                      <input class="form-control " id="cemail" value=${usuario.getUserProfile().getTelefono()} type="text" name="telefono" readonly />
		                    </div>
		                  </div>
		                  
	
						</div>
	                  <!-- row -->
	                  <div class="col-lg-6">
		                  <div class="form-group readonly-field">
			                  <label class="col-sm-2 col-sm-2 control-label">Correo</label>
			                  <div class="col-sm-10">
			                    <input type="text" class="form-control" value='${usuario.getCorreo()}' name="correo" placeholder="Contrase&ntilde;a" readonly>
			                  </div>
			              </div>
			              <div class="form-group readonly-field">
			                  <label class="col-sm-2 col-sm-2 control-label">Pa&iacute;s</label>
			                  <div class="col-sm-10">
			                    <input type="text" class="form-control" name="pais" value='${usuario.getPais()}' placeholder="Contrase&ntilde;a" readonly>
			                  </div>
			              </div>
		                  <div class="form-group readonly-field">
		                    <label for="cname" class="control-label col-lg-2">Twitter</label>
		                    <div class="col-lg-10">
		                    <#if usuario.getUserProfile().getUrl_twitter() != "null">
		                      	<input class=" form-control" id="cname" name="name" minlength="2" value='${usuario.getUserProfile().getUrl_twitter()}' type="text" readonly />
		                    <#else>
		                    	<input class=" form-control" id="cname" name="name" minlength="2" value="No posee" type="text" readonly />
		                    </#if>
		                    </div>
		                  </div>
		                  <div class="form-group readonly-field">
		                    <label for="cemail" class="control-label col-lg-2">Instagram</label>
		                    <div class="col-lg-10">
		                      <#if usuario.getUserProfile().getUrl_instagram() != "null">
		                      	<input class=" form-control" id="cname" name="name" minlength="2" value='${usuario.getUserProfile().getUrl_instagram()}' type="text" readonly />
		                     <#else>
		                    	<input class=" form-control" id="cname" name="name" minlength="2" value="No posee" type="text" readonly />
		                     </#if>
		                    </div>
		                  </div>
		                  <div class="form-group readonly-field">
			                  <label class="col-sm-2 col-sm-2 control-label">Facebook</label>
			                  <div class="col-sm-10">
			                    <#if usuario.getUserProfile().getUrl_facebook() != "null">
		                      	<input class=" form-control" id="cname" name="name" minlength="2" value='${usuario.getUserProfile().getUrl_facebook()}' type="text" readonly />
		                      <#else>
		                    	<input class=" form-control" id="cname" name="name" minlength="2" value="No posee" type="text" readonly />
		                      </#if>
			                  </div>
			              </div>
						</div>
					</div><!--fin col 12-->
                  <!-- row -->
                  
                  
                  
			        <div class="row mt">
			         <button type="button" class="btn btn-success btn-aregar-elemento-curriculum" onClick="MostrarModalCursoRealizado()">Agregar Curso Realizado</button>
			          <div class="col-md-12">
			            <div class="content-panel">
			              <table id="table_curso" name="table_curso" class="table table-striped table-advance table-hover">
			                <h4><i class="fa fa-angle-right"></i> Cursos Realizados</h4>
			                <hr>
			                <thead>
			                  <tr>
			                    <th>Nombre</th>
			                    <th>Institucion</th>
			                    <th>Descripcion</th>
			                    <th>Fecha inicio</th>
			                    <th>Fecha fin</th>
			                    <th></th>
			                  </tr>
			                </thead>
			                <tbody>
			                	<#list cursos_realizados as curso_realizado>
									<tr>
										<td>${curso_realizado.nombre}</td>
										<td>${curso_realizado.institucion}</td>
										<td>${curso_realizado.descripcion}</td>
										<td>${curso_realizado.fecha_inicio}</td>
										<td>${curso_realizado.fecha_fin}</td>
										<td><button class="btn btn-danger btn-xs" onClick="eliminarFila(this,'${curso_realizado.id}','CursoRealizado')"><i class="fa fa-trash-o"></i></button></td>
									</tr>
								</#list>
			                </tbody>
			              </table>
			            </div>
			            <!-- /content-panel -->
			          </div>
			          <!-- /col-md-12 -->
			        </div>
			        
			        
			        
			        
			        <div class="row mt">
			         <button type="button" class="btn btn-success btn-aregar-elemento-curriculum" onClick="MostrarModalExperienciaLaboral()">Agregar Experiencia Laboral</button>
			          <div class="col-md-12">
			            <div class="content-panel">
			              <table id="table_experiencia" name="table_experiencia" class="table table-striped table-advance table-hover">
			                <h4><i class="fa fa-angle-right"></i> Experiencia Laboral</h4>
			                <hr>
			                <thead>
			                  <tr>
			                    <th>Empresa</th>
			                    <th >Cargo</th>
			                    <th>Descripci&oacute;n</th>
			                    <th>Fecha inicio</th>
			                    <th>Fecha fin</th>
			                    <th></th>
			                  </tr>
			                </thead>
			                <tbody>
			                	<#list experiencias_laborales as experiencia_laboral>
									<tr>
										<td>${experiencia_laboral.nombre_empresa}</td>
										<td>${experiencia_laboral.cargo}</td>
										<td>${experiencia_laboral.descripcion}</td>
										<td>${experiencia_laboral.fecha_inicio}</td>
										<td>${experiencia_laboral.fecha_culminacion}</td>
										<td><button class="btn btn-danger btn-xs" onClick="eliminarFila(this,'${experiencia_laboral.id}','ExperienciaLaboral')"><i class="fa fa-trash-o"></i></button></td>
									</tr>
								</#list>
			                </tbody>
			              </table>
			            </div>
			            <!-- /content-panel -->
			          </div>
			          <!-- /col-md-12 -->
			        </div>
                  
                  <div class="row mt">
			         <button type="button" class="btn btn-success btn-aregar-elemento-curriculum" onClick="MostrarModalDetalleEstudio()">Agregar Detalle Estudio</button>
			          <div class="col-md-12">
			            <div class="content-panel">
			              <table id="table_detalle" name="table_detalle" class="table table-striped table-advance table-hover">
			                <h4><i class="fa fa-angle-right"></i> Detalles de Estudios</h4>
			                <hr>
			                <thead>
			                  <tr>
			                    <th>Institucion</th>
			                    <th> Profesion</th>
			                    <th>Descripci&oacute;n</th>
			                    <th> Fecha Inicio</th>
			                    <th> Fecha Final</th>
			                    <th></th>
			                  </tr>
			                </thead>
			                <tbody>
								<#list detalles_estudios as detalle_estudio>
									<tr>
										<td>${detalle_estudio.institucion}</td>
										<td>${detalle_estudio.getNombreProfesion()}</td>
										<td>${detalle_estudio.descripcion}</td>
										<td>${detalle_estudio.fecha_inicio}</td>
										<td>${detalle_estudio.fecha_culminacion}</td>
										<td><button class="btn btn-danger btn-xs" onClick="eliminarFila(this,'${detalle_estudio.id}','DetalleEstudio')"><i class="fa fa-trash-o"></i></button></td>
									</tr>
								</#list>
			                </tbody>
			              </table>
			            </div>
			            <!-- /content-panel -->
			          </div>
			          <!-- /col-md-12 -->
			        </div>
                  <br/>
                  
		                  <div class="form-group ">
		                    <label for="cemail" class="control-label col-lg-2">Observaciones</label>
		                    <div class="col-lg-10">
		                    	<#if  curriculum.observaciones?? && curriculum.observaciones?has_content>
		                      		<textarea class="form-control " id="cemail" type="text" name="observaciones" >${curriculum.observaciones}</textarea>
		                    	<#else>
		                    		<textarea class="form-control " id="cemail" type="text" name="observaciones" ></textarea>
		                    	</#if>
		                    </div>
		                  </div>
		                  
		                  <div class="form-group ">
		                    <label for="cemail" class="control-label col-lg-2">Conocimientos extras</label>
		                    <div class="col-lg-10">
			                    <#if  curriculum.conocimientos_adicionales?? && curriculum.conocimientos_adicionales?has_content>
			                      <textarea class="form-control " id="cemail" type="text" name="conocimientos_extras">${curriculum.conocimientos_adicionales}</textarea>
		                    	<#else>
		                    		<textarea class="form-control " id="cemail" type="text" name="conocimientos_extras"></textarea>
		                    	</#if>
		                    </div>
		                  </div>
		                  
		                  <input type="hidden" id="hidden_table_detalle" name="hidden_table_detalle" >
		                  <input type="hidden" id="hidden_table_curso" name="hidden_table_curso" >
		                  <input type="hidden" id="hidden_table_experiencia" name="hidden_table_experiencia" >
                  
                  <div class="form-group">
                    <div class="btn-operaciones col-lg-10">
                      <button class="btn btn-theme" type="submit">Guardar cambios</button>
                      <button class="btn btn-theme04" type="button">Cancelar</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!-- /form-panel -->
          </div>
          <!-- /col-lg-12 -->
        </div>
        <!-- /row -->
      </section>
    </section>
    <!--main content end-->
    
       <!-- Modal Curso Realizado-->
       <div aria-hidden="true" tabindex="-1" id="modalCursoRealizado" class="modal">
         <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" onClick="CerrarModalCursoRealizado()" aria-hidden="true">&times;</button>
               <h4 class="modal-title">Curso Realizado</h4>
             </div>
             <div class="modal-body" style="min-height: 250px;">
                <form id="formCursoRealizado">  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Nombre</label>
                    <div class="col-lg-10">
                      <input class=" form-control input-modal" id="name_curso" name="name_curso" minlength="2" type="text" required />
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Descripcion</label>
                    <div class="col-lg-10">
                      <textarea class=" form-control input-modal" id="descripcion_curso" name="descripcion_curso" minlength="2" type="text" required > </textarea>
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Fecha de Inicio </label>
                    <div class="col-lg-10">
                      <input class=" form-control input-modal" id="fechaini_curso" name="fechaini_curso" minlength="2" type="date" required />
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Fecha Final</label>
                    <div class="col-lg-10">
                      <input class=" form-control input-modal" id="fechafin_curso" name="fechafin_curso" minlength="2" type="date" required />
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="cemail" class="control-label col-lg-2">Institucion</label>
                    <div class="col-lg-10">
                      <input class="form-control input-modal" id="institucion_curso" type="text" name="institucion_curso" required />
                    </div>
                  </div>
			  </form>
	                  
             </div>
             <div class="modal-footer centered">
             	<a class="btn btn-theme03" type="button" id="btn-eliminar" onClick="AgregarCursoRealizado()">Agregar</a>
             	<button class="btn btn-theme04" type="button" onClick="CerrarModalCursoRealizado()">Cancelar</button>                             
             </div>
           </div>
         </div>
       </div>
    <!-- modal modal curso realizado-->
    
    
    
    <!--main content end-->
       <!-- Modal Experiencia laboral-->
       <div aria-hidden="true" tabindex="-1" id="modalExperienciaLaboral" class="modal">
         <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" onClick="CerrarModalExperienciaLaboral()" aria-hidden="true">&times;</button>
               <h4 class="modal-title">Experiencia laboral</h4>
             </div>
             <div class="modal-body" style="min-height: 250px;">
               
               <form id="formExperienciaLaboral">
               	  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Empresa</label>
                    <div class="col-lg-10">
                      <input class=" form-control input-modal" id="empresa_experiencia" name="empresa_experiencia" minlength="2" type="text" required />
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Cargo</label>
                    <div class="col-lg-10">
                      <input class=" form-control input-modal" id="cargo_experiencia" name="cargo_experiencia" minlength="2" type="text" required />
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Descripci&oacute;n</label>
                    <div class="col-lg-10">
                      <textarea class=" form-control input-modal" id="descripcion_experiencia" name="descripcion_experiencia" minlength="2" type="text" required ></textarea>
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Fecha de Inicio </label>
                    <div class="col-lg-10">
                      <input class=" form-control input-modal" id="fechaini_experiencia" name="fechaini_experiencia" minlength="2" type="date" required />
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Fecha Final</label>
                    <div class="col-lg-10">
                      <input class=" form-control input-modal" id="fechafin_experiencia" name="fechafin_experiencia" minlength="2" type="date" required />
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cemail" class="control-label col-lg-2">Especialidades</label>
                    <div class="col-lg-10">
                      <select class="form-control input-modal" id="especialidad_id" name="especialidad" required >
                      	<#list especialidades as especialidad >
                      		<option id ="option_especialidad" value='${especialidad.getId()}' label='${especialidad.getNombre()}'> ${especialidad.getNombre()} </option>
                      	</#list>
                      </select>
                    </div>
                  </div>
               </form>
             </div>
             <div class="modal-footer centered">
             	<a class="btn btn-theme03" type="button" id="btn-eliminar" onClick="AgregarExperienciaLaboral()">Agregar</a>
             	<button class="btn btn-theme04" type="button" onClick="CerrarModalExperienciaLaboral()">Cancelar</button>                             
             </div>
           </div>
         </div>
       </div>
    <!-- modal experiencia laboral-->
    
     <!-- Modal detalle estudio-->
       <div aria-hidden="true" tabindex="-1" id="modalDetalleEstudio" class="modal">
         <div class="modal-dialog">
           <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" onClick="CerrarModalDetalleEstudio()" aria-hidden="true">&times;</button>
               <h4 class="modal-title">Detalle de Estudio</h4>
             </div>
             <div class="modal-body" style="min-height: 250px;">
               <form id="formDetalleEstudio">
               	  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Institucion</label>
                    <div class="col-lg-10">
                      <input class=" form-control input-modal" id="institucion_detalle_estudio" name="institucion_detalle_estudio" type="text" required />
                    </div>
                  </div>
                   <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Descripcion</label>
                    <div class="col-lg-10">
                      <textarea class=" form-control input-modal" id="descripcion_detalle_estudio" name="descripcion_detalle_estudio" type="text" required ></textarea>
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Fecha de Inicio </label>
                    <div class="col-lg-10">
                      <input class=" form-control input-modal" id="fechaini_detalle_estudio" name="fechaini_detalle_estudio" type="date" required />
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Fecha Final</label>
                    <div class="col-lg-10">
                      <input class=" form-control input-modal" id="fechafin_detalle_estudio" name="fechafin_detalle_estudio" type="date" required />
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="cemail" class="control-label col-lg-2">Profesion</label>
                    <div class="col-lg-10">
                      <select class="form-control input-modal" id="profesion_id_detalle_estudio" name="profesion_id_detalle_estudio" required >
                      	<#list profesiones as profesion >
                      		<option id ="option_profesion" value='${profesion.getId()}' label='${profesion.getCarrera()}' > ${profesion.getCarrera()} </option>
                      	</#list>
                      </select>
                    </div>
                  </div>
               </form>
             </div>
             <div class="modal-footer centered">
             	<a class="btn btn-theme03" type="button" id="btn-eliminar" onClick="AgregarDetalleEstudio()" >Agregar</a>
             	<button class="btn btn-theme04" type="button" onClick="CerrarModalDetalleEstudio()">Cancelar</button>                             
             </div>
           </div>
         </div>
       </div>
       <br/>
    <!-- modal detalle estudio-->
    
    <!--footer start-->
    <!-- footer class="site-footer custom-site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Dashio</strong>. All Rights Reserved
        </p>
        <div class="credits">
          Created with Dashio template by <a href="https://templatemag.com/">TemplateMag</a>
        </div>
        <a href="index.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer-->
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="../dashboard/lib/curriculum.js"></script>
  <script src="../dashboard/lib/custom.js"></script>
  <script src="../dashboard/lib/jquery/jquery.min.js"></script>
  <script src="../dashboard/lib/custom.js"></script>
  <script src="../dashboard/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="../dashboard/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="../dashboard/lib/jquery.scrollTo.min.js"></script>
  <script src="../dashboard/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="../dashboard/lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="../dashboard/lib/common-scripts.js"></script>

  
  <!--script for this page-->
  <script src="../dashboard/lib/sparkline-chart.js"></script>
  <script src="../dashboard/lib/zabuto_calendar.js"></script>
  <script type="text/javascript">

  </script>
  <script type="application/javascript">
    $(document).ready(function() {
      $("#date-popover").popover({
        html: true,
        trigger: "manual"
      });
      $("#date-popover").hide();
      $("#date-popover").click(function(e) {
        $(this).hide();
      });

      $("#my-calendar").zabuto_calendar({
        action: function() {
          return myDateFunction(this.id, false);
        },
        action_nav: function() {
          return myNavFunction(this.id);
        },
        ajax: {
          url: "show_data.php?action=1",
          modal: true
        },
        legend: [{
            type: "text",
            label: "Special event",
            badge: "00"
          },
          {
            type: "block",
            label: "Regular event",
          }
        ]
      });
    });

    function myNavFunction(id) {
      $("#date-popover").hide();
      var nav = $("#" + id).data("navigation");
      var to = $("#" + id).data("to");
      console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
  </script>
</body>

</html>
