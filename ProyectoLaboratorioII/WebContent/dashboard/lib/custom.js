function MostrarModal(id,gestion){
	$('#ModalEliminar').css({ display: "block" });
	$('#btn-eliminar').attr('href', "/ProyectoLaboratorioII/admin/"+gestion+"?operacion=eliminar&id=" + id);
}

function MostrarModalReactivacion(id, gestion){
	console.log("Hola");
	$('#ModalReactivar').css({display: "block"});
	$('#btn-reactivar').attr('href', "/ProyectoLaboratorioII/admin/"+gestion+"?operacion=reactivar&id=" + id);
}

function CerrarModal(){
	$('#ModalEliminar').css({ display: "none" });
	$('#ModalReactivar').css({display: "none"});
}

function EliminarRegistro(id){
    $('#btn-eliminar').attr('href', "/ProyectoLaboratorioII/admin/usuario?operacion=eliminar&id=" + id);
}

function MostrarModalCursoRealizado(){
	$('#modalCursoRealizado').css({ display: "block" });
}

function CerrarModalCursoRealizado(){
	$('#modalCursoRealizado').css({ display: "none" });
}

function MostrarModalExperienciaLaboral(){
	$('#modalExperienciaLaboral').css({ display: "block" });
}

function CerrarModalExperienciaLaboral(){
	$('#modalExperienciaLaboral').css({ display: "none" });
}

function MostrarModalDetalleEstudio(){
	$('#modalDetalleEstudio').css({ display: "block" });
}

function CerrarModalDetalleEstudio(){
	$('#modalDetalleEstudio').css({ display: "none" });
}

function agregarDato(){
	
	  let nombre = $('#name_curso').val();
	  let fecha_inicio = $('#fechaini_curso').val();
	  let fecha_final = $('#fechafin_curso').val();
	  let institucion = $('#institucion_curso').val();
	  let descripcion = $('#descripcion_curso').val();
	
	  $("#table_curso")
	  .append(
			  $('<tr>').attr('id', nombre)
			  .append(
					$('<td>')
					.append(
						nombre	
					)
			  )
			  .append(
					$('<td>')
					.append(
						institucion	
					)
			  )
			  .append(
					$('<td>')
					.append(
						descripcion	
					)
			  )
			  .append(
					$('<td>')
					.append(
						fecha_inicio	
					)
			  )
			  .append(
					$('<td>')
					.append(
						fecha_final	
					)
			  )
			  .append(
					$('<td>')
					.append(
						$('<button>').addClass("btn btn-danger btn-xs").attr('onClick','eliminarFila(this)')
						.append(
							$('<i>').addClass("fa fa-trash-o ")
						)
					)
			  )
	  );
	  $('#modalCursoRealizado').css({ display: "none" });
	  $('#name_curso').val("");
	  $('#fechaini_curso').val("");
	  $('#fechafin_curso').val("");
	  $('#descripcion_curso').val("");
	  $('#institucion_curso').val("");
}


function agregarDatoExperiencia(){
	
	  let nombre = $('#empresa_experiencia').val();
	  let cargo = $('#cargo_experiencia').val();
	  let descripcion = $('#descripcion_experiencia').val();
	  let fecha_inicio = $('#fechaini_experiencia').val();
	  let fecha_final = $('#fechafin_experiencia').val();
	
	  $("#table_experiencia")
	  .append(
			  $('<tr>').attr('id', nombre)
			  .append(
					$('<td>')
					.append(
						nombre	
					)
			  )
			  .append(
					$('<td>')
					.append(
						cargo	
					)
			  )
			  .append(
					$('<td>')
					.append(
						descripcion	
					)
			  )
			  .append(
					$('<td>')
					.append(
						fecha_inicio	
					)
			  )
			  .append(
					$('<td>')
					.append(
						fecha_final	
					)
			  )
			  .append(
					$('<td>')
					.append(
						$('<button>').addClass("btn btn-danger btn-xs").attr('onClick','eliminarFila(this)')
						.append(
							$('<i>').addClass("fa fa-trash-o ")
						)
					)
			  )
	  );
	  $('#modalExperienciaLaboral').css({ display: "none" });
	  $('#empresa_experiencia').val("");
	  $('#fechaini_experiencia').val("");
	  $('#fechafin_experiencia').val("");
	  $('#descripcion_experiencia').val("");
	  $('#institucion_experiencia').val("");
}

function agregarDetalleEstudio(){
	  let nombre = $('#institucion_detalle').val();
	  let profesion= $('#profesion_id').val();
	  let descripcion = $('#descripcion_detalle').val();
	  let fecha_inicio = $('#fechaini_detalle').val();
	  let fecha_final = $('#fechafin_detalle').val();
	  var index=0;
	  
	  $("#table_detalle")
	  .append(
			  $('<tr>').attr('id', nombre)
			  .append(
					$('<td name="institucion_detalle">')
					.append(
						nombre
					)
			  )
			  .append(
					$('<td>')
					.append(
						profesion
					)
			  )
			   .append(
					$('<td name="id_profesion_detalle">')
					.append(
						$("#option_profesion").attr("othervalue")
					)
			  )
			  .append(
					$('<td name="descripcion_detalle">')
					.append(
						descripcion
					)
			  )
			  .append(
					$('<td name="fechaini_detalle">')
					.append(
						fecha_inicio	
					)
			  )
			  .append(
					$('<td name="fechafin_detalle">')
					.append(
						fecha_final	
					)
			  )
			  .append(
					$('<td>')
					.append(
						$('<button>').addClass("btn btn-danger btn-xs").attr('onClick','eliminarFila(this)')
						.append(
							$('<i>').addClass("fa fa-trash-o ")
						)
					)
			  )
	  );
	  $('#modalDetalleEstudio').css({ display: "none" });
	  $('#institucion_detalle').val("");
	  $('#profesion_id').val("");
	  $('#fechafin_detalle').val("");
	  $('#descripcion_detalle').val("");
	  $('#fechaini_detalle').val("");

}

/*function eliminarFila(id) {
	  $(id).parents('tr').remove();
}*/


function tablasPrueba(){
	var rows_curso = { rows_curso: [] }; 

	var $th = $('#table_curso th'); 
	$('#table_curso tbody tr').each(function(i, tr){ 
	    var obj = {}, $tds = $(tr).find('td'); 
	    $th.each(function(index, th){ 
	     obj[$(th).text()] = $tds.eq(index).text(); 
	    }); 
	    rows_curso.rows_curso.push(obj); 
	}); 
	$('#hidden_table_curso').val(JSON.stringify(rows_curso));
	
	var rows_experiencia = { rows_experiencia: [] }; 

	var $th = $('#table_experiencia th'); 
	$('#table_experiencia tbody tr').each(function(i, tr){ 
	    var obj = {}, $tds = $(tr).find('td'); 
	    $th.each(function(index, th){ 
	     obj[$(th).text()] = $tds.eq(index).text(); 
	    }); 
	    rows_experiencia.rows_experiencia.push(obj); 
	}); 
	$('#hidden_table_experiencia').val(JSON.stringify(rows_experiencia));
	
	var rows_detalle = { rows_detalle: [] }; 

	var $th = $('#table_detalle th'); 
	
	$('#table_detalle tbody tr').each(function(i, tr){ 
	    var obj = {}, $tds = $(tr).find('td'); 
	    $th.each(function(index, th){ 
	     obj[$(th).text()] = $tds.eq(index).text(); 
	    }); 
	    rows_detalle.rows_detalle.push(obj); 
	}); 
	
	$('#hidden_table_detalle').val(JSON.stringify(rows_detalle));
}

function tablaCurso(){
	var rows_curso = { rows_curso: [] }; 

	var $th = $('#table_curso th'); 
	$('#table_curso tbody tr').each(function(i, tr){ 
	    var obj = {}, $tds = $(tr).find('td'); 
	    $th.each(function(index, th){ 
	     obj[$(th).text()] = $tds.eq(index).text(); 
	    }); 
	    rows_curso.rows_curso.push(obj); 
	}); 
	$('#hidden_table_curso').val(JSON.stringify(rows_curso));
}

function tablaExperiencia(){
	var rows_experiencia = { rows_experiencia: [] }; 

	var $th = $('#table_experiencia th'); 
	$('#table_experiencia tbody tr').each(function(i, tr){ 
	    var obj = {}, $tds = $(tr).find('td'); 
	    $th.each(function(index, th){ 
	     obj[$(th).text()] = $tds.eq(index).text(); 
	    }); 
	    rows_experiencia.rows_experiencia.push(obj); 
	}); 
	$('#hidden_table_experiencia').val(JSON.stringify(rows_experiencia));
}
function tablaDetalle(){
	var rows_detalle = { rows_detalle: [] }; 

	var $th = $('#table_detalle th'); 
	$('#table_detalle tbody tr').each(function(i, tr){ 
	    var obj = {}, $tds = $(tr).find('td'); 
	    $th.each(function(index, th){ 
	     obj[$(th).text()] = $tds.eq(index).text(); 
	    }); 
	    rows_detalle.rows_detalle.push(obj); 
	}); 
	$('#hidden_table_detalle').val(JSON.stringify(rows_detalle));
}



function tablesTo(){
	tablaCurso();
	tablaExperiencia();
	tablaDetalle();
}

function tablas(){
	var rows_curso = { rows_curso: [] }; 

	var $th = $('#table_curso th'); 
	$('#table_curso tbody tr').each(function(i, tr){ 
	    var obj = {}, $tds = $(tr).find('td'); 
	    $th.each(function(index, th){ 
	     obj[$(th).text()] = $tds.eq(index).text(); 
	    }); 
	    rows_curso.rows_curso.push(obj); 
	}); 
	$('#hidden_table_curso').val(JSON.stringify(rows_curso));
	
	var rows_experiencia = { rows_experiencia: [] }; 

	var $th = $('#table_experiencia th'); 
	$('#table_experiencia tbody tr').each(function(i, tr){ 
	    var obj = {}, $tds = $(tr).find('td'); 
	    $th.each(function(index, th){ 
	     obj[$(th).text()] = $tds.eq(index).text(); 
	    }); 
	    rows_experiencia.rows_experiencia.push(obj); 
	}); 
	$('#hidden_table_experiencia').val(JSON.stringify(rows_experiencia));
	
	var rows_detalle = { rows_detalle: [] }; 

	var $th = $('#table_detalle th'); 
	
	$('#table_detalle tbody tr').each(function(i, tr){ 
	    var obj = {}, $tds = $(tr).find('td'); 
	    $th.each(function(index, th){ 
	     obj[$(th).text()] = $tds.eq(index).text(); 
	    }); 
	    rows_detalle.rows_detalle.push(obj); 
	}); 
	
	$('#hidden_table_detalle').val(JSON.stringify(rows_detalle));
}
