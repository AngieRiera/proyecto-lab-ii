//valida solo numeros enteros (no deja tipear algo no sea nuemro)
function validarEntero(e){
	//Nota: Backspace = 8, Enter = 13, '0' = 48, '9' = 57
    evt = e ? e : event;
    key = (window.Event) ? evt.which : evt.keyCode;
    var formulario = document.frmEmpleados;
    if((key>=48 && key<=57) || key <= 13)
    	{
    	if(evt.target.name.toString() == "cedula"){
    		formulario.cedula.style.borderColor='#2b3553'
    	}
    	return true;
    	}
      
    else 
      return false;
 }


//valida solo letras (no deja tipear algo no letra o espacio)
function validarSoloLetras(e, nombreForm){	
	//Nota: Backspace = 8, Enter = 13, 'A' = 65, 'Z' = 90	 'a' = 97  'z' = 122 espacio = 32
    evt = e ? e : event;
    console.log(evt.target.name.toString());
    var formulario = document.getElementById(nombreForm);
    key = (window.Event) ? evt.which : evt.keyCode;
	if  (key <= 13 ||  key == 32 ||(key >= 65 && key <= 90) || (key >= 97 && key <= 122)){
		if(evt.target.name.toString() == "nombre"){
			formulario.nombre.style.borderColor=''
		 }
		return true;
			
	}
		
	else
		return false;
}

function validarCorreo(e){	
	evt = e ? e : event;
	var resultado = true;
	var formulario = document.frmUsuario;
	if(evt.target.value != ""){
		formulario.correo.style.borderColor='';
	}
	return resultado;
}

function validarContrasenia(passwordInput, confirmPasswordInput){
	var resultado = true;
	if(passwordInput.value == confirmPasswordInput.value){
		confirmPasswordInput.style.borderColor='';
		resultado = true
	}else{
		alert('La contraseña de confirmación y la contraseña no son iguales.');
        confirmPasswordInput.style.borderColor= '#f84d6d';
        confirmPasswordInput.focus();
		resultado = false;
	}
	return resultado
}

function validarCambioContraseniaConfirmacion(e){
	evt = e ? e : event;
	var contrasenia = $('#contrasenia').val();
	var confirmPasswordInput = $('#confirmar_contrasenia');
	var confirmPasswordInputValue = evt.target.value;
	console.log(contrasenia)
	console.log(confirmPasswordInputValue)
	console.log(contrasenia == confirmPasswordInputValue)
	var resultado = true;
	if(contrasenia == confirmPasswordInputValue){
		$('#confirmar_contrasenia').tooltip('hide');
		confirmPasswordInput.css({borderColor: ''});
		resultado = true
	}else{
		$('#confirmar_contrasenia').tooltip({'trigger':'focus', 'title': 'La contraseña de confirmacion debe coincidir con la contraseña'});
		confirmPasswordInput.css({borderColor: '#f84d6d'});
        confirmPasswordInput.focus();
	}
	return resultado
}

function campoRequerido(formInput,campo){
    var resultado = true;
	if ((formInput.value == "") || (formInput.value.length == 0)){
        alert('Por favor introduzca un valor en ' + campo +'.');
        formInput.style.borderColor= '#f84d6d';
        formInput.focus();
		resultado = false;
	}
	else{
		formInput.style.borderColor=''
	}
	return resultado;
}

function validarSeleccionCombo(e, nombreForm){
	evt = e ? e : event;
	var formulario = document.getElementById(nombreForm);
	if(nombreForm == "frmUsuario"){
		switch(evt.target.name.toString()){
		case "rol":
			formulario.rol.style.borderColor='';
			return true;
		default:
			formulario.pais.style.borderColor='';
			return true;
		}
		return false;
	}
}

function validarComboBox(htmlCombo, campo){
	var resultado = true;
	if(htmlCombo.value == "-1"){
		alert('Por favor introduzca un valor en ' + campo +'.');
		htmlCombo.style.borderColor= '#f84d6d';
		htmlCombo.focus();
		resultado = false;
	}
	return resultado;
}

function validarFormulario(nombreForm)
{
	var formulario = document.getElementById(nombreForm); 
	console.log("hola");
	if(nombreForm == "frmUsuario"){
		if (!campoRequerido(formulario.nombre,"Nombre del Usuario")) return false;
		if (!campoRequerido(formulario.correo,"Correo del Usuario")) return false;
		if((formulario.confirmar_contrasenia != null || formulario.confirmar_contrasenia != "") && !validarContrasenia(formulario.contrasenia, formulario.confirmar_contrasenia)) return false;
		if (!validarComboBox(formulario.rol,"Rol del Usuario")) return false;
	    if (!validarComboBox(formulario.pais,"Pais del Usuario")) return false;
	    else
	    	return true;
	}
}

