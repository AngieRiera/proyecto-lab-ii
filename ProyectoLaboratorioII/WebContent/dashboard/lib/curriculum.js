function eliminarFila(id,id_registro,tipo) {
	
	  $(id).parents('tr').remove();
	  
	  $.ajax({
			url: "/ProyectoLaboratorioII/empleado/curriculum?accion=eliminar"+tipo,
			type: "POST",
			data: {"id":id_registro},
			success: function(response) {
					//agregarFila(estado);
			},
			error: function(jqXHR, estado, error) {
			},
			complete: function(jqXHR, estado) {

			},
			timeout: 10000
		});
}

function AgregarCursoRealizado() {
	
	$.ajax({
		url: "/ProyectoLaboratorioII/empleado/curriculum?accion=AgregarCursoRealizado",
		type: "POST",
		data: $("#formCursoRealizado").serialize(),
		success: function(response) {
				//agregarFila(estado);
		},
		error: function(jqXHR, estado, error) {
		},
		complete: function(jqXHR, estado) {

		},
		timeout: 10000
	});
	let nombre = $('#name_curso').val();
	  let fecha_inicio = $('#fechaini_curso').val();
	  let fecha_final = $('#fechafin_curso').val();
	  let institucion = $('#institucion_curso').val();
	  let descripcion = $('#descripcion_curso').val();
	
	  $("#table_curso")
	  .append(
			  $('<tr>').attr('id', nombre)
			  .append(
					$('<td>')
					.append(
						nombre	
					)
			  )
			  .append(
					$('<td>')
					.append(
						institucion	
					)
			  )
			  .append(
					$('<td>')
					.append(
						descripcion	
					)
			  )
			  .append(
					$('<td>')
					.append(
						fecha_inicio	
					)
			  )
			  .append(
					$('<td>')
					.append(
						fecha_final	
					)
			  )
			  .append(
					$('<td>')
					.append(
						$('<button>').addClass("btn btn-danger btn-xs").attr('onClick','eliminarFila(this)')
						.append(
							$('<i>').addClass("fa fa-trash-o ")
						)
					)
			  )
	  );
	  $('#modalCursoRealizado').css({ display: "none" });
	  $('#name_curso').val("");
	  $('#fechaini_curso').val("");
	  $('#fechafin_curso').val("");
	  $('#descripcion_curso').val("");
	  $('#institucion_curso').val("");
}

function AgregarExperienciaLaboral() {
	
	$.ajax({
		url: "/ProyectoLaboratorioII/empleado/curriculum?accion=AgregarExperienciaLaboral",
		type: "POST",
		data: $("#formExperienciaLaboral").serialize(),
		success: function(response) {
				//agregarFila(estado);
		},
		error: function(jqXHR, estado, error) {
		},
		complete: function(jqXHR, estado) {

		},
		timeout: 10000
	});

	  let nombre = $('#empresa_experiencia').val();
	  let cargo = $('#cargo_experiencia').val();
	  let descripcion = $('#descripcion_experiencia').val();
	  let fecha_inicio = $('#fechaini_experiencia').val();
	  let fecha_final = $('#fechafin_experiencia').val();
	
	  $("#table_experiencia")
	  .append(
			  $('<tr>').attr('id', nombre)
			  .append(
					$('<td>')
					.append(
						nombre	
					)
			  )
			  .append(
					$('<td>')
					.append(
						cargo	
					)
			  )
			  .append(
					$('<td>')
					.append(
						descripcion	
					)
			  )
			  .append(
					$('<td>')
					.append(
						fecha_inicio	
					)
			  )
			  .append(
					$('<td>')
					.append(
						fecha_final	
					)
			  )
			  .append(
					$('<td>')
					.append(
						$('<button>').addClass("btn btn-danger btn-xs").attr('onClick','eliminarFila(this)')
						.append(
							$('<i>').addClass("fa fa-trash-o ")
						)
					)
			  )
	  );
	  $('#modalExperienciaLaboral').css({ display: "none" });
	  $('#empresa_experiencia').val("");
	  $('#fechaini_experiencia').val("");
	  $('#fechafin_experiencia').val("");
	  $('#descripcion_experiencia').val("");
	  $('#institucion_experiencia').val("");
}


function AgregarDetalleEstudio() {
	$.ajax({
		url: "/ProyectoLaboratorioII/empleado/curriculum?accion=AgregarDetalleEstudio",
		type: "POST",
		data: $("#formDetalleEstudio").serialize(),
		success: function(response) {
				//agregarFila(estado);
		},
		error: function(jqXHR, estado, error) {
		},
		complete: function(jqXHR, estado) {

		},
		timeout: 10000
	});
	
	  let nombre = $('#institucion_detalle_estudio').val();
	  let profesion=$('#profesion_id_detalle_estudio option:selected').text(); //$('#option_profesion').attr("othervalue")
	  let descripcion = $('#descripcion_detalle_estudio').val();
	  let fecha_inicio = $('#fechaini_detalle_estudio').val();
	  let fecha_final = $('#fechafin_detalle_estudio').val();
	  var index=0;
	  
	  $("#table_detalle")
	  .append(
			  $('<tr>').attr('id', nombre)
			  .append(
					$('<td name="institucion_detalle">')
					.append(
						nombre
					)
			  )
			  .append(
					$('<td>')
					.append(
						profesion
					)
			  )
			   /*.append(
					$('<td name="id_profesion_detalle">')
					.append(
						$("#option_profesion").attr("value")
					)
			  )*/
			  .append(
					$('<td name="descripcion_detalle">')
					.append(
						descripcion
					)
			  )
			  .append(
					$('<td name="fechaini_detalle">')
					.append(
						fecha_inicio	
					)
			  )
			  .append(
					$('<td name="fechafin_detalle">')
					.append(
						fecha_final	
					)
			  )
			  .append(
					$('<td>')
					.append(
						$('<button>').addClass("btn btn-danger btn-xs").attr('onClick','eliminarFila(this)')
						.append(
							$('<i>').addClass("fa fa-trash-o ")
						)
					)
			  )
	  );
	  $('#modalDetalleEstudio').css({ display: "none" });
	  $('#institucion_detalle').val("");
	  $('#profesion_id').val("");
	  $('#fechafin_detalle').val("");
	  $('#descripcion_detalle').val("");
	  $('#fechaini_detalle').val("");
}