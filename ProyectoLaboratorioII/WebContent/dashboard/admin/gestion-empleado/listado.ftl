<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Dashio - Bootstrap Admin Template</title>

<#include "../utils/menu.ftl">
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Empleados</h3>
        <a type="button" class="btn btn-success btn-aregar-elemento-curriculum" href="/ProyectoLaboratorioII/admin/empleado?operacion=registrar" style="margin-top:10px; margin-left:0px;">Nuevo registro</a>
        <#if mensaje?has_content>
          	<#if mensaje.type == "success">
	          	<div class="alert alert-success alert-dismissible" >
					<button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Cerrar">
					 x
					</button>
					<span data-notify="icon" class="tim-icons icon-bell-55"></span>
					<span>${mensaje.text}</span>
				</div>
			<#else>
	          	<div class="alert alert-danger alert-dismissible">
					<button type="button" aria-hidden="true" class="close" data-dismiss="alert" aria-label="Cerrar">
					  x
					</button>
					<span data-notify="icon" class="tim-icons icon-bell-55"></span>
					<span>${mensaje.text}</span>
				</div>
			</#if>
          </#if>
        <div class="row mb">
          <!-- page start-->
          <div class="content-panel">
            <div class="adv-table">
              <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="hidden-table-info">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>C&eacute;dula</th>
                    <th>Nombre </th>
                    <th>Apellido</th>
                    <th>G&eacute;nero</th>
                    <th>Estado del registro</th>
                    <th class="hidden-phone table-field-options">Opciones</th>
                  </tr>
                </thead>
                
                <tbody>
					<#list empleados as empleado>
					<tr>	
						<td><span id="id" name="id">${empleado.id}</span></td>
						<td><span id="cedula">${empleado.cedula}</span></td>
						<#if empleado.usuario?? && empleado.usuario.getUserProfile()?? && empleado.usuario.getUserProfile().getNombre()?? && empleado.usuario.getUserProfile().getNombre()?has_content>
							<td><span id="nombre"> ${empleado.usuario.getUserProfile().getNombre()} </span></td>
						<#else>
							<td><span id="nombre"> No posee </span></td>
						</#if>
						<#if empleado.usuario?? && empleado.usuario.getUserProfile()?? && empleado.usuario.getUserProfile().getApellido()?? && empleado.usuario.getUserProfile().getApellido()?has_content>
							<td><span id="apellido"> ${empleado.usuario.getUserProfile().getApellido()} </span></td>
						<#else>
							<td><span id="apellido"> No posee </span></td>
						</#if>
						<#if empleado.genero == 1>	
							<td><span id="genero">Mujer</span></td>
						<#else>
							<td><span id="genero">Hombre</span></td>
						</#if>
						
						<#if empleado.status == 1>
							<td><span id="status">Activo</span></td>
						<#else>
							<td><span id="status">Inactivo</span></td>
						</#if>
						
	                    <td class="center hidden-phone">
							<a type="button" class="btn btn-round btn-info" href="../admin/empleado?operacion=ver&id=${empleado.id}">Ver</a>
			              	<a type="button" class="btn btn-round btn-warning" href="../admin/empleado?operacion=modificar&id=${empleado.id}">Modificar</a>
			              	<a type="button" class="btn btn-round btn-danger"  href="../admin/empleado?operacion=eliminar&id=${empleado.id}">Eliminar</a>
						</td>	
											
					</tr>					
					</#list>
				</tbody>
                
              </table>
            </div>
          </div>
          <!-- page end-->
        </div>
        <!-- /row -->
      </section>
      <!-- /wrapper -->
    </section>
    <!-- /MAIN CONTENT -->
    <!--main content end-->
    <!--footer start-->
    <!--footer class="site-footer custom-site-footer position-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Dashio</strong>. All Rights Reserved
        </p>
        <div class="credits">

          Created with Dashio template by <a href="https://templatemag.com/">TemplateMag</a>
        </div>
        <a href="index.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer  -->
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
   <script src="../dashboard/lib/core/jquery.min.js" type="text/javascript"></script>
  <script src="../dashboard/lib/core/popper.min.js" type="text/javascript"></script>
  <script src="../dashboard/lib/core/bootstrap.min.js" type="text/javascript"></script>
  <script src="../dashboard/lib/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="../dashboard/lib/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="../dashboard/lib/plugins/nouislider.min.js" type="text/javascript"></script>
  <!-- Chart JS -->
  <script src="../dashboard/lib/plugins/chartjs.min.js"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src="../dashboard/lib/plugins/moment.min.js"></script>
  <script src="../dashboard/lib/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
  <!-- Control Center for Black UI Kit: parallax effects, scripts for the example pages etc -->
  <script src="../dashboard/lib/blk-design-system.min.js?v=1.0.0" type="text/javascript"></script>
  
  
  <script src="../dashboard/lib/custom.js"></script>
  <script src="../dashboard/lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="../dashboard/lib/advanced-datatable/js/jquery.js"></script>
  <script src="../dashboard/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="../dashboard/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="../dashboard/lib/jquery.scrollTo.min.js"></script>
  <script src="../dashboard/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="../dashboard/lib/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="../dashboard/lib/advanced-datatable/js/DT_bootstrap.js"></script>
  <!--common script for all pages-->
  <script src="../dashboard/lib/common-scripts.js"></script>
  <!--script for this page-->
  <script type="text/javascript">
    /* Formating function for row details */
    function fnFormatDetails(oTable, nTr) {
      var aData = oTable.fnGetData(nTr);
      var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
      sOut += '<tr><td>Rendering engine:</td><td>' + aData[1] + ' ' + aData[4] + '</td></tr>';
      sOut += '<tr><td>Link to source:</td><td>Could provide a link here</td></tr>';
      sOut += '<tr><td>Extra info:</td><td>And any further details here (images etc)</td></tr>';
      sOut += '</table>';

      return sOut;
    }

    $(document).ready(function() {
      /*
       * Insert a 'details' column to the table
       */
      var nCloneTh = document.createElement('th');
      var nCloneTd = document.createElement('td');
      nCloneTd.innerHTML = '<img src="lib/advanced-datatable/images/details_open.png">';
      nCloneTd.className = "center";

      $('#hidden-table-info thead tr').each(function() {
        this.insertBefore(nCloneTh, this.childNodes[0]);
      });

      $('#hidden-table-info tbody tr').each(function() {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
      });

      /*
       * Initialse DataTables, with no sorting on the 'details' column
       */
      var oTable = $('#hidden-table-info').dataTable({
        "aoColumnDefs": [{
          "bSortable": false,
          "aTargets": [0]
        }],
        "aaSorting": [
          [1, 'asc']
        ]
      });

      /* Add event listener for opening and closing details
       * Note that the indicator for showing which row is open is not controlled by DataTables,
       * rather it is done here
       */
      $('#hidden-table-info tbody td img').live('click', function() {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
          /* This row is already open - close it */
          this.src = "lib/advanced-datatable/media/images/details_open.png";
          oTable.fnClose(nTr);
        } else {
          /* Open this row */
          this.src = "lib/advanced-datatable/images/details_close.png";
          oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
      });
    });
  </script>
</body>

</html>
