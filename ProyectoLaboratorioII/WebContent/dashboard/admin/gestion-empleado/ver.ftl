<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Dashboard - Administrador</title>

  <#include "../utils/menu.ftl">
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">

		<div class="row mt">
          <div class="col-lg-12">
            <h4><i class="fa fa-angle-right"></i> Consulta de datos del empleado</h4>
            <div class="form-panel">
              <div class=" form">
                <form class="cmxform form-horizontal style-form" id="commentForm" method="post" action="">
                
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Cedula</label>
                    <div class="col-lg-10">
                    	<#if empleado?? && empleado.cedula?? && empleado.cedula?has_content>
                    	  <input class=" form-control readonly-field" id="cname" name="cedula" minlength="2" type="text" readonly value="${empleado.cedula}"/>
                    	<#else>
                    	  <input class=" form-control readonly-field" id="cname" name="cedula" minlength="2" type="text" readonly value="No posee"/>
                    	</#if>
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Nombre</label>
                    <div class="col-lg-10">
                     <#if user_profile?? && user_profile.nombre?? && user_profile.nombre?has_content>
                    	<input class=" form-control readonly-field" id="cname" name="nombre" minlength="2" type="text" readonly value="${user_profile.nombre} " />
                     <#else>
                     	<input class=" form-control readonly-field" id="cname" name="nombre" minlength="2" type="text" readonly value="No posee" />
                     </#if>
                    </div>
                  </div>
                  
                 <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Apellido</label>
                    <div class="col-lg-10">
                     <#if user_profile?? && user_profile.apellido?? && user_profile.apellido?has_content>
                    	<input class=" form-control readonly-field" id="cname" name="apellido" minlength="2" type="text" readonly value="${user_profile.apellido} " />
                     <#else>
                     	<input class=" form-control readonly-field" id="cname" name="apellido" minlength="2" type="text" readonly value="No posee" />
                     </#if>
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Fecha nacimiento</label>
                    <div class="col-lg-10">
                     <#if empleado?? && empleado.fecha_nacimiento?? && empleado.fecha_nacimiento?has_content>
                    	<input class=" form-control readonly-field" id="cname" name="fecha_nacimiento" minlength="2" type="text" readonly  value="${empleado.fecha_nacimiento}"/>
                     <#else>
                     	<input class=" form-control readonly-field" id="cname" name="fecha_nacimiento" minlength="2" type="text" readonly  value="No posee"/>
                     </#if>
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Genero</label>
                    <div class="col-lg-10">
                	<#if empleado?? && empleado.genero?? && empleado.genero?has_content>
	                      <#if empleado.genero == 1> 
	                      	<input class=" form-control readonly-field" id="cname" name="genero" minlength="2" type="text" readonly value="Mujer" />
	                      <#else>
	                        <input class=" form-control readonly-field" id="cname" name="genero" minlength="2" type="text" readonly value="Hombre" />
	                      </#if>
                     <#else>
                       <input class=" form-control readonly-field" id="cname" name="genero" minlength="2" type="text" readonly value="No posee" />
                     </#if>
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Observaciones</label>
                    <div class="col-lg-10">
                    	<#if empleado?? && empleado.observaciones?? && empleado.observaciones?has_content>
                    	  <input class=" form-control readonly-field" id="cname" name="observaciones" minlength="2" type="text" readonly value="${empleado.observaciones}" />
                    	<#else>
                    	  <input class=" form-control readonly-field" id="cname" name="observaciones" minlength="2" type="text" readonly value="No posee" />
                    	</#if>  
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Direcci&oacute;n</label>
                    <div class="col-lg-10">
                       <#if user_profile?? && user_profile.direccion?? && user_profile.direccion?has_content>
                    	<input class=" form-control readonly-field" id="cname" name="direccion" minlength="2" type="text" readonly value="${user_profile.direccion}" />
                       <#else>
                       	<input class=" form-control readonly-field" id="cname" name="direccion" minlength="2" type="text" readonly value="No posee" />
                       </#if>	
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Tel&eacute;fono</label>
                    <div class="col-lg-10">
                      <#if user_profile?? && user_profile.telefono?? && user_profile.telefono?has_content>
                    	<input class=" form-control readonly-field" id="cname" name="telefono" minlength="2" type="text" readonly value="${user_profile.telefono}" />
                      <#else>
                      	<input class=" form-control readonly-field" id="cname" name="telefono" minlength="2" type="text" readonly value="No posee" />
                      </#if>
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Twitter</label>
                    <div class="col-lg-10">
                    	<#if user_profile?? && user_profile.url_twitter?? && user_profile.url_twitter?has_content>
                    		<input class=" form-control readonly-field" id="cname" name="url_twitter" minlength="2" type="text" readonly value="${user_profile.url_twitter}" />
                    	<#else>
                    		<input class=" form-control readonly-field" id="cname" name="url_twitter" minlength="2" type="text" readonly value="No posee" />
                    	</#if>
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Instagram</label>
                    <div class="col-lg-10">
                      <#if user_profile?? && user_profile.url_instagram?? && user_profile.url_instagram?has_content>
                    	<input class=" form-control readonly-field" id="cname" name="url_instagram" minlength="2" type="text" readonly value="${user_profile.url_instagram}"/>
                      <#else>
                     	 <input class=" form-control readonly-field" id="cname" name="url_instagram" minlength="2" type="text" readonly value="No posee"/>
                      </#if>
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Facebook</label>
                    <div class="col-lg-10">
                    <#if user_profile?? && user_profile.url_facebook?? && user_profile.url_facebook?has_content>
                    	<input class=" form-control readonly-field" id="cname" name="url_facebook" minlength="2" type="text" readonly value="${user_profile.url_facebook}" />
                    <#else>
                    	<input class=" form-control readonly-field" id="cname" name="url_facebook" minlength="2" type="text" readonly value="No posee" />
                    </#if>
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Pais</label>
                    <div class="col-lg-10">
                    <#if usuario?? && usuario.pais?? && usuario.pais?has_content>
                      <input class=" form-control readonly-field" id="cname" name="pais" minlength="2" type="text" readonly value="${usuario.pais.getNombre()}" />
                    <#else>
                      <input class=" form-control readonly-field" id="cname" name="pais" minlength="2" type="text" readonly value="No posee" />
                    </#if>
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Correo electr&oacute;nico</label>
                    <div class="col-lg-10">
                    <#if usuario?? && usuario.correo?? && usuario.correo?has_content>
                      <input class=" form-control readonly-field" id="cname" name="correo" minlength="2" type="text" required readonly value="${usuario.correo}"/>
                    <#else>
                      <input class=" form-control readonly-field" id="cname" name="correo" minlength="2" type="text" required readonly value="No posee"/>
                    </#if>
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Rol</label>
                    <div class="col-lg-10">
                      <input class=" form-control readonly-field" id="cname" name="status" minlength="2" type="text" readonly value="Empleado"/>
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Estado del registro</label>
                    <div class="col-lg-10">
                    <#if empleado?? && empleado.status?? && empleado.status?has_content>
	                    <#if empleado.status == 1>
	                      <input class=" form-control readonly-field" id="cname" name="status" minlength="2" type="text" readonly value="Activo"/>
	                    <#else>
	                    	<input class=" form-control readonly-field" id="cname" name="status" minlength="2" type="text" readonly value="Inactivo"/>
	                    </#if>	
	                <#else>
	                	<input class=" form-control readonly-field" id="cname" name="status" minlength="2" type="text" readonly value="No posee"/>
	                </#if>
                    </div>
                  </div>
                  
                 <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                      <a class="btn btn-primary" type="submit" href="../admin/empleado?operacion=modificar&id=${empleado.id}">Modificar</a>
                      <a class="btn btn-theme" type="submit" href="../admin/empleado">Salir</a>
                    </div>
                  </div>
                </div>
                  
                </form>
              </div>
            </div>
            <!-- /form-panel -->
          </div>
          <!-- /col-lg-12 -->
        </div>
        <!-- /row -->
      </section>
    </section>
    <!--main content end-->
    <!--footer start-->
    <!-- footer class="site-footer custom-site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Dashio</strong>. All Rights Reserved
        </p>
        <div class="credits">
          Created with Dashio template by <a href="https://templatemag.com/">TemplateMag</a>
        </div>
        <a href="index.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer-->
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="../dashboard/lib/jquery/jquery.min.js"></script>

  <script src="../dashboard/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="../dashboard/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="../dashboard/lib/jquery.scrollTo.min.js"></script>
  <script src="../dashboard/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="../dashboard/lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="../dashboard/lib/common-scripts.js"></script>
  <script type="text/javascript" src="../dashboard/lib/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="../dashboard/lib/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="../dashboard/lib/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script type="text/javascript" src="../dashboard/lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript" src="../dashboard/lib/bootstrap-daterangepicker/moment.min.js"></script>
  <script type="text/javascript" src="../dashboard/lib/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  
  <!--script for this page-->
  <script src="../dashboard/lib/sparkline-chart.js"></script>
  <script src="../dashboard/lib/zabuto_calendar.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      var unique_id = $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: 'Welcome to Dashio!',
        // (string | mandatory) the text inside the notification
        text: 'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo.',
        // (string | optional) the image to display on the left
        image: '../dashboard/img/ui-sam.jpg',
        // (bool | optional) if you want it to fade out on its own or just sit there
        sticky: false,
        // (int | optional) the time you want it to be alive for before fading out
        time: 8000,
        // (string | optional) the class name you want to apply to that specific message
        class_name: 'my-sticky-class'
      });

      return false;
    });
  </script>
  <script type="application/javascript">
    $(document).ready(function() {
      $("#date-popover").popover({
        html: true,
        trigger: "manual"
      });
      $("#date-popover").hide();
      $("#date-popover").click(function(e) {
        $(this).hide();
      });

      $("#my-calendar").zabuto_calendar({
        action: function() {
          return myDateFunction(this.id, false);
        },
        action_nav: function() {
          return myNavFunction(this.id);
        },
        ajax: {
          url: "show_data.php?action=1",
          modal: true
        },
        legend: [{
            type: "text",
            label: "Special event",
            badge: "00"
          },
          {
            type: "block",
            label: "Regular event",
          }
        ]
      });
    });

    function myNavFunction(id) {
      $("#date-popover").hide();
      var nav = $("#" + id).data("navigation");
      var to = $("#" + id).data("to");
      console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
  </script>
</body>

</html>
