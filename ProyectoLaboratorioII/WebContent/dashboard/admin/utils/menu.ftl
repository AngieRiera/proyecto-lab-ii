  <!-- Favicons -->
  <link href="../dashboard/img/favicon.png" rel="icon">
  <link href="../dashboard/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Bootstrap core CSS -->
  <link href="../dashboard/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--external css-->
  <link href="../dashboard/lib/font-awesome/css/font-awesome.css" rel="stylesheet" />
  <link href="../dashboard/lib/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="../dashboard/lib/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="../dashboard/lib/advanced-datatable/css/DT_bootstrap.css" />
  <!-- Custom styles for this template -->
  <link href="../dashboard/css/style.css" rel="stylesheet">
  <link href="../dashboard/css/style-responsive.css" rel="stylesheet">
  
    <link rel="stylesheet" type="text/css" href="../dashboard/lib/bootstrap-fileupload/bootstrap-fileupload.css" />
  <link rel="stylesheet" type="text/css" href="../dashboard/lib/bootstrap-datepicker/css/datepicker.css" />
  <link rel="stylesheet" type="text/css" href="../dashboard/lib/bootstrap-daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" type="text/css" href="../dashboard/lib/bootstrap-timepicker/compiled/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="../dashboard/lib/bootstrap-datetimepicker/datertimepicker.css" />   
  
    <!-- Estilos custom -->
  <link href="../dashboard/css/custom.css" rel="stylesheet">

</head>
<body>
  <section id="container" class="section-full-size">
    <!-- **********************************************************************************************************************************************************
        TOP BAR CONTENT & NOTIFICATIONS
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->

      <!--logo end-->
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
        <li><a class="logout custom-logout" href="/ProyectoLaboratorioII">Cerrar Sesi&oacute;n</a></li>
          <li><a class="logout custom-logout" href="/ProyectoLaboratorioII">Salir</a></li>
        </ul>
      </div>
    </header>
    <!--header end-->
    <!-- **********************************************************************************************************************************************************
        MAIN SIDEBAR MENU
        *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse position-sidebar">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="profile.html"><img src="../dashboard/img/ui-sam.jpg" class="img-circle" width="80"></a></p>
          <h5 class="centered">${usuario.getUserProfile().getNombre()} ${usuario.getUserProfile().getApellido()}</h5>
          <li class="mt">
            <a class="active" href="/ProyectoLaboratorioII/admin">
              <i class="fa fa-dashboard"></i>
              <span>Dashboard</span>
              </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-th"></i>
              <span>Archivo</span>
              </a>
            <ul class="sub">
              <li><a href="/ProyectoLaboratorioII/admin/usuario">Gesti&oacute;n de usuarios</a></li>
              <li><a href="/ProyectoLaboratorioII/admin/empleado">Gesti&oacute;n de empleados</a></li>
              <li><a href="/ProyectoLaboratorioII/admin/empresa">Gesti&oacute;n de empresas</a></li>
              <li><a href="/ProyectoLaboratorioII/admin/jornada">Gesti&oacute;n de jornadas</a></li>
              <li><a href="/ProyectoLaboratorioII/admin/sector-actividad">Gesti&oacute;n de sectores A.</a></li>
              <li><a href="/ProyectoLaboratorioII/admin/pais">Gesti&oacute;n de pa&iacute;ses</a></li>
              <li><a href="/ProyectoLaboratorioII/admin/estado">Gesti&oacute;n de estados</a></li>
              <li><a href="/ProyectoLaboratorioII/admin/profesion">Gesti&oacute;n de profesiones</a></li>
              <li><a href="/ProyectoLaboratorioII/admin/rol">Gesti&oacute;n de roles</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class=" fa fa-bar-chart-o"></i>
              <span>Estadisticas</span>
              </a>
          </li>
        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->