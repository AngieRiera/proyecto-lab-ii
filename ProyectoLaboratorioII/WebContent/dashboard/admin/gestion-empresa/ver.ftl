<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Dashboard - Administrador</title>
  <#include "../utils/menu.ftl">
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">

		<div class="row mt">
          <div class="col-lg-12">
            <h4><i class="fa fa-angle-right"></i> Datos de la empresa</h4>
            <div class="form-panel">
              <div class=" form">
                <div class="cmxform form-horizontal style-form" id="commentForm">
                  <div class="form-group ">
                    <label for="cemail" class="control-label col-lg-2">Correo electr&oacute;nico</label>
                    <div class="col-lg-10">
                      <input class="form-control readonly-field" id="cemail" type="email" value="${empresa.correoUsuario()}" name="correo" readonly />
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="ccomment" class="control-label col-lg-2">Pa�s</label>
                    <div class="col-lg-10">
                      <input class=" form-control readonly-field" id="cname" name="rif" minlength="2" type="text" value="${empresa.getUsuario().nombrePais()}" readonly />
                    </div>
                  </div>
                                   
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Rif</label>
                    <div class="col-lg-10">
                      <input class=" form-control readonly-field" id="cname" name="rif" minlength="2" type="text" value="${empresa.rif}" readonly />
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Direccion</label>
                    <div class="col-lg-10">
                      <input class=" form-control readonly-field" id="cname" name="direccion" minlength="2" type="text" value="${empresa.direccion}" readonly />
                    </div>
                  </div>
                  
                  <div class="form-group ">
                    <label for="ccomment" class="control-label col-lg-2">Sector de actividad</label>
                    <div class="col-lg-10">
                      <input class=" form-control readonly-field" id="cname" name="sector_actividad" minlength="2" type="text" value="${empresa.nombreSectorActivida()}" readonly />
                    </div>
                  </div>
                   
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Nombre</label>
                    <div class="col-lg-10">
                      <input class=" form-control readonly-field" id="cname" name="nombre" minlength="2" type="text" value="${empresa.getUsuario().getUserProfile().getNombre()}" readonly />
                    </div>
                  </div>

                  
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">tel&eacute;fono</label>
                    <div class="col-lg-10">
                      <input class=" form-control readonly-field" id="cname" name="telefono" minlength="2" type="text" value="${empresa.getUsuario().getUserProfile().getTelefono()}" readonly />
                    </div>
                  </div>

                  <div class="form-group ">
                    <label for="ccomment" class="control-label col-lg-2">Estado del registro</label>
                    <div class="col-lg-10">
                      <input class=" form-control readonly-field" id="cname" name="name" minlength="2" type="text" value="${empresa.statusToString()}" readonly />
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                      <a class="btn btn-primary" type="submit" href="/ProyectoLaboratorioII/admin/empresa?operacion=modificar&id=${empresa.id}">Modificar</a>

                      <a class="btn btn-theme" type="submit" href="/ProyectoLaboratorioII/admin/empresa">Salir</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /form-panel -->
          </div>
          <!-- /col-lg-12 -->
        </div>
        <!-- /row -->
      </section>
    </section>
    <!--main content end-->
    <!--footer start-->
    <!-- footer class="site-footer custom-site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Dashio</strong>. All Rights Reserved
        </p>
        <div class="credits">
          Created with Dashio template by <a href="https://templatemag.com/">TemplateMag</a>
        </div>
        <a href="index.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer-->
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="../dashboard/lib/jquery/jquery.min.js"></script>

  <script src="../dashboard/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="../dashboard/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="../dashboard/lib/jquery.scrollTo.min.js"></script>
  <script src="../dashboard/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="../dashboard/lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="../dashboard/lib/common-scripts.js"></script>

  
  <!--script for this page-->
  <script src="../dashboard/lib/sparkline-chart.js"></script>
  <script src="../dashboard/lib/zabuto_calendar.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      var unique_id = $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: 'Welcome to Dashio!',
        // (string | mandatory) the text inside the notification
        text: 'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo.',
        // (string | optional) the image to display on the left
        image: '../dashboard/img/ui-sam.jpg',
        // (bool | optional) if you want it to fade out on its own or just sit there
        sticky: false,
        // (int | optional) the time you want it to be alive for before fading out
        time: 8000,
        // (string | optional) the class name you want to apply to that specific message
        class_name: 'my-sticky-class'
      });

      return false;
    });
  </script>
  <script type="application/javascript">
    $(document).ready(function() {
      $("#date-popover").popover({
        html: true,
        trigger: "manual"
      });
      $("#date-popover").hide();
      $("#date-popover").click(function(e) {
        $(this).hide();
      });

      $("#my-calendar").zabuto_calendar({
        action: function() {
          return myDateFunction(this.id, false);
        },
        action_nav: function() {
          return myNavFunction(this.id);
        },
        ajax: {
          url: "show_data.php?action=1",
          modal: true
        },
        legend: [{
            type: "text",
            label: "Special event",
            badge: "00"
          },
          {
            type: "block",
            label: "Regular event",
          }
        ]
      });
    });

    function myNavFunction(id) {
      $("#date-popover").hide();
      var nav = $("#" + id).data("navigation");
      var to = $("#" + id).data("to");
      console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
  </script>
</body>

</html>
