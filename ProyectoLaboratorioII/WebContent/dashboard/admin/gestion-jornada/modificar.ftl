<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Dashboard">
  <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <title>Dashboard - Administrador</title>
	<#include "../utils/menu.ftl">
    <!-- **********************************************************************************************************************************************************
        MAIN CONTENT
        *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">

		<div class="row mt">
          <div class="col-lg-12">
            <h4><i class="fa fa-angle-right"></i> Modificaci&oacute;n de datos de jornada</h4>
            <div class="form-panel">  
              <div class=" form">
              
                <form class="cmxform form-horizontal style-form" id="commentForm" method="post" action="../admin/jornada?operacion=guardar&id=${jornada.id}">
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Nombre</label>
                    <div class="col-lg-10">
                      <input class=" form-control" id="cname" name="nombre" minlength="2" type="text" required value="${jornada.nombre}"/>
                    </div>
                  </div>                 
                  <div class="form-group ">
                    <label for="cname" class="control-label col-lg-2">Descripcion</label>
                    <div class="col-lg-10">
                      <input class=" form-control" id="cname" name="descripcion" minlength="2" type="text" required value="${jornada.descripcion}"/>
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="ccomment" class="control-label col-lg-2">Estado del registro</label>
                    <div class="col-lg-10">
                      <select class="form-control" name="status">
		                  <option value=1 >Activo</option>
		                  <option value=2>Inactivo</option>
		               </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                      <button class="btn btn-theme" type="submit">Guardar cambios</button>
                      <a class="btn btn-theme04" type="button" href="../admin/jornada">Cancelar</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <!-- /form-panel -->
          </div>
          <!-- /col-lg-12 -->
        </div>
        <!-- /row -->
      </section>
    </section>
    <!--main content end-->
    <!--footer start-->
    <!-- footer class="site-footer custom-site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Dashio</strong>. All Rights Reserved
        </p>
        <div class="credits">
          Created with Dashio template by <a href="https://templatemag.com/">TemplateMag</a>
        </div>
        <a href="index.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer-->
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="../dashboard/lib/jquery/jquery.min.js"></script>

  <script src="../dashboard/lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="../dashboard/lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="../dashboard/lib/jquery.scrollTo.min.js"></script>
  <script src="../dashboard/lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="../dashboard/lib/jquery.sparkline.js"></script>
  <!--common script for all pages-->
  <script src="../dashboard/lib/common-scripts.js"></script>

  
  <!--script for this page-->
  <script src="../dashboard/lib/sparkline-chart.js"></script>
  <script src="../dashboard/lib/zabuto_calendar.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      var unique_id = $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: 'Welcome to Dashio!',
        // (string | mandatory) the text inside the notification
        text: 'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo.',
        // (string | optional) the image to display on the left
        image: '../dashboard/img/ui-sam.jpg',
        // (bool | optional) if you want it to fade out on its own or just sit there
        sticky: false,
        // (int | optional) the time you want it to be alive for before fading out
        time: 8000,
        // (string | optional) the class name you want to apply to that specific message
        class_name: 'my-sticky-class'
      });

      return false;
    });
  </script>
  <script type="application/javascript">
    $(document).ready(function() {
      $("#date-popover").popover({
        html: true,
        trigger: "manual"
      });
      $("#date-popover").hide();
      $("#date-popover").click(function(e) {
        $(this).hide();
      });

      $("#my-calendar").zabuto_calendar({
        action: function() {
          return myDateFunction(this.id, false);
        },
        action_nav: function() {
          return myNavFunction(this.id);
        },
        ajax: {
          url: "show_data.php?action=1",
          modal: true
        },
        legend: [{
            type: "text",
            label: "Special event",
            badge: "00"
          },
          {
            type: "block",
            label: "Regular event",
          }
        ]
      });
    });

    function myNavFunction(id) {
      $("#date-popover").hide();
      var nav = $("#" + id).data("navigation");
      var to = $("#" + id).data("to");
      console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
  </script>
</body>

</html>
