package daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import daos.utils.GenericDAO;
import modelo.Pais;

public class PaisDAO extends GenericDAO {
	
	private static PaisDAO instancia;
	
	public static PaisDAO getInstancia() {
		if (instancia == null) {
			instancia = new PaisDAO();
		}
		return instancia;
	}
	
	public PaisDAO() {
		// TODO Auto-generated constructor stub
		super();
	}

	
	@SuppressWarnings("unchecked")
	public List<Pais> queryAll() {
		return super.queryAll(Pais.class);
	}
	
	public Pais get(Serializable id) {
		return (Pais)super.get(Pais.class, id);
	}

	public void save(Pais pais) {
		super.save(pais);
	}

	public void update(Pais pais) {
		super.update(pais);
	}

	public void saveOrUpdate(Pais pais) {
		super.saveOrUpdate(pais);
	}

	public void delete(Pais pais) {
		super.delete(pais);
	}


}

