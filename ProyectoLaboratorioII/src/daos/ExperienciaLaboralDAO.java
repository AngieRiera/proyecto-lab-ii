package daos;

import java.io.Serializable;
import java.util.List;

import modelo.ExperienciaLaboral;
import modelo.dao.utils.GenericDAO;

public class ExperienciaLaboralDAO extends GenericDAO{
	
	private static ExperienciaLaboralDAO instancia;	
	public ExperienciaLaboralDAO getInstancia() {
		if (instancia == null) {
			instancia = new ExperienciaLaboralDAO();
		}
		return instancia;
	}

	/*private EmpresaDao() {
		super();
	}*/
	
	public ExperienciaLaboralDAO() {
		
	}

	public List<ExperienciaLaboral> queryAll() {
		return super.queryAll(ExperienciaLaboral.class);
	}

	public ExperienciaLaboral get(Serializable id) {
		return (ExperienciaLaboral)super.get(ExperienciaLaboral.class, id);
	}

	public void save(ExperienciaLaboral experienciaLaboral) {
		super.save(experienciaLaboral);
	}

	public void update(ExperienciaLaboral experienciaLaboral) {
		super.update(experienciaLaboral);
	}

	public void saveOrUpdate(ExperienciaLaboral experienciaLaboral) {
		super.saveOrUpdate(experienciaLaboral);
	}

	public void delete(ExperienciaLaboral experienciaLaboral) {
		super.delete(experienciaLaboral);
	}
}
