package daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import modelo.dao.utils.*;
import modelo.CursoRealizado;

public class CursoRealizadoDAO extends GenericDAO {
	
	private static CursoRealizadoDAO instancia;
	
	public static CursoRealizadoDAO getInstancia() {
		if (instancia == null) {
			instancia = new CursoRealizadoDAO();
		}
		return instancia;
	}
	
	public CursoRealizadoDAO() {
		// TODO Auto-generated constructor stub
		super();
	}

	@SuppressWarnings("unchecked")
	public List<CursoRealizado> queryAll() {
		return super.queryAll(CursoRealizado.class);
	}
	
	public CursoRealizado get(Serializable id) {
		return (CursoRealizado)super.get(CursoRealizado.class, id);
	}

	public void save(CursoRealizado curso_realizado) {
		super.save(curso_realizado);
	}

	public void update(CursoRealizado curso_realizado) {
		super.update(curso_realizado);
	}

	public void saveOrUpdate(CursoRealizado curso_realizado) {
		super.saveOrUpdate(curso_realizado);
	}

	public void delete(CursoRealizado curso_realizado) {
		super.delete(curso_realizado);
	}

}

