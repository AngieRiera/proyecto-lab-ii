package daos;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import daos.utils.GenericDAO;
import modelo.CurriculumEmpresa;
import modelo.Empleado;
import modelo.Postulacion;
import modelo.dao.utils.Sesion;

public class CurriculumEmpresaDAO extends GenericDAO{
	
	private static CurriculumEmpresaDAO instancia;
	
	public static CurriculumEmpresaDAO getInstancia() {
		if (instancia == null) {
			instancia = new CurriculumEmpresaDAO();
		}
		return instancia;
	}
	
	public CurriculumEmpresaDAO() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	public List<CurriculumEmpresa> curriculumPorEmpleado(int id_curriculum) throws Exception {
		List<CurriculumEmpresa> curriculums_empresa = new ArrayList<CurriculumEmpresa>();
		Session em = Sesion.getInstancia().openSession();
		
		try {
			curriculums_empresa = (List<CurriculumEmpresa>)em.createCriteria(CurriculumEmpresa.class).setFetchMode("curriculum_empresa", FetchMode.JOIN).add(Restrictions.eq("curriculum.id", id_curriculum)).list();
			System.out.println(curriculums_empresa.size());
		}
		catch(Exception e) {
	         throw new Exception(e.getMessage(),e.getCause());
	    }
		finally {  
			em.close();  
	    }
		
		return curriculums_empresa;
	}
}
	
	

