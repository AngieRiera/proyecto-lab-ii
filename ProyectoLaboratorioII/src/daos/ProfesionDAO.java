package daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import modelo.dao.utils.*;
import modelo.Profesion;

public class ProfesionDAO extends GenericDAO {
	
	private static ProfesionDAO instancia;
	
	public static ProfesionDAO getInstancia() {
		if (instancia == null) {
			instancia = new ProfesionDAO();
		}
		return instancia;
	}
	
	public ProfesionDAO() {
		// TODO Auto-generated constructor stub
		super();
	}

	
	@SuppressWarnings("unchecked")
	public List<Profesion> queryAll() {
		return super.queryAll(Profesion.class);
	}
	
	public Profesion get(Serializable id) {
		return (Profesion)super.get(Profesion.class, id);
	}

	public void save(Profesion profesion) {
		super.save(profesion);
	}

	public void update(Profesion profesion) {
		super.update(profesion);
	}

	public void saveOrUpdate(Profesion profesion) {
		super.saveOrUpdate(profesion);
	}

	public void delete(Profesion profesion) {
		super.delete(profesion);
	}


}

