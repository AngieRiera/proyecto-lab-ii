package daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import modelo.dao.utils.*;
import modelo.Curriculum;
import modelo.Postulacion;

public class PostulacionDAO extends GenericDAO {
	
	private static PostulacionDAO instancia;
	
	public static PostulacionDAO getInstancia() {
		if (instancia == null) {
			instancia = new PostulacionDAO();
		}
		return instancia;
	}
	
	public PostulacionDAO() {
		// TODO Auto-generated constructor stub
		super();
	}

	
	@SuppressWarnings("unchecked")
	public List<Postulacion> queryAll() {
		return super.queryAll(Postulacion.class);
	}
	
	public Postulacion get(Serializable id) {
		return (Postulacion)super.get(Postulacion.class, id);
	}

	public void save(Postulacion postulacion) {
		super.save(postulacion);
	}

	public void update(Postulacion postulacion) {
		super.update(postulacion);
	}

	public void saveOrUpdate(Postulacion postulacion) {
		super.saveOrUpdate(postulacion);
	}

	public void delete(Postulacion postulacion) {
		super.delete(postulacion);
	}
	public Curriculum curriculum(int id_curriculum) throws Exception{
		Curriculum dato = new Curriculum();
		
		Session em = Sesion.getInstancia().openSession();
			
		try {			
//			 (List<OfertaEmpleo>)em.createQuery("from oferta_empleo a where a.id_empresa="+id_empresa+"").list();
			System.out.println(id_curriculum);
			dato=(Curriculum) em.createCriteria(Curriculum.class).setFetchMode("curriculum", FetchMode.JOIN).add(Restrictions.eq("postulacion.id", id_curriculum));
			} 
		catch (Exception e) {             
		       
	         throw new Exception(e.getMessage(),e.getCause());
	        } finally {  
	          em.close();  
	        } 
	       
	        return dato; 
		}
	public boolean confirmarPostulacion(int idOferta, int idEmpleado) throws Exception {
		
		
		Session em = Sesion.getInstancia().openSession();
		Postulacion postulacion= new Postulacion();	
		try {			
//			 (List<OfertaEmpleo>)em.createQuery("from oferta_empleo a where a.id_empresa="+id_empresa+"").list();
			postulacion = (Postulacion) em.createCriteria(Postulacion.class).setFetchMode("postulacion", FetchMode.JOIN).add(Restrictions.eq("empleado.id", idEmpleado)).add(Restrictions.eq("oferta_empleo.id", idOferta)).uniqueResult();
			if(postulacion!= null) {
				
				return true;
			}
			
				
			}catch (Exception e) {             
		      System.out.println(e);
	         throw new Exception(e.getMessage(),e.getCause());
	        } finally {  
	          em.close();  
	        } 
	       
	        return false; 
		
	}
	
	public List<Postulacion> postulaciones(int id_oferta) {
		List<Postulacion> postulaciones = new ArrayList<Postulacion>();
		Session em = Sesion.getInstancia().openSession();
		
		postulaciones = em.createCriteria(Postulacion.class).setFetchMode("postulacion",FetchMode.JOIN).add(Restrictions.eq("oferta_empleo.id", id_oferta)).list();
		return postulaciones;
		
	}

}

