package daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import daos.utils.GenericDAO;
import modelo.UserProfile;
import modelo.Usuario;
import modelo.dao.utils.Sesion;

public class UsuarioDAO extends GenericDAO {
	
	private static UsuarioDAO instancia;
	
	public static UsuarioDAO getInstancia() {
		if (instancia == null) {
			instancia = new UsuarioDAO();
		}
		return instancia;
	}
	
	public UsuarioDAO() {
		// TODO Auto-generated constructor stub
		super();
	}

	
	@SuppressWarnings("unchecked")
	public List<Usuario> queryAll() {
		return super.queryAll(Usuario.class);
	}
	
	public Usuario getUsuarioPorEmail(String correo) {
		Usuario usuario = null;
		Session sesion = Sesion.getInstancia().openSession();
		usuario = (Usuario) sesion.createCriteria(Usuario.class).add(Restrictions.eq("correo", correo)).uniqueResult();
		return usuario;
	}
		
	public Usuario get(Serializable id) {
		return (Usuario)super.get(Usuario.class, id);
	}

	public void save(Usuario usuario) {
		super.save(usuario);
	}

	public void update(Usuario usuario) {
		super.update(usuario);
	}

	public void saveOrUpdate(Usuario usuario) {
		super.saveOrUpdate(usuario);
	}

	public boolean delete(Usuario usuario) {
		return super.delete(usuario);
	}
	
	public Usuario getUsuarioCorreo(String correo) {
		Session session = this.sesion.openSession();
		Usuario usuario = (Usuario)session.createCriteria(Usuario.class).add(Restrictions.eq("correo", correo)).uniqueResult();
		return usuario;
	}


}

