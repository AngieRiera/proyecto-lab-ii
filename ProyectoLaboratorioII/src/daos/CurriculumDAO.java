package daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import modelo.dao.utils.*;
import modelo.Curriculum;
import modelo.CursoRealizado;
import modelo.DetalleEstudio;
import modelo.Empleado;
import modelo.ExperienciaLaboral;
import modelo.Postulacion;

public class CurriculumDAO extends GenericDAO {
	
	private static CurriculumDAO instancia;
	
	public static CurriculumDAO getInstancia() {
		if (instancia == null) {
			instancia = new CurriculumDAO();
		}
		return instancia;
	}
	
	public CurriculumDAO() {
		// TODO Auto-generated constructor stub
		super();
	}

	@SuppressWarnings("unchecked")
	public List<Curriculum> queryAll() {
		return super.queryAll(Curriculum.class);
	}
	
	public Curriculum get(Serializable id) {
		return (Curriculum)super.get(Curriculum.class, id);
	}

	public void save(Curriculum curriculum) {
		super.save(curriculum);
	}

	public void update(Curriculum curriculum) {
		super.update(curriculum);
	}

	public void saveOrUpdate(Curriculum curriculum) {
		super.saveOrUpdate(curriculum);
	}

	public void delete(Curriculum curriculum) {
		super.delete(curriculum);
	}
	
	//
	public Curriculum getCurriculumPorEmpleadoId(int id) {
		Session sesion = Sesion.getInstancia().openSession();
		Curriculum curriculum = (Curriculum) sesion.createCriteria(Curriculum.class).add(Restrictions.eq("empleado.id", id)).uniqueResult();
		sesion.close();
		return curriculum;
		
	}
	
	public List<DetalleEstudio> obtenerDetallesEstudio(int id) throws Exception{
		List<DetalleEstudio> datos = new ArrayList<DetalleEstudio>();
		
		Session em = Sesion.getInstancia().openSession();
			
		try {			
//			 (List<OfertaEmpleo>)em.createQuery("from oferta_empleo a where a.id_empresa="+id_empresa+"").list();
			//System.out.println(id_oferta_empleo);
			datos=(List<DetalleEstudio>)em.createCriteria(DetalleEstudio.class).setFetchMode("detalle_estudio", FetchMode.JOIN).add(Restrictions.eq("curriculum.id", id)).list();
			System.out.println(datos.size());
		} 
		catch (Exception e) {             
		       
	         throw new Exception(e.getMessage(),e.getCause());
	        } finally {  
	          em.close();  
	        } 
	       
	        return datos; 
	}
	
	public List<ExperienciaLaboral> obtenerExperienciasLaborales(int id) throws Exception{
		List<ExperienciaLaboral> datos = new ArrayList<ExperienciaLaboral>();
		
		Session em = Sesion.getInstancia().openSession();
			
		try {			
//			 (List<OfertaEmpleo>)em.createQuery("from oferta_empleo a where a.id_empresa="+id_empresa+"").list();
			//System.out.println(id_oferta_empleo);
			datos=(List<ExperienciaLaboral>)em.createCriteria(ExperienciaLaboral.class).setFetchMode("experiencia_laboral", FetchMode.JOIN).add(Restrictions.eq("curriculum.id", id)).list();
			System.out.println(datos.size());
		} 
		catch (Exception e) {             
		       
	         throw new Exception(e.getMessage(),e.getCause());
	        } finally {  
	          em.close();  
	        } 
	       
	        return datos; 
	}
	
	public List<CursoRealizado> obtenerCursosRealizados(int id) throws Exception{
		List<CursoRealizado> datos = new ArrayList<CursoRealizado>();
		
		Session em = Sesion.getInstancia().openSession();
			
		try {			
//			 (List<OfertaEmpleo>)em.createQuery("from oferta_empleo a where a.id_empresa="+id_empresa+"").list();
			//System.out.println(id_oferta_empleo);
			datos=(List<CursoRealizado>)em.createCriteria(CursoRealizado.class).setFetchMode("curso_realizado", FetchMode.JOIN).add(Restrictions.eq("curriculum.id", id)).list();
			System.out.println(datos.size());
		} 
		catch (Exception e) {             
		       
	         throw new Exception(e.getMessage(),e.getCause());
	        } finally {  
	          em.close();  
	        } 
	       
	        return datos; 
	}
	public DetalleEstudio getDetalleEstudio(int id) {
		System.out.println("id del detalle de estudio:"+ id);
		Session sesion = Sesion.getInstancia().openSession();
		DetalleEstudio detalle_estudio = (DetalleEstudio) sesion.createCriteria(DetalleEstudio.class).add(Restrictions.eq("id", id)).uniqueResult();
		sesion.close();
		return detalle_estudio;
		
	} 

}

