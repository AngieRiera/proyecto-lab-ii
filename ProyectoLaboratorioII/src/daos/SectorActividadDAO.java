package daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import daos.utils.GenericDAO;
import modelo.SectorActividad;;

public class SectorActividadDAO extends GenericDAO {
	
	private static SectorActividadDAO instancia;
	
	public static SectorActividadDAO getInstancia() {
		if (instancia == null) {
			instancia = new SectorActividadDAO();
		}
		return instancia;
	}
	
	public SectorActividadDAO() {
		// TODO Auto-generated constructor stub
		super();
	}

	
	@SuppressWarnings("unchecked")
	public List<SectorActividad> queryAll() {
		return super.queryAll(SectorActividad.class);
	}
	
	public SectorActividad get(Serializable id) {
		return (SectorActividad)super.get(SectorActividad.class, id);
	}

	public void save(SectorActividad sectoractividad) {
		super.save(sectoractividad);
	}

	public void update(SectorActividad sectoractividad) {
		super.update(sectoractividad);
	}

	public void saveOrUpdate(SectorActividad sectoractividad) {
		super.saveOrUpdate(sectoractividad);
	}

	public boolean delete(SectorActividad sectoractividad) {
		return super.delete(sectoractividad);
	}


}

