package daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import daos.utils.GenericDAO;
import modelo.Municipio;

public class MunicipioDAO extends GenericDAO {
	
	private static MunicipioDAO instancia;
	
	public static MunicipioDAO getInstancia() {
		if (instancia == null) {
			instancia = new MunicipioDAO();
		}
		return instancia;
	}
	
	public MunicipioDAO() {
		// TODO Auto-generated constructor stub
		super();
	}

	
	@SuppressWarnings("unchecked")
	public List<Municipio> queryAll() {
		return super.queryAll(Municipio.class);
	}
	
	public Municipio get(Serializable id) {
		return (Municipio)super.get(Municipio.class, id);
	}

	public void save(Municipio municipio) {
		super.save(municipio);
	}

	public void update(Municipio municipio) {
		super.update(municipio);
	}

	public void saveOrUpdate(Municipio municipio) {
		super.saveOrUpdate(municipio);
	}

	public void delete(Municipio municipio) {
		super.delete(municipio);
	}

}

