package daos;

import java.io.Serializable;
import java.util.List;

import modelo.Jornada;
import modelo.dao.utils.GenericDAO;

public class JornadaDAO extends GenericDAO {
	
	private static JornadaDAO instancia;	
	public static JornadaDAO getInstancia() {
		if (instancia == null) {
			instancia = new JornadaDAO();
		}
		return instancia;
	}

	public JornadaDAO() {
		
	}

	public List<Jornada> queryAll() {
		return super.queryAll(Jornada.class);
	}

	public Jornada get(Serializable id) {
		return (Jornada)super.get(Jornada.class, id);
	}

	public void save(Jornada jornada) {
		super.save(jornada);
	}

	public void update(Jornada jornada) {
		super.update(jornada);
	}

	public void saveOrUpdate(Jornada jornada) {
		super.saveOrUpdate(jornada);
	}

	public void delete(Jornada jornada) {
		super.delete(jornada);
	}
	
}