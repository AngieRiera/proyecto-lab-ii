package daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import modelo.dao.utils.*;
import modelo.Especialidad;

public class EspecialidadDAO extends GenericDAO {
	
	private static EspecialidadDAO instancia;
	
	public static EspecialidadDAO getInstancia() {
		if (instancia == null) {
			instancia = new EspecialidadDAO();
		}
		return instancia;
	}
	
	public EspecialidadDAO() {
		// TODO Auto-generated constructor stub
		super();
	}

	@SuppressWarnings("unchecked")
	public List<Especialidad> queryAll() {
		return super.queryAll(Especialidad.class);
	}
	
	public Especialidad get(Serializable id) {
		return (Especialidad)super.get(Especialidad.class, id);
	}

	public void save(Especialidad especialidad) {
		super.save(especialidad);
	}

	public void update(Especialidad especialidad) {
		super.update(especialidad);
	}

	public void saveOrUpdate(Especialidad especialidad) {
		super.saveOrUpdate(especialidad);
	}

	public void delete(Especialidad especialidad) {
		super.delete(especialidad);
	}

}

