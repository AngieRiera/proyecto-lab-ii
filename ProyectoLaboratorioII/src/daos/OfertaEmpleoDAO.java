package daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import modelo.dao.utils.*;
import modelo.OfertaEmpleo;
import modelo.Postulacion;

public class OfertaEmpleoDAO extends GenericDAO {
	
	private static OfertaEmpleoDAO instancia;
	
	public static OfertaEmpleoDAO getInstancia() {
		if (instancia == null) {
			instancia = new OfertaEmpleoDAO();
		}
		return instancia;
	}
	
	public OfertaEmpleoDAO() {
		// TODO Auto-generated constructor stub
		super();
	}

	
	@SuppressWarnings("unchecked")
	public List<OfertaEmpleo> queryAll() {
		return super.queryAll(OfertaEmpleo.class);
	}
	
	public OfertaEmpleo get(Serializable id) {
		return (OfertaEmpleo)super.get(OfertaEmpleo.class, id);
	}

	public void save(OfertaEmpleo oferta_empleo) {
		super.save(oferta_empleo);
	}

	public void update(OfertaEmpleo oferta_empleo) {
		super.update(oferta_empleo);
	}

	public void saveOrUpdate(OfertaEmpleo oferta_empleo) {
		super.saveOrUpdate(oferta_empleo);
	}

	public void delete(OfertaEmpleo oferta_empleo) {
		super.delete(oferta_empleo);
	}
	public List<Postulacion> postulados(int id_oferta_empleo) throws Exception{
		List<Postulacion> datos = new ArrayList<Postulacion>();
		
		Session em = Sesion.getInstancia().openSession();
			
		try {			
//			 (List<OfertaEmpleo>)em.createQuery("from oferta_empleo a where a.id_empresa="+id_empresa+"").list();
			System.out.println(id_oferta_empleo);
			datos=(List<Postulacion>)em.createCriteria(Postulacion.class).setFetchMode("postulacion", FetchMode.JOIN).add(Restrictions.eq("oferta_empleo.id", id_oferta_empleo)).list();
			System.out.println(datos.size());
		} 
		catch (Exception e) {             
		       
	         throw new Exception(e.getMessage(),e.getCause());
	        } finally {  
	          em.close();  
	        } 
	       
	        return datos; 
	}

}

