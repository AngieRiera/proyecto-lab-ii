package daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import modelo.dao.utils.*;
import modelo.Rol;

public class RolDAO extends GenericDAO {
	
	private static RolDAO instancia;
	
	public static RolDAO getInstancia() {
		if (instancia == null) {
			instancia = new RolDAO();
		}
		return instancia;
	}
	
	public RolDAO() {
		// TODO Auto-generated constructor stub
		super();
	}

	
	@SuppressWarnings("unchecked")
	public List<Rol> queryAll() {
		return super.queryAll(Rol.class);
	}
	
	public Rol get(Serializable id) {
		return (Rol)super.get(Rol.class, id);
	}

	public void save(Rol rol) {
		super.save(rol);
	}

	public void update(Rol rol) {
		super.update(rol);
	}

	public void saveOrUpdate(Rol rol) {
		super.saveOrUpdate(rol);
	}

	public void delete(Rol rol) {
		super.delete(rol);
	}


}

