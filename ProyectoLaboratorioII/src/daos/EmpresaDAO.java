package daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import modelo.dao.utils.*;
import modelo.CurriculumEmpresa;
import modelo.Empleado;
import modelo.Empresa;
import modelo.OfertaEmpleo;
import modelo.Postulacion;

public class EmpresaDAO extends GenericDAO {
	
	private static EmpresaDAO instancia;
	
	public static EmpresaDAO getInstancia() {
		if (instancia == null) {
			instancia = new EmpresaDAO();
		}
		return instancia;
	}
	
	public EmpresaDAO() {
		// TODO Auto-generated constructor stub
		super();
	}

	@SuppressWarnings("unchecked")
	public List<Empresa> queryAll() {
		return super.queryAll(Empresa.class);
	}
	
	public Empresa get(Serializable id) {
		return (Empresa)super.get(Empresa.class, id);
	}

	public void save(Empresa empresa) {
		super.save(empresa);
	}

	public void update(Empresa empresa) {
		super.update(empresa);
	}

	public void saveOrUpdate(Empresa empresa) {
		super.saveOrUpdate(empresa);
	}

	public void delete(Empresa empresa) {
		super.delete(empresa);
	}
	public List<OfertaEmpleo> oferta_empleos(int id_empresa) throws Exception{
		List<OfertaEmpleo> datos = new ArrayList<OfertaEmpleo>();
		
		Session em = Sesion.getInstancia().openSession();
			
		try {			
//			 (List<OfertaEmpleo>)em.createQuery("from oferta_empleo a where a.id_empresa="+id_empresa+"").list();
			System.out.println(id_empresa);
			datos=(List<OfertaEmpleo>)em.createCriteria(OfertaEmpleo.class).setFetchMode("oferta_empleo", FetchMode.JOIN).add(Restrictions.eq("empresa.id", id_empresa)).list();
			} 
		catch (Exception e) {             
		       
	         throw new Exception(e.getMessage(),e.getCause());
	        } finally {  
	          em.close();  
	        } 
	       
	        return datos; 
		}
	public Empresa getEmpresaPorUserId(int id) {
		Session sesion = Sesion.getInstancia().openSession();
		Empresa empresa = (Empresa) sesion.createCriteria(Empresa.class).add(Restrictions.eq("usuario.id", id)).uniqueResult();
		return empresa;
	}
	
public boolean confirmarPostulacion(int idCurriculum, int idEmpresa) throws Exception {
		
		
		Session em = Sesion.getInstancia().openSession();
		CurriculumEmpresa curriculum_empresa= new CurriculumEmpresa();	
		try {			
			curriculum_empresa = (CurriculumEmpresa) em.createCriteria(CurriculumEmpresa.class).setFetchMode("curriculum_empresa", FetchMode.JOIN).add(Restrictions.eq("curriculum.id", idCurriculum)).add(Restrictions.eq("empresa.id", idEmpresa)).uniqueResult();
			System.out.println(curriculum_empresa.getId()+"curriculum");
			if(curriculum_empresa!= null) {
				
				return true;
			}		
				
			} 
		catch (Exception e) {             
		      System.out.println(e+"errooor");

	        } finally {  
	          em.close();  
	        }
		return false;
		 
	       
	       
		
	}

}

