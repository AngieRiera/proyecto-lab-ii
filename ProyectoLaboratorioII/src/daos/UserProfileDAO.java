package daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import modelo.dao.utils.*;
import modelo.UserProfile;
import modelo.Usuario;

public class UserProfileDAO extends GenericDAO {
	
	private static UserProfileDAO instancia;
	
	public static UserProfileDAO getInstancia() {
		if (instancia == null) {
			instancia = new UserProfileDAO();
		}
		return instancia;
	}
	
	public UserProfileDAO() {
		// TODO Auto-generated constructor stub
		super();
	}

	
	@SuppressWarnings("unchecked")
	public List<UserProfile> queryAll() {
		return super.queryAll(UserProfile.class);
	}
	
	public UserProfile get(Serializable id) {
		return (UserProfile)super.get(UserProfile.class, id);
	}
	
	public UserProfile getUsuarioPorId(int id) {
		Session session = Sesion.getInstancia().openSession();
		UserProfile userProfile = (UserProfile)session.createCriteria(UserProfile.class).setFetchMode("user_profile", FetchMode.JOIN).add(Restrictions.eq("usuario.id", id)).uniqueResult();	
		return userProfile;
	}
	
	public UserProfile getUserProfileFromUser(int id) {
		Session session = Sesion.getInstancia().openSession();
		  UserProfile userprofile = (UserProfile)session.createCriteria(UserProfile.class).add(Restrictions.eq("usuario.id", id)).uniqueResult();
		  return userprofile;
	 }

	public void save(UserProfile user_profile) {
		super.save(user_profile);
	}

	public void update(UserProfile user_profile) {
		super.update(user_profile);
	}

	public void saveOrUpdate(UserProfile user_profile) {
		super.saveOrUpdate(user_profile);
	}

	public void delete(UserProfile user_profile) {
		super.delete(user_profile);
	}


}

