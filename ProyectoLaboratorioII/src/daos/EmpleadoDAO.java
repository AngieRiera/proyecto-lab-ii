package daos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import modelo.dao.utils.*;
import modelo.Curriculum;
import modelo.Empleado;
import modelo.Empresa;
import modelo.Postulacion;
import modelo.Usuario;

public class EmpleadoDAO extends GenericDAO {
	
	private static EmpleadoDAO instancia;
	
	public static EmpleadoDAO getInstancia() {
		if (instancia == null) {
			instancia = new EmpleadoDAO();
		}
		return instancia;
	}
	
	public EmpleadoDAO() {
		// TODO Auto-generated constructor stub
		super();
	}

	@SuppressWarnings("unchecked")
	public List<Empleado> queryAll() {
		return super.queryAll(Empleado.class);
	}
	
	public Empleado get(Serializable id) {
		return (Empleado)super.get(Empleado.class, id);
	}

	public void save(Empleado empleado) {
		super.save(empleado);
	}

	public void update(Empleado empleado) {
		super.update(empleado);
	}

	public void saveOrUpdate(Empleado empleado) {
		super.saveOrUpdate(empleado);
	}

	public void delete(Empleado empleado) {
		super.delete(empleado);
	}
	
	//
	public Empleado getEmpleadoPorUserId(int id) {
		Session sesion = Sesion.getInstancia().openSession();
		Empleado empleado = (Empleado) sesion.createCriteria(Empleado.class).add(Restrictions.eq("usuario.id", id)).uniqueResult();
		sesion.close();
		return empleado;
	}
	//Se realiza metodo para obtener empleado por cedula unica
	public Empleado getEmpleadoPorCedula(String cedula) {
		Session sesion = Sesion.getInstancia().openSession();
		Empleado empleado = (Empleado) sesion.createCriteria(Empleado.class).add(Restrictions.eq("cedula", cedula)).uniqueResult();
		return empleado;
	}
	public Curriculum getCurriculumId(int id) {
		Session sesion = Sesion.getInstancia().openSession();
		Curriculum curriculum = (Curriculum) sesion.createCriteria(Curriculum.class).add(Restrictions.eq("empleado.id", id)).uniqueResult();
		sesion.close();
		return curriculum;
	}
	
	public List<Postulacion> postulaciones(int id_empleado) throws Exception{
		List<Postulacion> datos = new ArrayList<Postulacion>();
		
		Session em = Sesion.getInstancia().openSession();
			
		try {			
//			 (List<OfertaEmpleo>)em.createQuery("from oferta_empleo a where a.id_empresa="+id_empresa+"").list();
			System.out.println(id_empleado);
			datos=(List<Postulacion>)em.createCriteria(Postulacion.class).setFetchMode("postulacion", FetchMode.JOIN).add(Restrictions.eq("empleado.id", id_empleado)).list();
			} 
		catch (Exception e) {             
		       
	         throw new Exception(e.getMessage(),e.getCause());
	        } finally {  
	          em.close();  
	        } 
	       
	        return datos; 
		}
	public List<Postulacion> contrataciones(int id_empleado) throws Exception{
		List<Postulacion> datos = new ArrayList<Postulacion>();
		
		Session em = Sesion.getInstancia().openSession();
			
		try {			
//			 (List<OfertaEmpleo>)em.createQuery("from oferta_empleo a where a.id_empresa="+id_empresa+"").list();
			System.out.println(id_empleado);
			datos=(List<Postulacion>)em.createCriteria(Postulacion.class).setFetchMode("postulacion", FetchMode.JOIN).add(Restrictions.eq("empleado.id", id_empleado)).add(Restrictions.eq("status", 2)).list();
			} 
		catch (Exception e) {             
		       
	         throw new Exception(e.getMessage(),e.getCause());
	        } finally {  
	          em.close();  
	        } 
	       
	        return datos; 
		}

}

