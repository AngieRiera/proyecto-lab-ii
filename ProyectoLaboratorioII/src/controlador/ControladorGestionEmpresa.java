package controlador;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import daos.EmpresaDAO;
import daos.PaisDAO;
import daos.RolDAO;
import daos.SectorActividadDAO;
import daos.UserProfileDAO;
import daos.UsuarioDAO;
import modelo.Pais;
import modelo.Rol;
import modelo.SectorActividad;
import modelo.UserProfile;
import modelo.Usuario;
import modelo.Empresa;

public class ControladorGestionEmpresa extends HttpServlet {
	
	private UsuarioDAO usrDao;
	private PaisDAO paisDao;
	private UserProfileDAO userProfileDao;
	private EmpresaDAO empresaDao;
	private SectorActividadDAO sectorActividadDao;
	
	public ControladorGestionEmpresa() {
		usrDao = new UsuarioDAO();
		paisDao = new PaisDAO();
		userProfileDao = new UserProfileDAO();
		empresaDao = new EmpresaDAO();
		sectorActividadDao = new SectorActividadDAO();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub	
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException {
		String operacion = request.getParameter("operacion");
		System.out.println(operacion);
		if(operacion != null) {
			String id = request.getParameter("id");
			if(id != null){
				if(operacion.equals("modificar")) {
					System.out.println("modificar algo");
					Empresa empresa = obtenerUnaEmpresa(Integer.parseInt(id));
					Usuario usuario = empresa.getUsuario();
					System.out.println("id del usuario "+usuario.getId());
					//UserProfile user_profile = userProfileDao.getUserProfileFromUser(Integer.parseInt(id));
					UserProfile user_profile = usuario.getUserProfile();
					request.setAttribute("empresa", empresa);
					request.setAttribute("user_profile", user_profile);
					//System.out.println("Nombre del usuario "+user_profile.getNombre());
					request.setAttribute("paises", obtenerPaises());
					request.setAttribute("sectores_actividad", obtenerSectoresActividad());
					RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-empresa/modificar.ftl");
					rd.forward(request,response);
				}
				else if(operacion.equals("guardarCambio")) {
					GuardarCambio(request, response);
					response.sendRedirect("/ProyectoLaboratorioII/admin/empresa");
				}
				else if(operacion.equals("ver")) {
					Empresa empresa = obtenerUnaEmpresa(Integer.parseInt(id));
					request.setAttribute("empresa",empresa);
					RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-empresa/ver.ftl");
					rd.forward(request,response);
				}
				else if(operacion.equals("eliminar")) {
					Empresa empresa = obtenerUnaEmpresa(Integer.parseInt(id));					
					if(!String.valueOf(empresa).equals("null")) {
						empresa.setStatus(2);
						empresaDao.update(empresa);
						JSONObject message = new JSONObject();
						message.put("type", "success");
						message.put("text", "Se ha eliminado con exito");
						System.out.println("NO Se ejecutó el eliminar");
						request.setAttribute("mensaje", message);
					}
					else {
						JSONObject message = new JSONObject();
						message.put("type", "Error");
						message.put("text", "No se ha podido eliminar debido a que el registro se encuentra relacionado en el sistema");
						System.out.println("NO Se ejecutó el eliminar");
						request.setAttribute("mensaje", message);
					}
					List<Empresa> listadoEmpresas = obtenerEmpresas();
					request.setAttribute("empresas",listadoEmpresas);
					RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-empresa/listado.ftl");
					rd.forward(request,response);
				}
			}
			
			else if(operacion.equals("registrar")) {
				
				request.setAttribute("sectores_actividad", obtenerSectoresActividad());
				request.setAttribute("paises", obtenerPaises());
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-empresa/registrar.ftl");
				rd.forward(request,response);
			}
			else if(operacion.equals("guardarNuevo")) {
				GuardarNuevo(request,response);
				response.sendRedirect("/ProyectoLaboratorioII/admin/empresa");
			}
		}
		else {
			List<Empresa> listadoEmpresas = obtenerEmpresas();
			System.out.println("Cantidad de empresas registradas"+listadoEmpresas.size());
			request.setAttribute("empresas",listadoEmpresas);
			RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-empresa/listado.ftl");
			rd.forward(request,response);
			
			/*RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-usuario/listado.ftl");
			rd.forward(request,response);*/
		}
	
	}
	
	//Metodos cruds
	public void agregarUsuario(Usuario usuario) {
		/*Rol rol = new Rol();
		rol = (Rol) usrDao.get(Rol.class, 6);
		Pais pais = (Pais) usrDao.get(Pais.class, 1);
		Usuario usuario = new Usuario("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);*/
		try {
			usrDao.save(usuario);
			System.out.println("Insertado Exitosamente");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void agregarEmpresa(Empresa empresa) {
		/*Rol rol = new Rol();
		rol = (Rol) usrDao.get(Rol.class, 6);
		Pais pais = (Pais) usrDao.get(Pais.class, 1);
		Usuario usuario = new Usuario("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);*/
		try {
			usrDao.save(empresa);
			System.out.println("Insertado Exitosamente");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void agregarUserProfile(UserProfile user_profile) {
		/*Rol rol = new Rol();
		rol = (Rol) usrDao.get(Rol.class, 6);
		Pais pais = (Pais) usrDao.get(Pais.class, 1);
		Usuario usuario = new Usuario("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);*/
		try {
			usrDao.save(user_profile);
			System.out.println("Insertado Exitosamente");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public List<Empresa> obtenerEmpresas(){
		List<Empresa> lista = empresaDao.queryAll();
		return lista;
	}
	
	public List<Pais> obtenerPaises(){
		List<Pais> lista = paisDao.queryAll();
		return lista;
	}
	
	public List<SectorActividad> obtenerSectoresActividad(){
		List<SectorActividad> lista = sectorActividadDao.queryAll();
		return lista;
	}
	
	public UserProfile obtenerUnUserProfile(int idUserProfile) {
		UserProfile user_profile = userProfileDao.get(idUserProfile);
		return user_profile;
	}
	
	public Empresa obtenerUnaEmpresa(int idEmpresa) {
		Empresa empresa = empresaDao.get(idEmpresa);
		return empresa;
	}
	
	public Usuario obtenerUnUsuario(int idUsuario){
		Usuario usuario = usrDao.get(idUsuario);
		return usuario;
		/*try {
			System.out.println("ID: " + usuario.getId());
			System.out.println("contraseña: " + usuario.getContrasenna());
			System.out.println("contraseña Confirmación: " +usuario.getContrasennaconfirmacion());
			System.out.println("correo: " +usuario.getCorreo());
			System.out.println("ultimo inicio sesion: " +usuario.getUltimoiniciosesion().toString());
			System.out.println("------Datos del Rol--------");
			if(!String.valueOf(usuario.getRol()).equals("null")) {
				System.out.println("Nombre: " + usuario.getRol().getNombre());
				System.out.println("Descripcion: " + usuario.getRol().getDescripcion());
				System.out.println("Status: " + usuario.getRol().getStatus());	
			}else {
				System.out.println("Rol: undefined");	
			}
			System.out.println("-------Datos del Pais------");
			if(!String.valueOf(usuario.getPais()).equals("null")) {
				System.out.println("Nombre: " + usuario.getPais().getNombre());
				System.out.println("Status: " + usuario.getPais().getStatus());
			}else {
				System.out.println("Pais: undefined");
			}
			
			System.out.println("Cargado Exitosamente");
			return usuario;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;*/
	}
	
	public List<Usuario> obtenerUsuarios(){
		try {
			List<Usuario> lista = usrDao.queryAll();
			
			int i=1;
			for (Usuario usuario : lista){
				System.out.println("-------------Datos del Usuario: " + i);
				System.out.println("ID: " + usuario.getId());
				System.out.println("contraseña: " + usuario.getContrasenna());
				System.out.println("contraseña Confirmación: " +usuario.getContrasennaconfirmacion());
				System.out.println("correo: " +usuario.getCorreo());
				System.out.println("ultimo inicio sesion: " +usuario.getUltimoiniciosesion().toString());
				System.out.println("------Datos del Rol--------");
				if(!String.valueOf(usuario.getRol()).equals("null")) {
					System.out.println("Nombre: " + usuario.getRol().getId());
					System.out.println("Descripcion: " + usuario.getRol().getDescripcion());
					System.out.println("Status: " + usuario.getRol().getStatus());	
				}else {
					System.out.println("Rol: undefined");	
				}
				System.out.println("-------Datos del Pais------");
				if(!String.valueOf(usuario.getPais()).equals("null")) {
					System.out.println("Nombre: " + usuario.getPais().getNombre());
					System.out.println("Status: " + usuario.getPais().getStatus());
				}else {
					System.out.println("Pais: "+usuario.nombreRol()+"");
				}
				i++;

			}
			return lista;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void eliminarUsuario(int idUsuario) {
		try {
			Usuario usuario = usrDao.get(idUsuario);
			usrDao.delete(usuario);
			System.out.println("Usuario eliminado exito");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public boolean modificarEmpresa(Empresa empresa) {
		try {
			empresaDao.update(empresa);
			System.out.println("Datos de la empresa modificado exitosamente");
			return true;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean modificarUsuario(Usuario usuario) {
		try {
			usrDao.update(usuario);
			System.out.println("Datos del usuario modificado exitosamente");
			return true;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean modificarUserProfile(UserProfile userProfile) {
		try {
			userProfileDao.update(userProfile);
			System.out.println("Datos de la empresa modificado exitosamente");
			return true;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean modificarGeneral(Empresa empresa, Usuario usuario, UserProfile userProfile) {
		if(modificarUserProfile(userProfile)) {
			System.out.println("la modificación del user profile fue exitosa");
			if(modificarEmpresa(empresa)) {
				System.out.println("la modificación de la empresa fue exitosa");
				if(modificarUsuario(usuario)) {
					System.out.println("la modificación del usuario fue exitosa");
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean GuardarNuevo(HttpServletRequest request, HttpServletResponse response){
		System.out.println("id del usuario "+usrDao.getUsuarioPorEmail("jejoalca7145@gmail.com").getId());
		if(request.getParameter("pais").equals("") ||
			request.getParameter("sector_actividad").equals("")) {
			System.out.println("no se seleccino un select");
		}
		else {
			System.out.println("va a guardar un nuevo registro");
			Usuario usuario = new Usuario();
			usuario.setCorreo(request.getParameter("correo"));
			usuario.setContrasenna(request.getParameter("contrasenia"));
			usuario.setContrasennaconfirmacion(request.getParameter("confirmar-contrasenia"));
			usuario.setUltimoiniciosesion(new Date());
			usuario.setStatus(Integer.parseInt(request.getParameter("status")));
			Rol rol = (Rol) usrDao.get(Rol.class, 3);
			Pais pais = (Pais) usrDao.get(Pais.class, Integer.parseInt(request.getParameter("pais")));
			usuario.setRol(rol);
			usuario.setPais(pais);
			agregarUsuario(usuario);
			
			Empresa empresa = new Empresa();
			empresa.setDireccion(request.getParameter("direccion"));
			empresa.setRif(request.getParameter("rif"));
			empresa.setStatus(Integer.parseInt(request.getParameter("status")));
			SectorActividad sectorActividad = (SectorActividad) sectorActividadDao.get(SectorActividad.class, Integer.parseInt(request.getParameter("sector_actividad")));
			usuario = usrDao.getUsuarioPorEmail(request.getParameter("correo"));
			System.out.println("Id del usuario: "+usuario.getId());
			empresa.setSector_actividad(sectorActividad);
			empresa.setUsuario(usuario);
			agregarEmpresa(empresa);
			
			UserProfile userProfile = new UserProfile();
			userProfile.setNombre(request.getParameter("nombre"));
			userProfile.setDireccion(request.getParameter("direccion"));
			userProfile.setTelefono(request.getParameter("telefono"));
			userProfile.setStatus(Integer.parseInt(request.getParameter("status")));
			userProfile.setUsuario(usuario);
			agregarUserProfile(userProfile);
			//Usuario usuario = new Usuario("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);
		}
		return false;
	}
	
	public boolean GuardarCambio(HttpServletRequest request, HttpServletResponse response){
		if(request.getParameter("sector_actividad").equals("") 
			|| request.getParameter("pais").equals("")) {
			System.out.println("no se selecciono rol");
		}
		else {
			Empresa empresa = obtenerUnaEmpresa(Integer.parseInt(request.getParameter("id")));
			empresa.setDireccion(request.getParameter("direccion"));
			empresa.setRif(request.getParameter("rif"));
			empresa.setStatus(Integer.parseInt(request.getParameter("status")));
			SectorActividad sectorActividad = (SectorActividad) sectorActividadDao.get(SectorActividad.class, Integer.parseInt(request.getParameter("sector_actividad")));
			empresa.setSector_actividad(sectorActividad);
			
			Usuario usuario = empresa.getUsuario();
			usuario.setStatus(Integer.parseInt(request.getParameter("status")));
			Pais pais = (Pais) usrDao.get(Pais.class, Integer.parseInt(request.getParameter("pais")));
			usuario.setPais(pais);
	
			UserProfile userProfile = usuario.getUserProfile();
			userProfile.setNombre(request.getParameter("nombre"));
			userProfile.setDireccion(request.getParameter("direccion"));
			userProfile.setTelefono(request.getParameter("telefono"));
			userProfile.setStatus(Integer.parseInt(request.getParameter("status")));
			
			userProfile.setUsuario(usuario);
			empresa.setUsuario(usuario);
			
			if(modificarGeneral(empresa, usuario, userProfile))
				System.out.println("la modificación fue exitosa");
			//Usuario usuario = new Usuario("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);
		}
		return false;
	}
}
