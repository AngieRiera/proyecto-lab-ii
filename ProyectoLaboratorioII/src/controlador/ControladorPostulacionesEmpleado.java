package controlador;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import daos.EmpleadoDAO;
import daos.EmpresaDAO;
import daos.PaisDAO;
import daos.PostulacionDAO;
import daos.RolDAO;
import daos.UsuarioDAO;
import modelo.Empleado;
import modelo.Empresa;
import modelo.Jornada;
import modelo.OfertaEmpleo;
import modelo.Pais;
import modelo.Postulacion;
import modelo.Profesion;
import modelo.Rol;
import modelo.Usuario;

public class ControladorPostulacionesEmpleado extends HttpServlet {
	
	private PostulacionDAO dao_postulacion;
	private EmpleadoDAO dao_empleado;
	
	public ControladorPostulacionesEmpleado() {
		// TODO Auto-generated constructor stub
		dao_postulacion = new PostulacionDAO();
		dao_empleado = new EmpleadoDAO();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub	
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession misession= (HttpSession) request.getSession();
		Usuario usuario = (Usuario) misession.getAttribute("usuario");
		String operacion = request.getParameter("operacion");
		System.out.println(operacion);
		String id =request.getParameter("id");
		if(operacion!=null) {
			if(operacion.equals("cancelar")) {			
				Postulacion postulacion = dao_postulacion.get(Integer.parseInt(id)) ;
				System.out.print("estatus postulacion: "+ postulacion.getStatus());
				postulacion.setStatus(4);
				dao_postulacion.update(postulacion);
				Empleado empleado = dao_empleado.getEmpleadoPorUserId(usuario.getId());
				request.setAttribute("postulaciones", dao_empleado.postulaciones(empleado.getId()));
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/empleado/listados/listado-postulaciones.ftl");
				rd.forward(request,response);
			}else if(operacion.equals("verCurriculum")){
										
			}else {
				
			}
		}else {		
			Empleado empleado = dao_empleado.getEmpleadoPorUserId(usuario.getId());
			List<Postulacion> postulaciones = dao_empleado.postulaciones(empleado.getId());
			System.out.println("Cantidad de postulaciones del empleado "+postulaciones.size());
			request.setAttribute("postulaciones", postulaciones);
			RequestDispatcher rd=request.getRequestDispatcher("/dashboard/empleado/listados/listado-postulaciones.ftl");
			rd.forward(request,response);
			}
	}
	
}

