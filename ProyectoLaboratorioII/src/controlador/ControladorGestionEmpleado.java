package controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import daos.EmpleadoDAO;
import daos.PaisDAO;
import daos.RolDAO;
import daos.UserProfileDAO;
import daos.UsuarioDAO;
import modelo.Empleado;
import modelo.Jornada;
import modelo.Pais;
import modelo.Rol;
import modelo.UserProfile;
import modelo.Usuario;
import org.json.*;
public class ControladorGestionEmpleado extends HttpServlet {
	
	private EmpleadoDAO empleado_dao;
	private UsuarioDAO usuario_dao;
	
	private PaisDAO pais_dao;
	private RolDAO rol_dao;
	private UserProfileDAO user_profile_dao;
	
	public ControladorGestionEmpleado() {
		empleado_dao = new EmpleadoDAO();
		usuario_dao = new UsuarioDAO();
		pais_dao = new PaisDAO();
		rol_dao = new RolDAO();
		user_profile_dao = new UserProfileDAO();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			try {
				RealizarAccion(request,response);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			try {
				RealizarAccion(request,response);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException {
		String operacion = request.getParameter("operacion");
		System.out.println(operacion);
		if(operacion != null) {
			if(operacion.equals("modificar")) {
				ModificarEmpleado(request, response);
			}
			else if(operacion.equals("ver")) {
				VerEmpleado(request, response);
			}
			else if(operacion.equals("registrar")) {
				List<Pais> lista_paises = pais_dao.queryAll();
				Set<Pais> paises = new LinkedHashSet<Pais>(lista_paises);
				
				List<Rol> lista_roles = rol_dao.queryAll();
				Set<Rol> roles = new LinkedHashSet<Rol>(lista_roles);
				
				request.setAttribute("paises", paises);
				request.setAttribute("roles", roles);
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-empleado/registrar.ftl");
				rd.forward(request,response);
			}
			else if(operacion.equals("guardar")) {
				if(GuardarEmpleado(request, response)) {
					ListaEmpleados(request, response);
				}else {
					List<Empleado> lista_empleados = empleado_dao.queryAll();
					//Se usa coleccion para eliminar datos repetidos por producto cartesiano
					Set<Empleado> empleados = new LinkedHashSet<Empleado>(lista_empleados);
					JSONObject message = new JSONObject();
					message.put("type", "danger");
					message.put("text", "No se han registrado los datos del usuario con exito por fallos en el servidor");
					request.setAttribute("mensaje", message);
					request.setAttribute("empleados", empleados);
					RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-empleado/listado.ftl");
					rd.forward(request,response);
				}
				
			}
			else if(operacion.equals("eliminar")) {
				EliminarEmpleado(request, response);
				ListaEmpleados(request, response);
			}	
		}
		else {
			ListaEmpleados(request, response);
		}
	
	}
	
	public void ListaEmpleados(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException {
		
		List<Empleado> lista_empleados = empleado_dao.queryAll();
		//Se usa coleccion para eliminar datos repetidos por producto cartesiano
		Set<Empleado> empleados = new LinkedHashSet<Empleado>(lista_empleados);
//		List absolutemployees = new ArrayList();
//		
//		for(Empleado empleado:empleados) {
//			Usuario usuario = empleado.getUsuario();
//			UserProfile user_profile = user_profile_dao.getUserProfileFromUser(usuario.getId());
//			
//			JSONObject empl = new JSONObject();
//			empl.put("id", empleado.getId());
//			empl.put("cedula", empleado.getCedula());
//			empl.put("nombre", user_profile.getNombre());
//			empl.put("apellido", user_profile.getApellido());
//			empl.put("genero", empleado.getGenero());
//			empl.put("status", empleado.getStatus());
//			absolutemployees.add(empl);
//			
//		}
		request.setAttribute("empleados", empleados);
		RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-empleado/listado.ftl");
		rd.forward(request,response);
	}
	
	public void VerEmpleado(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		Empleado empleado = empleado_dao.get(Integer.parseInt(request.getParameter("id")));
		
		Usuario usuario = empleado.getUsuario();
		UserProfile user_profile = user_profile_dao.getUserProfileFromUser(usuario.getId());
		
		
		request.setAttribute("empleado", empleado);
		request.setAttribute("usuario", usuario);
		request.setAttribute("user_profile", user_profile);
		RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-empleado/ver.ftl");
		rd.forward(request,response);
	}
	
	public void ModificarEmpleado(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		List<Pais> lista_paises = pais_dao.queryAll();
		Set<Pais> paises = new LinkedHashSet<Pais>(lista_paises);
		
		List<Rol> lista_roles = rol_dao.queryAll();
		Set<Rol> roles = new LinkedHashSet<Rol>(lista_roles);
		
		request.setAttribute("paises", paises);
		request.setAttribute("roles", roles);	
		Empleado empleado = empleado_dao.get(Integer.parseInt(request.getParameter("id")));
		Usuario usuario = empleado.getUsuario();
		UserProfile user_profile = user_profile_dao.getUserProfileFromUser(usuario.getId());
		
		request.setAttribute("empleado", empleado);
		request.setAttribute("usuario", usuario);
		request.setAttribute("user_profile", user_profile);
		RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-empleado/modificar.ftl");
		rd.forward(request,response);
	}
	
	public boolean GuardarEmpleado(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Empleado empleado = new Empleado();
		Usuario usuario = new Usuario();
		UserProfile user_profile = new UserProfile();
		try {
			if(request.getParameter("id") != null) {
					empleado = empleado_dao.get(Integer.parseInt(request.getParameter("id")));
					usuario = empleado.getUsuario();
					user_profile = user_profile_dao.getUserProfileFromUser(usuario.getId());	
					System.out.println(request.getParameter("statusEmpleado"));
//					usuario.setStatus(Integer.parseInt());
					Pais pais = pais_dao.get(Integer.parseInt(request.getParameter("pais")));
					usuario.setPais(pais);
					
					empleado.setCedula(request.getParameter("cedula"));
					String fecha = request.getParameter("fecha_nacimiento");
					String dia = fecha.substring(0, 2);
					String mes = fecha.substring(3,5);
					String anio = fecha.substring(6, 10);
					String fecha_final = dia +"-"+mes+"-"+anio;
					empleado.setFecha_nacimiento(empleado.StringToDate(fecha_final));
					empleado.setGenero(Integer.parseInt(request.getParameter("genero")));
					empleado.setStatus(Integer.parseInt(request.getParameter("statusEmpleado")));
					
					user_profile.setNombre(request.getParameter("nombre"));
					user_profile.setApellido(request.getParameter("apellido"));
					user_profile.setDireccion(request.getParameter("direccion"));
					user_profile.setTelefono(request.getParameter("telefono"));
					user_profile.setUrl_facebook(request.getParameter("url_facebook"));
					user_profile.setUrl_instagram(request.getParameter("url_instagram"));
					user_profile.setUrl_twitter(request.getParameter("url_twitter"));
					user_profile.setStatus(Integer.parseInt(request.getParameter("statusEmpleado")));		
					
					empleado_dao.update(empleado);
					usuario_dao.update(usuario);
					user_profile_dao.update(user_profile);
					JSONObject message = new JSONObject();
					message.put("type", "success");
					message.put("text", "Datos del Empleado modificados satisfactoriamente.");
					request.setAttribute("mensaje", message);
					return true;
				}
			else {			
				if(request.getParameter("contrasenia_emple").equals(request.getParameter("confirmar_contrasenia_emple"))) {
					Usuario user = usuario_dao.getUsuarioPorEmail(request.getParameter("correo"));
					Empleado emple = empleado_dao.getEmpleadoPorCedula(request.getParameter("cedula"));
					if(String.valueOf(user).equals("null") && String.valueOf(emple).equals("null")) {
						usuario.setCorreo(request.getParameter("correo"));
						usuario.setContrasenna(request.getParameter("contrasenia_emple"));
						usuario.setContrasennaconfirmacion(request.getParameter("confirmar_contrasenia_emple"));
						usuario.setStatus(1);
						
						Pais pais = pais_dao.get(Integer.parseInt(request.getParameter("pais")));
						usuario.setPais(pais);
						Rol rol = rol_dao.get(3);
						usuario.setRol(rol);			
						usuario_dao.save(usuario);			
						Usuario usuario_extra = usuario_dao.getUsuarioCorreo(usuario.getCorreo());		
						
						user_profile.setUsuario(usuario_extra);
						user_profile.setNombre(request.getParameter("nombre"));
						user_profile.setApellido(request.getParameter("apellido"));
						user_profile.setDireccion(request.getParameter("direccion"));
						user_profile.setTelefono(request.getParameter("telefono"));
						user_profile.setUrl_facebook(request.getParameter("url_facebook"));
						user_profile.setUrl_instagram(request.getParameter("url_instagram"));
						user_profile.setUrl_twitter(request.getParameter("url_twitter"));
						user_profile.setStatus(1);
						user_profile_dao.save(user_profile);
						
						empleado.setUsuario(usuario_extra);
						empleado.setCedula(request.getParameter("cedula"));
						String fecha = request.getParameter("fecha_nacimiento");
						String dia = fecha.substring(0, 2);
						String mes = fecha.substring(3,5);
						String anio = fecha.substring(6, 10);
						String fecha_final = dia +"-"+mes+"-"+anio;
						System.out.print(fecha_final);
						empleado.setFecha_nacimiento(empleado.StringToDate(fecha_final));
						empleado.setGenero(Integer.parseInt(request.getParameter("genero")));
						empleado.setStatus(1);
						empleado_dao.save(empleado);	
						JSONObject message = new JSONObject();
						message.put("type", "success");
						message.put("text", "Empleado registrado satisfactoriamente.");
						request.setAttribute("mensaje", message);
						return true;
					}else {
						if(!String.valueOf(user).equals("null")) {
							JSONObject message = new JSONObject();
							message.put("type", "danger");
							message.put("text", "El correo ya ha sido tomado por otro usuario.");
							request.setAttribute("mensaje", message);
						}else {
							JSONObject message = new JSONObject();
							message.put("type", "danger");
							message.put("text", "La cedula ya ha sido tomada por otro empleado.");
							request.setAttribute("mensaje", message);
							
						}
						List<Pais> lista_paises = pais_dao.queryAll();
						Set<Pais> paises = new LinkedHashSet<Pais>(lista_paises);
						
						List<Rol> lista_roles = rol_dao.queryAll();
						Set<Rol> roles = new LinkedHashSet<Rol>(lista_roles);
						
						request.setAttribute("paises", paises);
						request.setAttribute("roles", roles);
						RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-empleado/registrar.ftl");
						rd.forward(request,response);
					}
				}
		
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void EliminarEmpleado(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Empleado empleado = empleado_dao.get(Integer.parseInt(request.getParameter("id")));
		Usuario usuario = empleado.getUsuario();
		UserProfile user_profile = user_profile_dao.getUserProfileFromUser(usuario.getId());		
		
		empleado.setStatus(2);
		usuario.setStatus(2);
		user_profile.setStatus(2);
		empleado_dao.update(empleado);
		usuario_dao.update(usuario);
		user_profile_dao.update(user_profile);
	}

}
