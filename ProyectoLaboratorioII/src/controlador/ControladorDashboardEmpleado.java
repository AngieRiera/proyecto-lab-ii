package controlador;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelo.Usuario;

public class ControladorDashboardEmpleado extends HttpServlet{
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RealizarAccion(request, response);
	}
	
	public void RealizarAccion (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession sesion = (HttpSession)request.getSession();
		Usuario usuario = (Usuario) sesion.getAttribute("usuario");
		
		if(usuario != null) {
			if(usuario.getRol().getId() == 2) {
				request.getRequestDispatcher("/dashboard/empleado/index.ftl").forward(request, response);
			}
			else
				request.getRequestDispatcher("/login.ftl").forward(request, response);
		}
		else
			request.getRequestDispatcher("/login.ftl").forward(request, response);
	}
}
