package controlador;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;

import daos.CurriculumDAO;
import daos.EmpleadoDAO;
import daos.UserProfileDAO;
import modelo.Curriculum;
import modelo.CursoRealizado;
import modelo.DetalleEstudio;
import modelo.Empleado;
import modelo.ExperienciaLaboral;
import modelo.Pais;
import modelo.Rol;
import modelo.UserProfile;
import modelo.Usuario;

public class ControladorDetalleEmpleado extends HttpServlet{
	
	private EmpleadoDAO empleadoDao;
	private CurriculumDAO curriculumDao;
	private UserProfileDAO user_profileDao;
	
	public ControladorDetalleEmpleado() {
		empleadoDao = new EmpleadoDAO();
		curriculumDao = new CurriculumDAO();
		user_profileDao = new UserProfileDAO();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			RealizarAccion(request,response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Mostrar(request, response);
		RequestDispatcher rd = request.getRequestDispatcher("/detalleEmpleado.ftl");
		rd.forward(request, response);
	}
	
	public void Mostrar(HttpServletRequest request, HttpServletResponse response) throws Exception{
		Empleado empleado = empleadoDao.get(Integer.parseInt(request.getParameter("id")));
		System.out.println(empleado.getCedula());
		Usuario usuario = empleado.getUsuario();
		Curriculum curriculum = curriculumDao.getCurriculumPorEmpleadoId(empleado.getId());
		
		//List<CursoRealizado> lista_cursos = curriculum.getCursos_realizado();
		List<CursoRealizado> lista_cursos = curriculumDao.obtenerCursosRealizados(curriculum.getId());
		Set<CursoRealizado> cursos = new LinkedHashSet<CursoRealizado>(lista_cursos);
		List<DetalleEstudio> lista_detalles = curriculumDao.obtenerDetallesEstudio(curriculum.getId());
		//List<DetalleEstudio> lista_detalles = curriculum.getDetalles_estudio();
		Set<DetalleEstudio> detalles = new LinkedHashSet<DetalleEstudio>(lista_detalles);
		List<ExperienciaLaboral> lista_experiencias = curriculumDao.obtenerExperienciasLaborales(curriculum.getId());
		//List<ExperienciaLaboral> lista_experiencias = curriculum.getExperiencias_laborales();
		Set<ExperienciaLaboral> experiencias = new LinkedHashSet<ExperienciaLaboral>(lista_experiencias);
		
		System.out.println(usuario.getCorreo());
		request.setAttribute("experiencias", experiencias);
		request.setAttribute("cursos", cursos);
		request.setAttribute("detalles", detalles);
		request.setAttribute("empleado", empleado);
		request.setAttribute("usuario", usuario);
		request.setAttribute("curriculum", curriculum);
		
	}
}
