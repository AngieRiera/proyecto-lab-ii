package controlador;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import daos.EspecialidadDAO;
import daos.PaisDAO;
import daos.ProfesionDAO;
import daos.SectorActividadDAO;
import modelo.Especialidad;
import modelo.Estado;
import modelo.Municipio;
import modelo.Pais;
import modelo.Profesion;
import modelo.SectorActividad;

public class ControladorGestionProfesion extends HttpServlet {
	private ProfesionDAO dao_profesion;
	private EspecialidadDAO dao_especialidad;
	private SectorActividadDAO dao_sector;
	public ControladorGestionProfesion() {
		super();
		this.dao_profesion = ProfesionDAO.getInstancia();
		dao_especialidad = EspecialidadDAO.getInstancia();
		dao_sector = SectorActividadDAO.getInstancia();
		
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RealizarAccion(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RealizarAccion(request,response);
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operacion = request.getParameter("operacion");
		System.out.println(operacion);
		if(operacion != null) {
			String id = request.getParameter("id");
			if(operacion.equals("modificar")) {
				Profesion profesion = dao_profesion.get(Integer.parseInt(id));
				request.setAttribute("profesion",profesion);
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-profesion/modificar.ftl");
				rd.forward(request,response);
			}
			else if(operacion.equals("ver")) {
				Profesion profesion = dao_profesion.get(Integer.parseInt(id));
				request.setAttribute("profesion",profesion);
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-profesion/ver.ftl");
				rd.forward(request,response);
			}
			else if(operacion.equals("registrar")) {				
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-profesion/registrar.ftl");
				rd.forward(request,response);
			}else if(operacion.equals("guardar")) {
				System.out.println("***********");
				profesionNuevo(request, response);
				response.sendRedirect("/ProyectoLaboratorioII/admin/profesion");
			}else if(operacion.equals("guardarCambio")) {
				GuardarCambio(request,response);
				response.sendRedirect("/ProyectoLaboratorioII/admin/profesion");
			}else if(operacion.equals("eliminar")) {
				Profesion profesion = dao_profesion.get(Integer.parseInt(id));
				profesion.setStatus(2);
				modificarProfesion(profesion);
				response.sendRedirect("/ProyectoLaboratorioII/admin/profesion");
			}
		}
		else {
			Set<Profesion> profesiones = new LinkedHashSet<Profesion>(dao_profesion.queryAll());
			System.out.println("cantidad de profesiones: "+profesiones.size());
			request.setAttribute("profesionList", profesiones );			
			RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-profesion/listado.ftl");
			rd.forward(request,response);
		}	
	
	}
	protected void profesionNuevo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("va a guardar un nuevo registro");
		Profesion profesion = new Profesion();
		profesion.setCarrera(request.getParameter("carrera"));
		profesion.setNivel_estudio(request.getParameter("nivel"));
		profesion.setStatus(Integer.parseInt(request.getParameter("status")));
		
		try {
			dao_profesion.save(profesion);
			System.out.println("Insertado Exitosamente");
		}catch(Exception e){
			e.printStackTrace();
		}						
	}
	public void GuardarCambio(HttpServletRequest request, HttpServletResponse response){
		
		Profesion profesion = dao_profesion.get(Integer.parseInt(request.getParameter("id")));
		profesion.setCarrera(request.getParameter("carrera"));
		profesion.setNivel_estudio(request.getParameter("nivel"));		
		profesion.setStatus(Integer.parseInt(request.getParameter("status")));		
		modificarProfesion(profesion);
		//Usuario usuario = new Usuario("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);
	
		}
	public void modificarProfesion(Profesion profesion) {
		try {
			dao_profesion.update(profesion);
			System.out.println("Datos del usuario modificado exitosamente");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
	
