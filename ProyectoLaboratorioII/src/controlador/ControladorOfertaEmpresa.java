package controlador;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import daos.EmpresaDAO;
import daos.EspecialidadDAO;
import daos.EstadoDAO;
import daos.JornadaDAO;
import daos.OfertaEmpleoDAO;
import daos.PaisDAO;
import daos.PostulacionDAO;
import modelo.Empresa;
import modelo.Especialidad;
import modelo.Estado;
import modelo.Jornada;
import modelo.OfertaEmpleo;
import modelo.Pais;
import modelo.Postulacion;
import modelo.Usuario;

public class ControladorOfertaEmpresa extends HttpServlet {

	private EmpresaDAO dao_empresa = new EmpresaDAO() ;
	private OfertaEmpleoDAO dao_oferta = new OfertaEmpleoDAO() ;
	private PostulacionDAO dao_postulado = new PostulacionDAO() ;	
	private PaisDAO dao_pais = new PaisDAO();
	private EstadoDAO dao_estado = new EstadoDAO();
	private JornadaDAO dao_jornada = new JornadaDAO();
	private EspecialidadDAO dao_especialidad = new EspecialidadDAO();
	public ControladorOfertaEmpresa() {
		EmpresaDAO.getInstancia();
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			RealizarAccion(request,response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			RealizarAccion(request,response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String operacion = request.getParameter("operacion");
		System.out.println(operacion);
		if(operacion != null) {
			String id = request.getParameter("id");
			if(operacion.equals("modificar")) {
				OfertaEmpleo oferta = dao_oferta.get(Integer.parseInt(id));
				request.setAttribute("oferta",oferta);
				request.setAttribute("jornadas",obtenerJornadas());
				request.setAttribute("especialidades",obtenerEspecialidades());
				request.setAttribute("paises",obtenerPaises());
				request.setAttribute("estados",obtenerEstados());
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/empresa/ofertaEmpleo/modificar.ftl");
				rd.forward(request,response);
			}
			else if(operacion.equals("ver")) {
				OfertaEmpleo oferta = dao_oferta.get(Integer.parseInt(id));
				request.setAttribute("oferta",oferta);
				

				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/empresa/ofertaEmpleo/ver.ftl");
				rd.forward(request,response);
			}
			else if(operacion.equals("guardar")) {
				
				ofertaNuevo(request,response);
				response.sendRedirect("/ProyectoLaboratorioII/empresa/oferta-empleo");
			}else if(operacion.equals("registrar")) {
				
				request.setAttribute("paisList", obtenerPaises());
				request.setAttribute("estadoList", obtenerEstados());
				request.setAttribute("jornadaList", obtenerJornadas());
				request.setAttribute("especialidadList", obtenerEspecialidades());
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/empresa/ofertaEmpleo/registrar.ftl");
				rd.forward(request,response);
			}
			else if(operacion.equals("verPostulados"))
			{
				request.setAttribute("postuladosList", dao_oferta.postulados(Integer.parseInt(request.getParameter("id"))));
				request.setAttribute("oferta", dao_oferta.get(Integer.parseInt(request.getParameter("id"))) );
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/empresa/ofertaEmpleo/listado-postulados.ftl");
				rd.forward(request,response);
			}else if(operacion.equals("verCurriculum"))
			{
				System.out.println(dao_postulado.curriculum(Integer.parseInt(request.getParameter("id"))).getId());
				System.out.println("*****************************************");
				request.setAttribute("curriculum", dao_postulado.curriculum(Integer.parseInt(request.getParameter("id"))));
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/empresa/ofertaEmpleo/curriculum.ftl");
				rd.forward(request,response);
			}else if(operacion.equals("aceptarPostulacion"))
			{
				Postulacion postulado = dao_postulado.get(Integer.parseInt(request.getParameter("id")));
				System.out.println(postulado.getStatus());
				postulado.setStatus(2);
				dao_postulado.update(postulado);
				request.setAttribute("postuladosList", dao_oferta.postulados(Integer.parseInt(request.getParameter("id_oferta"))));
				request.setAttribute("oferta", dao_oferta.get(Integer.parseInt(request.getParameter("id_oferta"))) );
				
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/empresa/ofertaEmpleo/listado-postulados.ftl");
				rd.forward(request,response);
			}else if(operacion.equals("rechazarPostulacion"))
			{
				Postulacion postulado = dao_postulado.get(Integer.parseInt(request.getParameter("id")));
				System.out.println(postulado.getStatus());
				postulado.setStatus(3);
				dao_postulado.update(postulado);
				request.setAttribute("postuladosList", dao_oferta.postulados(Integer.parseInt(request.getParameter("id_oferta"))));
				request.setAttribute("oferta", dao_oferta.get(Integer.parseInt(request.getParameter("id_oferta"))) );
				
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/empresa/ofertaEmpleo/listado-postulados.ftl");
				rd.forward(request,response);
			}else if(operacion.equals("guardarCambio")) {
				GuardarCambio(request,response);
				response.sendRedirect("/ProyectoLaboratorioII/empresa/oferta-empleo");
			}else if(operacion.equals("eliminar")) {
				OfertaEmpleo oferta = dao_oferta.get(Integer.parseInt(request.getParameter("id")));
				oferta.setStatus(2);
				dao_oferta.update(oferta);
				response.sendRedirect("/ProyectoLaboratorioII/empresa/oferta-empleo");
			}
		}
		else {
			try {
				HttpSession mysesion = request.getSession(true);
				Usuario usuario = (Usuario) mysesion.getAttribute("usuario");
				List<OfertaEmpleo> ofertas_empleos = dao_empresa.oferta_empleos(dao_empresa.getEmpresaPorUserId(usuario.getId()).getId());
				request.setAttribute("ofertaList", ofertas_empleos );
				
			}catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}			
			
			
			RequestDispatcher rd=request.getRequestDispatcher("/dashboard/empresa/ofertaEmpleo/listado.ftl");
			rd.forward(request,response);
		}
	
	}
	public List<Pais> obtenerPaises(){
		
		List<Pais> lista = dao_pais.queryAll();
		System.out.println(lista.get(1).getNombre());
		return lista;		
	}
	public List<Estado> obtenerEstados(){
		List<Estado> lista = dao_estado.queryAll();
		System.out.println(lista.get(1).getNombre());
		return lista;		
	}
	protected void ofertaNuevo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, ParseException {
		HttpSession mysesion = request.getSession(true);
		Usuario usuario = (Usuario) mysesion.getAttribute("usuario");
		OfertaEmpleo oferta = new OfertaEmpleo();
		oferta.setNombre(request.getParameter("nombre"));
		oferta.setFecha_culminacion(oferta.StringToDate(request.getParameter("fecha_culminacion")));
		oferta.setFecha_emision(oferta.StringToDate(request.getParameter("fecha_emision")));
		oferta.setSalario(Float.parseFloat(request.getParameter("salario")));
		oferta.setStatus(Integer.parseInt(request.getParameter("status")));		
		Empresa empresa = dao_empresa.get(dao_empresa.getEmpresaPorUserId(usuario.getId()).getId());		
		oferta.setEmpresa(empresa);
		Jornada jornada = dao_jornada.get(Integer.parseInt(request.getParameter("jornada")));
		oferta.setJornada(jornada);
		Especialidad especialidad = dao_especialidad.get(Integer.parseInt(request.getParameter("especialidad")));
		oferta.setEspecialidad(especialidad);
		Pais pais = dao_pais.get(Integer.parseInt(request.getParameter("pais")));
		oferta.setPais(pais);
		Estado estado = dao_estado.get(Integer.parseInt(request.getParameter("estado")));
		oferta.setEstado(estado);	
		try {
			dao_oferta.save(oferta);
			System.out.println("Insertado Exitosamente");
		}catch(Exception e){
			e.printStackTrace();
		}			
}
	public List<Jornada> obtenerJornadas(){
		List<Jornada> lista = dao_jornada.queryAll();		
		return lista;		
	}
	public List<Especialidad> obtenerEspecialidades(){
		List<Especialidad> lista = dao_especialidad.queryAll();		
		return lista;		
	}
public void GuardarCambio(HttpServletRequest request, HttpServletResponse response){
	HttpSession misession= (HttpSession) request.getSession();
	Usuario usuario = (Usuario) misession.getAttribute("usuario");
	
	OfertaEmpleo oferta = dao_oferta.get(Integer.parseInt(request.getParameter("id")));
	System.out.println("Nombre por busqueda: "+oferta.getNombre());
	oferta.setNombre(request.getParameter("nombre"));
	oferta.setFecha_culminacion(oferta.StringToDate(request.getParameter("fecha_culminacion")));	
	oferta.setFecha_emision(oferta.StringToDate(request.getParameter("fecha_emision")));
	oferta.setSalario(Float.parseFloat(request.getParameter("salario")));
	oferta.setStatus(Integer.parseInt(request.getParameter("status")));	
	
	System.out.println("nombre de la oferta"+oferta.getNombre());
	System.out.println("Fecha culminacion de la oferta"+oferta.getFecha_culminacion());
	System.out.println("fecha emision de la oferta"+oferta.getFecha_emision());
	Empresa empresa = dao_empresa.get(dao_empresa.getEmpresaPorUserId(usuario.getId()).getId());
	System.out.println("nombre de la empresa"+empresa.getUsuario().getCorreo());
	oferta.setEmpresa(empresa);
	Jornada jornada = dao_jornada.get(Integer.parseInt(request.getParameter("jornada")));
	oferta.setJornada(jornada);
	Especialidad especialidad = dao_especialidad.get(Integer.parseInt(request.getParameter("especialidad")));
	oferta.setEspecialidad(especialidad);
	Pais pais = dao_pais.get(Integer.parseInt(request.getParameter("pais")));
	System.out.println("nombre del pais"+pais.getNombre());
	oferta.setPais(pais);
	Estado estado = dao_estado.get(Integer.parseInt(request.getParameter("estado")));
	oferta.setEstado(estado);	
	try {
		dao_oferta.update(oferta);
		System.out.println("Insertado Exitosamente");
	}catch(Exception e){
		e.printStackTrace();
	}
}
}
