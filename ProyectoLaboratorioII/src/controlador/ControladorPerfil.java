package controlador;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import daos.EmpleadoDAO;
import daos.EmpresaDAO;
import daos.PaisDAO;
import daos.SectorActividadDAO;
import daos.UserProfileDAO;
import daos.UsuarioDAO;
import modelo.Empleado;
import modelo.Empresa;
import modelo.Pais;
import modelo.SectorActividad;
import modelo.UserProfile;
import modelo.Usuario;

@MultipartConfig
public class ControladorPerfil extends HttpServlet{
	
	private EmpleadoDAO empleadoDao;
	private EmpresaDAO empresaDao;
	private PaisDAO paisDao;
	private SectorActividadDAO sector_actividadDao;
	private UsuarioDAO usuarioDao;
	private UserProfileDAO user_profileDao;
	private SectorActividadDAO sectorDao;
	
	
	public ControladorPerfil() {
		empleadoDao = new EmpleadoDAO();
		empresaDao = new EmpresaDAO();
		paisDao = new PaisDAO();
		sector_actividadDao = new SectorActividadDAO();
		usuarioDao = new UsuarioDAO();
		user_profileDao = new UserProfileDAO();
		sectorDao = new SectorActividadDAO();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RealizarAccion(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RealizarAccion(request,response);
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operacion = request.getParameter("operacion");
		System.out.println(operacion);
		HttpSession sesion = (HttpSession) request.getSession();
		Usuario usuario = (Usuario) sesion.getAttribute("usuario");	
		
		if(usuario != null) {
			if(operacion !=null) {
				if(operacion.equals("guardar")) {
					Guardar(request, response, usuario);
					Modificar(request, response, usuario);
				}
				else {
					Modificar(request, response, usuario);
				}
			}
			else {
				Modificar(request, response, usuario);
			}
		}
		else {
			RequestDispatcher rd = request.getRequestDispatcher("login.ftl");
			rd.forward(request, response);
		}
	
	}
	
	public void Modificar (HttpServletRequest request, HttpServletResponse response, Usuario usuario) throws ServletException, IOException {
		List<Pais> lista_paises = paisDao.queryAll();
		Set<Pais> paises = new LinkedHashSet<Pais>(lista_paises);
		request.setAttribute("paises", paises);
		
		if(usuario.getRol().getId() == 1 ) {
			request.setAttribute("usuario", usuario);
			RequestDispatcher rd = request.getRequestDispatcher("perfil/perfilAdministrador.ftl");
			rd.forward(request, response);
		}
		else if(usuario.getRol().getId() == 2) {
			Empleado empleado = empleadoDao.getEmpleadoPorUserId(usuario.getId());
			request.setAttribute("empleado", empleado);
			request.setAttribute("usuario", usuario);
			RequestDispatcher rd = request.getRequestDispatcher("perfil/perfilEmpleado.ftl");
			rd.forward(request, response);
		}
		else {
			List<SectorActividad> lista_sectores = sector_actividadDao.queryAll();
			Set<SectorActividad> sectores = new LinkedHashSet<SectorActividad>(lista_sectores);
			Empresa empresa = empresaDao.getEmpresaPorUserId(usuario.getId());
			request.setAttribute("sectores", sectores);
			request.setAttribute("empresa", empresa);
			request.setAttribute("usuario", usuario);
			RequestDispatcher rd = request.getRequestDispatcher("perfil/perfilEmpresa.ftl");
			rd.forward(request, response);
		}

	}
	
	public void Guardar (HttpServletRequest request, HttpServletResponse response, Usuario usuario) throws ServletException, IOException {
		UserProfile user_profile = new UserProfile();
		String path = "C:\\Users\\Angie\\Documents\\Universidad\\Semestre 7\\lab II\\proyecto-lab-ii\\ProyectoLaboratorioII\\WebContent\\path\\uploads";
		
		if(usuario.getRol().getId() == 1) {
			user_profile = usuario.getUserProfile();	
			
			Pais pais = paisDao.get(Integer.parseInt(request.getParameter("pais")));
			usuario.setPais(pais);
			
			user_profile.setNombre(request.getParameter("nombre"));
			user_profile.setApellido(request.getParameter("apellido"));
			user_profile.setDireccion(request.getParameter("direccion"));
			user_profile.setTelefono(request.getParameter("telefono"));
			
			//trae las imagenes
			Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
			String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
			
			//configurar el path de la imagen (colocar la carpeta en webcontent)
			File file = new File(path, fileName); //se coloca el path del directorio en donde se guardara la imagen
			System.out.println(file.getAbsolutePath());
			user_profile.setFoto(fileName);
			
			//guarda la imagen
			try (InputStream input = filePart.getInputStream()) {
			    Files.copy(input, file.toPath());
			}
			
			usuarioDao.update(usuario);
			user_profileDao.update(user_profile);
		}
		else if(usuario.getRol().getId() == 2) {
			Empleado empleado = empleadoDao.getEmpleadoPorUserId(usuario.getId());
			user_profile = usuario.getUserProfile();	
						
			Pais pais = paisDao.get(Integer.parseInt(request.getParameter("pais")));
			usuario.setPais(pais);
			
			empleado.setGenero(Integer.parseInt(request.getParameter("genero")));
			
			user_profile.setNombre(request.getParameter("nombre"));
			user_profile.setApellido(request.getParameter("apellido"));
			user_profile.setDireccion(request.getParameter("direccion"));
			user_profile.setTelefono(request.getParameter("telefono"));
			user_profile.setUrl_facebook(request.getParameter("url_facebook"));
			user_profile.setUrl_instagram(request.getParameter("url_instagram"));
			user_profile.setUrl_twitter(request.getParameter("url_twitter"));		

			//trae las imagenes
			Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
			String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
			
			//configurar el path de la imagen (colocar la carpeta en webcontent)
			File file = new File(path, fileName); //se coloca el path del directorio en donde se guardara la imagen
			System.out.println(file.getAbsolutePath());
			user_profile.setFoto(fileName);
			
			//guarda la imagen
			try (InputStream input = filePart.getInputStream()) {
			    Files.copy(input, file.toPath());
			}
			
			usuarioDao.update(usuario);
			empleadoDao.update(empleado);
			user_profileDao.update(user_profile);
		}
		else {
			Empresa empresa = empresaDao.getEmpresaPorUserId(usuario.getId());
			user_profile = usuario.getUserProfile();
			
			empresa.setSitio_web(request.getParameter("sitio_web"));
			SectorActividad sectorActividad = sector_actividadDao.get(Integer.parseInt(request.getParameter("sector_actividad")));
			empresa.setSector_actividad(sectorActividad);
			
			Pais pais = paisDao.get(Integer.parseInt(request.getParameter("pais")));
			usuario.setPais(pais);
	
			user_profile.setNombre(request.getParameter("nombre"));
			user_profile.setDireccion(request.getParameter("direccion"));
			user_profile.setTelefono(request.getParameter("telefono"));
			user_profile.setUrl_facebook(request.getParameter("url_facebook"));
			user_profile.setUrl_instagram(request.getParameter("url_instagram"));
			user_profile.setUrl_twitter(request.getParameter("url_twitter"));
			
			//trae las imagenes
			Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
			String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
			
			//configurar el path de la imagen (colocar la carpeta en webcontent)
			File file = new File(path, fileName); //se coloca el path del directorio en donde se guardara la imagen
			System.out.println(file.getAbsolutePath());
			user_profile.setFoto(fileName);
			
			//guarda la imagen
			try (InputStream input = filePart.getInputStream()) {
			    Files.copy(input, file.toPath());
			}
			
			empresaDao.update(empresa);
			usuarioDao.update(usuario);
			user_profileDao.update(user_profile);
		}
	}
	
}
