package controlador;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.json.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import daos.SectorActividadDAO;
import modelo.Jornada;
import modelo.SectorActividad;
import modelo.Usuario;

public class ControladorGestionSectorActividad extends HttpServlet {
	
	private SectorActividadDAO sectorActDao;

	
	public ControladorGestionSectorActividad() {
		// TODO Auto-generated constructor stub
		sectorActDao = new SectorActividadDAO();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub	
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException {
		String operacion = request.getParameter("operacion");
		System.out.println(operacion);
		if(operacion != null) {
			String id = request.getParameter("id");
			if(id != null){
				if(operacion.equals("modificar")) {
					SectorActividad sector_actividad = obtenerUnSectorActividad(Integer.parseInt(id));
					request.setAttribute("sector_actividad",sector_actividad);
					RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-sectorActividad/modificar.ftl");
					rd.forward(request,response);
				}
				else if(operacion.equals("guardarCambio")) {
					GuardarCambio(request, response);
					response.sendRedirect("/ProyectoLaboratorioII/admin/sector-actividad");
				}
				else if(operacion.equals("ver")) {
					SectorActividad sector_actividad = obtenerUnSectorActividad(Integer.parseInt(id));
					request.setAttribute("sector_actividad",sector_actividad);
					RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-sectorActividad/ver.ftl");
					rd.forward(request,response);
				}
				else if(operacion.equals("eliminar")) {
					SectorActividad sector_actividad = obtenerUnSectorActividad(Integer.parseInt(id));
					if(!String.valueOf(sector_actividad).equals("null")) {
						sector_actividad.setStatus(2);
						sectorActDao.update(sector_actividad);
						JSONObject message = new JSONObject();
						message.put("type", "success");
						message.put("text", "Se ha eliminado con exito");
						System.out.println("NO Se ejecutó el eliminar");
						request.setAttribute("mensaje", message);
					}
					else {
						JSONObject message = new JSONObject();
						message.put("type", "Error");
						message.put("text", "No se ha podido eliminar debido a que el registro se encuentra relacionado en el sistema");
						System.out.println("NO Se ejecutó el eliminar");
						request.setAttribute("mensaje", message);
					}
					List<SectorActividad> listadoSectorActividad = obtenerSectorActividads();
					Set<SectorActividad> sectores = new LinkedHashSet<SectorActividad>(listadoSectorActividad);
					//System.out.print(listadoSectorActividad.size());
					request.setAttribute("sectores_actividad",sectores);
					RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-sectorActividad/listado.ftl");
					rd.forward(request,response);
					
					
					//response.sendRedirect("/ProyectoLaboratorioII/admin/sector-actividad");
				}
			}
			
			else if(operacion.equals("registrar")) {
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-sectorActividad/registrar.ftl");
				rd.forward(request,response);
			}
			else if(operacion.equals("guardarNuevo")) {
				GuardarNuevo(request,response);
				response.sendRedirect("/ProyectoLaboratorioII/admin/sector-actividad");
			}
		}
		else {
			List<SectorActividad> listadoSectorActividad = obtenerSectorActividads();
			Set<SectorActividad> sectores = new LinkedHashSet<SectorActividad>(listadoSectorActividad);
			System.out.println("cantidad de sectores: "+sectores.size());
			request.setAttribute("sectores_actividad",sectores);
			RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-sectorActividad/listado.ftl");
			rd.forward(request,response);
			
			/*RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-usuario/listado.ftl");
			rd.forward(request,response);*/
		}
	
	}
	
	//Metodos cruds
	public void agregarSectorActividad(SectorActividad sector_actividad) {
		/*Rol rol = new Rol();
		rol = (Rol) usrDao.get(Rol.class, 6);
		Pais pais = (Pais) usrDao.get(Pais.class, 1);
		SectorActividad usuario = new SectorActividad("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);*/
		try {
			sectorActDao.save(sector_actividad);
			System.out.println("Insertado Exitosamente");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public SectorActividad obtenerUnSectorActividad(int idSectorActividad){
		SectorActividad sector_actividad= new SectorActividad();
		sector_actividad = sectorActDao.get(idSectorActividad);
		return sector_actividad;
		/*try {
			/*System.out.println("ID: " + usuario.getId());
			System.out.println("contraseña: " + usuario.getContrasenna());
			System.out.println("contraseña Confirmación: " +usuario.getContrasennaconfirmacion());
			System.out.println("correo: " +usuario.getCorreo());
			System.out.println("ultimo inicio sesion: " +usuario.getUltimoiniciosesion().toString());
			System.out.println("------Datos del Rol--------");
			if(!String.valueOf(usuario.getRol()).equals("null")) {
				System.out.println("Nombre: " + usuario.getRol().getNombre());
				System.out.println("Descripcion: " + usuario.getRol().getDescripcion());
				System.out.println("Status: " + usuario.getRol().getStatus());	
			}else {
				System.out.println("Rol: undefined");	
			}
			System.out.println("-------Datos del Pais------");
			if(!String.valueOf(usuario.getPais()).equals("null")) {
				System.out.println("Nombre: " + usuario.getPais().getNombre());
				System.out.println("Status: " + usuario.getPais().getStatus());
			}else {
				System.out.println("Pais: undefined");
			}
			
			System.out.println("Cargado Exitosamente");
			return usuario;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;*/
	}
	
	public List<SectorActividad> obtenerSectorActividads(){
		try {
			List<SectorActividad> lista = sectorActDao.queryAll();
			
			/*int i=1;
			for (SectorActividad usuario : lista){
				System.out.println("-------------Datos del SectorActividad: " + i);
				System.out.println("ID: " + usuario.getId());
				System.out.println("contraseña: " + usuario.getContrasenna());
				System.out.println("contraseña Confirmación: " +usuario.getContrasennaconfirmacion());
				System.out.println("correo: " +usuario.getCorreo());
				System.out.println("ultimo inicio sesion: " +usuario.getUltimoiniciosesion().toString());
				System.out.println("------Datos del Rol--------");
				if(!String.valueOf(usuario.getRol()).equals("null")) {
					System.out.println("Nombre: " + usuario.getRol().getId());
					System.out.println("Descripcion: " + usuario.getRol().getDescripcion());
					System.out.println("Status: " + usuario.getRol().getStatus());	
				}else {
					System.out.println("Rol: undefined");	
				}
				System.out.println("-------Datos del Pais------");
				if(!String.valueOf(usuario.getPais()).equals("null")) {
					System.out.println("Nombre: " + usuario.getPais().getNombre());
					System.out.println("Status: " + usuario.getPais().getStatus());
				}else {
					System.out.println("Pais: "+usuario.nombreRol()+"");
				}
				i++;

			}*/
			return lista;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void eliminarSectorActividad(int idSectorActividad) {
		try {
			SectorActividad usuario = sectorActDao.get(idSectorActividad);
			sectorActDao.delete(usuario);
			System.out.println("SectorActividad eliminado exito");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void modificarSectorActividad(SectorActividad sector_actividad) {
		try {
			

			sectorActDao.update(sector_actividad);
			System.out.println("Datos del sector acividad modificado exitosamente");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean GuardarNuevo(HttpServletRequest request, HttpServletResponse response){
		/*if(request.getParameter("rol").equals("-1")) {
			System.out.println("no se selecciono rol");
		}
		else {*/
			System.out.println("va a guardar un nuevo registro");
			SectorActividad sector_actividad = new SectorActividad();
			sector_actividad.setNombre(request.getParameter("nombre"));
			sector_actividad.setDescripcion(request.getParameter("descripcion"));
			sector_actividad.setStatus(Integer.parseInt(request.getParameter("status")));
			System.out.println(sector_actividad.getNombre());
			System.out.println(sector_actividad.getDescripcion());
			agregarSectorActividad(sector_actividad);
			//SectorActividad usuario = new SectorActividad("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);
		//}
		return false;
	}
	
	public boolean GuardarCambio(HttpServletRequest request, HttpServletResponse response){
		/*if(request.getParameter("rol").equals("-1") 
			|| request.getParameter("pais").equals("-1")) {
			System.out.println("no se selecciono rol");
		}
		else {*/
			SectorActividad sector_actividad = obtenerUnSectorActividad(Integer.parseInt(request.getParameter("id")));
			sector_actividad.setNombre(request.getParameter("nombre"));
			sector_actividad.setDescripcion(request.getParameter("descripcion"));
			sector_actividad.setStatus(Integer.parseInt(request.getParameter("status")));
			//System.out.println(sector_actividad.getContrasenna()+" "+sector_actividad.getContrasennaconfirmacion());
			modificarSectorActividad(sector_actividad);
			//SectorActividad usuario = new SectorActividad("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);
		//}
		return false;
	}
}
