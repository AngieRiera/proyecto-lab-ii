package controlador;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ControladorGestionMunicipio extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RealizarAccion(request,response);
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operacion = request.getParameter("operacion");
		System.out.println(operacion);
		if(operacion != null) {
			if(operacion.equals("modificar")) {
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-municipio/modificar.ftl");
				rd.forward(request,response);
			}
			else if(operacion.equals("ver")) {

				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-municipio/ver.ftl");
				rd.forward(request,response);
			}
			else if(operacion.equals("registrar")) {
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-municipio/registrar.ftl");
				rd.forward(request,response);
			}
		}
		else {
			RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-municipio/listado.ftl");
			rd.forward(request,response);
		}
	
	}
}
