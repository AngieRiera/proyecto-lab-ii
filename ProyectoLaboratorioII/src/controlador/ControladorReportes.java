package controlador;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServlet;

import daos.EmpleadoDAO;
import modelo.Empleado;
import modelo.Empresa;

public class ControladorReportes extends HttpServlet {
	
	private EmpleadoDAO empleadoDao;
	
	public ControladorReportes() {
		this.empleadoDao = EmpleadoDAO.getInstancia();
	}

	public static void main(String[] args) {
		System.out.println("Promedio: "+ new ControladorReportes().promedioEmpleadosHombres()+" %");
		System.out.println("Cantidad de empresas: "+ new ControladorReportes().cantidadEmpresasActivas()+" empresas");
	}
	
	public float promedioEmpleadosHombres() {
		List<Empleado> lista_empleados = empleadoDao.queryAll();
		Set<Empleado> empleados = new LinkedHashSet<Empleado>(lista_empleados);
		int contHombres = 0;
		float total = empleados.size();
		float promedio = 0;
		for(Empleado empleado : empleados) {
			System.out.println(empleado.GeneroToString());
			if(empleado.GeneroToString().equals("Hombre")) {
				contHombres++;
			}
		}
		if(total>0) {
			promedio = (float)contHombres/total;
			
		}
		return promedio*100;
	}

	public float promedioEmpleadosMujeres() {
		List<Empleado> lista_empleados = empleadoDao.queryAll();
		Set<Empleado> empleados = new LinkedHashSet<Empleado>(lista_empleados);
		int contMujeres= 0;
		float total = empleados.size();
		float promedio = 0;
		for(Empleado empleado : empleados) {
			System.out.println(empleado.GeneroToString());
			if(empleado.GeneroToString().equals("Mujer")) {
				contMujeres++;
			}
		}
		if(total>0) {
			promedio = (float)contMujeres/total;
			
		}
		return promedio*100;
	}
	
	public int cantidadEmpresasActivas() {
		List<Empresa> lista_empresas = empleadoDao.queryAll(Empresa.class);
		Set<Empresa> empresas = new LinkedHashSet<Empresa>(lista_empresas);
		int cont=0;
		for(Empresa empresa:empresas) {
			if(empresa.getStatus() == 1) {
				cont++;
			}
		}
		return cont;
	}
	
}

