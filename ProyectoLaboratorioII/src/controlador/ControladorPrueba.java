package controlador;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.json.JSONException;

import daos.UserProfileDAO;
import daos.UsuarioDAO;
import modelo.UserProfile;
import modelo.Usuario;

@MultipartConfig
public class ControladorPrueba extends HttpServlet{
	

	private UserProfileDAO user_profileDao;
	private UsuarioDAO usuarioDao;
	
	public ControladorPrueba() {
		user_profileDao = new UserProfileDAO();
		usuarioDao = new UsuarioDAO();
	}
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MostrarGuardar(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MostrarGuardar(request, response);
	}
	
	public void MostrarGuardar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String operacion = request.getParameter("operacion");
		
		if(operacion == null) {
			Usuario usuario = usuarioDao.get(34);
			request.setAttribute("usuario", usuario);
			RequestDispatcher rd = request.getRequestDispatcher("/prueba.ftl");
			rd.forward(request, response);
		}
		else {
			GuardarImagen(request, response);
			RequestDispatcher rd = request.getRequestDispatcher("/prueba.ftl");
			rd.forward(request, response);
		}
	}
	
	public void GuardarImagen(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		/*HttpSession sesion = (HttpSession) request.getSession();
		Usuario usuario = (Usuario) sesion.getAttribute("usuario");*/
		
		Usuario usuario = usuarioDao.get(34);
		
		//recibir imagen de la peticion
		Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
		String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
		
		//configurar el path de la imagen (colocar la carpeta en webcontent)
		File file = new File("C:\\Users\\Angie\\Documents\\Universidad\\Semestre 7\\lab II\\proyecto-lab-ii\\ProyectoLaboratorioII\\WebContent\\path\\uploads", fileName); //se coloca el path del directorio en donde se guardara la imagen
		System.out.println(file.getAbsolutePath());
		UserProfile user_profile = usuario.getUserProfile();
		user_profile.setFoto(fileName);
		user_profileDao.update(user_profile);
		
		//guarda la imagen
		try (InputStream input = filePart.getInputStream()) {
		    Files.copy(input, file.toPath());
		}
		
		request.setAttribute("usuario", usuario);
	}
}
