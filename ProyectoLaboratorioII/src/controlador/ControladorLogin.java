package controlador;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import daos.EmpleadoDAO;
import daos.EmpresaDAO;
import daos.EstadoDAO;
import daos.JornadaDAO;
import daos.MunicipioDAO;
import daos.OfertaEmpleoDAO;
import daos.PaisDAO;
import daos.SectorActividadDAO;
import daos.UsuarioDAO;
import daos.utils.Sesion;
import modelo.Curriculum;
import modelo.Empleado;
import modelo.Empresa;
import modelo.Estado;
import modelo.Jornada;
import modelo.Municipio;
import modelo.OfertaEmpleo;
import modelo.Pais;
import modelo.SectorActividad;
import modelo.Usuario;

import daos.CurriculumDAO;
import java.util.LinkedHashSet;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

public class ControladorLogin extends HttpServlet {

	private UsuarioDAO usuario_dao;
	private PaisDAO paisDao;
	private EstadoDAO estadoDao;
	private JornadaDAO jornadaDao;
	private SectorActividadDAO setoractividadDao;
	private MunicipioDAO munciopioDao;
	private OfertaEmpleoDAO ofertaEmpleoDao;
	private EmpresaDAO empresaDao;
	private EmpleadoDAO empleadoDao;
	private CurriculumDAO curriculumDao;
	
	public ControladorLogin() {
		usuario_dao = new UsuarioDAO();
		this.paisDao = PaisDAO.getInstancia();
		this.estadoDao = EstadoDAO.getInstancia();
		this.jornadaDao = JornadaDAO.getInstancia();
		this.setoractividadDao = SectorActividadDAO.getInstancia();
		this.munciopioDao = MunicipioDAO.getInstancia();
		this.ofertaEmpleoDao = OfertaEmpleoDAO.getInstancia();
		this.empresaDao = EmpresaDAO.getInstancia();
		this.empleadoDao = EmpleadoDAO.getInstancia();
		this.curriculumDao = CurriculumDAO.getInstancia();
		
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException {
		String operacion = request.getParameter("operacion");
		System.out.println(operacion);
		if(operacion != null) {
			if(operacion.equals("login")) {
				System.out.println("HELOAKSKJDHSJDJASHD");
				IniciarSesion(request, response);
			}else {
				CerrarSesion(request, response);
			}
		}
		else {
			RequestDispatcher rd=request.getRequestDispatcher("/dashboard/login.ftl");
			rd.forward(request,response);
		}
	
	}
	
	public void CerrarSesion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException {
		HttpSession session = request.getSession();
		session.invalidate();
		JSONObject message = new JSONObject();
		message.put("type", "success");
		message.put("text", "Se ha cerrado sesi&oacute;n con exito");
		request.setAttribute("mensaje", message);
		RequestDispatcher rd = request.getRequestDispatcher("/dashboard/login.ftl");
		rd.forward(request, response);
	}	
	
	public void IniciarSesion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException {
		Usuario usuario = usuario_dao.getUsuarioCorreo(request.getParameter("correo"));
		
		if(usuario != null) {
			if(usuario.getContrasenna().equals(request.getParameter("contrasenna"))) {
				//Sesion de HTTP Usuario
				HttpSession session = request.getSession(true);
				session.setAttribute("usuario", usuario);
				//Sesion Hibernate
				Sesion sesion = Sesion.getInstancia();
				Session sesion_creada = sesion.openSession();
				if(usuario.getRol().getId() == 1) {
					RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/index.ftl");
					rd.forward(request, response);
				}
				else if(usuario.getRol().getId() == 2) {
					List<OfertaEmpleo> ofertas = ofertaEmpleoDao.queryAll();
					List<OfertaEmpleo> ofertas_slices = ofertas.subList(0, ofertas.size()/2);
					List<Pais> lista_paises = paisDao.queryAll();
					List<Estado> lista_estado = estadoDao.queryAll();
					List<Jornada> lista_jornada = jornadaDao.queryAll();
					List<SectorActividad> lista_sector_actividad = setoractividadDao.queryAll();
					List<Municipio> lista_municipios = munciopioDao.queryAll();
					Set<Pais> paises = new LinkedHashSet<Pais>(lista_paises);
					Set<Estado> estados = new LinkedHashSet<Estado>(lista_estado);
					Set<Jornada> jornadas = new LinkedHashSet<Jornada>(lista_jornada);
					Set<Municipio> municipios = new LinkedHashSet<Municipio>(lista_municipios);
					Set<SectorActividad> sector_actividades = new LinkedHashSet<SectorActividad>(lista_sector_actividad);
					Empleado empleado = (Empleado) sesion_creada.createCriteria(Empleado.class).setFetchMode("empleado", FetchMode.JOIN).add(Restrictions.eq("usuario.id", usuario.getId())).uniqueResult();
					request.setAttribute("empleado", empleado);
					session.setAttribute("empleado", empleado);
					request.setAttribute("Empresas", empresaDao.queryAll());
					request.setAttribute("Ofertas", ofertas);
					request.setAttribute("OfertasFooter", ofertas_slices);
					request.setAttribute("Paises", paises);
					request.setAttribute("Estados", estados);
					request.setAttribute("Jornadas", jornadas);
					request.setAttribute("Sectores", sector_actividades);
					request.setAttribute("Municipios", municipios);
					RequestDispatcher rd = request.getRequestDispatcher("/index.ftl");
					rd.forward(request, response);
				}
				else {
					Empresa empresa = (Empresa) sesion_creada.createCriteria(Empresa.class).setFetchMode("empresa", FetchMode.JOIN).add(Restrictions.eq("usuario.id", usuario.getId())).uniqueResult();
					List<OfertaEmpleo> ofertas = ofertaEmpleoDao.queryAll();
					List<OfertaEmpleo> ofertas_slices = ofertas.subList(0, ofertas.size()/2);
					List<Empleado> empleados = empleadoDao.queryAll();
					List<Pais> lista_paises = paisDao.queryAll();
					List<Estado> lista_estado = estadoDao.queryAll();
					List<Jornada> lista_jornada = jornadaDao.queryAll();
					List<SectorActividad> lista_sector_actividad = setoractividadDao.queryAll();
					List<Municipio> lista_municipios = munciopioDao.queryAll();
					Set<Pais> paises = new LinkedHashSet<Pais>(lista_paises);
					Set<Estado> estados = new LinkedHashSet<Estado>(lista_estado);
					Set<Jornada> jornadas = new LinkedHashSet<Jornada>(lista_jornada);
					Set<Municipio> municipios = new LinkedHashSet<Municipio>(lista_municipios);
					Set<SectorActividad> sector_actividades = new LinkedHashSet<SectorActividad>(lista_sector_actividad);
					session.setAttribute("empresa", empresa);
					List<Empleado> nuevos_empleados = new ArrayList<Empleado>();
					for(Empleado empleado:empleados) {
						Curriculum curriculum = curriculumDao.getCurriculumPorEmpleadoId(empleado.getId());
						if(!String.valueOf(curriculum).equals("null")) {
							nuevos_empleados.add(empleado);
						}
					}
					Set<Empleado> nuevos_empleados_con_cv = new LinkedHashSet<Empleado>(nuevos_empleados);
					request.setAttribute("Empleados", nuevos_empleados_con_cv);
					request.setAttribute("Ofertas", ofertaEmpleoDao.queryAll());
					request.setAttribute("OfertasFooter", ofertas_slices);
					request.setAttribute("Paises", paises);
					request.setAttribute("Estados", estados);
					request.setAttribute("Jornadas", jornadas);
					request.setAttribute("Sectores", sector_actividades);
					request.setAttribute("Municipios", municipios);
					RequestDispatcher rd = request.getRequestDispatcher("/index.ftl");
					rd.forward(request, response);
				}
				
			}
			else {
				RequestDispatcher rd = request.getRequestDispatcher("/dashboard/login.ftl");
				rd.forward(request, response);
			}
		}
		else {
			RequestDispatcher rd = request.getRequestDispatcher("/dashboard/login.ftl");
			rd.forward(request, response);
		}
	}
}