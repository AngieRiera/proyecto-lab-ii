package controlador;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import daos.CurriculumDAO;
import daos.CursoRealizadoDAO;
import daos.EmpleadoDAO;
import daos.EspecialidadDAO;
import daos.ExperienciaLaboralDAO;
import daos.PaisDAO;
import daos.ProfesionDAO;
import daos.RolDAO;
import daos.UsuarioDAO;
import modelo.Curriculum;
import modelo.CursoRealizado;
import modelo.DetalleEstudio;
import modelo.Empleado;
import modelo.Especialidad;
import modelo.ExperienciaLaboral;
import modelo.Jornada;
import modelo.Pais;
import modelo.Profesion;
import modelo.Rol;
import modelo.Usuario;

public class ControladorCurriculum extends HttpServlet {
	
	private UsuarioDAO usrDao;
	private RolDAO rolDao;
	private PaisDAO paisDao;
	private EmpleadoDAO empleadoDao;
	private CurriculumDAO curriculumDao;
	private ProfesionDAO profesionDao;
	private EspecialidadDAO especialidadDao;
	private CursoRealizadoDAO cursoDao;
	private ExperienciaLaboralDAO experiencia_laboralDao;
	
	public ControladorCurriculum() {
		// TODO Auto-generated constructor stub
		usrDao = new UsuarioDAO();
		rolDao = new RolDAO();
		paisDao = new PaisDAO();
		empleadoDao = new EmpleadoDAO();
		curriculumDao = new CurriculumDAO();
		profesionDao = new ProfesionDAO();
		especialidadDao = new EspecialidadDAO();
		cursoDao = new CursoRealizadoDAO();
		experiencia_laboralDao = new ExperienciaLaboralDAO();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub	
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String operacion = request.getParameter("operacion");
		HttpSession misession= (HttpSession) request.getSession();
		Usuario usuario = (Usuario) misession.getAttribute("usuario");
		Empleado empleado = empleadoDao.getEmpleadoPorUserId(usuario.getId());
		request.setAttribute("usuario", usuario);
		String accion = request.getParameter("accion");
		System.out.println(operacion);
			
		if(accion != null) {
			Curriculum curriculum = curriculumDao.getCurriculumPorEmpleadoId(empleado.getId());
			if(accion.equals("AgregarDetalleEstudio")) {
				System.out.println("esta haciendo el agregar detalle de estudio");
				DetalleEstudio detalleEstudio = new DetalleEstudio();
				detalleEstudio.setDescripcion(request.getParameter("descripcion_detalle_estudio"));
				System.out.println(request.getParameter("descripcion_detalle_estudio"));
				detalleEstudio.setInstitucion(request.getParameter("institucion_detalle_estudio"));
				detalleEstudio.setDescripcion(request.getParameter("descripcion_detalle_estudio"));
				detalleEstudio.setFecha_inicio(StringToDate(request.getParameter("fechaini_detalle_estudio")));
				detalleEstudio.setFecha_culminacion(StringToDate(request.getParameter("fechafin_detalle_estudio")));
				System.out.println("id profesion: "+Integer.parseInt(request.getParameter("profesion_id_detalle_estudio")));
				Profesion profesion = buscarProfesion(Integer.parseInt(request.getParameter("profesion_id_detalle_estudio")));
				detalleEstudio.setProfesion(profesion);
				System.out.println("id del curriculum"+curriculum.getId());
				detalleEstudio.setCurriculum(curriculum);
				detalleEstudio.setStatus(1);

				curriculumDao.save(detalleEstudio);
			}
			
			else if(accion.equals("AgregarExperienciaLaboral")) {
				System.out.println("esta haciendo el agregar experencia laboral");
				ExperienciaLaboral experienciaLaboral = new ExperienciaLaboral();
				experienciaLaboral.setNombre_empresa(request.getParameter("empresa_experiencia"));
				System.out.println("nombre de la empresa:"+request.getParameter("empresa_experiencia"));
				experienciaLaboral.setCargo(request.getParameter("cargo_experiencia"));
				experienciaLaboral.setDescripcion(request.getParameter("descripcion_experiencia"));
				experienciaLaboral.setFecha_inicio(StringToDate(request.getParameter("fechaini_experiencia")));
				experienciaLaboral.setFecha_culminacion(StringToDate(request.getParameter("fechafin_experiencia")));
				
				Especialidad especialidad = buscarEspecialidad(Integer.parseInt(request.getParameter("especialidad")));
				experienciaLaboral.setEspecialidad(especialidad);
				experienciaLaboral.setStatus(1);
				experienciaLaboral.setCurriculum(curriculum);
				curriculumDao.save(experienciaLaboral);
			}else if(accion.equals("AgregarCursoRealizado")) {
				System.out.println("esta haciendo el agregar curso realizado");
				CursoRealizado curso_realizado = new CursoRealizado();
				curso_realizado.setNombre(request.getParameter("name_curso"));
				curso_realizado.setDescripcion(request.getParameter("descripcion_curso"));
				curso_realizado.setFecha_inicio(StringToDate(request.getParameter("fechaini_curso")));
				curso_realizado.setFecha_fin(StringToDate(request.getParameter("fechafin_curso")));
				curso_realizado.setInstitucion(request.getParameter("institucion_curso"));
				curso_realizado.setStatus(1);			
				curso_realizado.setCurriculum(curriculum);
				curriculumDao.save(curso_realizado);
			}
			else if(accion.equals("eliminarCursoRealizado")) {
				cursoDao.delete(cursoDao.get(Integer.parseInt(request.getParameter("id"))));
			}
			else if(accion.equals("eliminarExperienciaLaboral")) {
				experiencia_laboralDao.delete(experiencia_laboralDao.get(Integer.parseInt(request.getParameter("id"))));
			}
			else if(accion.equals("eliminarDetalleEstudio")) {
				DetalleEstudio detalle_estudio=curriculumDao.getDetalleEstudio(Integer.parseInt(request.getParameter("id")));
				System.out.println("id detalle: "+detalle_estudio.getId());
				curriculumDao.delete(detalle_estudio);
			}
		}
		
		if(operacion != null) {
			if(operacion.equals("guardar")) {
				Curriculum curriculum = curriculumDao.getCurriculumPorEmpleadoId(empleado.getId());			
				System.out.println("id del curriculum:"+curriculum.getId());
				curriculum.setConocimientos_adicionales(request.getParameter("conocimientos_extras"));
				curriculum.setObservaciones(request.getParameter("observaciones"));
				curriculumDao.update(curriculum);
				response.sendRedirect("/ProyectoLaboratorioII/empleado/curriculum?operacion=consultar");
				/*Guardar(request, response, usuario);
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/empleado/curriculum/registrar.ftl");
				rd.forward(request,response);*/
				
			}
			
			else if(operacion.equals("consultar")) {
				Curriculum curriculum = curriculumDao.getCurriculumPorEmpleadoId(empleado.getId());
				if( curriculum== null) {
					Curriculum new_curriculum = new Curriculum();
					new_curriculum.setStatus(1);
					new_curriculum.setFecha_registro(new Date());
					new_curriculum.setEmpleado(empleado);
					curriculumDao.save(new_curriculum);
				}
				else {
					System.out.println(curriculum.getObservaciones());
					List<DetalleEstudio> detalles_estudio = curriculumDao.obtenerDetallesEstudio(curriculum.getId());
					List<ExperienciaLaboral> experiencias_laborales = curriculumDao.obtenerExperienciasLaborales(curriculum.getId());
					List<CursoRealizado> cursos_realizados = curriculumDao.obtenerCursosRealizados(curriculum.getId());
					
					
					request.setAttribute("cursos_realizados", cursos_realizados);					
					request.setAttribute("experiencias_laborales", experiencias_laborales);
					request.setAttribute("detalles_estudios",detalles_estudio);
					request.setAttribute("curriculum", curriculum);
				}
				Set<Especialidad> especialidades = new LinkedHashSet<Especialidad>(especialidadDao.queryAll());
				Set<Profesion> profesiones = new LinkedHashSet<Profesion>(profesionDao.queryAll());
				request.setAttribute("especialidades", especialidades);
				request.setAttribute("profesiones", profesiones);
				request.setAttribute("usuario", usuario);
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/empleado/curriculum/registrar.ftl");
				rd.forward(request,response);
				
			}
		}
		
		Set<Especialidad> especialidades = new LinkedHashSet<Especialidad>(especialidadDao.queryAll());
		Set<Profesion> profesiones = new LinkedHashSet<Profesion>(profesionDao.queryAll());
		request.setAttribute("especialidades", especialidades);
		request.setAttribute("profesiones", profesiones);
		request.setAttribute("usuario", usuario);
		RequestDispatcher rd=request.getRequestDispatcher("/dashboard/empleado/curriculum/registrar.ftl");
		rd.forward(request,response);
		
	}
	
	public Date StringToDate(String fecha) {
		  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
		  LocalDate date = LocalDate.parse(fecha, formatter);
		  Date fecha_real = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
		  return fecha_real;
	}
	
	public Profesion buscarProfesion(int id) {
		return profesionDao.get(id);
	}
	public Especialidad buscarEspecialidad(int id) {
		return especialidadDao.get(id);
	}
	
	/*public void Guardar(HttpServletRequest request, HttpServletResponse response, Usuario usuario) throws ServletException, IOException, JSONException{
		Curriculum curriculum = new Curriculum();
		
		if(request.getParameter("id") != null) {
			curriculum = curriculumDao.get(request.getParameter("id"));
			curriculum.setConocimientos_adicionales(request.getParameter("conocimientos_extras"));
			curriculum.setObservaciones(request.getParameter("observaciones"));
			curriculumDao.update(curriculum);
		}
		else {
			curriculum.setConocimientos_adicionales(request.getParameter("conocimientos_extras"));
			curriculum.setObservaciones(request.getParameter("observaciones"));
			curriculum.setStatus(1);
			curriculum.setFecha_registro(curriculum.LocalDateToDate());
			Empleado empleado = empleadoDao.getEmpleadoPorUserId(usuario.getId());
			curriculum.setEmpleado(empleado);
			
			//pruebas con listas
			CursoRealizado curso_realizado = new CursoRealizado();
			List<CursoRealizado> lista_cursos = new ArrayList<CursoRealizado>();
			curso_realizado.setInstitucion("UCLA");
			curso_realizado.setDescripcion("Descripcion del curso");
			curso_realizado.setNombre("Curso php");
			curso_realizado.setFecha_inicio(curriculum.LocalDateToDate());
			curso_realizado.setFecha_fin(curriculum.LocalDateToDate());
			curso_realizado.setStatus(1);
			lista_cursos.add(curso_realizado);
			
			curso_realizado = new CursoRealizado();
			curso_realizado.setInstitucion("CADIF1");
			curso_realizado.setDescripcion("Descripcion del curso");
			curso_realizado.setNombre("Curso ruby");
			curso_realizado.setFecha_inicio(curriculum.LocalDateToDate());
			curso_realizado.setFecha_fin(curriculum.LocalDateToDate());
			curso_realizado.setStatus(1);
			lista_cursos.add(curso_realizado);
			
			curriculum.setCursos_realizado(lista_cursos);
			curriculumDao.save(curriculum);
		}
		
		System.out.println("Hola");
		
		
		System.out.println(request.getParameter("hidden_table_detalle"));
		System.out.println(request.getParameter("hidden_table_curso"));
		System.out.println(request.getParameter("hidden_table_experiencia"));
		
	}
	
	public void Modificar(HttpServletRequest request, HttpServletResponse response, Usuario usuario) throws ServletException, IOException, JSONException{
		Empleado empleado = empleadoDao.getEmpleadoPorUserId(usuario.getId());
		Curriculum curriculum = curriculumDao.getCurriculumPorEmpleadoId(empleado.getId());
		List<CursoRealizado> cursos = curriculum.getCursos_realizado();
		request.setAttribute("empleado", empleado);
		request.setAttribute("curriculum", curriculum);
		request.setAttribute("cursos", cursos);
	}*/
	
}

