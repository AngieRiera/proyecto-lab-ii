package controlador;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import daos.PaisDAO;
import modelo.Empleado;
import modelo.Pais;
import modelo.SectorActividad;



public class ControladorGestionPais extends HttpServlet {
	
	private PaisDAO dao_pais;
	
	public ControladorGestionPais() {
		super();
		this.dao_pais = PaisDAO.getInstancia();
		
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RealizarAccion(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RealizarAccion(request,response);
	}
		
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operacion = request.getParameter("operacion");
		System.out.println(request.getParameter("id"));
		if(operacion != null) {
			String id = request.getParameter("id");
			if(operacion.equals("modificar")) {					
				Pais pais = dao_pais.get(Integer.parseInt(id));
				request.setAttribute("pais",pais);
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-pais/modificar.ftl");
				rd.forward(request,response);			
			}
			else if(operacion.equals("ver")) {				
				Pais pais = dao_pais.get(Integer.parseInt(id));
				request.setAttribute("pais",pais);
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-pais/ver.ftl");
				rd.forward(request,response);
			}
			else if(operacion.equals("registrar")) {				
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-pais/registrar.ftl");				
				rd.forward(request,response);
			}else if(operacion.equals("guardar"))
			{				
				paisNuevo(request,response);
				response.sendRedirect("/ProyectoLaboratorioII/admin/pais");
			}else if(operacion.equals("guardarCambio"))
			{				
				GuardarCambio(request,response);
				response.sendRedirect("/ProyectoLaboratorioII/admin/pais");
			}else if(operacion.equals("eliminar")) {
				Pais pais = dao_pais.get(Integer.parseInt(id));
				pais.setStatus(2);
				modificarPais(pais);
				response.sendRedirect("/ProyectoLaboratorioII/admin/pais");
			}
			else {
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-pais/listado.ftl");
				rd.forward(request,response);
			}
		}
		else {
			
			Set<Pais> paises = new LinkedHashSet<Pais>(this.dao_pais.queryAll());
			request.setAttribute("paisList", paises);
			RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-pais/listado.ftl");
			rd.forward(request,response);
		}
	
	}
	protected void paisNuevo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			System.out.println("va a guardar un nuevo registro");
			Pais pais = new Pais();
			pais.setNombre(request.getParameter("nombre"));
			pais.setStatus(Integer.parseInt(request.getParameter("status")));
			System.out.println(pais.getNombre());
			try {
				dao_pais.save(pais);
				System.out.println("Insertado Exitosamente");
			}catch(Exception e){
				e.printStackTrace();
			}			
	}
		public void GuardarCambio(HttpServletRequest request, HttpServletResponse response){
			
				Pais pais = dao_pais.get(Integer.parseInt(request.getParameter("id")));
				pais.setNombre(request.getParameter("name"));
				
				pais.setStatus(Integer.parseInt(request.getParameter("status")));
				
				modificarPais(pais);
				//Usuario usuario = new Usuario("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);
			
		}
		public void modificarPais(Pais pais) {
			try {
				dao_pais.update(pais);
				System.out.println("Datos del usuario modificado exitosamente");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		@Override
		protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
			String data = br.readLine();
			System.out.println(data);
			String id = data.substring(7);
			System.out.println(id);
			Pais pais;
			pais = this.dao_pais.get(Integer.parseInt(id));
			if (pais != null) {
				System.out.println("Empleado no null");
				this.dao_pais.delete(pais);
				response.getWriter().print("ok");
			}
			else response.getWriter().print("No se pudo eliminar el pais.");
			
			
		}
	
}
	
	

