package controlador;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import daos.EmpresaDAO;
import daos.PaisDAO;
import daos.PostulacionDAO;
import daos.RolDAO;
import daos.UsuarioDAO;
import modelo.Empleado;
import modelo.Empresa;
import modelo.Jornada;
import modelo.OfertaEmpleo;
import modelo.Pais;
import modelo.Postulacion;
import modelo.Profesion;
import modelo.Rol;
import modelo.Usuario;

public class ControladorContratacionEmpresa extends HttpServlet {
	
	private EmpresaDAO dao_empresa;
	private UsuarioDAO dao_usuario;
	private PostulacionDAO dao_postulacion;
	
	public ControladorContratacionEmpresa() {
		// TODO Auto-generated constructor stub
		dao_empresa = new EmpresaDAO();
		dao_usuario = new UsuarioDAO();
		dao_postulacion = new PostulacionDAO();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("MENSAJE DE PRUEBA CONTRATACION EMPRESA GET");
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub	
		System.out.println("MENSAJE DE PRUEBA CONTRATACION EMPRESA POST");
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession misession= (HttpSession) request.getSession();
		Usuario usuario = (Usuario) misession.getAttribute("usuario");
		request.setAttribute("usuario", usuario);
		Empresa empresa = dao_empresa.getEmpresaPorUserId(usuario.getId());
		System.out.println("ID Usuario:"+usuario.getId());
		System.out.println("ID Empresa:"+empresa.getId());
		List<OfertaEmpleo> ofertas = dao_empresa.oferta_empleos(empresa.getId());
		List<Postulacion> postulaciones_listado = new ArrayList<Postulacion>();
		List<Postulacion> postulaciones = new ArrayList<Postulacion>();
		System.out.println("Ofertas size:"+ofertas.size());
		for(int i=0; i<ofertas.size(); i++ ){
			System.out.println("ID Oferta: "+ofertas.get(i).getId());
			OfertaEmpleo oferta = ofertas.get(i);
			postulaciones_listado = dao_postulacion.postulaciones(oferta.getId());
			for(int j=0; j<postulaciones_listado.size(); j++) {
				postulaciones.add(postulaciones_listado.get(j));
			}
		}
		request.setAttribute("postulaciones", postulaciones);
		RequestDispatcher rd=request.getRequestDispatcher("/dashboard/empresa/listados/listado-contrataciones.ftl");
		rd.forward(request,response);
	}
	
}

