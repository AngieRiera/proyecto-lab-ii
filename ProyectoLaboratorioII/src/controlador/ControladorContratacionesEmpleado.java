package controlador;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import daos.EmpleadoDAO;
import daos.OfertaEmpleoDAO;
import daos.PaisDAO;
import daos.RolDAO;
import daos.UsuarioDAO;
import modelo.Empleado;
import modelo.Jornada;
import modelo.OfertaEmpleo;
import modelo.Pais;
import modelo.Profesion;
import modelo.Rol;
import modelo.Usuario;

public class ControladorContratacionesEmpleado extends HttpServlet {
	
	private UsuarioDAO usrDao;
	private RolDAO rolDao;
	private PaisDAO paisDao;
	private EmpleadoDAO empleadoDao;
	private OfertaEmpleoDAO ofertaDao;
	
	public ControladorContratacionesEmpleado() {
		// TODO Auto-generated constructor stub
		usrDao = new UsuarioDAO();
		rolDao = new RolDAO();
		paisDao = new PaisDAO();
		empleadoDao = new EmpleadoDAO();
		ofertaDao = new OfertaEmpleoDAO();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub	
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String operacion = request.getParameter("operacion");
		String id = request.getParameter("id");
		HttpSession misession= (HttpSession) request.getSession();
		Usuario usuario = (Usuario) misession.getAttribute("usuario");
		request.setAttribute("usuario", usuario);
		request.setAttribute("profesiones", usrDao.queryAll(Profesion.class));
		System.out.println(operacion);
		if(operacion != null) {
			if(operacion.equals("verOferta")) {
				OfertaEmpleo oferta = ofertaDao.get(Integer.parseInt(id));
				request.setAttribute("ofertaEmpleo",oferta);
				request.setAttribute("mensaje", 1);
				RequestDispatcher rd=request.getRequestDispatcher("/detalleOfertaEmpleo.ftl");
				rd.forward(request,response);
			}
		}
		else {
			Empleado empleado = empleadoDao.getEmpleadoPorUserId(usuario.getId());
			request.setAttribute("contrataciones", empleadoDao.contrataciones(empleado.getId()));
			RequestDispatcher rd=request.getRequestDispatcher("/dashboard/empleado/listados/listado-contrataciones.ftl");
			rd.forward(request,response);
		}
		
	}
		
}


