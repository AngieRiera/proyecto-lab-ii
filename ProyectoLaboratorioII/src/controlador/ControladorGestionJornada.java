package controlador;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import daos.JornadaDAO;
import modelo.Jornada;
import modelo.Profesion;

public class ControladorGestionJornada extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 313689681589759377L;
	JornadaDAO jornada_dao;
	
	public ControladorGestionJornada() {
		jornada_dao = new JornadaDAO();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RealizarAccion(request,response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RealizarAccion(request,response);
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operacion = request.getParameter("operacion");
		System.out.println(operacion);
		if(operacion != null) {
			if(operacion.equals("modificar")) {
				ModificarJornada(request, response);
			}
			else if(operacion.equals("ver")) {
				VerJornada(request, response);
			}
			else if(operacion.equals("registrar")) {
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-jornada/registrar.ftl");
				rd.forward(request,response);
			}
			else if(operacion.equals("guardar")) {
				GuardarJornada(request, response);
				ListaJornadas(request, response);
			}
			else if(operacion.equals("eliminar")) {
				Jornada jornada = jornada_dao.get(Integer.parseInt(request.getParameter("id")));
				jornada.setStatus(2);
				jornada_dao.update(jornada);
				ListaJornadas(request, response);
			}
		}
		else {
			ListaJornadas(request, response);
		}
	
	}
	
	public void ListaJornadas(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Set<Jornada> jornadas = new LinkedHashSet<Jornada>(jornada_dao.queryAll());
		System.out.println("cantidad de jornadas: "+jornadas.size());
		request.setAttribute("jornadas",jornadas );
		
		RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-jornada/listado.ftl");
		rd.forward(request,response);
	}
	
	public void VerJornada(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Jornada jornada = jornada_dao.get(Integer.parseInt(request.getParameter("id")));
		request.setAttribute("jornada", jornada);
		RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-jornada/ver.ftl");
		rd.forward(request,response);
	}
	
	public void GuardarJornada(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("nombre").equals("") || request.getParameter("descripcion").equals("") || request.getParameter("status").equals("")) {
			
		}
		else {
			if(request.getParameter("id") != null) {
				Jornada jornada = new Jornada();
				jornada.setId(Integer.parseInt(request.getParameter("id")));
				jornada.setNombre(request.getParameter("nombre"));
				jornada.setDescripcion(request.getParameter("descripcion"));
				jornada.setStatus(Integer.parseInt(request.getParameter("status")));
				jornada_dao.update(jornada);
			}
			else {
				Jornada jornada = new Jornada();
				jornada.setNombre(request.getParameter("nombre"));
				jornada.setDescripcion(request.getParameter("descripcion"));
				jornada.setStatus(Integer.parseInt(request.getParameter("status")));
				jornada_dao.saveOrUpdate(jornada);
			}
		}
	}
	
	public void ModificarJornada(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Jornada jornada = jornada_dao.get(Integer.parseInt(request.getParameter("id")));
		request.setAttribute("jornada", jornada);
		RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-jornada/modificar.ftl");
		rd.forward(request,response);
	}
}
