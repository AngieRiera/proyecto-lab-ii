package controlador;
import java.nio.file.AtomicMoveNotSupportedException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import daos.SectorActividadDAO;
import modelo.*;

public class SectorActividadDaoTest {

	private SectorActividadDAO sectoractividadDao;
	
	public SectorActividadDaoTest() {
		// TODO Auto-generated constructor stub
		sectoractividadDao = new SectorActividadDAO();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new SectorActividadDaoTest().agregarSectorActividad();
//		new SectorActividadDaoTest().obtenerUnRol(6);
//		new SectorActividadDaoTest().obtenerRoles();
//		new SectorActividadDaoTest().eliminarSectorActividad(12);
//		new SectorActividadDaoTest().modificarRol(11);
	}
	
	public void agregarSectorActividad() {
		SectorActividad sectoractividad = new SectorActividad("Informatica", "Sector dedicado a la fabricacion de productos industriales destinados a la parte agricola", 1);
		try {
			
			//sectoractividadDao.save(sectoractividad);
			List<SectorActividad> sectores = sectoractividadDao.queryAll();
			System.out.println(sectores.size());
			//sectores.size();
			sectoractividad = sectoractividadDao.get(2);
			System.out.println("Nombre: "+sectoractividad.getNombre());
			System.out.println("Descripcion: "+sectoractividad.getDescripcion());
			//System.out.println("Insertado Exitosamente");
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
/*
	public void obtenerUnRol(int idRol){
		Rol rol= new Rol();
		rol = sectoractividadDao.get(idRol);
		try {
			System.out.println("ID: " + rol.getId());
			System.out.println("Nombre: " + rol.getNombre());
			System.out.println("Descripcion: " +rol.getDescripcion());
			List<Usuario> usuarios = rol.getUsuarios();	
			System.out.println("----Usuarios con el rol: " + rol.getNombre());
			for (Usuario usr : usuarios){
				System.out.println(usr.getCorreo());
			}
			
			System.out.println("Cargado Exitosamente");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void obtenerRoles(){
		try {
			List<Rol> lista = sectoractividadDao.queryAll();
			Set<Rol> roles = new LinkedHashSet<Rol>(lista);			
			Iterator it = roles.iterator(); 
			int i=1;
			while (it.hasNext()){
	            Rol rol = (Rol) it.next();
				System.out.println("-------------Datos del Rol: " + i);
				System.out.println("ID: " + rol.getId());
				System.out.println("Nombre: " + rol.getNombre());
				System.out.println("Descripcion: " +rol.getDescripcion());
				List<Usuario> usuarios = rol.getUsuarios();	
				System.out.println("----Usuarios con el rol: " + rol.getNombre());
				for (Usuario usr : usuarios){
					System.out.println(usr.getCorreo());
				}
				i++;

			}

			System.out.println("Cargados Exitosamente");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void eliminarRol(int idRol) {
		try {
			Rol rol = sectoractividadDao.get(idRol);
			if(rol.getUsuarios().size() == 0) {
				rol.setStatus(2);
				sectoractividadDao.update(rol);
				System.out.println("Rol eliminado con exito");
			}
			else {
				System.out.println("ERROR: El rol esta asociado a estos usuarios: ");
				System.out.println("----Usuarios con el rol: " + rol.getNombre());
				List<Usuario> usuarios = rol.getUsuarios();	
				for (Usuario usr : usuarios){
					System.out.println(usr.getCorreo());
				}
			}
				
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void modificarRol(int idRol) {
		try {
			Rol rol = null;
			rol = sectoractividadDao.get(idRol);
			if(!String.valueOf(rol).equals("null")) {
				rol.setNombre("Jugador");
				sectoractividadDao.update(rol);
				System.out.println("Datos del rol modificado exitosamente");
			}else {
				System.out.println("ERROR: El rol no existe ");
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
*/

}
