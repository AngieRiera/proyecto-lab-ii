package controlador;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import daos.PaisDAO;
import daos.RolDAO;
import modelo.Pais;
import modelo.Rol;
import modelo.SectorActividad;

public class ControladorGestionRol extends HttpServlet {

	private RolDAO dao_rol;
	
	public ControladorGestionRol() {
		super();
		this.dao_rol = RolDAO.getInstancia();
		
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RealizarAccion(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RealizarAccion(request,response);
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operacion = request.getParameter("operacion");
		System.out.println(operacion);
		if(operacion != null) {
			String id = request.getParameter("id"); 
			if(operacion.equals("modificar")) {
				request.setAttribute("rol",dao_rol.get(Integer.parseInt(id)));				
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-rol/modificar.ftl");
				rd.forward(request,response);
			}
			else if(operacion.equals("ver")) {
				request.setAttribute("rol", dao_rol.get(Integer.parseInt(id)));				
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-rol/ver.ftl");
				rd.forward(request,response);
			}
			else if(operacion.equals("registrar")) {
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-rol/registrar.ftl");
				rd.forward(request,response);
			}else if(operacion.equals("guardar")) {
				rolNuevo(request, response);
				response.sendRedirect("/ProyectoLaboratorioII/admin/rol");
			}
			else if(operacion.equals("guardarCambio")) {
				GuardarCambio(request, response);
				response.sendRedirect("/ProyectoLaboratorioII/admin/rol");
			}else if(operacion.equals("eliminar")) {
				Rol rol = dao_rol.get(Integer.parseInt(request.getParameter("id")));
				rol.setStatus(2);
				modificarRol(rol);
				response.sendRedirect("/ProyectoLaboratorioII/admin/rol");
			}
		}
		else {
			Set<Rol> roles = new LinkedHashSet<Rol>(dao_rol.queryAll());
			request.setAttribute("rolList", roles );
			RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-rol/listado.ftl");
			rd.forward(request,response);
		}
	
	}
	protected void rolNuevo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("va a guardar un nuevo registro");
		Rol rol = new Rol();
		rol.setNombre(request.getParameter("nombre"));
		rol.setDescripcion(request.getParameter("descripcion"));
		rol.setStatus(Integer.parseInt(request.getParameter("status")));
		System.out.println(rol.getNombre());
		try {
			dao_rol.save(rol);
			System.out.println("Insertado Exitosamente");
		}catch(Exception e){
			e.printStackTrace();
		}			
}
	public void GuardarCambio(HttpServletRequest request, HttpServletResponse response){
		
		Rol rol = dao_rol.get(Integer.parseInt(request.getParameter("id")));
		rol.setNombre(request.getParameter("nombre"));
		rol.setDescripcion(request.getParameter("descripcion"));
		rol.setStatus(Integer.parseInt(request.getParameter("status")));		
		modificarRol(rol);
		//Usuario usuario = new Usuario("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);
	
	}
	public void modificarRol(Rol rol) {
		try {
			dao_rol.update(rol);
			System.out.println("Datos del usuario modificado exitosamente");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
