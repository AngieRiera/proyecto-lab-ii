package controlador;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import daos.EstadoDAO;
import daos.PaisDAO;
import modelo.Estado;
import modelo.Pais;
import modelo.SectorActividad;

public class ControladorGestionEstado extends HttpServlet {
	
	private EstadoDAO dao_estado;
	private PaisDAO dao_pais;
	
	public ControladorGestionEstado() {
		super();
		this.dao_estado = EstadoDAO.getInstancia();
		dao_pais = PaisDAO.getInstancia();
		
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RealizarAccion(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RealizarAccion(request,response);
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operacion = request.getParameter("operacion");
		System.out.println(operacion);
		if(operacion != null) {
			String id = request.getParameter("id");
			if(operacion.equals("modificar")) {
				Estado estado = dao_estado.get(Integer.parseInt(id));				
				request.setAttribute("estado",estado);
				request.setAttribute("paises", obtenerPaises());				
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-estado/modificar.ftl");
				rd.forward(request,response);
			}
			else if(operacion.equals("ver")) {
				PaisDAO dao_pais = PaisDAO.getInstancia();
				Estado estado = dao_estado.get(Integer.parseInt(id));
				//Pais pais = dao_pais.get(estado.getPais());				
				request.setAttribute("estado",estado);				
				//request.setAttribute("pais", pais);
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-estado/ver.ftl");
				rd.forward(request,response);
			}
			else if(operacion.equals("registrar")) {
				PaisDAO dao_pais = PaisDAO.getInstancia();
				request.setAttribute("paises", dao_pais.queryAll());				
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-estado/registrar.ftl");
				rd.forward(request,response);
			}
			else if(operacion.equals("guardar")) {
				estadoNuevo(request, response);
				response.sendRedirect("/ProyectoLaboratorioII/admin/estado");
			}else if(operacion.equals("guardarCambio")) {
				GuardarCambio(request,response);
				response.sendRedirect("/ProyectoLaboratorioII/admin/estado");
			}else if(operacion.equals("eliminar")) {
				Estado estado = dao_estado.get(Integer.parseInt(id));
				estado.setStatus(2);
				modificarEstado(estado);
				response.sendRedirect("/ProyectoLaboratorioII/admin/estado");
			}
			else {
				request.setAttribute("estadoList", this.dao_estado.queryAll());
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-estado/listado.ftl");
				rd.forward(request,response);
			}
		}
		else {			
			Set<Estado> estados = new LinkedHashSet<Estado>(this.dao_estado.queryAll());
			request.setAttribute("estadoList", estados);
			RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-estado/listado.ftl");
			rd.forward(request,response);
		}
	
	}
	
	public Set<Pais> obtenerPaises(){
		Set<Pais> paises = new LinkedHashSet<Pais>(dao_pais.queryAll());

		return paises;
		
	}
	
	public void GuardarCambio(HttpServletRequest request, HttpServletResponse response){
		
		Estado estado = dao_estado.get(Integer.parseInt(request.getParameter("id")));
		Pais pais = dao_pais.get(Integer.parseInt(request.getParameter("pais")));
		estado.setNombre(request.getParameter("nombre"));
		estado.setCodigo_postal(request.getParameter("codigo_postal"));
		estado.setPais(pais);		
		estado.setStatus(Integer.parseInt(request.getParameter("status")));		
		modificarEstado(estado);
		//Usuario usuario = new Usuario("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);
	
}
	public void modificarEstado(Estado estado) {
		try {
			dao_estado.update(estado);
			System.out.println("Datos del usuario modificado exitosamente");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
protected void estadoNuevo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	System.out.println("va a guardar un nuevo registro");
	Estado estado = new Estado();
	Pais pais = new Pais();
	System.out.println(request.getParameter("pais"));
	System.out.println("ENTROOOOOOOOO");
	pais = dao_pais.get(Integer.parseInt(request.getParameter("pais")));
	System.out.println(pais.getNombre());
	System.out.println("NOMBREEE");	
	estado.setNombre(request.getParameter("nombre"));
	estado.setCodigo_postal(request.getParameter("codigo_postal"));
	estado.setPais(pais);
	estado.setStatus(Integer.parseInt(request.getParameter("status")));
	
	try {
		dao_estado.save(estado);
		System.out.println("Insertado Exitosamente");
	}catch(Exception e){
		e.printStackTrace();
	}			
}
		
}
