package controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import daos.UsuarioDAO;
import modelo.Profesion;
import modelo.Usuario;

public class ControladorDashboardAdmin extends HttpServlet {
	
	private UsuarioDAO usuarioDao = new UsuarioDAO();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		float porcentaje_hombres = new ControladorReportes().promedioEmpleadosHombres();
		float porcentaje_mujeres = new ControladorReportes().promedioEmpleadosMujeres();
		request.setAttribute("cant_mujeres", porcentaje_mujeres);
		request.setAttribute("cant_hombres", porcentaje_hombres);
		List<Usuario> administradores = new ArrayList<Usuario>();
		List<Usuario> lista_usuarios = usuarioDao.queryAll();
		Set<Usuario> usuarios = new LinkedHashSet<Usuario>(lista_usuarios);
		for(Usuario usuario:usuarios) {
			if(!String.valueOf(usuario.getRol()).equals("null") && usuario.getRol().getId() == 2) {
				administradores.add(usuario);
			}
		}
		request.setAttribute("administradores", administradores);
		request.getRequestDispatcher("/dashboard/admin/index.ftl").forward(request, response);
	}
}
