package controlador;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import daos.PaisDAO;
import daos.RolDAO;
import daos.UserProfileDAO;
import daos.UsuarioDAO;
import modelo.Pais;
import modelo.Rol;
import modelo.UserProfile;
import modelo.Usuario;

public class ControladorGestionUsuario extends HttpServlet {
	
	private UsuarioDAO usrDao;
	private RolDAO rolDao;
	private PaisDAO paisDao;
	//Se instancia un objeto DAO de userProfile para efectos de tareas individuales de cada entidad
	private UserProfileDAO usrprflDao;
	
	public ControladorGestionUsuario() {
		// TODO Auto-generated constructor stub
		usrDao = new UsuarioDAO();
		rolDao = new RolDAO();
		paisDao = new PaisDAO();
		//Se instancia un nuevo objeto DAO de userProfile
		usrprflDao = new UserProfileDAO();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub	
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException {
		String operacion = request.getParameter("operacion");
		System.out.println(operacion);
		if(operacion != null) {
			String id = request.getParameter("id");
			if(id != null){
				//Se modifica la captura de datos de roles y paises, sin repetidos
				List<Rol> lista_roles = obtenerRoles();
				List<Pais> lista_paises = obtenerPaises();
				Set<Rol> roles = new LinkedHashSet<Rol>(lista_roles);
				Set<Pais> paises = new LinkedHashSet<Pais>(lista_paises);
				Usuario usuario = obtenerUnUsuario(Integer.parseInt(id));
				//Se verifica que exista el usuario en la BD
				if(!String.valueOf(usuario).equals("null")) {
					if(operacion.equals("modificar")) {
						System.out.println("modificar algo");
						request.setAttribute("usuario",usuario);
						request.setAttribute("roles", roles);
						request.setAttribute("paises", paises);
						RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-usuario/modificar.ftl");
						rd.forward(request,response);
					}
					else if(operacion.equals("guardarCambio")) {
						//Verificacion si ha guardado con �xito
						if(GuardarCambio(request, response)) {
							JSONObject message = new JSONObject();
							message.put("type", "success");
							message.put("text", "Se han modificado los datos del usuario con exito");
							request.setAttribute("mensaje", message);
							List<Usuario> listadoUsuarios = obtenerUsuarios();
							request.setAttribute("usuarios",listadoUsuarios);
							RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-usuario/listado.ftl");
							rd.forward(request,response);
						}
					}
					else if(operacion.equals("ver")) {
						request.setAttribute("usuario",usuario);
						RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-usuario/ver.ftl");
						rd.forward(request,response);
					}
					else if(operacion.equals("eliminar")) {					
						if(eliminarUsuario(usuario.getId())) {
							JSONObject message = new JSONObject();
							message.put("type", "success");
							message.put("text", "El usuario se ha elimando satisfactoriamente");
							System.out.println("Se ejecut� el eliminar");
							request.setAttribute("mensaje", message);
						}
						else {
							JSONObject message = new JSONObject();
							message.put("type", "danger");
							message.put("text", "No se ha podido eliminar debido a que el registro se encuentra relacionado en el sistema");
							System.out.println("NO Se ejecut� el eliminar");
							request.setAttribute("mensaje", message);
						}
						List<Usuario> listadoUsuarios = obtenerUsuarios();
						request.setAttribute("usuarios",listadoUsuarios);
						RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-usuario/listado.ftl");
						rd.forward(request,response);
					} else if(operacion.equals("reactivar")) {
						if(reactivarUsuario(usuario.getId())) {
							JSONObject message = new JSONObject();
							message.put("type", "success");
							message.put("text", "El usuario se ha reactivado satisfactoriamente");
							System.out.println("Se ejecut� el eliminar");
							request.setAttribute("mensaje", message);
						}
						else {
							JSONObject message = new JSONObject();
							message.put("type", "danger");
							message.put("text", "No se ha podido reactivar el usuario debido fallos en el servidor");
							System.out.println("NO Se ejecut� el eliminar");
							request.setAttribute("mensaje", message);
						}
						List<Usuario> listadoUsuarios = obtenerUsuarios();
						request.setAttribute("usuarios",listadoUsuarios);
						RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-usuario/listado.ftl");
						rd.forward(request,response);
					}
				}else {
					JSONObject message = new JSONObject();
					message.put("type", "danger");
					message.put("text", "El usuario no se encuentra registrado");
					request.setAttribute("mensaje", message);
					List<Usuario> listadoUsuarios = obtenerUsuarios();
					request.setAttribute("usuarios",listadoUsuarios);
					RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-usuario/listado.ftl");
					rd.forward(request,response);
				}
			}
			
			else if(operacion.equals("registrar")) {
				//Se modifica la manera de obtener roles y paises, sin repetidos
				List<Rol> lista_roles = obtenerRoles();
				List<Pais> lista_paises = obtenerPaises();
				Set<Rol> roles = new LinkedHashSet<Rol>(lista_roles);
				Set<Pais> paises = new LinkedHashSet<Pais>(lista_paises);
				request.setAttribute("roles", roles);
				request.setAttribute("paises", paises);
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-usuario/registrar.ftl");
				rd.forward(request,response);
			}
			else if(operacion.equals("guardarNuevo")) {
				Usuario usuario = usrDao.getUsuarioPorEmail(request.getParameter("correo"));
				if(String.valueOf(usuario).equals("null")) {
					if(GuardarNuevo(request,response)) {
						JSONObject message = new JSONObject();
						message.put("type", "success");
						message.put("text", "El usuario se ha registrado con �xito");
						request.setAttribute("mensaje", message);
						List<Usuario> listadoUsuarios = obtenerUsuarios();
						request.setAttribute("usuarios",listadoUsuarios);
						RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-usuario/listado.ftl");
						rd.forward(request,response);
					}else {
						JSONObject message = new JSONObject();
						message.put("type", "danger");
						message.put("text", "No se ha podido registrar al usuario por fallos en el servidor");
						System.out.println("NO Se ejecut� el registrar");
						request.setAttribute("mensaje", message);
						List<Usuario> listadoUsuarios = obtenerUsuarios();
						request.setAttribute("usuarios",listadoUsuarios);
						RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-usuario/listado.ftl");
						rd.forward(request,response);
					}
				}else {
					JSONObject message = new JSONObject();
					message.put("type", "danger");
					message.put("text", "El correo ingresado ya pertenece a un usuario.");
					System.out.println("NO Se ejecut� el registrar");
					request.setAttribute("mensaje", message);
					List<Rol> lista_roles = obtenerRoles();
					List<Pais> lista_paises = obtenerPaises();
					Set<Rol> roles = new LinkedHashSet<Rol>(lista_roles);
					Set<Pais> paises = new LinkedHashSet<Pais>(lista_paises);
					request.setAttribute("roles", roles);
					request.setAttribute("paises", paises);
					RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-usuario/registrar.ftl");
					rd.forward(request,response);
				}
				
			}
			else {
				List<Usuario> listadoUsuarios = obtenerUsuarios();
				request.setAttribute("usuarios",listadoUsuarios);
				RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-usuario/listado.ftl");
				rd.forward(request,response);
			}
		}
		else {
			List<Usuario> listadoUsuarios = obtenerUsuarios();
			request.setAttribute("usuarios",listadoUsuarios);
			RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-usuario/listado.ftl");
			rd.forward(request,response);
			
			/*RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-usuario/listado.ftl");
			rd.forward(request,response);*/
		}
	
	}
	
	//Metodos cruds
	
	public boolean agregarUsuario(Usuario usuario) {
		try {
			usrDao.save(usuario);
			System.out.println("El usuario se ha registrado exitosamente");
			return true;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean agregarUserProfile(UserProfile user_profile, String correo) {
		try {
			Usuario usuario = usrDao.getUsuarioPorEmail(correo);
			System.out.println("ID:"+ usuario.getId());
			user_profile.setUsuario(usuario);
			usrprflDao.save(user_profile);
			System.out.println("El userprofile se ha registrado exitosamente");
			return true;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean agregarUsuarioGeneral(Usuario usuario, UserProfile user_profile) {
		/*Rol rol = new Rol();
		rol = (Rol) usrDao.get(Rol.class, 6);
		Pais pais = (Pais) usrDao.get(Pais.class, 1);
		Usuario usuario = new Usuario("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);*/
		try {
			if(agregarUsuario(usuario)) {
				if(agregarUserProfile(user_profile, usuario.getCorreo())) {
					System.out.println("Insertado Exitosamente");
					return true;
				}else
					return false;
			}else {
				return false;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	public List<Rol> obtenerRoles(){
		List<Rol> lista = rolDao.queryAll();
		return lista;
	}
	
	public List<Pais> obtenerPaises(){
		List<Pais> lista = paisDao.queryAll();
		return lista;
	}
	
	public Usuario obtenerUnUsuario(int idUsuario){
		Usuario usuario= new Usuario();
		usuario = usrDao.get(idUsuario);
		try {
//			System.out.println("ID: " + usuario.getId());
//			System.out.println("contrase�a: " + usuario.getContrasenna());
//			System.out.println("contrase�a Confirmaci�n: " +usuario.getContrasennaconfirmacion());
//			System.out.println("correo: " +usuario.getCorreo());
//			System.out.println("ultimo inicio sesion: " +usuario.getUltimoiniciosesion().toString());
//			System.out.println("------Datos del UserProfile--------");
//			if(!String.valueOf(usuario.getUserProfile()).equals("null")) {
//				System.out.println("Nombre: " + usuario.getUserProfile().getNombre());	
//			}else {
//				System.out.println("UserProfile: undefined");	
//			}
//			if(!String.valueOf(usuario.getRol()).equals("null")) {
//				System.out.println("Nombre: " + usuario.getRol().getNombre());
//				System.out.println("Descripcion: " + usuario.getRol().getDescripcion());
//				System.out.println("Status: " + usuario.getRol().getStatus());	
//			}else {
//				System.out.println("Rol: undefined");	
//			}
//			System.out.println("-------Datos del Pais------");
//			if(!String.valueOf(usuario.getPais()).equals("null")) {
//				System.out.println("Nombre: " + usuario.getPais().getNombre());
//				System.out.println("Status: " + usuario.getPais().getStatus());
//			}else {
//				System.out.println("Pais: undefined");
//			}
//			
//			System.out.println("Cargado Exitosamente");
			return usuario;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Usuario> obtenerUsuarios(){
		try {
			List<Usuario> lista = usrDao.queryAll();
			
			int i=1;
//			for (Usuario usuario : lista){
//				System.out.println("-------------Datos del Usuario: " + i);
//				System.out.println("ID: " + usuario.getId());
//				if(!String.valueOf(usuario.getUserProfile()).equals("null")) {
//					System.out.println("Nombre: " + usuario.getUserProfile().getNombre());
//					System.out.println("Apellido: " + usuario.getUserProfile().getApellido());
//				}
//				System.out.println("contrase�a: " + usuario.getContrasenna());
//				System.out.println("contrase�a Confirmaci�n: " +usuario.getContrasennaconfirmacion());
//				System.out.println("correo: " +usuario.getCorreo());
//				System.out.println("ultimo inicio sesion: " +usuario.getUltimoiniciosesion().toString());
//				System.out.println("------Datos del Rol--------");
//				if(!String.valueOf(usuario.getRol()).equals("null")) {
//					System.out.println("Nombre: " + usuario.getRol().getId());
//					System.out.println("Descripcion: " + usuario.getRol().getDescripcion());
//					System.out.println("Status: " + usuario.getRol().getStatus());	
//				}else {
//					System.out.println("Rol: undefined");	
//				}
//				System.out.println("-------Datos del Pais------");
//				if(!String.valueOf(usuario.getPais()).equals("null")) {
//					System.out.println("Nombre: " + usuario.nombrePais());
//				}else {
//					System.out.println("Pais: "+usuario.nombreRol()+"");
//				}
//				i++;
//
//			}
			return lista;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean eliminarUsuario(int idUsuario) {
		try {
			Usuario usuario = usrDao.get(idUsuario);
			UserProfile user_profile = usuario.getUserProfile();
			usuario.setStatus(2);
			user_profile.setStatus(2);
			usrDao.update(usuario);
			usrprflDao.update(user_profile);
			System.out.println("Usuario eliminado exito");
			return true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean reactivarUsuario(int idUsuario) {
		try {
			Usuario usuario = usrDao.get(idUsuario);
			UserProfile user_profile = usuario.getUserProfile();
			usuario.setStatus(1);
			user_profile.setStatus(2);
			usrDao.update(usuario);
			usrprflDao.update(user_profile);
			System.out.println("Usuario reactivado con exito");
			return true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean modificarGeneralUsuario(Usuario usuario, UserProfile user_profile) {
		try {
			if(modificarUsuario(usuario)) {
				System.out.println("Datos del usuario modificado exitosamente");
				if(modificarUserProfile(user_profile)) {
					System.out.println("Datos del user_profile modificado exitosamente");
					return true;
				}
			}
			return false;
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean modificarUsuario(Usuario usuario) {
		try {
			usrDao.update(usuario);
			System.out.println("Datos del usuario modificado exitosamente");
			return true;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean modificarUserProfile(UserProfile user_profile) {
		try {
			usrprflDao.update(user_profile);
			System.out.println("Datos del user_profile modificado exitosamente");
			return true;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean GuardarNuevo(HttpServletRequest request, HttpServletResponse response){
		if(request.getParameter("rol").equals("-1")) {
			System.out.println("no se selecciono rol");
		}
		else {
			System.out.println("va a guardar un nuevo registro");
			Usuario usuario = new Usuario();
			UserProfile user_profile = new UserProfile();
			usuario.setCorreo(request.getParameter("correo"));
			usuario.setContrasenna(request.getParameter("contrasenia"));
			usuario.setContrasennaconfirmacion(request.getParameter("confirmar_contrasenia"));
			usuario.setUltimoiniciosesion(new Date());
			Rol rol = (Rol) usrDao.get(Rol.class, 2);
			Pais pais = (Pais) usrDao.get(Pais.class, Integer.parseInt(request.getParameter("pais")));
			usuario.setRol(rol);
			usuario.setPais(pais);
			user_profile.setNombre(request.getParameter("nombre"));
			user_profile.setApellido(request.getParameter("apellido"));
			user_profile.setStatus(1);
			usuario.setStatus(1);
			System.out.println(usuario.getContrasenna()+" "+usuario.getContrasennaconfirmacion());
			if(agregarUsuarioGeneral(usuario, user_profile)) {
				return true;
			}else {
				return false;
			}
			//Usuario usuario = new Usuario("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);
		}
		return false;
	}
	
	public boolean GuardarCambio(HttpServletRequest request, HttpServletResponse response){
		if(request.getParameter("rol").equals("-1") 
			|| request.getParameter("pais").equals("-1")) {
			System.out.println("no se selecciono rol");
			return false;
		}
		else {
			System.out.println("Verificando datos a modificar....");
			Usuario usuario = obtenerUnUsuario(Integer.parseInt(request.getParameter("id")));
			UserProfile user_profile = usuario.getUserProfile();
			Pais pais = (Pais) usrDao.get(Pais.class, Integer.parseInt(request.getParameter("pais")));
			usuario.setUltimoiniciosesion(new Date());
			usuario.setStatus(Integer.parseInt(request.getParameter("status")));
			usuario.setPais(pais);
			user_profile.setNombre(request.getParameter("nombre"));
			user_profile.setApellido(request.getParameter("apellido"));
			System.out.println(usuario.getContrasenna()+" "+usuario.getContrasennaconfirmacion());
			//Verificacion de modificacion general de user y userprofile
			if(modificarGeneralUsuario(usuario, user_profile)) {
				
				return true;
			}else {
				return false;
			}
			//Usuario usuario = new Usuario("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);
		}
	}
}
