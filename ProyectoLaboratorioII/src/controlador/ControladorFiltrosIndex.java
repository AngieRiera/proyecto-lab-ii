package controlador;

import daos.OfertaEmpleoDAO;
import daos.utils.Sesion;
import modelo.Empleado;
import modelo.Empresa;
import modelo.Especialidad;
import modelo.Jornada;
import modelo.OfertaEmpleo;
import modelo.Pais;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;
import java.util.List;


public class ControladorFiltrosIndex {


	protected Sesion sesion;
	
	public ControladorFiltrosIndex() {
		// TODO Auto-generated constructor stub
		this.sesion = Sesion.getInstancia();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		new ControladorFiltrosIndex().FiltroEmpresaByName("Je");
		new ControladorFiltrosIndex().FiltroEmpleado("", 0);
	}

	public List<OfertaEmpleo> FiltroOfertaEmpledoPorPais(int id_estado, int id_municipio, int id_especialidad, int id_jornada, int id_pais, String by_name, String by_salario) {
 
		Session session = this.sesion.openSession();
		Criteria criterio = session.createCriteria(OfertaEmpleo.class);
		if(!by_salario.equals("0") && by_name.equals("")) {
			if(by_salario.equals("0-100")) {
				criterio.add(Restrictions.gt("salario", Float.parseFloat("0"))).add(Restrictions.le("salario", Float.parseFloat("100"))).setFetchMode("oferta_empleo", FetchMode.JOIN);;
			}else {
				if(by_salario.equals("101-300")) {
					criterio.add(Restrictions.gt("salario", Float.parseFloat("100"))).add(Restrictions.le("salario", Float.parseFloat("300"))).setFetchMode("oferta_empleo", FetchMode.JOIN);;
				}else {
					if(by_salario.equals("301-500")) {
						criterio.add(Restrictions.gt("salario", Float.parseFloat("300"))).add(Restrictions.le("salario", Float.parseFloat("500"))).setFetchMode("oferta_empleo", FetchMode.JOIN);;
					}else {
						if(by_salario.equals("501-1000")) {
							criterio.add(Restrictions.gt("salario", Float.parseFloat("500"))).add(Restrictions.le("salario", Float.parseFloat("1000"))).setFetchMode("oferta_empleo", FetchMode.JOIN);;
						}else {
							if(!by_salario.equals("0")) {
								criterio.add(Restrictions.gt("salario", Float.parseFloat("1000"))).setFetchMode("oferta_empleo", FetchMode.JOIN);;
							}
						}
					}
				}
			}
		}else{
			if(!by_name.equals("") && by_salario.equals("0")) {
				criterio.add(Restrictions.like("nombre", "%"+by_name+"%")).setFetchMode("oferta_empleo", FetchMode.JOIN);
			}else {
				if(!by_name.equals("") && !by_salario.equals("0")) {
					if(by_salario.equals("0-100")) {
						criterio.add(Restrictions.gt("salario", Float.parseFloat("0"))).add(Restrictions.le("salario", Float.parseFloat("100"))).add(Restrictions.like("nombre", "%"+by_name+"%")).setFetchMode("oferta_empleo", FetchMode.JOIN);;
					}else {
						if(by_salario.equals("101-300")) {
							criterio.add(Restrictions.gt("salario", Float.parseFloat("100"))).add(Restrictions.le("salario", Float.parseFloat("300"))).add(Restrictions.like("nombre", "%"+by_name+"%")).setFetchMode("oferta_empleo", FetchMode.JOIN);;
						}else {
							if(by_salario.equals("301-500")) {
								criterio.add(Restrictions.gt("salario", Float.parseFloat("300"))).add(Restrictions.le("salario", Float.parseFloat("500"))).add(Restrictions.like("nombre", "%"+by_name+"%")).setFetchMode("oferta_empleo", FetchMode.JOIN);;
							}else {
								if(by_salario.equals("501-1000")) {
									criterio.add(Restrictions.gt("salario", Float.parseFloat("500"))).add(Restrictions.le("salario", Float.parseFloat("1000"))).add(Restrictions.like("nombre", "%"+by_name+"%")).setFetchMode("oferta_empleo", FetchMode.JOIN);;
								}else {
									if(!by_salario.equals("0")) {
										criterio.add(Restrictions.gt("salario", Float.parseFloat("1000"))).add(Restrictions.like("nombre", "%"+by_name+"%")).setFetchMode("oferta_empleo", FetchMode.JOIN);;
									}
								}
							}
						}
					}
				}else {
					criterio.setFetchMode("oferta_empleo", FetchMode.JOIN);
				}
			}
		}
		if(id_estado != 0) {
			criterio.add(Restrictions.eq("estado.id", id_estado));
		}
		if(id_municipio != 0) {
			criterio.add(Restrictions.eq("municipio.id", id_municipio));
		}
		if(id_especialidad!=0) {
			criterio.add(Restrictions.eq("especialidad.id", id_especialidad));
		}
		if(id_jornada != 0) {
			criterio.add(Restrictions.eq("jornada.id", id_jornada));
		}
		if(id_pais != 0) {
			criterio.add(Restrictions.eq("pais.id", id_pais));
		}
//		if(id_estado == 0 && id_municipio == 0 && id_especialidad == 0 && id_jornada == 0 && id_pais == 0) {
//			
//		}
//		criterio.add(Restrictions.eq("nombre", "El mejor trabajo web")).setFetchMode("oferta_empleo", FetchMode.JOIN) 
//					.add(Restrictions.eq("pais.id", 3))
//					.add(Restrictions.eq("estado.id", 10))
//					.add(Restrictions.eq("jornada.id", 1))
//					.add(Restrictions.eq("especialidad.id", 1));
		List<OfertaEmpleo> ofertas = criterio.list();
		return ofertas;
//		if(ofertas.size() > 0) {
//			int i = 0;
//			for(OfertaEmpleo oferta : ofertas) {
//				System.out.println("-------------Datos de la Oferta: " + i);
//				System.out.println("ID: " + oferta.getId());
//				System.out.println("Nombre: " + oferta.getNombre());
//				System.out.println("Salario: " + oferta.getSalario());
//				System.out.println("Fecha Emisión: " + oferta.getFecha_emision());
//				System.out.println("Fecha Culminación: " + oferta.getFecha_culminacion());
//				System.out.println("Status: " + oferta.getStatus());
//				Especialidad especialidad = oferta.getEspecialidad();
//				System.out.println("--------------DATOS de la Especialidad de la oferta: "+ oferta.getNombre());
//				System.out.println("ID: " + especialidad.getId());
//				System.out.println("Nombre: " + especialidad.getNombre());
//				System.out.println("Descripción: " + especialidad.getDescripcion());
//				Jornada jornada = oferta.getJornada();
//				System.out.println("--------------DATOS de la Jornada de la oferta: "+ oferta.getNombre());
//				System.out.println("ID: " + jornada.getId());
//				System.out.println("Nombre: " + jornada.getNombre());
//				System.out.println("Descripción: " + jornada.getDescripcion());
//				Empresa empresa = oferta.getEmpresa();
//				System.out.println("--------------DATOS de la Empresa que hace la oferta: "+ oferta.getNombre());
//				System.out.println("ID: " + empresa.getId());
//				System.out.println("Correo: " + empresa.getUsuario().getCorreo());
//				System.out.println("Sitio Web: " + empresa.getSitio_web());
//				System.out.println("Dedicada a: "+empresa.getSector_actividad().getDescripcion());
//				Pais pais = oferta.getPais();
//				System.out.println("--------------DATOS del Pais de la oferta: "+ oferta.getNombre());
//				System.out.println("ID: " + pais.getId());
//				System.out.println("Nombre: " + pais.getNombre());
//				i++;
//			}
//		}else {
//			System.out.println("No se encontraron coincidencias");
//		}
		
	}
	
	public List<Empresa> FiltroEmpresaByName(String by_name_empresa){

		Session session = this.sesion.openSession();
		List<Empresa> empresas = session.createQuery("select empresa from Empresa empresa JOIN empresa.usuario usuario, UserProfile usr_prfl where usuario.id=usr_prfl.usuario.id and usr_prfl.nombre like '%"+ by_name_empresa +"%'").list();
		System.out.println("Size: " + empresas.size());
		return empresas;
	}
	
	public List<Empleado> FiltroEmpleado(String by_name_empleado, int by_ubicacion_empleado){
		Session session = this.sesion.openSession();
		List<Empleado> empleados = new ArrayList<Empleado>();
		if(!by_name_empleado.equals("") && String.valueOf(by_ubicacion_empleado).equals("0")) {
			 empleados = session.createQuery("select empleado from Empleado empleado JOIN empleado.usuario usuario, UserProfile usr_prfl where usuario.id=usr_prfl.usuario.id and usr_prfl.nombre like '%"+ by_name_empleado +"%'").list();
		}else if(by_name_empleado.equals("") && !String.valueOf(by_ubicacion_empleado).equals("0")) {
			 empleados = session.createQuery("select empleado from Empleado empleado JOIN empleado.usuario usuario, UserProfile usr_prfl where usuario.id=usr_prfl.usuario.id and usuario.pais.id=" + by_ubicacion_empleado).list();
		}else if(!by_name_empleado.equals("") && !String.valueOf(by_ubicacion_empleado).equals("0")) {
			 empleados = session.createQuery("select empleado from Empleado empleado JOIN empleado.usuario usuario, UserProfile usr_prfl where usuario.id=usr_prfl.usuario.id and usuario.pais.id=" + by_ubicacion_empleado + " and usr_prfl.nombre like '%"+ by_name_empleado +"%'").list();
		}else {
			 empleados = session.createQuery("from Empleado empleado").list();	
		}
		System.out.println("Size: " + empleados.size());
		return empleados;
	}
}
