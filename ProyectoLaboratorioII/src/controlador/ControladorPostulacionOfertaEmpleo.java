package controlador;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import daos.PaisDAO;
import daos.PostulacionDAO;
import daos.RolDAO;
import daos.EmpleadoDAO;
import daos.OfertaEmpleoDAO;
import modelo.Pais;
import modelo.Postulacion;
import modelo.Rol;
import modelo.Usuario;
import modelo.Curriculum;
import modelo.CurriculumEmpresa;
import modelo.Empleado;
import modelo.Empresa;
import modelo.OfertaEmpleo;

public class ControladorPostulacionOfertaEmpleo extends HttpServlet {
	
	private OfertaEmpleoDAO usrDao;
	private RolDAO rolDao;
	private PaisDAO paisDao;
	private OfertaEmpleoDAO ofertaEmpleoDao;
	private EmpleadoDAO empleadoDao;
	private PostulacionDAO postulacionDao;
	
	public ControladorPostulacionOfertaEmpleo() {
		// TODO Auto-generated constructor stub
		usrDao = new OfertaEmpleoDAO();
		rolDao = new RolDAO();
		paisDao = new PaisDAO();
		ofertaEmpleoDao = new OfertaEmpleoDAO();
		empleadoDao = new EmpleadoDAO();
		postulacionDao = new PostulacionDAO();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub	
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, Exception {
		HttpSession mysesion = (HttpSession)request.getSession();
		String id = request.getParameter("id");
		String operacion = request.getParameter("operacion");
		System.out.println(operacion);
		Usuario usuario = (Usuario) mysesion.getAttribute("usuario");
		
		if(usuario != null) {
			if(usuario.getRol().getId() == 2) {
				if(operacion != null) {
					if(id != null){
						if(operacion.equals("postular")) {
							OfertaEmpleo ofertaEmpleo = obtenerUnOfertaEmpleo(Integer.parseInt(id));
							//Empresa empresa = obtenerUnEmpresa(Integer.parseInt(id));
							System.out.println("correo por sesion: "+usuario.getCorreo());
							Empleado empleado = empleadoDao.getEmpleadoPorUserId(usuario.getId());
							System.out.println("correo por empleado: "+empleado.getUsuario().getCorreo());
							
							
							Postulacion postulacion = new Postulacion();
							postulacion.setEmpleado(empleado);
							postulacion.setFecha(new Date());
							postulacion.setStatus(1);
							postulacion.setOferta_empleo(ofertaEmpleo);
							
							System.out.println("Nombre de la oferta de empleo a postularse: "+postulacion.getOferta_empleo().getNombre());
							postulacionDao.save(postulacion);
							
							response.sendRedirect("/ProyectoLaboratorioII/oferta-empleo?id="+id);
							
						}
					}
				}				
				else {
		
					Empleado empleado=empleadoDao.getEmpleadoPorUserId(usuario.getId());
					int mensaje= 2;
					if(postulacionDao.confirmarPostulacion(Integer.parseInt(id), empleado.getId()) == true )
					{
						mensaje = 1;
					}
					
					request.setAttribute("mensaje", mensaje);
					
					OfertaEmpleo ofertaEmpleo = obtenerUnOfertaEmpleo(Integer.parseInt(id));
					System.out.println("Nombre de la oferta de empleo:"+ofertaEmpleo.getNombre());
					System.out.println("Nombre de la oferta de la especialidad:"+ofertaEmpleo.getEspecialidad().getNombre());
					System.out.println("Pais de la oferta de empleo "+ ofertaEmpleo.nombrePais());
					request.setAttribute("ofertaEmpleo", ofertaEmpleo);
					RequestDispatcher rd=request.getRequestDispatcher("detalleOfertaEmpleo.ftl");
					rd.forward(request,response);
					
					/*RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-usuario/listado.ftl");
					rd.forward(request,response);*/
				}
			}
			else {
				if(id != null) {
					int mensaje = 3;
					request.setAttribute("mensaje", mensaje);
					OfertaEmpleo ofertaEmpleo = obtenerUnOfertaEmpleo(Integer.parseInt(id));
					request.setAttribute("ofertaEmpleo", ofertaEmpleo);
					RequestDispatcher rd=request.getRequestDispatcher("detalleOfertaEmpleo.ftl");
					rd.forward(request,response);
				}
				else {
					RequestDispatcher rd = request.getRequestDispatcher("index.flt");
					rd.forward(request, response);
				}
			}
		}
		else {
			if(id != null) {
				int mensaje = 3;
				request.setAttribute("mensaje", mensaje);
				OfertaEmpleo ofertaEmpleo = obtenerUnOfertaEmpleo(Integer.parseInt(id));
				request.setAttribute("ofertaEmpleo", ofertaEmpleo);
				RequestDispatcher rd=request.getRequestDispatcher("detalleOfertaEmpleo.ftl");
				rd.forward(request,response);
			}
			else {
				RequestDispatcher rd = request.getRequestDispatcher("index.flt");
				rd.forward(request, response);
			}
		}
	}
	
	
	//Metodos cruds
	//public void agregarOfertaEmpleo(OfertaEmpleo usuario) {
		/*Rol rol = new Rol();
		rol = (Rol) usrDao.get(Rol.class, 6);
		Pais pais = (Pais) usrDao.get(Pais.class, 1);
		OfertaEmpleo usuario = new OfertaEmpleo("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);*/
		/*try {
			usrDao.save(usuario);
			System.out.println("Insertado Exitosamente");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public List<Rol> obtenerRoles(){
		List<Rol> lista = rolDao.queryAll();
		return lista;
	}
	
	public List<Pais> obtenerPaises(){
		List<Pais> lista = paisDao.queryAll();
		return lista;
	}
	*/
	public OfertaEmpleo obtenerUnOfertaEmpleo(int idOfertaEmpleo){
		OfertaEmpleo oferta_empleo= new OfertaEmpleo();
		oferta_empleo = ofertaEmpleoDao.get(idOfertaEmpleo);
		return oferta_empleo;
	}
	/*
	public List<OfertaEmpleo> obtenerOfertaEmpleos(){
		try {
			List<OfertaEmpleo> lista = usrDao.queryAll();
			
			int i=1;
			for (OfertaEmpleo usuario : lista){
				System.out.println("-------------Datos del OfertaEmpleo: " + i);
				System.out.println("ID: " + usuario.getId());
				System.out.println("contraseña: " + usuario.getContrasenna());
				System.out.println("contraseña Confirmación: " +usuario.getContrasennaconfirmacion());
				System.out.println("correo: " +usuario.getCorreo());
				System.out.println("ultimo inicio sesion: " +usuario.getUltimoiniciosesion().toString());
				System.out.println("------Datos del Rol--------");
				if(!String.valueOf(usuario.getRol()).equals("null")) {
					System.out.println("Nombre: " + usuario.getRol().getId());
					System.out.println("Descripcion: " + usuario.getRol().getDescripcion());
					System.out.println("Status: " + usuario.getRol().getStatus());	
				}else {
					System.out.println("Rol: undefined");	
				}
				System.out.println("-------Datos del Pais------");
				if(!String.valueOf(usuario.getPais()).equals("null")) {
					System.out.println("Nombre: " + usuario.getPais().getNombre());
					System.out.println("Status: " + usuario.getPais().getStatus());
				}else {
					System.out.println("Pais: "+usuario.nombreRol()+"");
				}
				i++;

			}
			return lista;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void eliminarOfertaEmpleo(int idOfertaEmpleo) {
		try {
			OfertaEmpleo usuario = usrDao.get(idOfertaEmpleo);
			usrDao.delete(usuario);
			System.out.println("OfertaEmpleo eliminado exito");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void modificarOfertaEmpleo(OfertaEmpleo usuario) {
		try {
			usrDao.update(usuario);
			System.out.println("Datos del usuario modificado exitosamente");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean GuardarNuevo(HttpServletRequest request, HttpServletResponse response){
		if(request.getParameter("rol").equals("-1")) {
			System.out.println("no se selecciono rol");
		}
		else {
			System.out.println("va a guardar un nuevo registro");
			OfertaEmpleo usuario = new OfertaEmpleo();
			usuario.setCorreo(request.getParameter("correo"));
			usuario.setContrasenna(request.getParameter("contrasenia"));
			usuario.setContrasennaconfirmacion(request.getParameter("confirmar-contrasenia"));
			usuario.setUltimoiniciosesion(new Date());
			usuario.setStatus(Integer.parseInt(request.getParameter("status")));
			Rol rol = (Rol) usrDao.get(Rol.class, Integer.parseInt(request.getParameter("rol")));
			Pais pais = (Pais) usrDao.get(Pais.class, Integer.parseInt(request.getParameter("pais")));
			usuario.setRol(rol);
			usuario.setPais(pais);
			System.out.println(usuario.getContrasenna()+" "+usuario.getContrasennaconfirmacion());
			agregarOfertaEmpleo(usuario);
			//OfertaEmpleo usuario = new OfertaEmpleo("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);
		}
		return false;
	}
	
	public boolean GuardarCambio(HttpServletRequest request, HttpServletResponse response){
		if(request.getParameter("rol").equals("-1") 
			|| request.getParameter("pais").equals("-1")) {
			System.out.println("no se selecciono rol");
		}
		else {
			OfertaEmpleo usuario = obtenerUnOfertaEmpleo(Integer.parseInt(request.getParameter("id")));
			usuario.setCorreo(request.getParameter("correo"));
			usuario.setUltimoiniciosesion(new Date());
			usuario.setStatus(Integer.parseInt(request.getParameter("status")));
			Rol rol = (Rol) usrDao.get(Rol.class, Integer.parseInt(request.getParameter("rol")));
			Pais pais = (Pais) usrDao.get(Pais.class, Integer.parseInt(request.getParameter("pais")));
			usuario.setRol(rol);
			usuario.setPais(pais);
			System.out.println(usuario.getContrasenna()+" "+usuario.getContrasennaconfirmacion());
			modificarOfertaEmpleo(usuario);
			//OfertaEmpleo usuario = new OfertaEmpleo("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);
		}
		return false;
	}*/
}
