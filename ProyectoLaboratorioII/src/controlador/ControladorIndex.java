package controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import daos.*;
import modelo.*;

@WebServlet("/ControladorEstudiante")
public class ControladorIndex extends HttpServlet {
	
	private PaisDAO paisDao;
	private EstadoDAO estadoDao;
	private JornadaDAO jornadaDao;
	private SectorActividadDAO setoractividadDao;
	private MunicipioDAO munciopioDao;
	private OfertaEmpleoDAO ofertaEmpleoDao;
	private EmpresaDAO empresaDao;
	private EmpleadoDAO empleadoDao;
	private int id_estado, id_municipio, id_especialidad, id_jornada, id_pais, by_ubicacion_empleado;
	private String by_name, by_salario, by_name_empresa, by_name_empleado;
	private CurriculumDAO curriculumDao;
	
	public ControladorIndex() {
		super();
		this.paisDao = PaisDAO.getInstancia();
		this.estadoDao = EstadoDAO.getInstancia();
		this.jornadaDao = JornadaDAO.getInstancia();
		this.setoractividadDao = SectorActividadDAO.getInstancia();
		this.munciopioDao = MunicipioDAO.getInstancia();
		this.ofertaEmpleoDao = OfertaEmpleoDAO.getInstancia();
		this.empresaDao = EmpresaDAO.getInstancia();
		this.curriculumDao = CurriculumDAO.getInstancia();
		this.empleadoDao = EmpleadoDAO.getInstancia();
		this.id_especialidad = this.id_estado = this.id_municipio = this.id_jornada = this.id_pais = this.by_ubicacion_empleado = 0;
		this.by_name = "";
		this.by_salario = "";
		this.by_name_empresa = "";
		this.by_name_empleado= "";
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<OfertaEmpleo> ofertas_slices;
		String operacion = request.getParameter("operacion");
		System.out.println(operacion);
		List<Empleado> empleados = empleadoDao.queryAll();
		List<Pais> lista_paises = paisDao.queryAll();
		List<Estado> lista_estado = estadoDao.queryAll();
		List<Jornada> lista_jornada = jornadaDao.queryAll();
		List<SectorActividad> lista_sector_actividad = setoractividadDao.queryAll();
		List<Municipio> lista_municipios = munciopioDao.queryAll();
		Set<Pais> paises = new LinkedHashSet<Pais>(lista_paises);
		Set<Estado> estados = new LinkedHashSet<Estado>(lista_estado);
		Set<Jornada> jornadas = new LinkedHashSet<Jornada>(lista_jornada);
		Set<Municipio> municipios = new LinkedHashSet<Municipio>(lista_municipios);
		Set<SectorActividad> sector_actividades = new LinkedHashSet<SectorActividad>(lista_sector_actividad);
		List<Empleado> nuevos_empleados = new ArrayList<Empleado>();
		for(Empleado empleado:empleados) {
			Curriculum curriculum = curriculumDao.getCurriculumPorEmpleadoId(empleado.getId());
			if(!String.valueOf(curriculum).equals("null")) {
				nuevos_empleados.add(empleado);
			}
		}
		Set<Empleado> nuevos_empleados_con_cv = new LinkedHashSet<Empleado>(nuevos_empleados);
		if(operacion!=null) {
			if(operacion.equals("filtro")) {
				if(!request.getParameter("estado").equals("null")) {
					this.id_estado = Integer.parseInt(request.getParameter("estado"));
				}
				if(!request.getParameter("municipio").equals("null")) {
					this.id_municipio = Integer.parseInt(request.getParameter("municipio"));
				}
				if(!request.getParameter("especialidad").equals("null")) {
					this.id_especialidad = Integer.parseInt(request.getParameter("especialidad"));
				}
				if(!request.getParameter("jornada").equals("null")) {
					this.id_jornada = Integer.parseInt(request.getParameter("jornada"));
				}
				if(!request.getParameter("pais").equals("null")) {
					this.id_pais = Integer.parseInt(request.getParameter("pais"));
				} 
				if(!request.getParameter("by_name").equals("null")) {
					this.by_name = request.getParameter("by_name");
				} 
				if(!request.getParameter("by_salario").equals("null")) {
					this.by_salario = request.getParameter("by_salario");
				} 

				System.out.println("id_estado: " + this.id_estado);
				System.out.println("id_municipio: " + this.id_municipio);
				System.out.println("id_especialidad: " + this.id_especialidad);
				System.out.println("id_jornada: " + this.id_jornada);
				System.out.println("id_pais: " + this.id_pais);
				System.out.println("by_name: " + this.by_name);
				System.out.println("by_salario: " + this.by_salario);
				List<OfertaEmpleo> ofertas = new ControladorFiltrosIndex().FiltroOfertaEmpledoPorPais(id_estado, id_municipio, id_especialidad, id_jornada, id_pais, by_name, by_salario);
				ofertas_slices = ofertas.subList(0, ofertas.size()/2);
				request.setAttribute("Empresas", empresaDao.queryAll());
				request.setAttribute("Empleados", nuevos_empleados_con_cv);
				request.setAttribute("Ofertas", ofertas);
				request.setAttribute("OfertasFooter", ofertas_slices);
				request.setAttribute("Paises", paises);
				request.setAttribute("Estados", estados);
				request.setAttribute("Jornadas", jornadas);
				request.setAttribute("Sectores", sector_actividades);
				request.setAttribute("Municipios", municipios);
				request.getRequestDispatcher("/index.ftl").forward(request, response);
			}else {

				System.out.println("by_name_empresa:*********** " );
				if(operacion.equals("filtro_empresa")) {
					if(!request.getParameter("by_name_empresa").equals("null")) {
						this.by_name_empresa = request.getParameter("by_name_empresa");
					} 
					System.out.println("by_name_empresa:*********** " + this.by_name_empresa);
					List<Empresa> empresas = new ControladorFiltrosIndex().FiltroEmpresaByName(by_name_empresa); 
					List<OfertaEmpleo> ofertas = ofertaEmpleoDao.queryAll();
					ofertas_slices = ofertas.subList(0, ofertas.size()/2);
					request.setAttribute("Empresas", empresas);
					request.setAttribute("Empleados", nuevos_empleados_con_cv);
					request.setAttribute("Paises", paises);
					request.setAttribute("Estados", estados);
					request.setAttribute("Jornadas", jornadas);
					request.setAttribute("Sectores", sector_actividades);
					request.setAttribute("Municipios", municipios);
					request.setAttribute("OfertasFooter", ofertas_slices);
					request.setAttribute("Ofertas", ofertas);
					request.getRequestDispatcher("/index.ftl").forward(request, response);
				}else {
					if(operacion.equals("filtro_empleado")) {
						if(!request.getParameter("by_name_empleado").equals("null")) {
							this.by_name_empleado = request.getParameter("by_name_empleado");
						}
						if(!request.getParameter("by_ubicacion_empleado").equals("null")) {
							this.by_ubicacion_empleado = Integer.parseInt(request.getParameter("by_ubicacion_empleado"));
						}
						System.out.println("by_name_empleado:*********** " + this.by_name_empleado);
						System.out.println("by_ubicacion_empleado:*********** " + this.by_ubicacion_empleado);
						List<Empleado> empleados_filtro = new ControladorFiltrosIndex().FiltroEmpleado(by_name_empleado, by_ubicacion_empleado); 
						List<Empleado> nuevos_empleados_filtro = new ArrayList<Empleado>();
						for(Empleado empleado:empleados_filtro) {
							Curriculum curriculum = curriculumDao.getCurriculumPorEmpleadoId(empleado.getId());
							if(!String.valueOf(curriculum).equals("null")) {
								nuevos_empleados_filtro.add(empleado);
							}
						}
						Set<Empleado> nuevos_empleados_filtro_con_cv = new LinkedHashSet<Empleado>(nuevos_empleados_filtro);
						List<OfertaEmpleo> ofertas = ofertaEmpleoDao.queryAll();
						ofertas_slices = ofertas.subList(0, ofertas.size()/2);
						request.setAttribute("Empleados", nuevos_empleados_filtro_con_cv);
						request.setAttribute("Paises", paises);
						request.setAttribute("Estados", estados);
						request.setAttribute("Jornadas", jornadas);
						request.setAttribute("Sectores", sector_actividades);
						request.setAttribute("Municipios", municipios);
						request.setAttribute("OfertasFooter", ofertas_slices);
						request.setAttribute("Ofertas", ofertas);
						request.getRequestDispatcher("/index.ftl").forward(request, response);
					}else {
						List<OfertaEmpleo> ofertas = ofertaEmpleoDao.queryAll();
						ofertas_slices = ofertas.subList(0, ofertas.size()/2);
						request.setAttribute("Empresas", empresaDao.queryAll());
						request.setAttribute("Empleados", nuevos_empleados_con_cv);
						request.setAttribute("Paises", paises);
						request.setAttribute("Estados", estados);
						request.setAttribute("Jornadas", jornadas);
						request.setAttribute("Sectores", sector_actividades);
						request.setAttribute("Municipios", municipios);
						request.setAttribute("Ofertas", ofertas);
						request.setAttribute("OfertasFooter", ofertas_slices);
						request.getRequestDispatcher("/index.ftl").forward(request, response);
					}
				}
			}
		}else {
			System.out.println("size"+ empresaDao.queryAll().size());
			List<OfertaEmpleo> ofertas = ofertaEmpleoDao.queryAll();
			ofertas_slices = ofertas.subList(0, ofertas.size()/2);
			request.setAttribute("Empresas", empresaDao.queryAll());
			request.setAttribute("Empleados", nuevos_empleados_con_cv);
			request.setAttribute("Paises", paises);
			request.setAttribute("Estados", estados);
			request.setAttribute("Jornadas", jornadas);
			request.setAttribute("Sectores", sector_actividades);
			request.setAttribute("Municipios", municipios);
			request.setAttribute("Ofertas", ofertas);
			request.setAttribute("OfertasFooter", ofertas_slices);
			request.getRequestDispatcher("/index.ftl").forward(request, response);
		}
	}
}
