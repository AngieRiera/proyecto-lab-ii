package controlador;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;

import daos.CurriculumDAO;
import daos.CurriculumEmpresaDAO;
import daos.EmpleadoDAO;
import daos.EmpresaDAO;
import daos.UserProfileDAO;
import daos.UsuarioDAO;
import modelo.Curriculum;
import modelo.CurriculumEmpresa;
import modelo.Empleado;
import modelo.Empresa;
import modelo.Usuario;

public class ControladorCurriculumsEnviados extends HttpServlet{
	
	private CurriculumEmpresaDAO curriculum_empresaDao;
	private EmpleadoDAO empleadoDao;
	private CurriculumDAO curriculumDao;
	private EmpresaDAO empresaDao;

	public ControladorCurriculumsEnviados() {
		curriculum_empresaDao = new CurriculumEmpresaDAO();
		empleadoDao = new EmpleadoDAO();
		curriculumDao = new CurriculumDAO();
		empresaDao = new EmpresaDAO();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			RealizarAccion(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			RealizarAccion(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpSession sesion = (HttpSession) request.getSession();
		Usuario usuario = (Usuario) sesion.getAttribute("usuario");
		
		if(usuario != null) {
			if(usuario.getRol().getId() == 2) {
				Empleado empleado = empleadoDao.getEmpleadoPorUserId(usuario.getId());
				Curriculum curriculum = curriculumDao.getCurriculumPorEmpleadoId(empleado.getId());
				if(curriculum != null)
					ListaEmpresas(request, response, usuario);
				else {
					RequestDispatcher rd = request.getRequestDispatcher("/dashboard/empleado/index.ftl");
					rd.forward(request, response);
				}
			}
			else {
				RequestDispatcher rd = request.getRequestDispatcher("login.ftl");
				rd.forward(request, response);
			}
		}
		else {
			RequestDispatcher rd =request.getRequestDispatcher("login.ftl");
			rd.forward(request, response);
		}
	}
	
	public void ListaEmpresas(HttpServletRequest request, HttpServletResponse response, Usuario usuario) throws Exception {
		Empleado empleado = empleadoDao.getEmpleadoPorUserId(usuario.getId());
		Curriculum curriculum = curriculumDao.getCurriculumPorEmpleadoId(empleado.getId());
		
		//obtiene las relaciones del curriculum con las empresas a las que lo ha mandado
		List<CurriculumEmpresa> curriculum_empresas = curriculum_empresaDao.curriculumPorEmpleado(curriculum.getId());
		List<Empresa> empresas = new ArrayList<Empresa>();
		
		//llena una lista con la informacion de las empresas a las que se ha enviado el curriculo
		for(int i=0; i<curriculum_empresas.size();i++) {
			Empresa empresa = curriculum_empresas.get(i).getEmpresa();
			empresas.add(empresa);
		}
		
		request.setAttribute("empresas", empresas);
		RequestDispatcher rd = request.getRequestDispatcher("/dashboard/empleado/listados/listado-curriculums-enviados.ftl");
		rd.forward(request, response);
	}
}
