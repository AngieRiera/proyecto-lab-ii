package controlador;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import daos.PaisDAO;
import daos.RolDAO;
import daos.CurriculumDAO;
import daos.EmpleadoDAO;
import daos.EmpresaDAO;
import modelo.Pais;
import modelo.Rol;
import modelo.Usuario;
import modelo.Curriculum;
import modelo.CurriculumEmpresa;
import modelo.Empleado;
import modelo.Empresa;

public class ControladorDetalleEmpresa extends HttpServlet {
	
	private EmpresaDAO empresaDao;
	private RolDAO rolDao;
	private PaisDAO paisDao;
	private EmpleadoDAO empleadoDao;
	private CurriculumDAO curriculumDao;
	
	public ControladorDetalleEmpresa() {
		// TODO Auto-generated constructor stub
		empresaDao = new EmpresaDAO();
		rolDao = new RolDAO();
		paisDao = new PaisDAO();
		empleadoDao = new EmpleadoDAO();
		curriculumDao = new CurriculumDAO();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub	
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, Exception {
		HttpSession mysesion = (HttpSession)request.getSession();
		String id = request.getParameter("id");
		String operacion = request.getParameter("operacion");
		System.out.println(operacion);
		if(operacion != null) {
			if(id != null){
				if(operacion.equals("postular")) {
					Usuario usuario = (Usuario) mysesion.getAttribute("usuario");
					Empresa empresa = obtenerUnEmpresa(Integer.parseInt(id));
					System.out.println("correo por sesion: "+usuario.getCorreo());
					Empleado empleado = empleadoDao.getEmpleadoPorUserId(usuario.getId());
					System.out.println("correo por empleado: "+empleado.getUsuario().getCorreo());
					
					Curriculum curriculum = curriculumDao.getCurriculumPorEmpleadoId(empleado.getId());
					System.out.println("Descripcon del curriculum: "+curriculum.getConocimientos_adicionales());
					
					CurriculumEmpresa curriculumEmpresa = new CurriculumEmpresa();
					curriculumEmpresa.setFecha_envio(new Date());
					curriculumEmpresa.setCurriculum(curriculum);
					curriculumEmpresa.setEmpresa(empresa);
					curriculumEmpresa.setStatus(1);
					empresaDao.save(curriculumEmpresa);
					response.sendRedirect("/ProyectoLaboratorioII/detalle-empresa?id="+id);
				}
			}
		}		
		
		else {
			Usuario usuario = (Usuario) mysesion.getAttribute("usuario");
			Empleado empleado=empleadoDao.getEmpleadoPorUserId(usuario.getId());
			System.out.println(empleado.getId());
			int mensaje= 2;
			if(empresaDao.confirmarPostulacion(empleadoDao.getCurriculumId(empleado.getId()).getId(), Integer.parseInt(id) ) == true )
			{
				mensaje = 1;
			}
			
			request.setAttribute("mensaje", mensaje);
			Empresa detalleEmpresa = obtenerUnEmpresa(Integer.parseInt(id));
			request.setAttribute("detalleEmpresa", detalleEmpresa);
			System.out.println("Facebook: "+detalleEmpresa.getUsuario().getUserProfile().obtenerFacebook());
			detalleEmpresa.getUsuario().getUserProfile().getNombre();
			
			RequestDispatcher rd=request.getRequestDispatcher("detalleEmpresa.ftl");
			rd.forward(request,response);
			
			/*RequestDispatcher rd=request.getRequestDispatcher("/dashboard/admin/gestion-usuario/listado.ftl");
			rd.forward(request,response);*/
		}
	
	}
	
	//Metodos cruds
	//public void agregarEmpresa(Empresa usuario) {
		/*Rol rol = new Rol();
		rol = (Rol) usrDao.get(Rol.class, 6);
		Pais pais = (Pais) usrDao.get(Pais.class, 1);
		Empresa usuario = new Empresa("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);*/
		/*try {
			usrDao.save(usuario);
			System.out.println("Insertado Exitosamente");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public List<Rol> obtenerRoles(){
		List<Rol> lista = rolDao.queryAll();
		return lista;
	}
	
	public List<Pais> obtenerPaises(){
		List<Pais> lista = paisDao.queryAll();
		return lista;
	}
	*/
	public Empresa obtenerUnEmpresa(int idEmpresa){
		Empresa oferta_empleo= new Empresa();
		oferta_empleo = empresaDao.get(idEmpresa);
		return oferta_empleo;
	}
	/*
	public List<Empresa> obtenerEmpresas(){
		try {
			List<Empresa> lista = usrDao.queryAll();
			
			int i=1;
			for (Empresa usuario : lista){
				System.out.println("-------------Datos del Empresa: " + i);
				System.out.println("ID: " + usuario.getId());
				System.out.println("contraseña: " + usuario.getContrasenna());
				System.out.println("contraseña Confirmación: " +usuario.getContrasennaconfirmacion());
				System.out.println("correo: " +usuario.getCorreo());
				System.out.println("ultimo inicio sesion: " +usuario.getUltimoiniciosesion().toString());
				System.out.println("------Datos del Rol--------");
				if(!String.valueOf(usuario.getRol()).equals("null")) {
					System.out.println("Nombre: " + usuario.getRol().getId());
					System.out.println("Descripcion: " + usuario.getRol().getDescripcion());
					System.out.println("Status: " + usuario.getRol().getStatus());	
				}else {
					System.out.println("Rol: undefined");	
				}
				System.out.println("-------Datos del Pais------");
				if(!String.valueOf(usuario.getPais()).equals("null")) {
					System.out.println("Nombre: " + usuario.getPais().getNombre());
					System.out.println("Status: " + usuario.getPais().getStatus());
				}else {
					System.out.println("Pais: "+usuario.nombreRol()+"");
				}
				i++;

			}
			return lista;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void eliminarEmpresa(int idEmpresa) {
		try {
			Empresa usuario = usrDao.get(idEmpresa);
			usrDao.delete(usuario);
			System.out.println("Empresa eliminado exito");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void modificarEmpresa(Empresa usuario) {
		try {
			usrDao.update(usuario);
			System.out.println("Datos del usuario modificado exitosamente");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean GuardarNuevo(HttpServletRequest request, HttpServletResponse response){
		if(request.getParameter("rol").equals("-1")) {
			System.out.println("no se selecciono rol");
		}
		else {
			System.out.println("va a guardar un nuevo registro");
			Empresa usuario = new Empresa();
			usuario.setCorreo(request.getParameter("correo"));
			usuario.setContrasenna(request.getParameter("contrasenia"));
			usuario.setContrasennaconfirmacion(request.getParameter("confirmar-contrasenia"));
			usuario.setUltimoiniciosesion(new Date());
			usuario.setStatus(Integer.parseInt(request.getParameter("status")));
			Rol rol = (Rol) usrDao.get(Rol.class, Integer.parseInt(request.getParameter("rol")));
			Pais pais = (Pais) usrDao.get(Pais.class, Integer.parseInt(request.getParameter("pais")));
			usuario.setRol(rol);
			usuario.setPais(pais);
			System.out.println(usuario.getContrasenna()+" "+usuario.getContrasennaconfirmacion());
			agregarEmpresa(usuario);
			//Empresa usuario = new Empresa("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);
		}
		return false;
	}
	
	public boolean GuardarCambio(HttpServletRequest request, HttpServletResponse response){
		if(request.getParameter("rol").equals("-1") 
			|| request.getParameter("pais").equals("-1")) {
			System.out.println("no se selecciono rol");
		}
		else {
			Empresa usuario = obtenerUnEmpresa(Integer.parseInt(request.getParameter("id")));
			usuario.setCorreo(request.getParameter("correo"));
			usuario.setUltimoiniciosesion(new Date());
			usuario.setStatus(Integer.parseInt(request.getParameter("status")));
			Rol rol = (Rol) usrDao.get(Rol.class, Integer.parseInt(request.getParameter("rol")));
			Pais pais = (Pais) usrDao.get(Pais.class, Integer.parseInt(request.getParameter("pais")));
			usuario.setRol(rol);
			usuario.setPais(pais);
			System.out.println(usuario.getContrasenna()+" "+usuario.getContrasennaconfirmacion());
			modificarEmpresa(usuario);
			//Empresa usuario = new Empresa("Hoslasss@gmail", "123", "123", new Date(), 1, rol, pais);
		}
		return false;
	}*/
}
