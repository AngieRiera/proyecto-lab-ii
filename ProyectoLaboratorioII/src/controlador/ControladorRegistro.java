package controlador;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import daos.EmpleadoDAO;
import daos.EmpresaDAO;
import daos.PaisDAO;
import daos.RolDAO;
import daos.SectorActividadDAO;
import daos.UserProfileDAO;
import daos.UsuarioDAO;
import modelo.Empleado;
import modelo.Empresa;
import modelo.Pais;
import modelo.Rol;
import modelo.SectorActividad;
import modelo.UserProfile;
import modelo.Usuario;

public class ControladorRegistro extends HttpServlet {
	
	private UsuarioDAO usrDao;
	private RolDAO rolDao;
	private PaisDAO paisDao;
	private SectorActividadDAO sectorDao;
	private UserProfileDAO user_profileDao;
	private EmpleadoDAO empleadoDao;
	private EmpresaDAO empresaDao;
	
	public ControladorRegistro() {
		// TODO Auto-generated constructor stub
		usrDao = new UsuarioDAO();
		rolDao = new RolDAO();
		paisDao = new PaisDAO();
		user_profileDao = new UserProfileDAO();
		sectorDao = new SectorActividadDAO();
		empleadoDao = new EmpleadoDAO();
		empresaDao = new EmpresaDAO();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub	
		try {
			RealizarAccion(request,response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void RealizarAccion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException {
		String tipo = request.getParameter("tipo");
		System.out.println(tipo);
		String operacion = request.getParameter("operacion");
		
		if(tipo!= null) {			
			if(tipo.equals("empleado")) {				
				if(operacion !=null && operacion.equals("registrar")) {
					RegistrarEmpleado(request, response);
					RequestDispatcher rd=request.getRequestDispatcher("/login");
					rd.forward(request,response);
				}
				else {
					List<Pais> lista_paises = paisDao.queryAll();
					Set<Pais> paises = new LinkedHashSet<Pais>(lista_paises);
					request.setAttribute("paises", paises);
					request.setAttribute("tipo_registro", 2); //2. Empleado
					RequestDispatcher rd=request.getRequestDispatcher("/registro.ftl");
					rd.forward(request,response);
				}
			}
			else if(tipo.equals("empresa")) {
				if(operacion !=null && operacion.equals("registrar")) {
					RegistrarEmpresa(request, response);
					RequestDispatcher rd=request.getRequestDispatcher("/login");
					rd.forward(request,response);
				}
				else {
					List<Pais> lista_paises = paisDao.queryAll();
					Set<Pais> paises = new LinkedHashSet<Pais>(lista_paises);
					List<SectorActividad> lista_sectores = sectorDao.queryAll();
					Set<SectorActividad> sectores = new LinkedHashSet<SectorActividad>(lista_sectores);
					request.setAttribute("paises", paises);
					request.setAttribute("sectores", sectores);
					
					request.setAttribute("tipo_registro", 3); //3. Empresa
					RequestDispatcher rd=request.getRequestDispatcher("/registro.ftl");
					rd.forward(request,response);
				}
			}
			
		}
		else {
			
			RequestDispatcher rd=request.getRequestDispatcher("/tipoRegistro.ftl");
			rd.forward(request,response);
		}
	
	}
	
	public void RegistrarEmpleado(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Empleado empleado = new Empleado();
		Usuario usuario = new Usuario();
		UserProfile user_profile = new UserProfile();
		
		usuario.setCorreo(request.getParameter("correo"));		
		usuario.setContrasenna(request.getParameter("contrasenna"));
		usuario.setContrasennaconfirmacion(request.getParameter("confirmar_contrasenna"));
		usuario.setStatus(1);			
		Pais pais = paisDao.get(Integer.parseInt(request.getParameter("pais")));
		usuario.setPais(pais);
		Rol rol = rolDao.get(2);
		usuario.setRol(rol);			
		usrDao.save(usuario);
					
		Usuario usuario_extra = usrDao.getUsuarioCorreo(usuario.getCorreo());					
		user_profile.setUsuario(usuario_extra);
		user_profile.setNombre(request.getParameter("nombre"));
		user_profile.setApellido(request.getParameter("apellido"));
		user_profile.setDireccion(request.getParameter("direccion"));
		user_profile.setTelefono(request.getParameter("telefono"));
		user_profile.setUrl_facebook(request.getParameter("url_facebook"));
		user_profile.setUrl_instagram(request.getParameter("url_instagram"));
		user_profile.setUrl_twitter(request.getParameter("url_twitter"));
		user_profile.setStatus(1);
		user_profileDao.save(user_profile);
					
		empleado.setUsuario(usuario_extra);
		empleado.setCedula(request.getParameter("cedula"));
		empleado.setFecha_nacimiento(empleado.StringToDateRegistro(request.getParameter("fecha_nacimiento")));
		empleado.setGenero(Integer.parseInt(request.getParameter("genero")));
		empleado.setObservaciones(request.getParameter("observaciones"));
		empleado.setCalificacion(0);
		empleado.setStatus(1);
		empleadoDao.save(empleado);
		
		HttpSession session = request.getSession(true);
		session.setAttribute("usuario", usuario);
		
	}
	
	public void RegistrarEmpresa (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		Empresa empresa = new Empresa();
		Usuario usuario = new Usuario();
		UserProfile user_profile = new UserProfile();
		
			usuario.setCorreo(request.getParameter("correo"));
			usuario.setContrasenna(request.getParameter("contrasenna"));
			usuario.setContrasennaconfirmacion(request.getParameter("confirmar_contrasenna"));
			usuario.setStatus(1);			
			Pais pais = paisDao.get(Integer.parseInt(request.getParameter("pais")));
			usuario.setPais(pais);
			Rol rol = rolDao.get(3);
			System.out.println(rol.getNombre());
			usuario.setRol(rol);			
			usrDao.save(usuario);
			
			Usuario usuario_extra = usrDao.getUsuarioCorreo(usuario.getCorreo());					
			user_profile.setUsuario(usuario_extra);
			user_profile.setNombre(request.getParameter("nombre"));
			user_profile.setApellido(request.getParameter("apellido"));
			user_profile.setDireccion(request.getParameter("direccion"));
			user_profile.setTelefono(request.getParameter("telefono"));
			user_profile.setUrl_facebook(request.getParameter("url_facebook"));
			user_profile.setUrl_instagram(request.getParameter("url_instagram"));
			user_profile.setUrl_twitter(request.getParameter("url_twitter"));
			user_profile.setStatus(1);
			user_profileDao.save(user_profile);
			
			empresa.setUsuario(usuario_extra);
			empresa.setDireccion(request.getParameter("direccion"));
			empresa.setRif(request.getParameter("rif"));
			SectorActividad sector_actividad = sectorDao.get(Integer.parseInt(request.getParameter("sector_actividad")));
			empresa.setSector_actividad(sector_actividad);
			empresa.setSitio_web("sitio_web");
			empresa.setStatus(1);
			empresaDao.save(empresa);
			
			HttpSession session = request.getSession(true);
			session.setAttribute("usuario", usuario);
	}

}
