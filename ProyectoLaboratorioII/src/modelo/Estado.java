package modelo;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table(name="estado", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class Estado implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="estado_seq", sequenceName="sequence_estado", allocationSize=1)	
	@GeneratedValue(generator="estado_seq", strategy = GenerationType.SEQUENCE)
	private int id;
	private String nombre;
	private String codigo_postal;
	private int status;
	
	//relaciones
	@ManyToOne
	@JoinColumn(name="id_pais")
	private Pais pais;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="id_pais")
	private List<Municipio> municipios;
//	private Set<OfertaEmpleo> ofertas;

	public Estado() {
		 
	}

	public Estado(String nombre, String codigo_postal, int status, Pais pais) {
		super();
		this.nombre = nombre;
		this.codigo_postal = codigo_postal;
		this.status = status;
		this.pais = pais;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(nullable = false)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(nullable = false)
	public String getCodigo_postal() {
		return codigo_postal;
	}

	public void setCodigo_postal(String codigo_postal) {
		this.codigo_postal = codigo_postal;
	}

	@Column(nullable = false)
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	//relaciones

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}
	
	public List<Municipio> getMunicipios() {
		return municipios;
	}

	public void setMunicipios(List<Municipio> municipios) {
		this.municipios = municipios;
	}

	public String nombrePais() {
		if(this.pais != null) {
			return this.pais.getNombre();
		}
		return " ";
	}
	
	public int idPais() {
		if(this.pais!= null) {
			return this.pais.getId();
		}
		return -1;
	}
	
}
