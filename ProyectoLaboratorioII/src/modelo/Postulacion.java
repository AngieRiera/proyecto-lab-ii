package modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;

@Entity
@Table(name="postulacion", schema="public")
@PrimaryKeyJoinColumn
public class Postulacion implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="postulacion_seq", sequenceName="sequence_postulacion", allocationSize=1)	
	@GeneratedValue(generator="postulacion_seq", strategy = GenerationType.SEQUENCE)
	private int id;
	private Date fecha;
	private int status;
	
	//relaciones
	@ManyToOne
	@JoinColumn(name="id_empleado")
	private Empleado empleado;
	@ManyToOne
	@JoinColumn(name="id_oferta_empleo")
	private OfertaEmpleo oferta_empleo;
	
	public Postulacion() {
		
	}

	public Postulacion(Date fecha, int status, Empleado empleado, OfertaEmpleo oferta_empleo) {
		super();
		this.fecha = fecha;
		this.status = status;
		this.empleado = empleado;
		this.oferta_empleo = oferta_empleo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Column(nullable = false)
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	//relaciones
	
	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public OfertaEmpleo getOferta_empleo() {
		return oferta_empleo;
	}

	public void setOferta_empleo(OfertaEmpleo oferta_empleo) {
		this.oferta_empleo = oferta_empleo;
	}
	
}
