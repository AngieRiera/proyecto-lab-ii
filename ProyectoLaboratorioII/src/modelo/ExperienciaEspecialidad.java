package modelo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="experiencia_especialidad", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class ExperienciaEspecialidad implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	
	//relaciones
	private Especialidad especialidad;
	private ExperienciaLaboral experiencia_laboral;
	
	public ExperienciaEspecialidad() {
		
	}

	@Id
	@SequenceGenerator(name="experiencia_especialidad_seq", sequenceName="sequence_experiencia_especialidad", allocationSize=1)	
	@GeneratedValue(generator="experiencia_especialidad_seq", strategy = GenerationType.SEQUENCE)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	//relaciones
	@ManyToOne
	@JoinColumn(name="id_especialidad")
	public Especialidad getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(Especialidad especialidad) {
		this.especialidad = especialidad;
	}

	@ManyToOne
	@JoinColumn(name="id_experiencia_laboral")
	public ExperienciaLaboral getExperiencia_laboral() {
		return experiencia_laboral;
	}

	public void setExperiencia_laboral(ExperienciaLaboral experiencia_laboral) {
		this.experiencia_laboral = experiencia_laboral;
	}
	
	
	
}
