package modelo;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;


@Entity
@Table(name="detalle_estudio", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class DetalleEstudio implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="detalle_estudio_seq", sequenceName="sequence_detalle_estudio", allocationSize=1)	
	@GeneratedValue(generator="detalle_estudio_seq", strategy = GenerationType.SEQUENCE)
	private int id;
	private String institucion;
	private Date fecha_inicio;
	private Date fecha_culminacion;
	private String descripcion;
	private int status;
	
	//relaciones
	@ManyToOne
	@JoinColumn(name="id_curriculum")
	private Curriculum curriculum;
	@ManyToOne
	@JoinColumn(name="id_profesion")
	private Profesion profesion;
	
	public DetalleEstudio() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(nullable = false)
	public String getInstitucion() {
		return institucion;
	}

	public void setInstitucion(String institucion) {
		this.institucion = institucion;
	}

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	public Date getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(Date fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	public Date getFecha_culminacion() {
		return fecha_culminacion;
	}

	public void setFecha_culminacion(Date fecha_culminacion) {
		this.fecha_culminacion = fecha_culminacion;
	}

	@Column(nullable = false)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(nullable = false)
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	//relaciones una a muchos de la tabla intermedia con las otras dos tablas
	
	public Curriculum getCurriculum() {
		return curriculum;
	}

	public void setCurriculum(Curriculum curriculum) {
		this.curriculum = curriculum;
	}

	public Profesion getProfesion() {
		return profesion;
	}

	public void setProfesion(Profesion profesion) {
		this.profesion = profesion;
	}
	
	public String getNombreProfesion() {
		return profesion.getCarrera();
	}
	
}
