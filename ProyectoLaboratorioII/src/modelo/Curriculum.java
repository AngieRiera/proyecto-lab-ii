package modelo;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.TemporalType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Temporal;
import javax.persistence.OneToMany;
import javax.persistence.ManyToMany;

@Entity
@Table(name="curriculum", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class Curriculum implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//atributos

	@Id
	@SequenceGenerator(name="curriculum_seq", sequenceName="sequence_curriculum", allocationSize=1)	
	@GeneratedValue(generator="curriculum_seq", strategy = GenerationType.SEQUENCE)
	private int id;
	private String conocimientos_adicionales;
	private String observaciones;
	private Date fecha_registro;
	private int status;
	
//	//relaciones
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="id_curriculum")
	private List<CursoRealizado> cursos_realizados;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="id_curriculum")
	private List<DetalleEstudio> detalles_estudio;
	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name="id_curriculum")
	private List<CurriculumEmpresa> curriculum_empresas;
	@OneToOne
	@JoinColumn(name="id_empleado")
	private Empleado empleado;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="id_curriculum")
	private List<ExperienciaLaboral> experiencias_laborales;
	
	//Ambas representan la relacion entre profesion y curriculum, 
	//con la diferencia de que el set es de muchos a muchos y el detalle es de uno a muchos con esa entidad media
	//private Set<Profesion> lista_profesiones;
	//private Set<Empresa> lista_empresas;
	
	
	public Curriculum() {
	}

	public Curriculum(String conocimientos_adicionales, String observaciones, Date fecha_registro, int status,
			Empleado empleado) {
		super();
		this.conocimientos_adicionales = conocimientos_adicionales;
		this.observaciones = observaciones;
		this.fecha_registro = fecha_registro;
		this.status = status;
		this.empleado = empleado;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Column(nullable = false)
	public String getConocimientos_adicionales() {
		return conocimientos_adicionales;
	}

	public void setConocimientos_adicionales(String conocimientos_adicionales) {
		this.conocimientos_adicionales = conocimientos_adicionales;
	}

	@Column
	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	public Date getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	@Column(nullable = false)
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<CursoRealizado> getCursos_realizado() {
		return cursos_realizados;
	}

	public void setCursos_realizado(List<CursoRealizado> cursos_realizado) {
		this.cursos_realizados = cursos_realizado;
	}

	//relacion one to many con el modelo intermedio detalle estudio
	public List<DetalleEstudio> getDetalles_estudio() {
		return detalles_estudio;
	}

	public void setDetalles_estudio(List<DetalleEstudio> detalles_estudio) {
		this.detalles_estudio = detalles_estudio;
	}
	
	public List<CurriculumEmpresa> getCurriculum_empresas() {
		return curriculum_empresas;
	}

	public void setCurriculum_empresas(List<CurriculumEmpresa> curriculum_empresas) {
		this.curriculum_empresas = curriculum_empresas;
	}

	public Empleado getEmpleado() {
		return empleado;
	}
	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}
	
	public List<ExperienciaLaboral> getExperiencias_laborales() {
		return experiencias_laborales;
	} 

	public void setExperiencias_laborales(List<ExperienciaLaboral> experiencias_laborales) {
		this.experiencias_laborales = experiencias_laborales;
	}
	
	//obtener fecha actual
	public Date LocalDateToDate() {
		LocalDate date = LocalDate.now();
		Date fecha_real = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
		return fecha_real;
	}
	
	/*relacion mucho a mucho con profesion
	
	@ManyToMany(cascade = {CascadeType.ALL},fetch=FetchType.EAGER)
	@JoinTable(
		name="detalle_estudio", 
		joinColumns={@JoinColumn(name="id_curriculum")}, 
		inverseJoinColumns={@JoinColumn(name="id_profesion")})
	public Set<Profesion> getLista_profesiones() {
		return lista_profesiones;
	}

	public void setLista_profesiones(Set<Profesion> lista_profesiones) {
		this.lista_profesiones = lista_profesiones;
	}
	
	El JoinTable se coloca en la entidad que es "due;a" de la relacion
	joinColumns va la columna de la tabla due;a y en inverseJoinColumns va la clave de la otra tabla envuelta en la relacion
	@ManyToMany(cascade = {CascadeType.ALL}, fetch=FetchType.EAGER)
	@JoinTable(
			name="curriculum_empresa", 
			joinColumns={@JoinColumn(name="id_curriculum")}, 
			inverseJoinColumns={@JoinColumn(name="id_empresa")})
	public Set<Empresa> getLista_empresas() {
		return lista_empresas;
	}

	public void setLista_empresas(Set<Empresa> lista_empresas) {
		this.lista_empresas = lista_empresas;
	}*/
}
