package modelo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="pais", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class Pais implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name="pais_seq", sequenceName="sequence_pais", allocationSize=1)	
	@GeneratedValue(generator="pais_seq", strategy = GenerationType.SEQUENCE)
	private int id;
	private String nombre;
	private int status;
	
	//relaciones
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="id_pais")
	private List<Estado> estados;	

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="id_pais")
	private List<Usuario> usuarios;
//
//	@OneToMany(cascade = CascadeType.ALL, mappedBy = "pais", fetch = FetchType.EAGER)
//	private Set<OfertaEmpleo> ofertas_empleos;

	public Pais(String nombre, int status) {
		super();
		this.nombre = nombre;
		this.status = status;
	}

	
	public Pais() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(nullable = false)
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Column(nullable = false)
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	//relaciones
	public List<Estado> getEstados() {
		return estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}
//
//	public Set<OfertaEmpleo> getOfertas_empleos() {
//		return ofertas_empleos;
//	}
//
//	public void setOfertas_empleos(Set<OfertaEmpleo> ofertas_empleos) {
//		this.ofertas_empleos = ofertas_empleos;
//	}
	
	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	
	public String getIdString() {
		Integer id_temp = this.id;
		return id_temp.toString().replaceAll("\\.", "");
	}
	
	@Override
	public String toString() {
		return this.getNombre();
	}
}
