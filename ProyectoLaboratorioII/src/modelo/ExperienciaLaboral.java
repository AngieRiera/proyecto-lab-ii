package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;

@Entity
@Table(name="experiencia_laboral", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class ExperienciaLaboral implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="experiencia_laboral_seq", sequenceName="sequence_experiencia_laboral", allocationSize=1)	
	@GeneratedValue(generator="experiencia_laboral_seq", strategy = GenerationType.SEQUENCE)
	private int id;
	private String nombre_empresa;
	private Date fecha_inicio;
	private Date fecha_culminacion;
	private String cargo;
	private String descripcion;
	private int status;
	
	//relaciones
	@ManyToOne 
	@JoinColumn(name="id_curriculum")
	private Curriculum curriculum;	
//	private Set<ExperienciaEspecialidad> experiencia_especialidades;
	@ManyToOne
	@JoinColumn(name="id_especialidad")
	private Especialidad especialidad;
	
	public ExperienciaLaboral() {
		//especialidades = new HashSet();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(nullable = false)
	public String getNombre_empresa() {
		return nombre_empresa;
	}

	public void setNombre_empresa(String nombre_empresa) {
		this.nombre_empresa = nombre_empresa;
	}

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	public Date getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(Date fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	public Date getFecha_culminacion() {
		return fecha_culminacion;
	}

	public void setFecha_culminacion(Date fecha_culminacion) {
		this.fecha_culminacion = fecha_culminacion;
	}

	@Column(nullable = false)
	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	@Column(nullable = false)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(nullable = false)
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	
	//relaciones
	
	public Curriculum getCurriculum() {
		return curriculum;
	}

	public void setCurriculum(Curriculum curriculum) {
		this.curriculum = curriculum;
	}
 
//	@OneToMany(cascade = CascadeType.ALL, mappedBy = "experiencia_laboral", fetch = FetchType.EAGER)
//	public Set<ExperienciaEspecialidad> getExperiencia_especialidades() {
//		return experiencia_especialidades;
//	}
//
//	public void setExperiencia_especialidades(Set<ExperienciaEspecialidad> experiencia_especialidades) {
//		this.experiencia_especialidades = experiencia_especialidades;
//	}

	public Especialidad getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(Especialidad especialidad) {
		this.especialidad = especialidad; 
	}
}
