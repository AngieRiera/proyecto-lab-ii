package modelo;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.TemporalType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;

@Entity
@Table(name="sector_actividad", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class SectorActividad implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="sector_actividad_seq", sequenceName="sequence_sector_actividad", allocationSize=1)	
	@GeneratedValue(generator="sector_actividad_seq", strategy = GenerationType.SEQUENCE)
	private int id;
	private String nombre;
	private String descripcion;
	private int status;
	
//	//relaciones

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name="id_sector_actividad")
	private List<Empresa> empresas;
//	private List<Profesion> profesiones;
	
	public SectorActividad() {
		
	}

	public SectorActividad(String nombre, String descripcion, int status) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(nullable = false)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(nullable = false)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(nullable = false)
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	//relaciones
	public List<Empresa> getEmpresas() {
		return empresas;
	}

	public void setEmpresas(List<Empresa> empresas) {
		this.empresas = empresas;
	}
//
//	@OneToMany(cascade = CascadeType.ALL, mappedBy = "sector_actividad", fetch = FetchType.EAGER)
//	public List<Profesion> getProfesiones() {
//		return profesiones;
//	}
//
//	public void setProfesiones(List<Profesion> profesiones) {
//		this.profesiones = profesiones;
//	}
	
	public String statusToString() {
		if(getStatus() == 1) {
			return "Activo";
		}
		else if(getStatus() == 2) {
			return "Inactivo";
		}
		return "nulo";
	}
	
}
	
	


