package modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;

@Entity
@Table(name="curso_realizado", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class CursoRealizado implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="curso_realizado_seq", sequenceName="sequence_curso_realizado", allocationSize=1)	
	@GeneratedValue(generator="curso_realizado_seq", strategy = GenerationType.SEQUENCE)
	private int id;
	private String nombre;
	private String descripcion;
	private String institucion;
	private Date fecha_inicio;
	private Date fecha_fin;
	private int status;
	
	//relaciones
	@ManyToOne
	@JoinColumn(name="id_curriculum")
	private Curriculum curriculum;
	
	public CursoRealizado() {
		
	}
	
	public CursoRealizado(String nombre, String descripcion, String institucion, Date fecha_inicio, Date fecha_fin,
			int status, Curriculum curriculum) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.institucion = institucion;
		this.fecha_inicio = fecha_inicio;
		this.fecha_fin = fecha_fin;
		this.status = status;
		this.curriculum = curriculum;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(nullable = false)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(nullable = false)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(nullable = false)
	public String getInstitucion() {
		return institucion;
	}

	public void setInstitucion(String institucion) {
		this.institucion = institucion;
	}

	@Column
	@Temporal(TemporalType.DATE)
	public Date getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(Date fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	@Column
	@Temporal(TemporalType.DATE)
	public Date getFecha_fin() {
		return fecha_fin;
	}

	public void setFecha_fin(Date fecha_fin) {
		this.fecha_fin = fecha_fin;
	}

	@Column(nullable = false)
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	//relaciones
	public Curriculum getCurriculum() {
		return curriculum;
	}

	public void setCurriculum(Curriculum curriculum) {
		this.curriculum = curriculum;
	}
}
