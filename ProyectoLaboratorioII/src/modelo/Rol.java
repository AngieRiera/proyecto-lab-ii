package modelo;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.OneToMany;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;


@Entity
@Table(name= "rol", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class Rol implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name="rol_seq", sequenceName="sequence_rol", allocationSize=1)	
	@GeneratedValue(generator="rol_seq", strategy = GenerationType.SEQUENCE)
	private int id;
	private String nombre;
	private String descripcion;
	private int status;
	
	//relaciones
    @OneToMany(cascade= CascadeType.ALL, fetch=FetchType.EAGER)
    @JoinColumn(name="id_rol")
	private List<Usuario> usuarios;
	
	public Rol(String nombre, String descripcion, int status) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.status = status;
	}

	public Rol() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(nullable = false)
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Column(nullable = false)
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Column(nullable = false)
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	//relaciones
	public List<Usuario> getUsuarios(){
		return usuarios;
	}
	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	
	public String getIdString() {
		Integer id_temp = this.id;
		return id_temp.toString().replaceAll("\\.", "");
	}
	
	@Override
	public String toString() {
		return this.getNombre();
	}
	
}