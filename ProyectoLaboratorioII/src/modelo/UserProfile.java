package modelo;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.TemporalType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;

@Entity
@Table(name="user_profile", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class UserProfile implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String nombre;
	private String apellido;
	private String direccion;
	private String telefono;
	private String url_twitter;
	private String url_instagram;
	private String url_facebook;
	private int status;
	private String foto;
	
	//relaciones

	private Usuario usuario;
	 
	public UserProfile() {
		url_twitter = "";
		url_instagram = "";
		url_facebook = "";
	}

	@Id
	@SequenceGenerator(name="user_profile_seq", sequenceName="sequence_user_profile", allocationSize=1)	
	@GeneratedValue(generator="user_profile_seq", strategy = GenerationType.SEQUENCE)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(nullable = false)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	//Se modifica campo no nulo en true 
	@Column
	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	//Se modifica campo no nulo en true 
	@Column
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	@Column
	public String getUrl_twitter() {
		return url_twitter;
	}

	public void setUrl_twitter(String url_twitter) {
		this.url_twitter = url_twitter;
	}

	@Column
	public String getUrl_instagram() {
		return url_instagram;
	}

	public void setUrl_instagram(String url_instagram) {
		this.url_instagram = url_instagram;
	}

	@Column
	public String getUrl_facebook() {
		return url_facebook;
	}

	public void setUrl_facebook(String url_facebook) {
		this.url_facebook = url_facebook;
	}

	@Column(nullable = false)
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column
	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	@OneToOne
	@JoinColumn(name="id_usuario")
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}	
	
	public String obtenerTwitter() {
		if(this.url_twitter != null) {
			 return url_twitter;
		}
		return " ";
	}
	
	public String obtenerInstagram() {
		if(this.url_instagram != null) {
			 return url_instagram;
		}
		return " ";
	}

	public String obtenerFacebook() {
		if(this.url_facebook != null) {
			 return url_facebook;
		}
		return " ";
	}
	
	
	
	
}
