package modelo;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToMany;

@Entity
@Table(name="jornada", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class Jornada implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="jornada_seq", sequenceName="sequence_jornada", allocationSize=1)	
	@GeneratedValue(generator="jornada_seq", strategy = GenerationType.SEQUENCE)
	private int id;
	private String nombre;
	private String descripcion;
	private int status;
	
	//relaciones 
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="id_jornada")
	private List<OfertaEmpleo> ofertas_empleos;
	
	public Jornada(String nombre, String descripcion, int status) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.status = status;
	}

	public Jornada() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(nullable = false)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(nullable = false)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Column(nullable = false)
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	//relaciones
	public List<OfertaEmpleo> getOfertas_empleos() {
		return ofertas_empleos;
	}

	public void setOfertas_empleos(List<OfertaEmpleo> ofertas_empleos) {
		this.ofertas_empleos = ofertas_empleos;
	}

	public String statusToString() {
		if(this.status == 1) {
			return "Activo";
		}
		return "Inactivo";
	}
}
