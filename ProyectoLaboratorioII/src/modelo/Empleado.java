package modelo;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.util.List;
import java.util.Locale;

import javax.persistence.OneToOne;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table(name="empleado", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class Empleado implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="empleado_seq", sequenceName="sequence_empleado", allocationSize=1)	
	@GeneratedValue(generator="empleado_seq", strategy = GenerationType.SEQUENCE)
	private int id;
	private String cedula;
	private Date fecha_nacimiento;
	private int genero;
	private int calificacion;
	private String observaciones;
	private int status;
	
	//relaciones 
	@OneToOne
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="id_empleado")
	private List<Postulacion> postulaciones;
	
	public Empleado() {
	}

	public Empleado(String cedula, Date fecha_nacimiento, int genero, String observaciones,
			int status, Usuario usuario) {
		super();
		this.cedula = cedula;
		this.fecha_nacimiento = fecha_nacimiento;
		this.genero = genero;
		this.observaciones = observaciones;
		this.status = status;
		this.usuario = usuario;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(nullable = false)
	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	@Column(nullable = false)
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	public Date getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(Date fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	@Column(nullable = false)
	public int getGenero() {
		return genero;
	}

	public void setGenero(int genero) {
		this.genero = genero;
	}

	@Column
	public int getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(int calificacion) {
		this.calificacion = calificacion;
	}

	@Column
	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	//relaciones
	public List<Postulacion> getPostulaciones() {
		return postulaciones;
	}

	public void setPostulaciones(List<Postulacion> postulaciones) {
		this.postulaciones = postulaciones;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Date StringToDate(String fecha) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", Locale.ENGLISH);
		LocalDate date = LocalDate.parse(fecha, formatter);
		Date fecha_real = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
		return fecha_real;
	}
	
	public Date StringToDateRegistro(String fecha) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
		LocalDate date = LocalDate.parse(fecha, formatter);
		Date fecha_real = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
		return fecha_real;
	}
	
	public String GeneroToString() {
		if(genero == 1)
			return "Mujer";
		else
			return "Hombre";
	}
	
}
