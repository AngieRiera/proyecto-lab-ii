package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table(name="municipio", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class Municipio implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="municipio_seq", sequenceName="sequence_municipio", allocationSize=1)	
	@GeneratedValue(generator="municipio_seq", strategy = GenerationType.SEQUENCE)
	private int id;
	private String nombre;
	private int status;
	
	//relaciones
//	private Set<OfertaEmpleo> ofertas_empleos;
	@ManyToOne
	@JoinColumn(name="id_estado")
	private Estado estado;
	
	public Municipio() {
	}

	public Municipio(String nombre, int status, Estado estado) {
		super();
		this.nombre = nombre;
		this.status = status;
		this.estado = estado;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(nullable = false)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(nullable = false)
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

//	@OneToMany(cascade = CascadeType.ALL , mappedBy = "municipio", fetch = FetchType.EAGER)
//	public Set<OfertaEmpleo> getOfertas_empleos() {
//		return ofertas_empleos;
//	}

//	public void setOfertas_empleos(Set<OfertaEmpleo> ofertas_empleos) {
//		this.ofertas_empleos = ofertas_empleos;
//	}
	
	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

}
