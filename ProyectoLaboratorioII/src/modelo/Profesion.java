package modelo;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table(name="profesion", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class Profesion implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@SequenceGenerator(name="profesion_seq", sequenceName="sequence_profesion", allocationSize=1)	
	@GeneratedValue(generator="profesion_seq", strategy = GenerationType.SEQUENCE)
	private int id;
	private String carrera;
	private String nivel_estudio;
	private int status;
	
	//relaciones

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="id_profesion")
	private List<DetalleEstudio> detalles_estudios;
	

	@ManyToOne
	@JoinColumn(name="id_sector_actividad")
	private SectorActividad sector_actividad;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="id_profesion")
	private Set<ProfesionEspecialidad> profesiones_especialidades;
	
	//private Set<Curriculum> lista_curriculums;
	//private Set<Especialidad> lista_especialidades;
	public Profesion() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(nullable = false)
	public String getCarrera() {
		return carrera;
	}

	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}

	@Column(nullable = false)
	public String getNivel_estudio() {
		return nivel_estudio;
	}

	public void setNivel_estudio(String nivel_estudio) {
		this.nivel_estudio = nivel_estudio;
	}
	
	@Column(nullable = false)
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	//relacion uno a muchos con el detalle estudio
	public List<DetalleEstudio> getDetalles_estudios() {
		return detalles_estudios;
	}

	public void setDetalles_estudios(List<DetalleEstudio> detalles_estudios) {
		this.detalles_estudios = detalles_estudios;
	}

	public SectorActividad getSector_actividad() {
		return sector_actividad;
	}

	public void setSector_actividad(SectorActividad sector_actividad) {
		this.sector_actividad = sector_actividad;
	}

	public Set<ProfesionEspecialidad> getProfesiones_especialidades() {
		return profesiones_especialidades;
	}

	public void setProfesiones_especialidades(Set<ProfesionEspecialidad> profesiones_especialidadades) {
		this.profesiones_especialidades = profesiones_especialidadades;
	}
	
	//Se utiliza el mapped by en para la otra tabla
	/*@ManyToMany(cascade = {CascadeType.ALL},mappedBy="lista_profesiones")
	public Set<Curriculum> getLista_curriculums() {
		return lista_curriculums;
	}

	public void setLista_curriculums(Set<Curriculum> lista_curriculums) {
		this.lista_curriculums = lista_curriculums;
	}
	
	@ManyToMany(cascade = {CascadeType.ALL},fetch=FetchType.EAGER)
	@JoinTable(
		name="profesion_especialidad", 
		joinColumns={@JoinColumn(name="id_profesion")}, 
		inverseJoinColumns={@JoinColumn(name="id_especialidad")})
	public Set<Especialidad> getLista_especialidades() {
		return lista_especialidades;
	}

	public void setLista_especialidades(Set<Especialidad> lista_especialidades) {
		this.lista_especialidades = lista_especialidades;
	}*/
	
	public String nombreSectorActividad() {
		if(getSector_actividad()!= null) {
			return getSector_actividad().getNombre();
		}
		return "nulo";
	}
	
	public String statusToString() {
		if(getStatus()==1) {
			return "Activo";
		}
		return "Inactivo";
	}
	
	public int nivel_estudio_int()
	{
		return Integer.parseInt(this.nivel_estudio);
	}
	
	public String getIdString() {
		Integer id_temp = this.id;
		return id_temp.toString().replaceAll("\\.", "");
	}
	
	@Override
	public String toString() {
		return this.getCarrera();
	}

}
