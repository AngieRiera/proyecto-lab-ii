package modelo;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.OneToOne;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToMany;

@Entity
@Table(name="empresa", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class Empresa implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="empresa_seq", sequenceName="sequence_empresa", allocationSize=1)	
	@GeneratedValue(generator="empresa_seq", strategy = GenerationType.SEQUENCE)
	private int id;
	private String rif;
	private String direccion;
	private String sitio_web;
	private int calificacion;
	private int status;
	
	//relaciones
	@OneToOne
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
//	private Set<OfertaEmpleo> ofertas_empleos;
//	private Set<CurriculumEmpresa> curriculum_empresas;
	@ManyToOne(optional = true)
	@JoinColumn(name="id_sector_actividad")
	private SectorActividad sector_actividad;
	
	//private Set<Curriculum> lista_curriculums;
	
	
	public Empresa() {
	} 
	
	public Empresa(String rif, String direccion, String sitio_web, int status, Usuario usuario, SectorActividad sector_actividad) {
		super();
		this.rif = rif;
		this.direccion = direccion;
		this.sitio_web = sitio_web;
		this.status = status;
		this.usuario = usuario;
		this.sector_actividad = sector_actividad;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(nullable = false)
	public String getRif() {
		return rif;
	}
	public void setRif(String rif) {
		this.rif = rif;
	}
	
	@Column(nullable = false)
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	@Column
	public String getSitio_web() {
		return sitio_web;
	}
	public void setSitio_web(String sitio_web) {
		this.sitio_web = sitio_web;
	}
	
	@Column(nullable = false)
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	@Column
	public int getCalificacion() {
		return calificacion;
	}
	public void setCalificacion(int calificacion) {
		this.calificacion = calificacion;
	}
	
	//relaciones
	@OneToOne(optional = true)
	@JoinColumn(name="id_usuario")
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
//	@OneToMany(cascade = CascadeType.ALL, mappedBy = "empresa", fetch = FetchType.EAGER)
//	public Set<OfertaEmpleo> getOfertas_empleos() {
//		return ofertas_empleos;
//	}
//	public void setOfertas_empleos(Set<OfertaEmpleo> ofertas_empleos) {
//		this.ofertas_empleos = ofertas_empleos;
//	}
//	
//	@OneToMany(cascade = CascadeType.ALL, mappedBy = "empresa", fetch = FetchType.EAGER)
//	public Set<CurriculumEmpresa> getCurriculum_empresas() {
//		return curriculum_empresas;
//	}
//	public void setCurriculum_empresas(Set<CurriculumEmpresa> curriculum_empresas) {
//		this.curriculum_empresas = curriculum_empresas;
//	}
//	
	public SectorActividad getSector_actividad() {
		return sector_actividad;
	}
	public void setSector_actividad(SectorActividad sector_actividad) {
		this.sector_actividad = sector_actividad;
	}
	
	/*@ManyToMany(cascade = {CascadeType.ALL},mappedBy="lista_empresas")
	public Set<Curriculum> getLista_curriculums() {
		return lista_curriculums;
	}
	public void setLista_curriculums(Set<Curriculum> lista_curriculums) {
		this.lista_curriculums = lista_curriculums;
	}*/
	
	public int idSectorActividad() {
		return getSector_actividad().getId();
	}
	
	public String nombreSectorActivida() {
		return getSector_actividad().getNombre();
	}
	
	public String correoUsuario() {
		return getUsuario().getCorreo();
	}
	
	
	public String statusToString() {
		if(getStatus() == 1) {
			return "Activo";
		}
		else if(getStatus() == 2) {
			return "Inactivo";
		}
		return "nulo";
	}
	

	
}
