package modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import daos.UserProfileDAO;
import modelo.dao.utils.Sesion;


@Entity
@Table(name="usuario", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class Usuario implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="usuario_seq", sequenceName="sequence_usuario", allocationSize=1)	
	@GeneratedValue(generator="usuario_seq", strategy = GenerationType.SEQUENCE)
	private int id;
	private String correo;
	private String contrasenna;
	private String contrasennaconfirmacion;
	private Date ultimoiniciosesion;
	private int status;

	//relaciones
    @ManyToOne 
    @JoinColumn(name="id_pais")
	private Pais pais;
    @ManyToOne
    @JoinColumn(name="id_rol")
	private Rol rol;

	public Usuario() {
	} 

	public Usuario(String correo, String contrasenna, String contrasennaconfirmacion, Date ultimoiniciosesion,
			int status, Rol rol, Pais pais) {
		super();
		this.correo = correo;
		this.contrasenna = contrasenna;
		this.contrasennaconfirmacion = contrasennaconfirmacion;
		this.ultimoiniciosesion = ultimoiniciosesion;
		this.status = status;
		this.rol = rol;
		this.pais = pais;
	}

	//Getter and Setter methods
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(unique = true)
	public String getCorreo(){
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	@Column
	public String getContrasenna() {
		return contrasenna;
	}
	public void setContrasenna(String contrasenna) {
		this.contrasenna = contrasenna;
	}
	
	@Column
	public String getContrasennaconfirmacion() {
		return contrasennaconfirmacion;
	}
	public void setContrasennaconfirmacion(String contrasenna_con) {
		this.contrasennaconfirmacion = contrasenna_con;
	}
    
    @Column
    public int getStatus() {
    	return status;
    }
    public void setStatus(int status) {
    	this.status = status;
    }
    
	@Column
	@Temporal(TemporalType.DATE)
    public Date getUltimoiniciosesion() {
    	return ultimoiniciosesion;
    }
    public void setUltimoiniciosesion(Date ultimo_sesion) {
    	this.ultimoiniciosesion = ultimo_sesion;
    }
	
    //relaciones

	public Rol getRol() {
    	return rol;
    }    
    public void setRol(Rol rol) {
    	this.rol = rol;
    }
    
	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}
	
    public String nombreRol() {
		if(!String.valueOf(getRol()).equals("null"))
			return getRol().getNombre();
		return "undefined";
	}
	
	public String nombrePais() {
		if(!String.valueOf(getPais()).equals("null"))
			return getPais().getNombre();
		return "undefined";
	}
	
	public int idRol() {
		if(!String.valueOf(getRol()).equals("null"))
			return getRol().getId();
		return -1;
	}
	
	public int idPais() {
		if(!String.valueOf(getPais()).equals("null"))
			return getPais().getId();
		return -1;
	}
	public String statusToString() {
		if(getStatus() == 1) {
			return "Activo";
		}
		else if(getStatus() == 2) {
			return "Inactivo";
		}
		return "nulo";
	}
	
	public UserProfile getUserProfile() {
		UserProfileDAO userProfileDao = new UserProfileDAO();
		return userProfileDao.getUserProfileFromUser(getId());
	}
	public UserProfile getUserProfileFromUser() {
		Session session = Sesion.getInstancia().openSession();
		  UserProfile userprofile = (UserProfile)session.createCriteria(UserProfile.class).add(Restrictions.eq("usuario.id", this.id)).uniqueResult();
		  return userprofile;
	 }

}