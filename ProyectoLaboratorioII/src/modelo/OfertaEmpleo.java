package modelo;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.TemporalType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@Table(name="oferta_empleo", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class OfertaEmpleo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="oferta_empleo_seq", sequenceName="oferta_empleo_estado", allocationSize=1)	
	@GeneratedValue(generator="oferta_empleo_seq", strategy = GenerationType.SEQUENCE)
	private int id;
	private String nombre;
	private float salario;
	private Date fecha_emision;
	private Date fecha_culminacion;
	private int status;
	
	//relaciones 
	@ManyToOne
	@JoinColumn(name="id_empresa")
	private Empresa empresa;
	@ManyToOne
	@JoinColumn(name="id_pais")
	private Pais pais;
	@ManyToOne
	@JoinColumn(name="id_jornada")
	private Jornada jornada;
	@ManyToOne
	@JoinColumn(name="id_especialidad")
	private Especialidad especialidad;
	@ManyToOne(optional = true)
	@JoinColumn(name="id_municipio")
	private Municipio municipio;
	@ManyToOne(optional = true)
	@JoinColumn(name="id_estado")
	private Estado estado;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="id_oferta_empleo")
	private List<Postulacion> postulaciones;
	
	public OfertaEmpleo() {
	}
	
	public OfertaEmpleo(String nombre, float salario, Date fecha_emision, Date fecha_culminacion, int status,
			Empresa empresa, Pais pais, Jornada jornada, Especialidad especialidad) {
		super();
		this.nombre = nombre;
		this.salario = salario;
		this.fecha_emision = fecha_emision;
		this.fecha_culminacion = fecha_culminacion;
		this.status = status;
		this.empresa = empresa;
		this.pais = pais;
		this.jornada = jornada;
		this.especialidad = especialidad;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(nullable = false)
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@Column(nullable = false)
	public float getSalario() {
		return salario;
	}
	public void setSalario(float salario) {
		this.salario = salario;
	}
	
	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	public Date getFecha_emision() {
		return fecha_emision;
	}
	public void setFecha_emision(Date fecha_emision) {
		this.fecha_emision = fecha_emision;
	}
	
	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	public Date getFecha_culminacion() {
		return fecha_culminacion;
	}
	public void setFecha_culminacion(Date fecha_culminacion) {
		this.fecha_culminacion = fecha_culminacion;
	}
	
	@Column(nullable = false)
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	//relaciones
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public Pais getPais() {
		return pais;
	}
	public void setPais(Pais pais) {
		this.pais = pais;
	}
	

	public Jornada getJornada() {
		return jornada;
	}
	public void setJornada(Jornada jornada) {
		this.jornada = jornada;
	}
	
	public Especialidad getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(Especialidad especialidad) {
		this.especialidad = especialidad;
	}

	public List<Postulacion> getPostulaciones() {
		return postulaciones;
	}

	public void setPostulaciones(List<Postulacion> postulaciones) {
		this.postulaciones = postulaciones;
	}
	
	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
	//
	public String nombreEmpresa() {
		if(getEmpresa() != null)
			return getEmpresa().getUsuario().getUserProfile().getNombre();
		return "nulo";
	}
	
	public String nombreEspecialidad() {
		if(getEspecialidad() !=null)
			return getEspecialidad().getNombre();
		return "nulo";
	}
	public String nombreJornada() {
		if(getJornada() != null)
			return getJornada().getNombre();
		return "nulo";
	}
	
	public String nombrePais(){
		String resultado = "nulo";
		if(this.getPais() != null)
			resultado = this.getPais().getNombre();
		return resultado;
	}
	public Date StringToDate(String fecha) {
		  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
		  LocalDate date = LocalDate.parse(fecha, formatter);
		  Date fecha_real = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
		  return fecha_real;
		 }
	
	public String nombreEstado() {
		return this.estado.getNombre();
	}
	public String fechaCulminacionString() {
		return this.fecha_culminacion.toString().substring(0, 10);
	}
	public String fechaEmisionString() {
		return this.fecha_emision.toString().substring(0, 10);
	}
	
	public int getIdJornada() {
		if(this.jornada != null) {
			return this.jornada.getId();
		}
		return -1;
	}
	
	public String getNombreJornada() {
		if(this.jornada != null) {
			return this.jornada.getNombre();
		}
		return "";
	}
	

	
}
