package modelo;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table(name="profesion_especialidad", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class ProfesionEspecialidad implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Id
	@SequenceGenerator(name="profesion_especialidad_seq", sequenceName="sequence_profesion_especialidad", allocationSize=1)	
	@GeneratedValue(generator="profesion_especialidad_seq", strategy = GenerationType.SEQUENCE)
	private int id;

	@ManyToOne
	@JoinColumn(name="id_especialidad")
	private Especialidad especialidad;

	@ManyToOne
	@JoinColumn(name="id_profesion")
	private Profesion profesion;
	
	public ProfesionEspecialidad() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Especialidad getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(Especialidad especialidad) {
		this.especialidad = especialidad;
	}

	public Profesion getProfesion() {
		return profesion;
	}

	public void setProfesion(Profesion profesion) {
		this.profesion = profesion;
	}
	
	
}
