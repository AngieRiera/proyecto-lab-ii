package modelo;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;

@Entity
@Table(name="especialidad", schema="public")
@PrimaryKeyJoinColumn(name="id")

public class Especialidad implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@SequenceGenerator(name="especialidad_seq", sequenceName="sequence_especialidad", allocationSize=1)	
	@GeneratedValue(generator="especialidad_seq", strategy = GenerationType.SEQUENCE)
	private int id;
	private String nombre; 
	private String descripcion;
	private int status;
	
	//relaciones

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="id_especialidad")
	private List<OfertaEmpleo> ofertas;
//	private Set<ExperienciaEspecialidad> experiencia_especialidades;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="id_especialidad")
	private List<ProfesionEspecialidad> profesion_especialidades;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="id_especialidad")
	private List<ExperienciaLaboral> experiencias;
	//private Set<Profesion> profesiones;
	
	public Especialidad () {
		
	}

	public Especialidad(String nombre, String descripcion, int status) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(nullable = false)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(nullable = false)
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Column(nullable = false)
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<OfertaEmpleo> getOfertas() {
		return ofertas;
	}

	public void setOfertas(List<OfertaEmpleo> ofertas) {
		this.ofertas = ofertas;
	}	
	
	//relaciones uno a muchos
//	@OneToMany(cascade = CascadeType.ALL, mappedBy = "especialidad", fetch = FetchType.EAGER)
//	public Set<ExperienciaEspecialidad> getExperiencia_especialidades() {
//		return experiencia_especialidades;
//	}
//
//	public void setExperiencia_especialidades(Set<ExperienciaEspecialidad> experiencia_especialidades) {
//		this.experiencia_especialidades = experiencia_especialidades;
//	}
	
	public List<ProfesionEspecialidad> getProfesion_especialidades() {
		return profesion_especialidades;
	} 

	public void setProfesion_especialidades(List<ProfesionEspecialidad> profesion_especialidades) {
		this.profesion_especialidades = profesion_especialidades;
	}
	
	//relaciones muchos a muchos
	/*@ManyToMany(cascade = {CascadeType.ALL},mappedBy="especialidades")
	public Set<Profesion> getProfesiones() {
		return profesiones;
	}

	public void setProfesiones(Set<Profesion> lista_profesiones) {
		this.profesiones = lista_profesiones;
	}
*/
	public List<ExperienciaLaboral> getExperiencias() {
		return experiencias;
	}

	public void setExperiencias(List<ExperienciaLaboral> lista_experiencias) {
		this.experiencias = lista_experiencias;
	}
}
