package modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;

@Entity
@Table(name="curriculum_empresa", schema="public")
@PrimaryKeyJoinColumn(name="id")
public class CurriculumEmpresa implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="curriculum_empresa_seq", sequenceName="sequence_curriculum_empresa", allocationSize=1)	
	@GeneratedValue(generator="curriculum_empresa_seq", strategy = GenerationType.SEQUENCE)
	private int id;
	private Date fecha_envio;
	private int status;
	
	//relaciones
	@ManyToOne
	@JoinColumn(name="id_curriculum")
	private Curriculum curriculum;

	@ManyToOne
	@JoinColumn(name="id_empresa")
	private Empresa empresa;
	
	public CurriculumEmpresa() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	public Date getFecha_envio() {
		return fecha_envio;
	}

	public void setFecha_envio(Date fecha_envio) {
		this.fecha_envio = fecha_envio;
	}

	@Column(nullable = false)
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	//relaciones
	
	public Curriculum getCurriculum() {
		return curriculum;
	}

	public void setCurriculum(Curriculum curriculum) {
		this.curriculum = curriculum;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	
}
